package py.una.fp.horario.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import py.una.fp.horario.dto.AgrupacionAulasDto;
import py.una.fp.horario.dto.AulaDto;
import py.una.fp.horario.dto.TipoAulaDto;
import py.una.fp.horario.ejb.AulaEjb;
import py.una.fp.horario.secured.Secured;
import py.una.fp.horario.util.Mensaje;
import py.una.fp.horario.util.Respuesta;

@Stateless
@Path("/aula")
@Api(
	    value = "/aula", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON
	)
public class AulasRest {
	
	@EJB
	AulaEjb aulaEjb;

	Logger log = Logger.getLogger(AulasRest.class);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Listar Aulas",
	        notes = "Servicio para listar y filtrar las aulas"
	    )
	public Respuesta<ArrayList<AulaDto>> listAll(
			@QueryParam("offset") String offset,
			@QueryParam("nombre") String nombre,
			@QueryParam("capacidad") Integer capacidad,
			@QueryParam("tipo") Integer tipo,
			@QueryParam("column") String column,
			@QueryParam("order") Boolean order
			){
		List<AulaDto> list = null;
		String mensaje="";
		Boolean success = true;
		String reason = Mensaje.CONSULTA_EXITOSA;
		String type = "success";
		
		try {
			list = aulaEjb.getAll(nombre, capacidad, tipo, new Integer(offset), column, order);
		} catch (Exception e) {
			log.error("Failed! ",e);
			mensaje = Mensaje.ERROR_DATA_BASE;
			reason = e.getCause().getMessage();
			success = false;
			type = "error";
		}
		
		if(list != null){
			mensaje = (list.size()==11?offset:"ultima");
			if(list.size()==11){
				list.remove(10);
			}
		}else{
			mensaje = Mensaje.WARN_AULA_CONSULTA_VACIA;
			success = false;
			type = "info";
			reason = Mensaje.WARN;
		}		

		Respuesta<ArrayList<AulaDto>> r = new Respuesta<ArrayList<AulaDto>>(success, mensaje, reason, type, (ArrayList<AulaDto>) list);
		return r;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Crear aula",
	        notes = "Servicio para dar de alta nuevas aulas"
	    )
	public Respuesta<AulaDto> create(AulaDto aula){
		String mensaje=Mensaje.CREACION_AULA_EXITOSO;
		String reason = Mensaje.INFO;
		String type = "success";
		
		Boolean success = aulaEjb.create(aula.getNombre(), aula.getCapacidad(), aula.getIdTipoAula());
		if(!success){
			mensaje = Mensaje.CREACION_AULA_NO_EXITOSO;
			type = "error";
			reason = Mensaje.ERROR;
		}
		Respuesta<AulaDto> r = new Respuesta<AulaDto>(success, mensaje, reason, type);
		return r;
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id:[0-9][0-9]*}")
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Modificar aula",
	        notes = "Servicio para modificar un aula"
	    )
	public Respuesta<AulaDto> update(AulaDto aula, @PathParam("id") Integer id){
		String mensaje=Mensaje.EDICION_AULA_EXITOSO;
		String reason = Mensaje.INFO;
		String type = "success";
		
		Boolean success = aulaEjb.edit(id,aula.getNombre(), aula.getCapacidad(), aula.getIdTipoAula());
		if(!success){
			mensaje = Mensaje.EDICION_AULA_NO_EXITOSO;
			type = "error";
			reason = Mensaje.ERROR;
		}
		Respuesta<AulaDto> r = new Respuesta<AulaDto>(success, mensaje, reason, type);
		return r;
	}
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id:[0-9][0-9]*}")
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Eliminar aula",
	        notes = "Servicio para eliminar un aula"
	    )
	public Respuesta<AulaDto> delete(@PathParam("id") Integer id){
		String mensaje=Mensaje.ELIMINACION_AULA_EXITOSO;
		String reason = Mensaje.INFO;
		String type = "success";
		
		Boolean success = aulaEjb.delete(id);
		if(!success){
			mensaje = Mensaje.ELIMINACION_AULA_NO_EXITOSO;
			type = "error";
			reason = Mensaje.ERROR;
		}
		Respuesta<AulaDto> r = new Respuesta<AulaDto>(success, mensaje, reason, type);
		return r;
	}
	
	@GET
	@Path("/tipos")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Listar tipos de aulas",
	        notes = "Servicio para listar los tipos de aulas"
	    )
	public Respuesta<ArrayList<TipoAulaDto>> listAllTipoAulas(){
		List<TipoAulaDto> list = null;
		String mensaje="";
		Boolean success = true;
		String reason = Mensaje.CONSULTA_EXITOSA;
		String type = "success";
		
		try {
			list = aulaEjb.getAllTipoAula();
		} catch (Exception e) {
			log.error("Failed! ",e);
			mensaje = Mensaje.ERROR_DATA_BASE;
			reason = e.getCause().getMessage();
			success = false;
			type = "error";
		}
		
		if(list == null){
			mensaje = Mensaje.WARN_AULA_CONSULTA_VACIA;
			success = false;
			type = "info";
			reason = Mensaje.WARN;
		}		

		Respuesta<ArrayList<TipoAulaDto>> r = new Respuesta<ArrayList<TipoAulaDto>>(success, mensaje, reason, type, (ArrayList<TipoAulaDto>) list);
		return r;
	}
	
}

package py.una.fp.horario.dto.resultado;

public class DivisionHorarioResultadoDto {
	
	public Integer id;
	public Integer idTipoAulaHorarioResultado;
	public Integer start;
	public Integer end;
	public Integer idDivision;

}

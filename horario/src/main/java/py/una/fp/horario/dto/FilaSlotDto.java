package py.una.fp.horario.dto;

public class FilaSlotDto {
	
	private String a;
	private String s;
	private Integer i;
	
	public FilaSlotDto(String id, String start, String end, String aula){
		this.a = aula.trim().compareTo("NULL")==0?"":aula.trim();
		this.s = start.trim() + "-" + end.trim();
		try {
			this.i = new Integer(id.trim());
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public String getA() {
		return a;
	}
	public void setA(String a) {
		this.a = a;
	}
	public String getS() {
		return s;
	}
	public void setS(String s) {
		this.s = s;
	}
	public Integer getI() {
		return i;
	}
	public void setI(Integer i) {
		this.i = i;
	}
}

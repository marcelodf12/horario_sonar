package py.una.fp.horario.dto;

import java.util.List;

public class ProfesorDto {

	private Integer idProfesor;
	private String nombre;
	private String apellido;
	private String celular;
	private String titulo;
	private String cedula;
	private Boolean notificar;
	private String correo;
	private String slot;
	private Integer idSlot;
	
	private List<AsignaturaDto> asignaturas;

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public Integer getIdProfesor() {
		return idProfesor;
	}
	public void setIdProfesor(Integer idProfesor) {
		this.idProfesor = idProfesor;
	}
	public String getCedula() {
		return cedula;
	}
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	public Boolean getNotificar() {
		return notificar;
	}
	public void setNotificar(Boolean notificar) {
		this.notificar = notificar;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getSlot() {
		return slot;
	}
	public void setSlot(String slot) {
		this.slot = slot;
	}
	public Integer getIdSlot() {
		return idSlot;
	}
	public void setIdSlot(Integer idSlot) {
		this.idSlot = idSlot;
	}
	public List<AsignaturaDto> getAsignaturas() {
		return asignaturas;
	}
	public void setAsignaturas(List<AsignaturaDto> asignaturas) {
		this.asignaturas = asignaturas;
	}	
}

package py.una.fp.horario.dto;

import java.util.ArrayList;

public class AgrupacionAulasDto {
	
	private Integer id;
	private ArrayList<Integer> start;
	private ArrayList<Integer> end;
	private transient ArrayList<String> asignatura;
	private String profesor;
	private Integer cantidadAlumnos;
	private Integer tipoAula;
	private String grupo;
	public Integer idHorario;
	
	public AgrupacionAulasDto(){
		super();
		this.start = new ArrayList<Integer>();
		this.end = new ArrayList<Integer>();
		this.asignatura = new ArrayList<String>();
		this.grupo = "";
	}
	
	
	
	public void setAsignaturas(ArrayList<String> asignatura) {
		this.asignatura = asignatura;
	}
	public ArrayList<String> getAsignaturas() {
		return this.asignatura;
	}

	public String getGrupo() {
		if(grupo.compareTo("")==0)
			return "";
		return grupo.substring(0, this.grupo.length()-1);
	}
	public void addGrupo(String s) {
		this.grupo += s;
	}
	public void setGrupo(String s) {
		this.grupo = s;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getStart() {
		String s ="";
		for(Integer a: this.start)
			s+=a+";";
		if(s.compareTo("")==0)
			return "";
		return s.substring(0,s.length()-1);
	}
	public void setStart(String start) {
		String[] array = start.split(";");
		this.start = new ArrayList<Integer>();
		for(String s: array)
			this.start.add(new Integer(s));
	}
	public String getEnd() {
		String s ="";
		for(Integer a: this.end)
			s+=a+";";
		if(s.compareTo("")==0)
			return "";
		return s.substring(0,s.length()-1);
	}
	public void setEnd(String end) {
		String[] array = end.split(";");
		this.end = new ArrayList<Integer>();
		for(String s: array)
			this.end.add(new Integer(s));
	}
	
	public String getAsignatura() {
		String s ="";
		for(String a: this.asignatura)
			s+=a+";";
		if(s.compareTo("")==0)
			return "";
		return s.substring(0,s.length()-1);
	}
	public void setAsignatura(String asignatura) {
		String[] array = asignatura.split(";");
		this.asignatura = new ArrayList<String>();
		for(String s: array)
			this.asignatura.add(s);
	}
	public String getProfesor() {
		return profesor;
	}
	public void setProfesor(String profesor) {
		this.profesor = profesor;
	}
	public Integer getCantidadAlumnos() {
		return cantidadAlumnos;
	}
	public void setCantidadAlumnos(Integer cantidadAlumnos) {
		this.cantidadAlumnos = cantidadAlumnos;
	}
	public Integer getTipoAula() {
		return tipoAula;
	}
	public void setTipoAula(Integer tipoAula) {
		this.tipoAula = tipoAula;
	}
	
	public ArrayList<Integer> startSlots(){
		return this.start;
	}
	public ArrayList<Integer> endSlots(){
		return this.end;
	}
	public ArrayList<String> asignaturas(){
		return this.asignatura;
	}
	public void setearStart(ArrayList<Integer> s){
		this.start = s;
	}
	public void setearEnd(ArrayList<Integer> s){
		this.end = s;
	}
	public void setearAsignaturas(ArrayList<String> s){
		this.asignatura = s;
	}
	public void addAsignatura(String elemento){
		if(!this.asignatura.contains(elemento))
			this.asignatura.add(elemento);
	}
	

}

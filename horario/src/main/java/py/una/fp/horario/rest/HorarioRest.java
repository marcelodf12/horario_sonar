package py.una.fp.horario.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import py.una.fp.horario.dto.BorradorDto;
import py.una.fp.horario.dto.ConflictosDto;
import py.una.fp.horario.dto.FilaHorarioDto;
import py.una.fp.horario.dto.HorarioDto;
import py.una.fp.horario.ejb.HorarioEjb;
import py.una.fp.horario.ejb.NotificacionesEjb;
import py.una.fp.horario.secured.Secured;
import py.una.fp.horario.util.Mensaje;
import py.una.fp.horario.util.Respuesta;

@Stateless
@Path("/consultas")
@Api(
	    value = "/horario", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON
	)
public class HorarioRest {
	
	@EJB
	HorarioEjb horarioEbj;
	
	@EJB
	NotificacionesEjb notificacionesEjb;
	
	Logger log = Logger.getLogger(HorarioRest.class);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/horario/notificar/{id:[0-9][0-9]*}")
	@ApiOperation(
	        value = "Notificar a profesores",
	        notes = "Servicio para notificar a los profesores sus horarios por correo electrónico"
	    )
	public Respuesta<Object> notificar(@PathParam("id") Integer idHorario){
		String mensaje="";
		Boolean success = true;
		String reason = Mensaje.CONSULTA_EXITOSA;
		String type = "success";
		try {
			notificacionesEjb.notificar(idHorario);
		} catch (Exception e) {
			log.error("Failed! ",e);
			mensaje = Mensaje.ERROR_DATA_BASE;
			reason = e.getCause().getMessage();
			success = false;
			type = "error";
		}
		Respuesta<Object> r = new Respuesta<Object>(success, mensaje, reason, type, null);
		return r;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/horario/{id:[0-9][0-9]*}")
	@ApiOperation(
	        value = "Consultar horario",
	        notes = "Servicio para consultar un horario específico"
	    )
	public Response getHorario(@PathParam("id") String idHorario){
		List<FilaHorarioDto> list = null;
		String mensaje="";
		Boolean success = true;
		String reason = Mensaje.CONSULTA_EXITOSA;
		String type = "success";
		try {
			Integer id = null;
			if(idHorario.compareTo("00")!=0)
				id = new Integer(idHorario);
			list = horarioEbj.getHorario(id);
		} catch (Exception e) {
			log.error("Failed! ",e);
			mensaje = Mensaje.ERROR_DATA_BASE;
			reason = e.getCause().getMessage();
			success = false;
			type = "error";
		}
		if(list == null){
			mensaje = Mensaje.WARN_HORARIO_CONSULTA_VACIA;
			success = false;
			type = "info";
			reason = Mensaje.WARN;
		}
		CacheControl cc = new CacheControl();
		cc.setMaxAge(86400);
		cc.setPrivate(true);

		Respuesta<ArrayList<FilaHorarioDto>> r = new Respuesta<ArrayList<FilaHorarioDto>>(success, mensaje, reason, type, (ArrayList<FilaHorarioDto>) list);

		Response.ResponseBuilder builder = Response.ok(r);
		builder.cacheControl(cc);

		return builder.build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/version")
	@ApiOperation(
	        value = "Consultar última versión publicada",
	        notes = "Servicio para consultar el número de versión del último horario publicado"
	    )
	public Respuesta<Long> getVersion(){
		Long version = null;
		String mensaje="";
		Boolean success = true;
		String reason = Mensaje.CONSULTA_EXITOSA;
		String type = "success";
		try {
			version = horarioEbj.getVersion();
		} catch (Exception e) {
			log.error("Failed! ",e);
			mensaje = Mensaje.ERROR_DATA_BASE;
			reason = e.getCause().getMessage();
			success = false;
			type = "error";
		}
		Respuesta<Long> r = new Respuesta<Long>(success, mensaje, reason, type, version);
		return r;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/horarios")
	@Secured(perfil="1")
	@ApiOperation(
	        value = "Listar horario",
	        notes = "Servicio para listar horarios. El filtro es por año"
	    )
	public Respuesta<ArrayList<HorarioDto>> listAllHorarios(@QueryParam("anho") Integer anho){
		List<HorarioDto> list = null;
		String mensaje="";
		Boolean success = true;
		String reason = Mensaje.CONSULTA_EXITOSA;
		String type = "success";
		try {
			list = horarioEbj.listHorarios(new Integer(anho));
		} catch (Exception e) {
			log.error("Failed! ",e);
			mensaje = Mensaje.ERROR_DATA_BASE;
			reason = e.getCause().getMessage();
			success = false;
			type = "error";
		}
		if(list == null){
			mensaje = Mensaje.WARN_HORARIO_CONSULTA_VACIA;
			success = false;
			type = "info";
			reason = Mensaje.WARN;
			list = new ArrayList<HorarioDto>();
		}
		Respuesta<ArrayList<HorarioDto>> r = new Respuesta<ArrayList<HorarioDto>>(success, mensaje, reason, type, (ArrayList<HorarioDto>) list);
		return r;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/papelera")
	@Secured(perfil="1")
	@ApiOperation(
	        value = "Listar horarios en papelera",
	        notes = "Servicio para listar los horarios que están en la papelera"
	    )
	public Respuesta<ArrayList<HorarioDto>> listPapelera(){
		List<HorarioDto> list = null;
		String mensaje="";
		Boolean success = true;
		String reason = Mensaje.CONSULTA_EXITOSA;
		String type = "success";
		try {
			list = horarioEbj.listPapelera();
		} catch (Exception e) {
			log.error("Failed! ",e);
			mensaje = Mensaje.ERROR_DATA_BASE;
			reason = e.getCause().getMessage();
			success = false;
			type = "error";
		}
		if(list == null){
			mensaje = Mensaje.WARN_HORARIO_CONSULTA_VACIA;
			success = false;
			type = "info";
			reason = Mensaje.WARN;
			list = new ArrayList<HorarioDto>();
		}
		Respuesta<ArrayList<HorarioDto>> r = new Respuesta<ArrayList<HorarioDto>>(success, mensaje, reason, type, (ArrayList<HorarioDto>) list);
		return r;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/borrador")
	@Secured(perfil="1")
	@ApiOperation(
	        value = "Listar asignaturas de un borrador",
	        notes = "Servicio para las asignaturas de un horario en estado borrador"
	    )
	public Respuesta<ArrayList<BorradorDto>> listBorrador(@QueryParam("id") Integer id,
														  @QueryParam("dpto") String dpto,
														  @QueryParam("carrera") String carrera,
														  @QueryParam("asignatura") String asignatura,
														  @QueryParam("profesor") String profesor,
														  @QueryParam("column") String column,
														  @QueryParam("order") Boolean order,
														  @QueryParam("offset") Integer offset){
		List<BorradorDto> list = null;
		String mensaje="";
		Boolean success = true;
		String reason = Mensaje.CONSULTA_EXITOSA;
		String type = "success";
		try {
			//listBorrador(Integer id, String dpto, String carrera, String profesor, String asignatura, Boolean order, String column, Integer offset)
			list = horarioEbj.listBorrador(id, dpto, carrera, profesor, asignatura, order,	column ,offset);
		} catch (Exception e) {
			log.error("Failed! ",e);
			mensaje = Mensaje.ERROR_DATA_BASE;
			reason = e.getCause().getMessage();
			success = false;
			type = "error";
		}
		if(list != null){
			mensaje = (list.size()==26?offset+"":"ultima");
			if(list.size()==26)
				list.remove(25);
		}else{
			mensaje = Mensaje.WARN_CATEDRA_CONSULTA_VACIA;
			success = false;
			type = "info";
			reason = Mensaje.WARN;
		}
		Respuesta<ArrayList<BorradorDto>> r = new Respuesta<ArrayList<BorradorDto>>(success, mensaje, reason, type, (ArrayList<BorradorDto>) list);
		return r;
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("borrador/{id:[0-9][0-9]*}")
	@Secured(perfil="1")
	@ApiOperation(
	        value = "Actualizar borrador",
	        notes = "Servicio para actualizar una asignatura perteneciente a un horario en estado borrador"
	    )
	public Respuesta<BorradorDto> update(BorradorDto b, @PathParam("id") Integer id){
		String mensaje="";
		if(b.getSelected()){
			mensaje = Mensaje.ADD_SELECTED_BORRADOR;
		}else{
			mensaje = Mensaje.DEL_SELECTED_BORRADOR;
		}
		if(b.getDef()){
			mensaje += Mensaje.ADD_DEF_BORRADOR;
		}else{
			mensaje += Mensaje.DEL_DEF_BORRADOR;
		}
		
		Boolean success = horarioEbj.updateBorrador(id, b.getSelected(), b.getDef());
		String reason = "";
		String type = "success";
		if(!success){
			mensaje = Mensaje.EDICION_CATEDRA_NO_EXITOSO;
			type = "error";
			reason = Mensaje.ERROR;
		}
		Respuesta<BorradorDto> r = new Respuesta<BorradorDto>(success, mensaje, reason,type);
		return r;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/calcular/{id:[0-9][0-9]*}/{algoritmo}")
	@Secured(perfil="1")
	@ApiOperation(
	        value = "Calcular Horario",
	        notes = "Servicio para lanzar el proceso de cálculo de horario."
	    )
	public String calcular(@PathParam("id") Integer idHorario, @PathParam("algoritmo") String algoritmo, ArrayList<Integer> aRecalcular) throws IOException{
		return horarioEbj.calcular(idHorario,algoritmo, aRecalcular);
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/publicar/{id:[0-9][0-9]*}")
	@Secured(perfil="1")
	@ApiOperation(
	        value = "Publicar Horario",
	        notes = "Servicio para publica un horario específico"
	    )
	public Respuesta<Object> publicar(@PathParam("id") Integer idHorario) throws IOException{
		return horarioEbj.publicar(idHorario);
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/restaurar/{id:[0-9][0-9]*}")
	@Secured(perfil="1")
	@ApiOperation(
	        value = "Restaurar Horario",
	        notes = "Servicio para restaurar un horario de la papelera"
	    )
	public Respuesta<Object> restaurar(@PathParam("id") Integer idHorario) throws IOException{
		return horarioEbj.restaurar(idHorario);
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/descartar/{id:[0-9][0-9]*}")
	@Secured(perfil="1")
	@ApiOperation(
	        value = "Descartar Horario",
	        notes = "Servicio para enviar un horario a la papelera"
	    )
	public Respuesta<Object> descartar(@PathParam("id") Integer idHorario) throws IOException{
		return horarioEbj.descartar(idHorario);
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/horario/conflictos/{id:[0-9][0-9]*}")
	@Secured(perfil="1")
	@ApiOperation(
	        value = "Listar conflictos",
	        notes = "Servicio para consultar los solapamientos entre asiganturas de un horario"
	    )
	public Respuesta<ArrayList<ConflictosDto>> getConflictos(@PathParam("id") Integer idHorario){
		ArrayList<ConflictosDto> conflictos = horarioEbj.calcularConflictos(idHorario);
		Boolean success = true;
		String message = "";
		String reason = "";
		String type = "success";
		if(conflictos==null){
			success = false;
			message = "Ha ocurrido un error al acceder al horario";
			reason = "Excepción en la base de datos";
			type = "error";
		}else{
			message = "El horario tiene " + conflictos.size() + " conflictos";
			reason = "Consulta exitosa";
		}
		
		return new Respuesta<ArrayList<ConflictosDto>>(success,message,reason,type,conflictos);
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/horarioCalculado/{id:[0-9][0-9]*}")
	@Secured(perfil="1")
	@ApiOperation(
	        value = "Consultar horario",
	        notes = "Servicio para consultar un horario específico"
	    )
	public Respuesta<String> consultarHorario(@PathParam("id") String id){
		Respuesta<String> r = null;
		try {
			//horarioEbj.cargarHorario(id);
			r = new Respuesta<String>(true,"","","","LISTO");
		} catch (Exception e) {
			r = new Respuesta<String>(false,"","","",e.getMessage());
		}
		return r;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/horario")
	@Secured(perfil="1")
	@ApiOperation(
	        value = "Crear horario",
	        notes = "Servicio para crear un nuevo horario"
	    )
	public Respuesta<HorarioDto> create(HorarioDto horario){
		String mensaje=Mensaje.CREACION_HORARIO_EXITOSO;
		String reason = Mensaje.INFO;
		String type = "success";
		
		Boolean success = horarioEbj.nuevoHorario(horario.getPeriodo(), horario.getAnho());
		if(!success){
			mensaje = Mensaje.CREACION_HORARIO_NO_EXITOSO;
			type = "error";
			reason = Mensaje.ERROR;
		}
		Respuesta<HorarioDto> r = new Respuesta<HorarioDto>(success, mensaje, reason, type);
		return r;
	}
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/horario/{id:[0-9][0-9]*}")
	@Secured(perfil="1")
	@ApiOperation(
	        value = "Eliminar horario",
	        notes = "Servicio para eliminar un horario específico. Solo se puede eliminar los horarios de la papelera"
	    )
	public Respuesta<HorarioDto> delete(@PathParam("id") Integer idHorario){
		String mensaje=Mensaje.ELIMINACION_HORARIO_EXITOSO;
		String reason = Mensaje.INFO;
		String type = "success";
		
		Boolean success = horarioEbj.eliminar(idHorario);
		if(!success){
			mensaje = Mensaje.ELIMINACION_HORARIO_NO_EXITOSO;
			type = "error";
			reason = Mensaje.ERROR;
		}
		Respuesta<HorarioDto> r = new Respuesta<HorarioDto>(success, mensaje, reason, type);
		return r;
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/horario/cantidadAlumnos")
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Actualizar horario",
	        notes = "Servicio para actualizar los datos de un horario"
	    )
	public Respuesta<FilaHorarioDto> update(ArrayList<FilaHorarioDto> list){
		String mensaje=Mensaje.EDICION_HORARIO_EXITOSO;
		String reason = Mensaje.INFO;
		String type = "success";
		Boolean success = true;
		for(FilaHorarioDto f: list){
			success = success && horarioEbj.updateCantidadAlumnos(f);
		}
		if(!success){
			mensaje = Mensaje.EDICION_HORARIO_NO_EXITOSO;
			type = "error";
			reason = Mensaje.ERROR;
		}
		Respuesta<FilaHorarioDto> r = new Respuesta<FilaHorarioDto>(success, mensaje, reason, type);
		return r;
	}
	
}

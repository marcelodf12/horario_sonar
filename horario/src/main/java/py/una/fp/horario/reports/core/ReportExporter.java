package py.una.fp.horario.reports.core;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;

import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.HtmlExporter;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.export.SimpleCsvExporterConfiguration;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleHtmlExporterConfiguration;
import net.sf.jasperreports.export.SimpleHtmlExporterOutput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.export.SimpleWriterExporterOutput;
import net.sf.jasperreports.export.SimpleXlsReportConfiguration;
import py.una.fp.horario.reports.exceptions.JasperPOJOReportException;

/**
 * @class ReportExporter
 * @author Renato Costa
 */
public class ReportExporter {

	// TODO - Translate the code;

	// TODO - Translate the Comments;

	/*******
	 * Exporta um JasperPrint para um formato de CSV.
	 * 
	 * @param JasperPrint
	 *            de um jasper (compilado)
	 * @param Delimitador
	 *            - Valor que será injetado no CSV para delimitar o arquivo
	 *            (padrão é ';')
	 * @return byte array com os dados do relatório para transporte/gravação.
	 * @throws JasperPOJOReportException
	 */
	private static byte[] exportarParaCSV(JasperPrint print, String delimitador) throws JasperPOJOReportException {
		try {
			// Declara os itens necessários - Exportador, Configurações e
			// byteArrayOS que será populado na leitura
			JRCsvExporter exporterCSV = new JRCsvExporter();
			SimpleCsvExporterConfiguration cfg = new SimpleCsvExporterConfiguration();
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

			// Seta delimitador do arquivo, e a configuração
			cfg.setFieldDelimiter(delimitador);
			exporterCSV.setConfiguration(cfg);

			// Aponta o input e output
			exporterCSV.setExporterInput(new SimpleExporterInput(print));
			exporterCSV.setExporterOutput(new SimpleWriterExporterOutput(outputStream));

			// Aplica a exportação
			exporterCSV.exportReport();

			// Retorna o valor correto
			return outputStream.toByteArray();
		} catch (Exception e) {

			throw new JasperPOJOReportException(e);
		}
	}

	/*******
	 * Exporta um JasperPrint para um formato de CSV.
	 * 
	 * @param JasperPrint
	 *            de um jasper (compilado)
	 * @return byte array com os dados do relatório para transporte/gravação.
	 * @throws JasperPOJOReportException
	 */
	public static byte[] exportarParaCSV(JasperPrint print) throws JasperPOJOReportException {
		return exportarParaCSV(print, ";");
	}

	/*******
	 * Exporta um JasperPrint para um formato de CSV, com um delimitador de
	 * tabulação ('\t'). Gera uma string
	 * 
	 * @param JasperPrint
	 *            de um jasper (compilado)
	 * @return Byte array com os dados gerados
	 * @throws JasperPOJOReportException
	 */
	public static byte[] exportarParaTexto(JasperPrint print) throws JasperPOJOReportException {
		try {
			return exportarParaCSV(print, "\t");
		} catch (Exception e) {
			throw new JasperPOJOReportException(e);
		}
	}

	/*******
	 * Exporta um JasperPrint para um formato de XLS. Retorna, por padrão, uma
	 * página por Aba
	 * 
	 * @param JasperPrint
	 *            de um jasper (compilado)
	 * @return byte array com os dados do relatório para transporte/gravação.
	 * @throws JasperPOJOReportException
	 */
	public static byte[] exportarParaXLS(JasperPrint print) throws JasperPOJOReportException {
		return exportarParaXLS(print, true);
	}

	/*******
	 * Exporta um JasperPrint para um formato de XLS. Retorna todos os dados em
	 * uma única aba
	 * 
	 * @param JasperPrint
	 *            de um jasper (compilado)
	 * @return byte array com os dados do relatório para transporte/gravação.
	 * @throws JasperPOJOReportException
	 */
	public static byte[] exportarParaXLSAbaUnica(JasperPrint print) throws JasperPOJOReportException {
		return exportarParaXLS(print, false);
	}

	/*******
	 * Exporta um JasperPrint para um formato de XLS.
	 * 
	 * @param JasperPrint
	 *            de um jasper (compilado)
	 * @param umaPaginaPorAba
	 *            - Flag que indica se deve ser os dados devem ser trazidos em
	 *            sequência ou uma aba por página
	 * @return byte array com os dados do relatório para transporte/gravação.
	 * @throws JasperPOJOReportException
	 */
	private static byte[] exportarParaXLS(JasperPrint print, boolean umaPaginaPorAba) throws JasperPOJOReportException {
		try {
			// Declara os itens necessários - Exportador, Configurações e
			// byteArrayOS que será populado na leitura
			JRXlsExporter exporter = new JRXlsExporter();
			SimpleXlsReportConfiguration cfg = new SimpleXlsReportConfiguration();
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

			// Seta as Configurações necessárias
			cfg.setOnePagePerSheet(umaPaginaPorAba);
			cfg.setDetectCellType(true);
			cfg.setCollapseRowSpan(true);
			exporter.setConfiguration(cfg);

			// Seta Input e Output
			exporter.setExporterInput(new SimpleExporterInput(print));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputStream));

			// Executa a exportação
			exporter.exportReport();

			// Retorna o valor correto
			return outputStream.toByteArray();
		} catch (Exception e) {
			throw new JasperPOJOReportException(e);
		}
	}

	/*******
	 * Exporta um JasperPrint para um formato de PDF.
	 * 
	 * @param JasperPrint
	 *            de um jasper (compilado)
	 * @return byte array com os dados do relatório para transporte/gravação.
	 * @throws JasperPOJOReportException
	 */
	public static byte[] exportarParaPDF(JasperPrint print) throws JasperPOJOReportException {
		try {
			// Declara os itens necessários - Exportador, Configurações e
			// byteArrayOS que será populado na leitura
			JRPdfExporter exporter = new JRPdfExporter();
			SimplePdfExporterConfiguration cfg = new SimplePdfExporterConfiguration();
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

			// Seta a configuração
			exporter.setConfiguration(cfg);

			// Aponta o input e output
			exporter.setExporterInput(new SimpleExporterInput(print));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputStream));

			// Aplica a exportação
			exporter.exportReport();

			// Retorna o valor correto
			return outputStream.toByteArray();
		} catch (Exception e) {
			throw new JasperPOJOReportException(e);
		}
	}

	/*******
	 * Exporta um JasperPrint para um formato de HTML.
	 * 
	 * @param JasperPrint
	 *            de um jasper (compilado)
	 * @return byte array com os dados do relatório para transporte/gravação.
	 * @throws JasperPOJOReportException
	 */
	public static String exportarParaHTML(JasperPrint print) throws JasperPOJOReportException {
		try {
			// Declara os itens necessários - Exportador, Configurações e
			// String Buffer que será populado na leitura
			HtmlExporter exporter = new HtmlExporter();
			SimpleHtmlExporterConfiguration cfg = new SimpleHtmlExporterConfiguration();
			StringBuffer sbuffer = new StringBuffer();

			// Seta a configuração
			cfg.setHtmlHeader("");
			cfg.setHtmlFooter("");
			cfg.setBetweenPagesHtml("");
			exporter.setConfiguration(cfg);

			// Seta Input e Output
			exporter.setExporterInput(new SimpleExporterInput(print));
			exporter.setExporterOutput(new SimpleHtmlExporterOutput(sbuffer));

			// Gera Exportação
			exporter.exportReport();

			// Retorna a String
			return sbuffer.toString();
		} catch (Exception e) {
			throw new JasperPOJOReportException(e);
		}
	}

	/*****
	 * Método para encapsular comportamento de salvar um byte array em disco,
	 * caso necessário
	 * 
	 * @param data
	 *            - Dado a ser gravado
	 * @param path
	 *            - Caminho completo do arquivo
	 * @throws JasperPOJOReportException
	 */
	public static void gravarEmDisco(byte[] data, String path) throws JasperPOJOReportException {
		try {
			FileOutputStream file = new FileOutputStream(path);
			file.write(data);
			file.flush();
			file.close();
		} catch (Exception e) {
			throw new JasperPOJOReportException(e);
		}
	}
}

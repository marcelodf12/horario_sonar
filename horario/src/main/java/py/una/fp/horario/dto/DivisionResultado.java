package py.una.fp.horario.dto;

public class DivisionResultado {
	
	public Integer idAsignatura;
	public Integer idHorario;
	public String seccion;
	public String asignatura;
	public String profesor;
	public String carrera;
	public Integer semestre;
	public Integer nivel;
	public Integer tipoAula;
	public Integer start;
	public Integer end;
	public String enfasis;
	public Integer cantidadAlumnos;
	public Integer idAula;
	public Integer aula;
}

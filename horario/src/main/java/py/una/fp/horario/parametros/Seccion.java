package py.una.fp.horario.parametros;

import java.util.ArrayList;

public class Seccion {
	
	public int idDivision;
	
	public int idDivisionDB;
	
	public ArrayList<Slot> slots;
	
	public ArrayList<Integer> conflictos;
	
	public ArrayList<Integer> combinar;
	
	public int cantidadAlumnos;
	
	public int tipoAula;
	
	public int duracion;
	
	public int idSeccion;
	
	public transient int start;
	
	public transient String modo;
	
	public transient Integer hora;
	
	//public transient String grupo;
	
	public transient int [] idProfesores;
	
	public transient int [] slot_seccion;
	
	public transient int auxGrupo;
	
	public ArrayList<Integer> aulas;
	
	public transient String stringSlot;

}

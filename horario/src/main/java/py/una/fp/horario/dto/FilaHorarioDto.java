package py.una.fp.horario.dto;

import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

public class FilaHorarioDto {
	
	private Logger log = Logger.getLogger(FilaHorarioDto.class);
			
	// Atributos a enviar por la red
	private String a; // Asignatura
	private String p; // Profesor
	private String c; // Sigla Carrera
	private String s; // Seccion
	private String se; // Semestre
	private String e; // enfasis
	private ArrayList<FilaSlotDto> st; // Slots con aulas
	private Date p1; // Parcial 1
	private Date p2; // Parcial 2
	private Date f1; // Final 1
	private Date f2; // Final 2
	private String d; // Departamento
	private String n; // Nivel
	private String id; // id Horario Resultado
	private Integer ca;
	
	// Libreria Gson
	private final Gson gson = new Gson();
	
	public String getA() {
		return a;
	}
	public void setA(String a) {
		this.a = a;
	}
	public String getP() {
		return p;
	}
	public void setP(String p) {
		this.p = p;
	}
	public String getC() {
		return c;
	}
	public void setC(String c) {
		this.c = c;
	}
	public String getS() {
		return s;
	}
	public void setS(String s) {
		this.s = s;
	}
	public String getSt() {
		log.debug(gson.toJson(st));
		return gson.toJson(st);
	}
	
	public String getSe() {
		return se;
	}
	public void setSe(String se) {
		this.se = se;
	}
	public String getE() {
		return e;
	}
	public void setE(String e) {
		this.e = e;
	}
	public void setSt(String str) {
		String[] stArray = str.split(";");
		ArrayList<FilaSlotDto> r = new ArrayList<FilaSlotDto>();
		int cantidadDivisiones = stArray.length / 4;
		for(int n=0; n<cantidadDivisiones; n++){
			FilaSlotDto f = new FilaSlotDto(
					stArray[n],
					stArray[n+cantidadDivisiones],
					stArray[n+cantidadDivisiones*2],
					stArray[n+cantidadDivisiones*3]
					);
			r.add(f);
		}
		st=r;
	}
	public Date getP1() {
		return p1;
	}
	public void setP1(Date p1) {
		this.p1 = p1;
	}
	public Date getP2() {
		return p2;
	}
	public void setP2(Date p2) {
		this.p2 = p2;
	}
	public Date getF1() {
		return f1;
	}
	public void setF1(Date f1) {
		this.f1 = f1;
	}
	public Date getF2() {
		return f2;
	}
	public void setF2(Date f2) {
		this.f2 = f2;
	}
	public String getD() {
		return d;
	}
	public void setD(String d) {
		this.d = d;
	}
	public String getN() {
		return n;
	}
	public void setN(String n) {
		this.n = n;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Integer getCa() {
		return ca;
	}
	public void setCa(Integer ca) {
		this.ca = ca;
	}
	
}

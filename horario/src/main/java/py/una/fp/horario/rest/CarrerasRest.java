package py.una.fp.horario.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import py.una.fp.horario.dto.CarreraDto;
import py.una.fp.horario.dto.ImportDto;
import py.una.fp.horario.ejb.CarreraEjb;
import py.una.fp.horario.ejb.ImportEjb;
import py.una.fp.horario.secured.Secured;
import py.una.fp.horario.util.Mensaje;
import py.una.fp.horario.util.Respuesta;

@Stateless
@Path("/carrera")
@Api(
	    value = "/carrera", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON
	)
public class CarrerasRest {
	
	@EJB
	CarreraEjb carreraEjb;
	
	@EJB
	ImportEjb importEjb;
	
	Logger log = Logger.getLogger(CarrerasRest.class);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/siglas")
	@ApiOperation(
	        value = "Listar Siglas de Carreras",
	        notes = "Servicio siglas y nombres de todas las carreras para ayuda"
	    )
	public Respuesta<ArrayList<CarreraDto>> listAllSiglas(){
		List<CarreraDto> list = null;
		String mensaje="";
		Boolean success = true;
		String reason = Mensaje.CONSULTA_EXITOSA;
		String type = "success";
		
		try {
			list = carreraEjb.getAllAll();
			for(CarreraDto c:list){
				c.setAsignaturas(null);
				c.setCodCarSec(null);
				c.setIdCarrera(null);
			}
		} catch (Exception e) {
			log.error("Failed! ",e);
			mensaje = Mensaje.ERROR_DATA_BASE;
			reason = e.getCause().getMessage();
			success = false;
			type = "error";
		}
		if(list == null){
			mensaje = Mensaje.WARN_CARRERA_CONSULTA_VACIA;
			success = false;
			type = "info";
			reason = Mensaje.WARN;
		}
		Respuesta<ArrayList<CarreraDto>> r = new Respuesta<ArrayList<CarreraDto>>(success, mensaje, reason, type, (ArrayList<CarreraDto>) list);
		return r;
	
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/all")
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Listar todas las carreras",
	        notes = "Servicio para listar todas las carreras"
	    )
	public Respuesta<ArrayList<CarreraDto>> listAllAll(){
		List<CarreraDto> list = null;
		String mensaje="";
		Boolean success = true;
		String reason = Mensaje.CONSULTA_EXITOSA;
		String type = "success";
		
		try {
			list = carreraEjb.getAllAll();
		} catch (Exception e) {
			log.error("Failed! ",e);
			mensaje = Mensaje.ERROR_DATA_BASE;
			reason = e.getCause().getMessage();
			success = false;
			type = "error";
		}
		if(list == null){
			mensaje = Mensaje.WARN_CARRERA_CONSULTA_VACIA;
			success = false;
			type = "info";
			reason = Mensaje.WARN;
		}
		Respuesta<ArrayList<CarreraDto>> r = new Respuesta<ArrayList<CarreraDto>>(success, mensaje, reason, type, (ArrayList<CarreraDto>) list);
		return r;
	
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Listar carreras",
	        notes = "Servicio para listar y filtrar carreras"
	    )
	public Respuesta<ArrayList<CarreraDto>> listAll(
			@QueryParam("offset") String offset,
			@QueryParam("nombre") String nombre,
			@QueryParam("sigla") String sigla,
			@QueryParam("column") String column,
			@QueryParam("order") Boolean order
			){
		
		List<CarreraDto> list = null;
		String mensaje="";
		Boolean success = true;
		String reason = Mensaje.CONSULTA_EXITOSA;
		String type = "success";
		
		try {
			list = carreraEjb.getAll(nombre, sigla, new Integer(offset), column, order);
		} catch (Exception e) {
			log.error("Failed! ",e);
			mensaje = Mensaje.ERROR_DATA_BASE;
			reason = e.getCause().getMessage();
			success = false;
			type = "error";
		}
		if(list != null){
			mensaje = (list.size()==11?offset:"ultima");
			if(list.size()==11){
				list.remove(10);
			}
		}else{
			mensaje = Mensaje.WARN_CARRERA_CONSULTA_VACIA;
			success = false;
			type = "info";
			reason = Mensaje.WARN;
		}
		Respuesta<ArrayList<CarreraDto>> r = new Respuesta<ArrayList<CarreraDto>>(success, mensaje, reason, type, (ArrayList<CarreraDto>) list);
		return r;
	}
	
	@GET
	@Path("/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Consultar carrera",
	        notes = "Servicio para consultar los datos de una carrera"
	    )
	public Respuesta<CarreraDto> get(@PathParam("id") Integer id){
		CarreraDto c = null;
		String mensaje="";
		Boolean success = true;
		String reason = Mensaje.CONSULTA_EXITOSA;
		String type = "success";
		
		try {
			c = carreraEjb.get(id);
		} catch (Exception e) {
			log.error("Failed! ",e);
			mensaje = Mensaje.ERROR_DATA_BASE;
			reason = e.getCause().getMessage();
			success = false;
			type = "error";
		}
		if(c == null){
			mensaje = Mensaje.ERROR_CARRERA_NO_EXISTE;
			success = false;
			type = "info";
		}
		Respuesta<CarreraDto> r = new Respuesta<CarreraDto>(success,mensaje,reason,type,c);
		return r;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Crear carrear",
	        notes = "Servicio para dar de alta una nueva carrera"
	    )
	public Respuesta<CarreraDto> create(CarreraDto carrera){
		String mensaje=Mensaje.CREACION_CARRERA_EXITOSO;
		String reason = Mensaje.INFO;
		String type = "success";
		
		Boolean success = carreraEjb.create(carrera);
		if(!success){
			mensaje = Mensaje.CREACION_CARRERA_NO_EXITOSO;
			type = "error";
			reason = Mensaje.ERROR;
		}
		Respuesta<CarreraDto> r = new Respuesta<CarreraDto>(success, mensaje, reason, type);
		return r;
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id:[0-9][0-9]*}")
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Modificar carrera",
	        notes = "Servicio para modificar una carrera"
	    )
	public Respuesta<CarreraDto> update(CarreraDto carrera, @PathParam("id") Integer id){
		String mensaje=Mensaje.EDICION_CARRERA_EXITOSO;
		String reason = Mensaje.INFO;
		String type = "success";
		
		Boolean success = carreraEjb.edit(carrera);
		if(!success){
			mensaje = Mensaje.EDICION_CARRERA_NO_EXITOSO;
			type = "error";
			reason = Mensaje.ERROR;
		}
		Respuesta<CarreraDto> r = new Respuesta<CarreraDto>(success, mensaje, reason, type);
		return r;
	}
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id:[0-9][0-9]*}")
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Eliminar carrera",
	        notes = "Servicio para eliminar una carrera"
	    )
	public Respuesta<CarreraDto> delete(@PathParam("id") Integer id){
		String mensaje=Mensaje.ELIMINACION_CARRERA_EXITOSO;
		String reason = Mensaje.INFO;
		String type = "success";
		
		Boolean success = carreraEjb.delete(id);
		if(!success){
			mensaje = Mensaje.ELIMINACION_CARRERA_NO_EXITOSO;
			type = "error";
			reason = Mensaje.ERROR;
		}
		Respuesta<CarreraDto> r = new Respuesta<CarreraDto>(success, mensaje, reason, type);
		return r;
	}
	
	
	
	
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id:[0-9][0-9]*}/conflictos")
	@ApiOperation(
	        value = "Listar grupos de asignaturas de una carrera",
	        notes = "Servicio para listar las asiganturas con su grupo correspondiente a una carrera"
	    )
	public Respuesta<ArrayList<ImportDto>> getHorario(@PathParam("id") String idCarrera){
		ArrayList<ImportDto> list = null;
		String mensaje="";
		Boolean success = true;
		String reason = Mensaje.CONSULTA_EXITOSA;
		String type = "success";
		try {
			list = importEjb.recuperarGrupos(new Integer(idCarrera));
		} catch (Exception e) {
			log.error("Failed! ",e);
			mensaje = Mensaje.ERROR_DATA_BASE;
			reason = e.getCause().getMessage();
			success = false;
			type = "error";
		}
		if(list == null){
			mensaje = Mensaje.WARN_HORARIO_CONSULTA_VACIA;
			success = false;
			type = "info";
			reason = Mensaje.WARN;
		}
		Respuesta<ArrayList<ImportDto>> r = new Respuesta<ArrayList<ImportDto>>(success, mensaje, reason, type, (ArrayList<ImportDto>) list);
		return r;
	}


	@POST
	@Path("/{id:[0-9][0-9]*}/conflictos")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(
	        value = "Actualizar grupos",
	        notes = "Servicio para actualizar los grupos de conflicto de una carrera"
	    )
	public Respuesta<String> updateCarrerasConflictosAsignaturas(List<ImportDto> datos) {
		String mensaje=Mensaje.UPDATE_ASIG_EXITOSO;
		String reason = Mensaje.INFO;
		String type = "success";
		String data = "";
		Boolean success = true;
		try {
			success = importEjb.actualizarPorArray(datos);
		} catch (Exception e) {
			mensaje = Mensaje.UPDATE_ASIG_NO_EXITOSO_ARRAY;
			reason= Mensaje.ERROR;
			type = "error";
			success = false;
			e.printStackTrace();
		}
		return new Respuesta<String>(success, mensaje, reason, type, data);
	}

}

package py.una.fp.horario.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import py.una.fp.horario.dto.ConfiguracionDto;
import py.una.fp.horario.ejb.ParametrosEjb;
import py.una.fp.horario.secured.Secured;
import py.una.fp.horario.util.Mensaje;
import py.una.fp.horario.util.Respuesta;

@Stateless
@Path("/configuracion")
@Api(
	    value = "/configuracion", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON
	)
public class ParametrosRest {

	@EJB
	ParametrosEjb parametrosEjb;
	
	Logger log = Logger.getLogger(ParametrosRest.class);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(perfil="5")
	@ApiOperation(
	        value = "Listar parámetros del sistema",
	        notes = "Servicio para listar los parámteros globales del sistema"
	    )
	public Respuesta<List<ConfiguracionDto>> getAll(){
		List<ConfiguracionDto> list = null;
		String mensaje=Mensaje.PELIGRO_CONFIGURACION;
		Boolean success = true;
		String reason = "Cuidado";
		String type = "warning";
		try {
			list = parametrosEjb.getAll();
		} catch (Exception e) {
			log.error("Failed! ",e);
			mensaje = Mensaje.ERROR_DATA_BASE;
			reason = e.getCause().getMessage();
			success = false;
			type = "error";
		}
		if(list == null){
			mensaje = Mensaje.ERROR_DATA_BASE;
			success = false;
			type = "error";
			reason = Mensaje.ERROR;
		}
		Respuesta<List<ConfiguracionDto>> r = new Respuesta<List<ConfiguracionDto>>(success, mensaje, reason, type, list);
		return r;
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{clave}")
	@Secured(perfil="5")
	@ApiOperation(
	        value = "Actualizar parámetro del sistema",
	        notes = "Servicio para actualizar un parámetro global de sistema"
	    )
	public Respuesta<Boolean> update(@PathParam("clave") String clave, ConfiguracionDto config){
		String mensaje=Mensaje.UPDATE_CONFIG_EXITOSO;
		String reason = Mensaje.INFO;
		String type = "success";
		
		Boolean success = parametrosEjb.edit(clave, config.getValor());
		if(!success){
			mensaje = Mensaje.ERROR_DATA_BASE;
			type = "error";
			reason = Mensaje.ERROR;
		}
		Respuesta<Boolean> r = new Respuesta<Boolean>(success, mensaje, reason, type);
		return r;
	}
	
}

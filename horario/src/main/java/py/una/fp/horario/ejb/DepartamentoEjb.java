package py.una.fp.horario.ejb;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import py.una.fp.horario.bd.DataBaseConfiguration;
import py.una.fp.horario.dto.DepartamentoDto;

@Stateless
public class DepartamentoEjb {

	
	@EJB
	DataBaseConfiguration dbc;
	
	Logger log = Logger.getLogger(DepartamentoEjb.class);
	
	public ArrayList<DepartamentoDto>getAll(String nombre, String sigla, String column, Boolean order){
		SqlSession session = dbc.getSession();
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("nombre", nombre);
			parameters.put("sigla", sigla);
			parameters.put("column", column);
			parameters.put("order", order);
			List<DepartamentoDto> list = session.selectList("py.una.fp.mapper.Departamento.getAll", parameters);
			session.close();
			if(list.size()==0)
				return null;
			return (ArrayList<DepartamentoDto>) list;
			
		
	}
	
	public boolean delete(Integer id){
		SqlSession session = dbc.getSession();
		boolean success = true;
		try{
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("id", id);
			session.selectList("py.una.fp.mapper.Departamento.delete", parameters);
		}catch(Exception e){
			success = false;
			log.error("Failed! ",e);
		}finally {
			session.close();
		}
		return success;
	}
	
	public boolean create(String nombre, String sigla){
		SqlSession session = dbc.getSession();
		boolean success = true;
		try{
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("nombre", nombre);
			parameters.put("sigla", sigla);
			session.selectList("py.una.fp.mapper.Departamento.insert", parameters);
		}catch(Exception e){
			success = false;
			log.error("Failed! ",e);
		}finally {
			session.close();
		}
		return success;
	}
	
	public boolean edit(Integer id, String nombre, String sigla){
		SqlSession session = dbc.getSession();
		boolean success = true;
		try{
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("id", id);
			parameters.put("nombre", nombre);
			parameters.put("sigla", sigla);
			session.selectList("py.una.fp.mapper.Departamento.update", parameters);
		}catch(Exception e){
			success = false;
			log.error("Failed! ",e);
		}finally {
			session.close();
		}
		return success;
	}
}

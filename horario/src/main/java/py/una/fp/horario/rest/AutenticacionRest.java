package py.una.fp.horario.rest;

import java.util.ArrayList;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import py.una.fp.horario.dto.PerfilDto;
import py.una.fp.horario.dto.UsuarioDto;
import py.una.fp.horario.ejb.UsuarioEjb;
import py.una.fp.horario.secured.Secured;
import py.una.fp.horario.util.Mensaje;
import py.una.fp.horario.util.Respuesta;

@Stateless
@Path("/autenticacion")
@Api(
	    value = "/autenticacion", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON
	)
public class AutenticacionRest {
	
	
	@EJB
	UsuarioEjb usuarioEbj;
	

	Logger log = Logger.getLogger(AutenticacionRest.class);

	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(
	        value = "Login",
	        notes = "Servicio para hacer login. La respuesta en un csv, el token es el primer elemento es el token, el segundo el identificador del profesor y el tercer elemento el identificador del usuario."
	    )
	public Respuesta<String> login(UsuarioDto u){
		Respuesta<String> r = new Respuesta<String>();
		r.setData(usuarioEbj.login(u.getNombre(), u.getPassword()));
		r.setSuccess(r.getData().compareTo("ERROR")!=0);
		if(!r.getSuccess()){
			r.setMessage("Usuario o Password invalido");
		}
		return r;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/perfil")
	@ApiOperation(
	        value = "Listar Perfiles",
	        notes = "Servicio para listar los roles de un usuario a partir del token. El token debe ir en message."
	    )
	public Respuesta<ArrayList<PerfilDto>> getPefiles(Respuesta<Object> token){
		Respuesta<ArrayList<PerfilDto>> r = new Respuesta<ArrayList<PerfilDto>>();
		try {
			r.setData(usuarioEbj.getPefilByToken(token.getMessage()));
			r.setSuccess(true);
		} catch (Exception e) {
			r.setSuccess(false);
			r.setType("error");
			r.setReason("Error Desconocido");
			r.setMessage(e.getMessage());
		}
		if(r.getData()==null) {
			r.setSuccess(false);
			r.setReason("Forbidden");
			r.setType("error");
			r.setMessage("Usted no esta autenticado. Favor loguearse");
		}
		return r;
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id:[0-9][0-9]*}")
	@Secured(perfil="4")
	@ApiOperation(
	        value = "Actualizar contraseña",
	        notes = "Servicio para cambiar la contraseña de un usuario"
	    )
	public Respuesta<UsuarioDto> updatePassword(UsuarioDto usuario, @PathParam("id") Integer id){
		String mensaje=Mensaje.EDICION_CONTRASENHA_EXITOSA;
		String reason = Mensaje.INFO;
		String type = "success";
		
		Boolean success = usuarioEbj.cambiarPassword(id,usuario.getNombre() , usuario.getPassword(), usuario.getPasswordNuevo());
		if(!success){
			mensaje = Mensaje.EDICION_CONTRASEÑA_NO_EXITOSO_INCORRECTA;
			type = "error";
			reason = Mensaje.ERROR;
		}
		Respuesta<UsuarioDto> r = new Respuesta<UsuarioDto>(success, mensaje, reason, type);
		return r;
	}

}

package py.una.fp.horario.ejb;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.AccessTimeout;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.TimerService;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import py.una.fp.horario.bd.DataBaseConfiguration;
import py.una.fp.horario.dto.AsignaturaDto;
import py.una.fp.horario.dto.DivisionDto;
import py.una.fp.horario.dto.HorarioResultado;
import py.una.fp.horario.dto.resultado.DivisionHorarioResultadoDto;
import py.una.fp.horario.dto.resultado.HorarioResultadoDto;
import py.una.fp.horario.dto.resultado.TipoAulaHorarioResultadoDto;
import py.una.fp.horario.util.Configuracion;
import py.una.fp.horario.util.Respuesta;

@Singleton
public class HorarioControlTimer {
	
	@Resource
	TimerService timerService;
	
	@EJB
	Configuracion conf;
	
	@EJB
	DataBaseConfiguration dbc;
	
	@EJB
	ReportesEjb reportesEbj;
	
	Logger log = Logger.getLogger(HorarioControlTimer.class);
	
	
	@Schedule(minute="*/1", hour="*", persistent=false)
	@AccessTimeout(value=-1)
    public synchronized void consultarCore() throws Exception {
        log.debug("********* EJECUTANDO TIMER ************");
        SqlSession session = dbc.getSession();
		List<Integer> list;
		HashMap<String, Object> parameters = new HashMap<>();
		list = session.selectList("py.una.fp.mapper.Horario.getHorariosPendientes", null);
        for(Integer idProcess: list){
        	try {
        		Respuesta<Double> r = consultarProgress(idProcess);
        		parameters.put("idProcess", idProcess);
        		log.debug("Process["+idProcess+"] progreso: " + r.getData());
        		Integer idHorario = session.selectOne("py.una.fp.mapper.Horario.getIdHorariosByProcess", parameters);
        		parameters.put("id", idHorario);
        		parameters.put("progreso", r.getData());
        		session.update("py.una.fp.mapper.Horario.updateProgress",parameters);
        		ArrayList<HorarioResultadoDto> horarioResultado = new ArrayList<HorarioResultadoDto>();
            	if(r.getData().intValue()==100){	
                	log.debug("Procesando Horario ["+idHorario+"]");
            		Respuesta<HorarioResultado> response = consultarResultado(idProcess);
            		if(response.getSuccess()){
            			HorarioResultado hr = response.getData();
            			if(hr.start.size()>0){
            				session.delete("py.una.fp.mapper.Horario.deleteHorarioResultado",parameters);
            				for(int n = 0;n<hr.idDivision.size();n++){
            					DivisionHorarioResultadoDto nuevaDiv = new DivisionHorarioResultadoDto();
    							nuevaDiv.start = hr.start.get(n);
    							nuevaDiv.end = hr.end.get(n);
    							nuevaDiv.idDivision = hr.idDivision.get(n);
	            				parameters.put("id", hr.idDivision.get(n));
	            				List<DivisionDto> div = session.selectList("py.una.fp.mapper.Asignatura.getDivisionById", parameters);
	            				DivisionDto d = div.get(0);
	            				boolean esHorario = false;
	            				for(HorarioResultadoDto h : horarioResultado){
	            					if(h.idAsignatura == d.getIdAsignatura()){
	            						esHorario = true;
	            						boolean esTipo = false;
		            					for(TipoAulaHorarioResultadoDto t: h.tipos){
		            						if(t.idTipoAula == d.getTipoAula()){
		            							esTipo = true;
		            							nuevaDiv.idTipoAulaHorarioResultado = t.id;
		            							t.divisiones.add(nuevaDiv);
		            							break;
		            						}
		            					}
		            					if(!esTipo){
		            						TipoAulaHorarioResultadoDto nuevoTipo = new TipoAulaHorarioResultadoDto();
		            						nuevoTipo.idHorarioResultado = h.id;
		            						nuevoTipo.idTipoAula  = d.getTipoAula();
		            						//#{id_horario_resultado}, #{tipo_aula}, #{aula}
		            						parameters.put("id_horario_resultado", nuevoTipo.idHorarioResultado);
		            						parameters.put("tipo_aula", nuevoTipo.idTipoAula);
		            						nuevoTipo.id = (Integer) session.selectList("py.una.fp.mapper.Horario.insertTipoAulaHorarioResultado",parameters).get(0);
		            						nuevoTipo.divisiones = new ArrayList<DivisionHorarioResultadoDto>();
		            						nuevoTipo.divisiones.add(nuevaDiv);
		            						//#{id_tipo_aula_horario_resultado}, #{start_slot}, #{end_slot}
		            						parameters.put("id_tipo_aula_horario_resultado", nuevoTipo.id);
		            						parameters.put("start_slot", nuevaDiv.start);
		            						parameters.put("end_slot", nuevaDiv.end);
		            						parameters.put("id_division", nuevaDiv.idDivision);
		            						session.selectList("py.una.fp.mapper.Horario.insertDivisionHorarioResultado",parameters);
		            					}
		            					break;
	            					}
	            				}
	            				if(!esHorario){
	            					HorarioResultadoDto nuevoHorarioResultado = new HorarioResultadoDto();
	            					nuevoHorarioResultado.idAsignatura = d.getIdAsignatura();
	            					nuevoHorarioResultado.tipos = new ArrayList<TipoAulaHorarioResultadoDto>();
	            					//#{id_asignatura}, #{cantidad_alumnos}, #{nuevo}
	            					parameters.put("id_asignatura", nuevoHorarioResultado.idAsignatura);
	            					parameters.put("cantidad_alumnos", 0);
	            					parameters.put("nuevo", false);
	            					parameters.put("id_horario", idHorario);
	            					nuevoHorarioResultado.id = (Integer) session.selectList("py.una.fp.mapper.Horario.insertHorarioResultado", parameters).get(0);
	            					TipoAulaHorarioResultadoDto nuevoTipo = new TipoAulaHorarioResultadoDto();
            						nuevoTipo.idHorarioResultado = nuevoHorarioResultado.id;
            						nuevoTipo.idTipoAula  = d.getTipoAula();
            						//#{id_horario_resultado}, #{tipo_aula}, #{aula}
            						parameters.put("id_horario_resultado", nuevoTipo.idHorarioResultado);
            						parameters.put("tipo_aula", nuevoTipo.idTipoAula);
            						nuevoTipo.id = (Integer) session.selectList("py.una.fp.mapper.Horario.insertTipoAulaHorarioResultado",parameters).get(0);
            						nuevoTipo.divisiones = new ArrayList<DivisionHorarioResultadoDto>();
            						nuevoTipo.divisiones.add(nuevaDiv);
            						//#{id_tipo_aula_horario_resultado}, #{start_slot}, #{end_slot}
            						parameters.put("id_tipo_aula_horario_resultado", nuevoTipo.id);
            						parameters.put("start_slot", nuevaDiv.start);
            						parameters.put("end_slot", nuevaDiv.end);
            						parameters.put("id_division", nuevaDiv.idDivision);
            						session.selectList("py.una.fp.mapper.Horario.insertDivisionHorarioResultado",parameters);
            						horarioResultado.add(nuevoHorarioResultado);
	            				}
	            			}
            				parameters.put("id", idHorario);
	            			parameters.put("estado", "S");
            			}else{// Si no es una asignacion de aulas
            				for(int n = 0;n<hr.idDivision.size();n++){
            					parameters.put("idTipo", hr.idDivision.get(n));
            					parameters.put("aula", hr.aulas.get(n));
            					session.update("py.una.fp.mapper.AgrupacionAulas.UpdateAulaEnResuladoTipoAula", parameters);
            				}
            				parameters.put("id", idHorario);
            				parameters.put("estado", "L");
            			}
            			parameters.put("id", idHorario);
            			List<AsignaturaDto> conFirma = session.selectList("py.una.fp.mapper.Asignatura.getAsigConFirma", parameters);
            			for(AsignaturaDto a: conFirma){
	            			parameters.put("id_asignatura", a.getIdAsignatura());
	    					parameters.put("cantidad_alumnos", 0);
	    					parameters.put("nuevo", false);
	    					parameters.put("id_horario", idHorario);
	    					session.selectList("py.una.fp.mapper.Horario.insertHorarioResultado", parameters);
            			}
    					session.update("py.una.fp.mapper.Horario.cambiarEstado",parameters);
        				reportesEbj.generateAllReports(idHorario);
            		}else{
            			log.debug(response.getReason());
            			log.debug(response.getMessage());
            		}
            	}
			} catch (Exception e) {
				log.error("Failed! ", e);
			}
        	
        }
        log.debug("********* FIN TIMER ************");
    }
	
	private Respuesta<Double> consultarProgress(Integer id) throws IOException{
		String uri =
			    "http://"+conf.getValor("core_ip")+":"+conf.getValor("core_port")+"/"+conf.getValor("context_root")+"/rest/calc/horario/progress/" + id;
			log.debug(uri);
			URL url = new URL(uri);
			HttpURLConnection connection =
			    (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Accept", "application/json");
			
			if (connection.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ connection.getResponseCode());
			}
			
			BufferedReader br = new BufferedReader(new InputStreamReader(
					(connection.getInputStream())));

			String json="";
			String output;
			log.debug("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				log.debug(output);
				json+=output;
			}
			Gson gson = new Gson();
			return gson.fromJson(json, new TypeToken<Respuesta<Double>>(){}.getType());
	}
	
	private Respuesta<HorarioResultado> consultarResultado(Integer id) throws IOException{
		String uri =
			    "http://"+conf.getValor("core_ip")+":"+conf.getValor("core_port")+"/"+conf.getValor("context_root")+"/rest/calc/horario/estado/" + id;
			URL url = new URL(uri);
			HttpURLConnection connection =
			    (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Accept", "application/json");
			
			if (connection.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ connection.getResponseCode());
			}
			
			BufferedReader br = new BufferedReader(new InputStreamReader(
					(connection.getInputStream())));

			String json="";
			String output;
			log.debug("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				log.debug(output);
				json+=output;
			}
			Gson gson = new Gson();
			return gson.fromJson(json, new TypeToken<Respuesta<HorarioResultado>>(){}.getType());
	}

}

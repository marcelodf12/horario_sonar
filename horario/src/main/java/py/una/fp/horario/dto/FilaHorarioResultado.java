package py.una.fp.horario.dto;

public class FilaHorarioResultado {
	
	public Integer id;
	public Integer start;
	public Integer end;
	public String asignatura;
	public String profesor;
	public Integer tipoAula;
	public Integer cantidadDeAlumnos;
	public String seccion;

}

package py.una.fp.horario.ejb;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.AccessTimeout;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.apache.pdfbox.util.PDFMergerUtility;

import py.una.fp.horario.bd.DataBaseConfiguration;
import py.una.fp.horario.dto.ElementsDto;
import py.una.fp.horario.dto.NotificaciontDto;
import py.una.fp.horario.dto.QueryReportDto;
import py.una.fp.horario.reports.core.ReportExporter;
import py.una.fp.horario.reports.core.ReportGenerator;
import py.una.fp.horario.util.Configuracion;

@Stateless
public class ReportesEjb {

	private static final String directorySeparator = System.getProperty("file.separator");
	
	private static final String[] diasCombo = {"LUN","MAR","MIE","JUE","VIE","SAB","DOM"};
	
	@EJB
	DataBaseConfiguration dbc;
	
	@EJB
	HorarioEjb horarioEjb;
	
    @EJB
    Configuracion conf;
	
	Logger log = Logger.getLogger(ReportesEjb.class);
	
	
	
	
	
	/*
	 * Método que retorna todos los reportes concatenados en un solo archivo
	 */
	public String getReports(QueryReportDto query, Integer idHorario){
		List<File> list = new ArrayList<File>();
		String dirPdf = conf.getValor("dir_pdf");
		for(ElementsDto e:query.getElements()){
			if(e.getSelected()){
				try {
					String path = dirPdf + directorySeparator + idHorario + directorySeparator + query.getRuta();
				    String file =  path + directorySeparator + query.getPrefijo() + e.getIdentificador() +".pdf";
				    log.debug("Reporte input: " + file);
				    File f = new File(file);
				    if(f.exists()) list.add(f);
				} catch (Exception e2) {
					log.error("Failed!", e2);
				}
			}
		}
		try {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HHmmss");
			Date today = Calendar.getInstance().getTime();
			String path = dirPdf + directorySeparator + "reportes_auxiliar";
			File folder = new File(path);
		    if(!folder.exists()) folder.mkdirs();
		    String file = (path + directorySeparator + query.getRuta() + " " + df.format(today) + ".pdf");
			log.debug("Reporte ouput: " + file);
			doMerge(list, file);
			return file;
		} catch (Exception e2) {
			log.error("Failed!", e2);
		}
		return null;
	}
	
	
	
	
	
	
	
	/*
	 * Método que retorna todos los identificadores posibles para construir un reporte
	 */
	public QueryReportDto getQueryReports(){
		QueryReportDto queryReportDto = new QueryReportDto();
		try {
			SqlSession session = dbc.getSession();
			HashMap<String, Object> parameters = new HashMap<>();
			List<ElementsDto> profesores = null;
			List<ElementsDto> aulas = null;
			List<ElementsDto> carreras = null;
			profesores = session.selectList("py.una.fp.mapper.Reportes.getProfesores", parameters);
			aulas = session.selectList("py.una.fp.mapper.Reportes.getAulas", parameters);
			carreras = session.selectList("py.una.fp.mapper.Reportes.getCarreras", parameters);
			queryReportDto.setAulas(aulas);
			queryReportDto.setProfesores(profesores);
			queryReportDto.setCarreras(carreras);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return queryReportDto;
	}
	

	
	
	
	
	
	
	/*
	 * GENERAR REPORTE DE HORARIO POR EL AULA
	 */
	@AccessTimeout(value=-1)
	public void reporteDeHorarioPorAula(Integer idHorario, List<NotificaciontDto> list, String reportProcess) {
		log.info("Iniciado generacion de reportes Horario por Aula UUID: " + reportProcess);

		HashMap<String, ArrayList<NotificaciontDto>> aulas = new HashMap<String, ArrayList<NotificaciontDto>>();

		for(NotificaciontDto n: list) {
			formatearFecha(n);
			ArrayList<NotificaciontDto> notificaciones = aulas.get(n.getAsignaturaAula());
			if(notificaciones==null) {
				notificaciones = new ArrayList<NotificaciontDto>();
				aulas.put(n.getAsignaturaAula(), notificaciones);
			}
			notificaciones.add(n);
		}
		for (Map.Entry<String, ArrayList<NotificaciontDto>> entry : aulas.entrySet()) {
			log.debug("Key = " + entry.getKey() + ", Cantidad de Registros = " + entry.getValue().size());
			List<NotificaciontDto> ds = entry.getValue();
		    log.info("  Preparando pdf del aula: " + ds.get(0).getAsignaturaAula());
		    String titulo = "Aula: " + ds.get(0).getAsignaturaAula();
		    Map<String,Object> parametros = prepararParametros(titulo, "Horario de clases por aula");
		    
			try { 
			    byte[] pdf = (new ReportGenerator<NotificaciontDto>()).toPDF("HorarioProfesor", parametros, ds);
			    String file =  prepararDirectorio("horario_aula_", "HorarioPorAula", idHorario, ds.get(0).getAsignaturaAula().toString());
			    ReportExporter.gravarEmDisco(pdf, file);
			    log.info("    PDF (" + file + ") generado correctamente");
			} catch (Exception e) {
				log.error("ERROR AL GENERAR REPORTES POR AULA ("+titulo+")");
				log.error( "failed!", e );
			}
			updateProgress(reportProcess, entry.getValue().size());
		}
		log.info("El proceso 'GENERAR REPORTES POR AULA' culminó");
	}
	
	
	
	
	
	
	
	
	
	/*
	 * GENERAR REPORTES DE HORARIO POR CARRERA
	 */
	@AccessTimeout(value=-1)
	public void reporteDeHorarioPorSemestre(Integer idHorario, List<NotificaciontDto> list, String reportProcess) {
		log.info("Iniciado generacion de reportes Horario por Carrera UUID: " + reportProcess);
		HashMap<Integer, ArrayList<NotificaciontDto>> carreras = new HashMap<Integer, ArrayList<NotificaciontDto>>();
		for(NotificaciontDto n: list) {
			formatearFecha(n);
			ArrayList<NotificaciontDto> notificaciones = carreras.get(n.getCarreraId());
			if(notificaciones==null) {
				notificaciones = new ArrayList<NotificaciontDto>();
				carreras.put(n.getCarreraId(), notificaciones);
			}
			notificaciones.add(n);
		}
		
		for (Map.Entry<Integer, ArrayList<NotificaciontDto>> entry : carreras.entrySet()) {
			log.debug("Key = " + entry.getKey() + ", Cantidad de Registros = " + entry.getValue().size());
		    List<NotificaciontDto> ds = entry.getValue();
		    
		    log.info("Preparando pdf del aula: " + ds.get(0).getAsignaturaAula());
		    String titulo = ds.get(0).getCarreras();
		    Map<String,Object> parametros = prepararParametros(titulo, "Horario de clases por carrera");
			try {	    
			    byte[] pdf = (new ReportGenerator<NotificaciontDto>()).toPDF("HorarioCarreras", parametros, ds);
			    String file =  prepararDirectorio("horario_carrera_", "HorarioPorCarrera", idHorario, ds.get(0).getCarreraId().toString());
			    ReportExporter.gravarEmDisco(pdf, file);
			    log.debug("    PDF generado");
				
			} catch (Exception e) {
				log.error("ERROR AL GENERAR REPORTES POR CARRERAS ("+titulo+")");
				log.error( "failed!", e );
			}
			updateProgress(reportProcess, entry.getValue().size());
		}
		log.info("El proceso 'GENERAR REPORTES POR CARRERA' culminó");
	}
	
	
	
	
	
	
	
	
	/*
	 * GENERAR REPORTES DE EXAMENES
	 */
	@AccessTimeout(value=-1)
	public void reporteDeExamenes(Integer idHorario, List<NotificaciontDto> list, String reportProcess) {
		log.info("Iniciado generacion de reportes Horario por Carrera UUID: " + reportProcess);
		HashMap<Integer, ArrayList<NotificaciontDto>> tablaHashPorProfesor = new HashMap<Integer, ArrayList<NotificaciontDto>>();
		HashMap<String, ArrayList<NotificaciontDto>> tablaHashPorAula = new HashMap<String, ArrayList<NotificaciontDto>>();
		HashMap<Integer, ArrayList<NotificaciontDto>> tablaHashPorSemestre = new HashMap<Integer, ArrayList<NotificaciontDto>>();
		for(NotificaciontDto n: list) {
			ArrayList<NotificaciontDto> grupoProfesorList = tablaHashPorProfesor.get(n.getProfesorId());
			if(grupoProfesorList==null) {
				grupoProfesorList = new ArrayList<NotificaciontDto>();
				tablaHashPorProfesor.put(n.getProfesorId(), grupoProfesorList);
			}
			grupoProfesorList.add(n);
			
			ArrayList<NotificaciontDto> grupoAulaList = tablaHashPorAula.get(n.getAsignaturaAula());
			if(grupoAulaList==null) {
				grupoAulaList = new ArrayList<NotificaciontDto>();
				tablaHashPorAula.put(n.getAsignaturaAula(), grupoAulaList);
			}
			grupoAulaList.add(n);
			
			ArrayList<NotificaciontDto> grupoSemestreList = tablaHashPorSemestre.get(n.getCarreraId());
			if(grupoSemestreList==null) {
				grupoSemestreList = new ArrayList<NotificaciontDto>();
				tablaHashPorSemestre.put(n.getCarreraId(), grupoSemestreList);
			}
			grupoSemestreList.add(n);
		}
			
		for (Map.Entry<String, ArrayList<NotificaciontDto>> entry : tablaHashPorAula.entrySet()) {
			log.debug("Key = " + entry.getKey() + ", Cantidad de Registros = " + entry.getValue().size());
			ArrayList<NotificaciontDto> ds = entry.getValue();
		    log.info("  Preparando pdf examenes del aula: " + ds.get(0).getAsignaturaAula());
		    String titulo = "Aula: " + ds.get(0).getAsignaturaAula();
		    Map<String,Object> parametros = prepararParametros(titulo, "Horario de exámenes por aulas");
		    try {
			    byte[] pdf = (new ReportGenerator<NotificaciontDto>()).toPDF("Examenes", parametros, ds);
			    String file =  prepararDirectorio("examenes_aula_", "ExamenesPorAula", idHorario, ds.get(0).getAsignaturaAula().toString());
			    ReportExporter.gravarEmDisco(pdf, file);
			    log.info("    PDF (" + file + ") generado correctamente");
		    } catch (Exception e) {
				log.error("ERROR AL GENERAR REPORTES DE EXAMENES POR AULA ("+titulo+")");
				log.error( "failed!", e );
			}
		    updateProgress(reportProcess, entry.getValue().size());
		}
		log.info("El proceso 'GENERAR REPORTE DE EXÁMENES POR AULA' culminó");
		
		for (Map.Entry<Integer, ArrayList<NotificaciontDto>> entry : tablaHashPorSemestre.entrySet()) {
			log.debug("Key = " + entry.getKey() + ", Cantidad de Registros = " + entry.getValue().size());
			ArrayList<NotificaciontDto> ds = entry.getValue();
		    log.info("Preparando pdf examenes de la carrera: " + ds.get(0).getCarreraId());
		    String titulo = "Carerra: " + ds.get(0).getCarreras();
		    Map<String,Object> parametros = prepararParametros(titulo, "Horario de exámenes por carrera");
		    try {			    		    
			    byte[] pdf = (new ReportGenerator<NotificaciontDto>()).toPDF("Examenes", parametros, ds);
			    String file =  prepararDirectorio("examenes_carrera_", "ExamenesPorCarrera", idHorario, ds.get(0).getCarreraId().toString());
			    ReportExporter.gravarEmDisco(pdf, file);
			    log.info("    PDF (" + file + ") generado correctamente");
		    } catch (Exception e) {
				log.error("ERROR AL GENERAR REPORTES DE EXAMENES POR CARRERA ("+titulo+")");
				log.error( "failed!", e );
			}
		    updateProgress(reportProcess, entry.getValue().size());
		}
		log.info("El proceso 'GENERAR REPORTE DE EXÁMENES POR CARRERA' culminó");
		
		for (Map.Entry<Integer, ArrayList<NotificaciontDto>> entry : tablaHashPorProfesor.entrySet()) {
			log.debug("Key = " + entry.getKey() + ", Cantidad de Registros = " + entry.getValue().size());
		    ArrayList<NotificaciontDto> ds = entry.getValue();
		    log.info("Preparando pdf examenes del profesor: " + ds.get(0).getProfesorId());
		    String titulo = ds.get(0).getProfesorTitulo() + " " + ds.get(0).getProfesorNombre() + " " + ds.get(0).getProfesorApellido();
		    Map<String,Object> parametros = prepararParametros(titulo, "Horario de exámenes por profesor");
		    try {			    
			    byte[] pdf = (new ReportGenerator<NotificaciontDto>()).toPDF("Examenes", parametros, ds);
			    String file =  prepararDirectorio("examenes_profesor_", "ExamenesPorProfesor", idHorario, ds.get(0).getProfesorId().toString());
			    ReportExporter.gravarEmDisco(pdf, file);
			    log.info("    PDF (" + file + ") generado correctamente");
		    } catch (Exception e) {
				log.error("ERROR AL GENERAR REPORTES DE EXAMENES POR PROFESOR ("+titulo+")");
				log.error( "failed!", e );
			}
		    updateProgress(reportProcess, entry.getValue().size());
		}
		log.info("El proceso 'GENERAR REPORTE DE EXÁMENES POR PROFESOR' culminó");
	}

	
	
	
	
	
	
	/*
	 * GENERAR REPORTE DE HORARIO POR PROFESOR
	 */
	@AccessTimeout(value=-1)
	public void reporteDeHorarioPorProfesor(Integer idHorario, List<NotificaciontDto> list, String reportProcess) {
		log.info("Iniciado generacion de reportes Horario por Profesor UUID: " + reportProcess);
		HashMap<Integer, ArrayList<NotificaciontDto>> profesores = new HashMap<Integer, ArrayList<NotificaciontDto>>();
		
		for(NotificaciontDto n: list) {
			log.debug(n.getProfesorId());
			formatearFecha(n);
			ArrayList<NotificaciontDto> notificaciones = profesores.get(n.getProfesorId());
			if(notificaciones==null) {
				notificaciones = new ArrayList<NotificaciontDto>();
				profesores.put(n.getProfesorId(), notificaciones);
			}
			notificaciones.add(n);
		}

		for (Map.Entry<Integer, ArrayList<NotificaciontDto>> entry : profesores.entrySet()) {
			log.debug("Key = " + entry.getKey() + ", Cantidad de Registros = " + entry.getValue().size());
		    List<NotificaciontDto> ds = entry.getValue();
		    String profesor = ds.get(0).getProfesorTitulo() + " " + ds.get(0).getProfesorNombre() + " " + ds.get(0).getProfesorApellido();			    
		    Map<String,Object> parametros = prepararParametros(profesor, "Horario de clases por profesor");
		    try {
			    byte[] pdf = (new ReportGenerator<NotificaciontDto>()).toPDF("HorarioProfesor", parametros, ds);
			    String file =  prepararDirectorio("horario_profesor_", "HorarioPorProfesor", idHorario, ds.get(0).getProfesorId().toString());
			    ReportExporter.gravarEmDisco(pdf, file);
			    log.info("    PDF (" + file + ") generado correctamente");
		    } catch (Exception e) {
				log.error("ERROR AL GENERAR REPORTES POR PROFESORES");
				log.error( "failed!", e );
			}
		    updateProgress(reportProcess, entry.getValue().size());
		}
		log.info("El proceso 'GENERAR REPORTES POR PROFESOR' culminó");
	}
	
	
	
	
	
	
	/*
	 * GENERAR TODOS LOS REPORTES
	 */
	@Asynchronous
	public void generateAllReports(Integer idHorario){
			SqlSession session = dbc.getSession();
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("idHorario", idHorario);
			List<NotificaciontDto> listHorarioPorSemestre = null;
			List<NotificaciontDto> listHorarioPorAula = null;
			List<NotificaciontDto> listExamenes = null;
			List<NotificaciontDto> listHorarioPorProfesor = null;
			if(idHorario !=null){
				listHorarioPorAula = session.selectList("py.una.fp.mapper.Reportes.getHorario", parameters);
				listHorarioPorSemestre = session.selectList("py.una.fp.mapper.Reportes.getHorarioPorSemestrePorCarrera", parameters);
				listExamenes = session.selectList("py.una.fp.mapper.Reportes.getExamenes", parameters);
				listHorarioPorProfesor = session.selectList("py.una.fp.mapper.Profesor.getNotificados", parameters);
			}
			Integer total=0;
			if(listHorarioPorAula!=null) total+=listHorarioPorAula.size();
			if(listHorarioPorSemestre!=null) total+=listHorarioPorSemestre.size();
			if(listExamenes!=null) total+=(listExamenes.size()*3);
			if(listHorarioPorProfesor!=null) total+=listHorarioPorProfesor.size();
			
			String uuid = java.util.UUID.randomUUID().toString().replaceAll("-", "").substring(0, 32);
			parameters.put("uuid", uuid);
			parameters.put("total", total);
			session.insert("py.una.fp.mapper.Reportes.insertProgress", parameters);
			session.close();
			if(listHorarioPorSemestre!=null) reporteDeHorarioPorSemestre(idHorario, listHorarioPorSemestre, uuid);
			if(listHorarioPorAula!=null) reporteDeHorarioPorAula(idHorario, listHorarioPorAula, uuid);
			if(listExamenes!=null) reporteDeExamenes(idHorario, listExamenes, uuid);
			if(listHorarioPorProfesor!=null) reporteDeHorarioPorProfesor(idHorario, listHorarioPorProfesor, uuid);
			updateProgress(uuid, null);
	}
	

	
	
	
	/*
	 * ACTUALIZAR EL PROGRESO
	 * Aumento la cantidad de registros procesados en step
	 * Si step es null, entonces marca el proceso como finalizado
	 */
	private void updateProgress(String uuid, Integer step){
		try{
			SqlSession session = dbc.getSession();
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("uuid", uuid);
			parameters.put("step", step);
			if(step!=null){
				session.update("py.una.fp.mapper.Reportes.updateProgress", parameters);
			}else{
				session.update("py.una.fp.mapper.Reportes.finalizarProgress", parameters);
			}
			session.close();
		}catch(Exception e){
			log.error("ERROR AL ACTUALIZAR EL PROGRESO DE REPORTE UUID: " + uuid);
			log.error( "failed!", e );
		}
	}
	
	
	
	
	
	
	
	/*
	 * CONSULTAR PROGRESO DE REPORTE
	 */
	public Long getReportProcess(){
		SqlSession session = dbc.getSession();
		Long progress = session.selectOne("py.una.fp.mapper.Reportes.getProgress");
		session.close();
		return progress;
	}
	
	
	
	
	
	
	
	/*
	 * UNIR VARIOS PDFS EN UN SOLO PDF
	 */
    private void doMerge(List<File> inputPdfList, String destination) throws Exception{
    	PDFMergerUtility ut = new PDFMergerUtility();
    	for(File file : inputPdfList){
    		ut.addSource(file);
    	}
    	ut.setDestinationFileName(destination);
    	ut.mergeDocuments();
        log.info("Pdf files merged successfully.");
    }
    
    
    
    
    
    /*
     * MÉTODO AUXILIAR PARA FORMATEAR FECHAS PARA LOS REPORTES
     */
	private void formatearFecha(NotificaciontDto n){
		double i = n.getStart()/288;
		n.setDia(diasCombo[(int) Math.floor(i)]);
		n.setInicio(slotToHora(n.getStart()));
		n.setFin(slotToHora(n.getEnd()));
	}
	
	
	
	
	/*
	 * MÉTODO AUXILIAR PARA SETEAR LOS PARÁMETROS PARA LOS REPORTES
	 */
	private Map<String,Object> prepararParametros(String titulo, String subTitulo){
		String fecha = "Última fecha de actualización: " + (new SimpleDateFormat("dd/MM/yyyy HH:mm")).format(new Date());
		Map<String,Object> parametros = new HashMap<String,Object>();
	    parametros.put("titulo", titulo);
	    parametros.put("fecha", fecha);
	    parametros.put("subTitulo", subTitulo);
		return parametros;
	}
	
	
	
	
	
	
	/*
	 * MÉTODO AUXILIAR PARA CONSULTAR DIRECTORIO DE PDF, SI NO EXISTE LOS CREA
	 */
	private String prepararDirectorio(String prefijo, String ruta, Integer idHorario, String identificador){
		String dirPdf = conf.getValor("dir_pdf");
		String path = dirPdf + directorySeparator + idHorario + directorySeparator + ruta;
	    File folder = new File(path);
	    if(!folder.exists()) folder.mkdirs();
	    String file =  path + directorySeparator + prefijo + identificador +".pdf";
	    return file;
	}
    
	
	
	
	
	/*
	 * Método auxiliar para transformar un slot en hora
	 */
	private String slotToHora(int slot) {
		slot = (slot-1)%288;
		int minTotal = slot*5;
		int hora = (int) Math.floor(minTotal/60);
		String horaStr;
		String minStr;
		horaStr = hora > 9 ? "" + hora: "0" + hora;
		int min = minTotal - hora*60;
		minStr = min > 9 ? "" + min: "0" + min;
		return horaStr+":"+minStr;
	}
}

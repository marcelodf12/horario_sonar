package py.una.fp.horario.dto;

import java.util.ArrayList;

public class HorarioResultado {
	
	public ArrayList<Integer> idDivision;
	
	public ArrayList<Integer> start;
	
	public ArrayList<Integer> end;
	
	public ArrayList<String> aulas;

	public HorarioResultado() {
		super();
		this.start = new ArrayList<Integer>();
		this.end =  new ArrayList<Integer>();
		this.idDivision = new ArrayList<Integer>();
	}
}
package py.una.fp.horario.rest;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import py.una.fp.horario.dto.AsignaturaCarreraDto;
import py.una.fp.horario.dto.AsignaturaDto;
import py.una.fp.horario.dto.AsignaturasPorSemestreDto;
import py.una.fp.horario.dto.AulaDto;
import py.una.fp.horario.ejb.AsignaturaEjb;
import py.una.fp.horario.secured.Secured;
import py.una.fp.horario.util.Mensaje;
import py.una.fp.horario.util.Respuesta;

@Stateless
@Path("/asignatura")
@Api(
	    value = "/asignatura", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON
	)
public class AsignaturaRest {
	
	@EJB
	AsignaturaEjb asignaturaEjb;
	
	Logger log = Logger.getLogger(AsignaturaRest.class);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Listar asignaturas",
	        notes = "Servicio para listar y filtrar las asignaturas. Se pueden filtrar por nombre de asignatura, codigo, periodo, "
	        		+ "turno, departamente y sección."
	    )
	public Respuesta<ArrayList<AsignaturaDto>> listAll(
			@ApiParam(value = "Filtro: Nombre de asignatura", required = false) @QueryParam("asignatura") String asignatura,
			@ApiParam(value = "Filtro: Código ACAD de asignatura", required = false) @QueryParam("codAsig") String codAsig,
			@ApiParam(value = "Filtro: Periodo", required = false) @QueryParam("periodo") String periodo,
			@ApiParam(value = "Filtro: Turnno", required = false) @QueryParam("turno") String turno,
			@ApiParam(value = "Filtro: Departamento", required = false) @QueryParam("dpto") String dpto,
			@ApiParam(value = "Filtro: Sección", required = false) @QueryParam("seccion") String seccion,
			@ApiParam(value = "Paginación: Desplazamiento", required = false) @QueryParam("offset") String offset,
			@ApiParam(value = "Ordenación: Columna a ordenar [asignatura|dpto|turno|seccion|cod_asig|periodo]", required = false) @QueryParam("column") String column,
			@ApiParam(value = "Ordenación: True=Ascendente, False=Descendente", required = false)@QueryParam("order") Boolean order
			){
		List<AsignaturaDto> list = null;
		Integer intPeriodo=periodo==null?null:new Integer(periodo);;
		String mensaje="";
		Boolean success = true;
		String reason = Mensaje.CONSULTA_EXITOSA;
		String type = "success";
		try {
			list = asignaturaEjb.getAll(asignatura, codAsig, intPeriodo, turno, dpto, seccion, new Integer(offset), column, order);
		} catch (Exception e) {
			log.error("Failed! ",e);
			mensaje = Mensaje.ERROR_DATA_BASE;
			reason = e.getMessage();
			success = false;
			type = "error";
		}
		if(list != null){
			mensaje = (list.size()==11?offset:"ultima");
			if(list.size()==11){
				list.remove(10);
			}
		}else{
			mensaje = Mensaje.WARN_ASIGNATURA_CONSULTA_VACIA;
			success = false;
			type = "info";
			reason = Mensaje.WARN;
		}
		Respuesta<ArrayList<AsignaturaDto>> r = new Respuesta<ArrayList<AsignaturaDto>>(success, mensaje, reason, type, (ArrayList<AsignaturaDto>) list);
		return r;
	}
	
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Crear asignaturas",
	        notes = "Servicio para dar de alta una nueva asignatura"
	    )
	public Respuesta<AulaDto> create(@ApiParam(value = "Asignatura a dar de alta", required = true) AsignaturaDto a){
		String mensaje=Mensaje.CREACION_ASIGNATURA_EXITOSO;
		String reason = Mensaje.INFO;
		String type = "success";
		Boolean success = asignaturaEjb.create(a);		
		if(!success){
			mensaje = Mensaje.CREACION_ASIGNATURA_NO_EXITOSO;
			type = "error";
			reason = Mensaje.ERROR;
		}
		Respuesta<AulaDto> r = new Respuesta<AulaDto>(success, mensaje, reason, type);
		return r;
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Modificar asignaturas",
	        notes = "Servicio para modificar una asignatura"
	    )
	public Respuesta<AulaDto> guardar(@ApiParam(value = "Asignatura a modificar", required = true) AsignaturaDto a){
		String mensaje=Mensaje.EDICION_ASIGNATURA_EXITOSO;
		String reason = Mensaje.INFO;
		String type = "success";
		Boolean success = asignaturaEjb.edit(a);		
		if(!success){
			mensaje = Mensaje.EDICION_ASIGNATURA_NO_EXITOSO;
			type = "error";
			reason = Mensaje.ERROR;
		}
		Respuesta<AulaDto> r = new Respuesta<AulaDto>(success, mensaje, reason, type);
		return r;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id:[0-9][0-9]*}")
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Buscar asignatura",
	        notes = "Servicio para retornar una asignatura a partir del Id."
	    )
	public Respuesta<AsignaturaDto> getById(
			@ApiParam(value = "Identificador de Asignatura", required = true) @PathParam("id") Integer id
			){
		AsignaturaDto a = null;
		String mensaje="";
		Boolean success = true;
		String reason = Mensaje.CONSULTA_EXITOSA;
		String type = "success";
		try {
			a = asignaturaEjb.get(id);
		} catch (Exception e) {
			log.error("Failed! ",e);
			mensaje = Mensaje.ERROR_DATA_BASE;
			reason = e.getCause().getMessage();
			success = false;
			type = "error";
		}
		if(a == null){
			mensaje = Mensaje.ERROR_ASIGNATURA_NO_EXISTE;
			success = false;
			type = "info";
			reason = Mensaje.ERROR;
		}
		Respuesta<AsignaturaDto> r = new Respuesta<AsignaturaDto>(success, mensaje, reason, type, a);
		return r;
	}
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id:[0-9][0-9]*}")
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Eliminar asignatura",
	        notes = "Servicio para eliminar una asignatura."
	    )
	public Respuesta<AulaDto> delete(@ApiParam(value = "Identificador de Asignatura", required = true) @PathParam("id") Integer id){
		String mensaje=Mensaje.ELIMINACION_ASIGNATURA_EXITOSO;
		String reason = Mensaje.INFO;
		String type = "success";
		
		Boolean success = asignaturaEjb.delete(id);
		if(!success){
			mensaje = Mensaje.ELIMINACION_ASIGNATURA_NO_EXITOSO;
			type = "error";
			reason = Mensaje.ERROR;
		}
		Respuesta<AulaDto> r = new Respuesta<AulaDto>(success, mensaje, reason, type);
		return r;
	}
	
	@POST
	@Path("/grupo/{id1:[0-9][0-9]*}/{id2:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Agregar Relación Grupo",
	        notes = "Servicio para agregar una relación de grupo entre las asignaturas con identificadores id1 y id2"
	    )
	public Respuesta<AulaDto> addGrupo(@PathParam("id1") Integer id1, @PathParam("id2") Integer id2){
		String mensaje=Mensaje.EDICION_ASIGNATURA_EXITOSO;
		String reason = Mensaje.INFO;
		String type = "success";
		Boolean success = asignaturaEjb.addGrupo(id1, id2);	
		if(!success){
			mensaje = Mensaje.EDICION_ASIGNATURA_NO_EXITOSO;
			type = "error";
			reason = Mensaje.ERROR;
		}
		Respuesta<AulaDto> r = new Respuesta<AulaDto>(success, mensaje, reason, type);
		return r;
	}
	
	@POST
	@Path("/conflicto/{id1:[0-9][0-9]*}/{id2:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Agregar Relación Conflicto",
	        notes = "Servicio para agregar una relación de conflicto entre las asignaturas con identificadores id1 y id2"
	    )
	public Respuesta<AulaDto> addConflicto(@PathParam("id1") Integer id1, @PathParam("id2") Integer id2){
		String mensaje=Mensaje.EDICION_ASIGNATURA_EXITOSO;
		String reason = Mensaje.INFO;
		String type = "success";
		Boolean success = asignaturaEjb.addConflicto(id1, id2);	
		if(!success){
			mensaje = Mensaje.EDICION_ASIGNATURA_NO_EXITOSO;
			type = "error";
			reason = Mensaje.ERROR;
		}
		Respuesta<AulaDto> r = new Respuesta<AulaDto>(success, mensaje, reason, type);
		return r;
	}
	@DELETE
	@Path("/coiciden/{id1:[0-9][0-9]*}/{id2:[0-9][0-9]*}/{coiciden}")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Agregar Relación",
	        notes = "Servicio para agregar una relación de grupo/conflicto entre las asignaturas con identificadores id1 y id2. Si el parametro coiciden es true se agrega un conflicto, si el parametro es false se agrega un grupo"
	    )
	public Respuesta<AulaDto> addGrupo(@PathParam("id1") Integer id1, @PathParam("id2") Integer id2, @PathParam("coiciden") Boolean coiciden){
		String mensaje=Mensaje.EDICION_ASIGNATURA_EXITOSO;
		String reason = Mensaje.INFO;
		String type = "success";
		Boolean success = asignaturaEjb.quitar(id1, id2, coiciden);	
		if(!success){
			mensaje = Mensaje.EDICION_ASIGNATURA_NO_EXITOSO;
			type = "error";
			reason = Mensaje.ERROR;
		}
		Respuesta<AulaDto> r = new Respuesta<AulaDto>(success, mensaje, reason, type);
		return r;
	}
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/carrera/{id:[0-9][0-9]*}")
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Listar Asignaturas de una Carrera",
	        notes = "Servicio para listar las asignaturas que pertenecen a una carrera"
	    )
	public Respuesta<List<AsignaturasPorSemestreDto>> getAsignaturasCarreraByIdCarrera(@PathParam("id") Integer idCarrera){
		List<AsignaturaCarreraDto> a = null;
		String mensaje="";
		Boolean success = true;
		String reason = Mensaje.CONSULTA_EXITOSA;
		String type = "success";
		

		List<AsignaturasPorSemestreDto> out = new ArrayList<AsignaturasPorSemestreDto>();
		
		try {
			a = asignaturaEjb.getAsignaturasDeLaCarrera(idCarrera);
			
			Hashtable<Integer,List<AsignaturaCarreraDto>> contenedor=new Hashtable<Integer,List<AsignaturaCarreraDto>>();
			for (AsignaturaCarreraDto asignatura : a) {
				Integer key = asignatura.getSemestre();
				if(key!=null){
					if(asignatura.getNombre() == null || asignatura.getNombre().trim().isEmpty() == true){
						asignatura.setNombre(asignatura.getNombrePrincipal());
					}
					if(contenedor.containsKey(key) == true){
						List<AsignaturaCarreraDto> asignaturas = contenedor.get(key);
						asignaturas.add(asignatura);
					} else {
						List<AsignaturaCarreraDto> asignaturas = new ArrayList<AsignaturaCarreraDto>();
						asignaturas.add(asignatura);
						contenedor.put(key, asignaturas);
					}
				}
			}
			
			Enumeration<Integer> llaves = contenedor.keys();
			while (llaves.hasMoreElements()) {
				AsignaturasPorSemestreDto asigCarrera = new AsignaturasPorSemestreDto();
				Integer key = llaves.nextElement();
				asigCarrera.setNombre("Semestre "+(key<10?("0"+key):key));
				List<AsignaturaCarreraDto> asignaturas = contenedor.get(key);
				asigCarrera.setAsignaturas(asignaturas);
				out.add(asigCarrera);
			}

			
			
		} catch (Exception e) {
			log.error("Failed! ",e);
			mensaje = Mensaje.ERROR_DATA_BASE;
			reason = e.getCause().getMessage();
			success = false;
			type = "error";
		}
		if(a == null){
			mensaje = Mensaje.ERROR_ASIGNATURAS_NO_EXISTE;
			success = false;
			type = "info";
			reason = Mensaje.ERROR;
		}
		Respuesta<List<AsignaturasPorSemestreDto>> r = new Respuesta<List<AsignaturasPorSemestreDto>>(success, mensaje, reason, type, out);
		return r;
	}
	
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/asignaturasRecalculo/")
	@ApiOperation(
	        value = "Listar asignaturas para recalculo",
	        notes = "Servicio para listar las asignaturas-carreras"
	    )
	public Respuesta<List<AsignaturaCarreraDto>> listAllAsignaturasRecalculo(
			@QueryParam("nombrePrincipal") String nombrePrincipal,
			@QueryParam("nombre") String nombre,
			@QueryParam("carrera") String carrera,
			@QueryParam("profesor") String profesor,
			@QueryParam("column") String column,
			@QueryParam("offset") String offset,
			@QueryParam("order") Boolean order
			){
		
		String mensaje="";
		Boolean success = true;
		String reason = Mensaje.CONSULTA_EXITOSA;
		String type = "success";
		List<AsignaturaCarreraDto> list = null;
		try {
			list = asignaturaEjb.getAllAsignaturasParaRecalculo(nombrePrincipal, nombre, carrera, profesor, new Integer(offset), column, order);
		} catch (Exception e) {
			log.error("Failed! ",e);
			mensaje = Mensaje.ERROR_DATA_BASE;
			reason = e.getCause().getMessage();
			success = false;
			type = "error";
		}
		if(list != null){
			mensaje = (list.size()==6?offset:"ultima");
			if(list.size()==6){
				list.remove(5);
			}
		}else{
			mensaje = Mensaje.WARN_ASIGNATURA_CONSULTA_VACIA;
			success = false;
			type = "info";
			reason = Mensaje.WARN;
		}
		Respuesta<List<AsignaturaCarreraDto>> r = new Respuesta<List<AsignaturaCarreraDto>>(success, mensaje, reason, type, list);
		return r;
	}
	
	@GET
	@Path("relaciones/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Listar Relacioens",
	        notes = "Servicio para listar todas las relaciones entre asignaturas. Si el parámetro coiciden es true se listan los grupos y si es false se listan los conflictos"
	    )
	public Respuesta<ArrayList<AsignaturaDto>> listAll(
			@QueryParam("coiciden") Boolean coiciden,
			@PathParam("id") Integer id
			){
		List<AsignaturaDto> list = null;
		String mensaje="";
		Boolean success = true;
		String reason = Mensaje.CONSULTA_EXITOSA;
		String type = "success";
		try {
			list = asignaturaEjb.getAllRelaciones(id,coiciden);
			mensaje=Mensaje.CONSULTA_EXITOSA;
		} catch (Exception e) {
			log.error("Failed! ",e);
			mensaje = Mensaje.ERROR_DATA_BASE;
			reason = e.getMessage();
			success = false;
			type = "error";
		}
		Respuesta<ArrayList<AsignaturaDto>> r = new Respuesta<ArrayList<AsignaturaDto>>(success, mensaje, reason, type, (ArrayList<AsignaturaDto>) list);
		return r;
	}

}

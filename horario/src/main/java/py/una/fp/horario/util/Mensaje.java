package py.una.fp.horario.util;

public class Mensaje {

	/**
	 * Mensajes del sistema.
	 */
	// Mensajes de sistema
	public static final String CONSULTA_EXITOSA = "Consulta Exitosa";
	public static final String INFO = "Información";
	public static final String WARN = "Advertencia";
	public static final String ERROR = "Error";
	
	// Parametros del sistema
	public static final String PELIGRO_CONFIGURACION = "Cuidado con los parametros a modificar. Podría dejar el sistema sin funcionar";
	public static final String UPDATE_CONFIG_EXITOSO = "Se ha editado correctamente la configuración";

	
	// Errores del sistema
	public static final String ERROR_AUTENTICACION = "Usuario o contraseña invalida.";
	public static final String CONTACT_ADMIN = "Si el problema persiste, contacte con el administrador.";
	public static final String ERROR_DATA_BASE = "Error en la base de datos." + CONTACT_ADMIN;
	public static final String ERROR_IO_FILE = "Error al procesar el archivo." + CONTACT_ADMIN;
	public static final String ERROR_SECCION_NO_EXISTE = "No existe la seccion.";
	public static final String ERROR_ASIGNATURA_NO_EXISTE = "No existe la asignatura.";
	public static final String ERROR_ASIGNATURAS_NO_EXISTE = "No existen asignaturas para la carrera.";
	public static final String ERROR_AULA_NO_EXISTE = "No existe el aula.";
	public static final String ERROR_CARRERA_NO_EXISTE = "No existe la carrera.";
	public static final String ERROR_CATEDRA_NO_EXISTE = "No existe la cátedra.";
	public static final String ERROR_DEPARTAMENTO_NO_EXISTE = "No existe el departamento.";
	public static final String ERROR_PROFESOR_NO_EXISTE = "No existe el profesor.";
	public static final String ERROR_USUARIO_NO_EXISTE = "No existe el usaurio.";
	public static final String ERROR_PERFILES_NO_EXISTE = "Usuario sin permisos asignados";
	
	// Warning
	public static final String WARN_SECCION_CONSULTA_VACIA = "No se encontró secciones que coicidan con el criterio de búsqueda.";
	public static final String WARN_ASIGNATURA_CONSULTA_VACIA = "No se encontró asignaturas que coicidan con el criterio de búsqueda.";
	public static final String WARN_AULA_CONSULTA_VACIA = "No se encontró aulas que coicidan con el criterio de búsqueda.";
	public static final String WARN_CARRERA_CONSULTA_VACIA = "No se encontró carreras que coicidan con el criterio de búsqueda.";
	public static final String WARN_CATEDRA_CONSULTA_VACIA = "No se encontró catedras que coicidan con el criterio de búsqueda.";
	public static final String WARN_DEPARTAMENTO_CONSULTA_VACIA = "No se encontró departamentos que coicidan con el criterio de búsqueda.";
	public static final String WARN_PROFESOR_CONSULTA_VACIA = "No se encontró profesores que coicidan con el criterio de búsqueda.";
	public static final String WARN_USUARIO_CONSULTA_VACIA = "No se encontró usuarios que coicidan con el criterio de búsqueda.";
	public static final String WARN_HORARIO_CONSULTA_VACIA = "No se encontró horarios que coicidan con el criterio de búsqueda.";
	
	// Confirmacion de Edicion
	
	public static final String EDICION_SECCION_EXITOSO = "La sección se editó correctamente.";
	public static final String EDICION_ASIGNATURA_EXITOSO = "La asignatura se editó correctamente.";
	public static final String EDICION_AULA_EXITOSO = "El aula se editó correctamente.";
	public static final String EDICION_CARRERA_EXITOSO = "La carrera se editó correctamente.";
	public static final String EDICION_CATEDRA_EXITOSO = "La cátedra se editó correctamente.";
	public static final String EDICION_DEPARTAMENTO_EXITOSO = "El departamento se editó correctamente.";
	public static final String EDICION_PROFESOR_EXITOSO = "El profesor se editó correctamente.";
	public static final String EDICION_USUARIO_EXITOSO = "El usuario se editó correctamente.";
	public static final String EDICION_CONTRASENHA_EXITOSA = "La contraseña se actualizó correctamente.";
	public static final String EDICION_SECCION_NO_EXITOSO = "Hubo un problema al editar la sección. " + CONTACT_ADMIN;
	public static final String EDICION_ASIGNATURA_NO_EXITOSO = "Hubo un problema al editar la asignatura. " + CONTACT_ADMIN;
	public static final String EDICION_AULA_NO_EXITOSO = "Hubo un problema al editar el aula. " + CONTACT_ADMIN;
	public static final String EDICION_CARRERA_NO_EXITOSO = "Hubo un problema al editar la carrera. " + CONTACT_ADMIN;
	public static final String EDICION_CATEDRA_NO_EXITOSO = "Hubo un problema al editar la cátedra. " + CONTACT_ADMIN;
	public static final String EDICION_DEPARTAMENTO_NO_EXITOSO = "Hubo un problema al editar el departamento. " + CONTACT_ADMIN;
	public static final String EDICION_PROFESOR_NO_EXITOSO = "Hubo un problema al editar el profesor. " + CONTACT_ADMIN;
	public static final String EDICION_USUARIO_NO_EXITOSO = "Hubo un problema al editar el usuario. " + CONTACT_ADMIN;
	public static final String EDICION_CONTRASEÑA_NO_EXITOSO = "Hubo un problema al actualizar la contraseña. " + CONTACT_ADMIN;
	public static final String EDICION_HORARIO_EXITOSO = "El horario se actualizó correctamente.";
	public static final String PUBLICAR_HORARIO_EXITOSO = "El horario se ha publicado correctamente. Se enviaran las notificaciones a los profesores.";
	public static final String EDICION_HORARIO_NO_EXITOSO = "Hubo un problema al editar el horario. " + CONTACT_ADMIN;
	public static final String EDICION_CONTRASEÑA_NO_EXITOSO_INCORRECTA = "Hubo un problema al actualizar la contraseña. \nLa contraseña actual introducida es incorrecta!";
	
	// Confirmacion de Creacion
	public static final String CREACION_SECCION_EXITOSO = "La sección se creó correctamente.";
	public static final String CREACION_ASIGNATURA_EXITOSO = "La asignatura se creó correctamente.";
	public static final String CREACION_AULA_EXITOSO = "El aula se creó correctamente.";
	public static final String CREACION_CARRERA_EXITOSO = "La carrera se creó correctamente.";
	public static final String CREACION_CATEDRA_EXITOSO = "La catedra se creó correctamente.";
	public static final String CREACION_DEPARTAMENTO_EXITOSO = "El departamento se creó correctamente.";
	public static final String CREACION_PROFESOR_EXITOSO = "El profesor se creó correctamente.";
	public static final String CREACION_USUARIO_PERFIL_EXITOSO = "El permiso se asignó correctamente.";
	public static final String CREACION_USUARIO_EXITOSO = "El usuario se creó correctamente.";
	public static final String CREACION_HORARIO_EXITOSO = "El horario se creó correctamente.";
	public static final String CREACION_SECCION_NO_EXITOSO = "Hubo un problema al crear la sección. " + CONTACT_ADMIN;
	public static final String CREACION_ASIGNATURA_NO_EXITOSO = "Hubo un problema al crear la asignatura. " + CONTACT_ADMIN;
	public static final String CREACION_AULA_NO_EXITOSO = "Hubo un problema al crear el aula. " + CONTACT_ADMIN;
	public static final String CREACION_CARRERA_NO_EXITOSO = "Hubo un problema al crear la carrera. " + CONTACT_ADMIN;
	public static final String CREACION_CATEDRA_NO_EXITOSO = "Hubo un problema al crear la catedra. " + CONTACT_ADMIN;
	public static final String CREACION_DEPARTAMENTO_NO_EXITOSO = "Hubo un problema al crear el departamento. " + CONTACT_ADMIN;
	public static final String CREACION_PROFESOR_NO_EXITOSO = "Hubo un problema al crear el profesor. " + CONTACT_ADMIN;
	public static final String CREACION_USUARIO_PERFIL_NO_EXITOSO = "Hubo un problema al asignar permiso." + CONTACT_ADMIN;
	public static final String CREACION_USUARIO_NO_EXITOSO = "Hubo un problema al crear el usuario. " + CONTACT_ADMIN;
	public static final String CREACION_HORARIO_NO_EXITOSO = "Hubo un problema al crear el horario. " + CONTACT_ADMIN;
	public static final String EXISTE_LOGIN_USUARIO = "Ya existe un usuario con el mismo nombre.";
	
	// Confirmacion de eliminacion
	public static final String ELIMINACION_SECCION_EXITOSO = "Se ha eliminado correctamente la sección.";
	public static final String ELIMINACION_ASIGNATURA_EXITOSO = "Se ha eliminado correctamente la asignatura.";
	public static final String ELIMINACION_AULA_EXITOSO = "Se ha eliminado correctamente el aula.";
	public static final String ELIMINACION_CARRERA_EXITOSO = "Se ha eliminado correctamente la carrera.";
	public static final String ELIMINACION_CATEDRA_EXITOSO = "Se ha eliminado correctamente la cátedra.";
	public static final String ELIMINACION_DEPARTAMENTO_EXITOSO = "Se ha eliminado correctamente el departamento.";
	public static final String ELIMINACION_PROFESOR_EXITOSO = "Se ha eliminado correctamente el profesor.";
	public static final String ELIMINACION_PERMISO_EXITOSO = "Se ha eliminado correctamente el permiso.";
	public static final String ELIMINACION_USUARIO_EXITOSO = "Se ha eliminado correctamente el usuario.";
	public static final String ELIMINACION_HORARIO_EXITOSO = "Se ha eliminado correctamente el horario.";
	public static final String ELIMINACION_SECCION_NO_EXITOSO = "Hubo un problema al eliminar la sección. " + CONTACT_ADMIN;
	public static final String ELIMINACION_ASIGNATURA_NO_EXITOSO = "Hubo un problema al eliminar la asignatura. " + CONTACT_ADMIN;
	public static final String ELIMINACION_AULA_NO_EXITOSO = "Hubo un problema al eliminar el aula. " + CONTACT_ADMIN;
	public static final String ELIMINACION_CARRERA_NO_EXITOSO = "No se puede eliminar la carrera debido a que existen asignaturas relacionadas. Debe eliminar todas las asignaturas relacionadas antes. " + CONTACT_ADMIN;
	public static final String ELIMINACION_CATEDRA_NO_EXITOSO = "Hubo un problema al eliminar la catedra. " + CONTACT_ADMIN;
	public static final String ELIMINACION_DEPARTAMENTO_NO_EXITOSO = "No se puede eliminar el departamento debido a que existen asignaturas relacionadas. Debe eliminar todas las asignaturas relacionadas antes. " + CONTACT_ADMIN;
	public static final String ELIMINACION_PROFESOR_NO_EXITOSO = "Hubo un problema al eliminar el profesor. " + CONTACT_ADMIN;
	public static final String ELIMINACION_PERMISO_NO_EXITOSO = "Hubo un problema al eliminar el permiso. " + CONTACT_ADMIN;
	public static final String ELIMINACION_USUARIO_NO_EXITOSO = "Hubo un problema al eliminar el usuario. " + CONTACT_ADMIN;
	public static final String ELIMINACION_HORARIO_NO_EXITOSO = "Hubo un problema al eliminar el horario. " + CONTACT_ADMIN;
	
	// Borradores
	public static final String ADD_SELECTED_BORRADOR = "Catedra seleccionada para calendarización";
	public static final String DEL_SELECTED_BORRADOR = "Catedra no seleccionada para calendarización";
	public static final String ADD_DEF_BORRADOR = "\nAgregado como Derecho a Examen Final";
	public static final String DEL_DEF_BORRADOR = "\nEliminado como Derecho a Examen Final";
	
	// Actualizacion de cantidad de alumnos
	public static final String UPDATE_INSCRIPTOS_EXITOSO = "Se ha actualizado correctamente la cantidad de inscriptos con el archivo: ";
	public static final String UPDATE_INSCRIPTOS_NO_EXITOSO = "Existe un problema con el formato del archivo. El archivo debe contener al menos los campos: CodAsig; Turno; Seccion; SinFirma; ConFirma";
	
	public static final String UPDATE_ASIG_EXITOSO = "Se ha actualizado correctamente las restricciones por carrera.";
	public static final String UPDATE_ASIG_NO_EXITOSO = "Existe un problema con el formato del archivo. El archivo debe contener al menos los campos: Grupo; Turno; Enfasis; Plan; Seccion; CodAsig; Asignatura; CodAsigCarrera; idCarrera; semestre; nivel";
	public static final String UPDATE_ASIG_NO_EXITOSO_ARRAY = "Hubo un error al actualizar las restricciones. " + CONTACT_ADMIN;

	
}

package py.una.fp.horario.reports.exceptions;

/**
 * Generic Exception Handler for the JasperPOJOReport operations 
 */
public class JasperPOJOReportException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * Default Constructor
	 * @param e
	 */
	public JasperPOJOReportException(Exception e) {
		super(e);
	}
 
}

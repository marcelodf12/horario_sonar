package py.una.fp.horario.dto;

import java.io.Serializable;
import java.util.ArrayList;

public class AsignaturaDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1807011713361254013L;
	private Integer idAsignatura;
	private String asignatura;
	private Integer periodo;
	private String dpto;
	private String seccion;
	private String turno;
	private Integer idDpto;
	private String slots;
	private String codAsig;
	private ArrayList<DivisionDto> divisiones;
	private ArrayList<ProfesorDto> profesores;
	private ArrayList<AsignaturaCarreraDto> carreras;
	
	public Integer getIdAsignatura() {
		return idAsignatura;
	}
	public void setIdAsignatura(Integer idAsignatura) {
		this.idAsignatura = idAsignatura;
	}
	public String getAsignatura() {
		return asignatura;
	}
	public void setAsignatura(String asignatura) {
		this.asignatura = asignatura;
	}
	public Integer getPeriodo() {
		return periodo;
	}
	public void setPeriodo(Integer periodo) {
		this.periodo = periodo;
	}
	public String getDpto() {
		return dpto;
	}
	public void setDpto(String dpto) {
		this.dpto = dpto;
	}
	public String getSeccion() {
		return seccion;
	}
	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}
	public Integer getIdDpto() {
		return idDpto;
	}
	public void setIdDpto(Integer idDpto) {
		this.idDpto = idDpto;
	}
	public String getSlots() {
		return slots;
	}
	public void setSlots(String slots) {
		this.slots = slots;
	}
	public ArrayList<DivisionDto> getDivisiones() {
		return divisiones;
	}
	
	public String getTurno() {
		return turno;
	}
	public void setTurno(String turno) {
		this.turno = turno;
	}
	public void setDivisiones(ArrayList<DivisionDto> divisiones) {
		this.divisiones = divisiones;
	}
	public ArrayList<ProfesorDto> getProfesores() {
		return profesores;
	}
	public void setProfesores(ArrayList<ProfesorDto> profesores) {
		this.profesores = profesores;
	}
	public String getCodAsig() {
		return codAsig;
	}
	public void setCodAsig(String codAsig) {
		this.codAsig = codAsig;
	}
	public ArrayList<AsignaturaCarreraDto> getCarreras() {
		return carreras;
	}
	public void setCarreras(ArrayList<AsignaturaCarreraDto> carreras) {
		this.carreras = carreras;
	}
}

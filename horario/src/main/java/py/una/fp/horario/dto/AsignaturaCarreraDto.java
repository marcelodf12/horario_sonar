package py.una.fp.horario.dto;

public class AsignaturaCarreraDto {
	
	private Integer idAsignaturaCarrera;
	private Integer idAsignatura;
	private Integer idCarrera;
	private String nombre;
	private Integer semestre;
	private String enfasis;
	private String plan;
	private Integer nivel;
	private String sigla;
	private String turno;
	private String seccion;
	

	private String nombreProfesor = "";
	private String nombrePrincipal = "";
	private String carrera = "";
	
	public String getCarrera() {
		return carrera;
	}
	public void setCarrera(String carrera) {
		this.carrera = carrera;
	}
	public String getNombreProfesor() {
		return nombreProfesor;
	}
	public void setNombreProfesor(String nombreProfesor) {
		this.nombreProfesor = nombreProfesor;
	}
	public String getNombrePrincipal() {
		return nombrePrincipal;
	}
	public void setNombrePrincipal(String nombrePrincipal) {
		this.nombrePrincipal = nombrePrincipal;
	}
	public Integer getIdAsignaturaCarrera() {
		return idAsignaturaCarrera;
	}
	public void setIdAsignaturaCarrera(Integer idAsignaturaCarrera) {
		this.idAsignaturaCarrera = idAsignaturaCarrera;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String carrera) {
		this.nombre = carrera;
	}
	public Integer getSemestre() {
		return semestre;
	}
	public void setSemestre(Integer semestre) {
		this.semestre = semestre;
	}
	public String getEnfasis() {
		return enfasis;
	}
	public void setEnfasis(String enfasis) {
		this.enfasis = enfasis;
	}
	public String getPlan() {
		return plan;
	}
	public void setPlan(String plan) {
		this.plan = plan;
	}
	public Integer getNivel() {
		return nivel;
	}
	public void setNivel(Integer nivel) {
		this.nivel = nivel;
	}
	public Integer getIdCarrera() {
		return idCarrera;
	}
	public void setIdCarrera(Integer idCarrera) {
		this.idCarrera = idCarrera;
	}
	public Integer getIdAsignatura() {
		return idAsignatura;
	}
	public void setIdAsignatura(Integer idAsignatura) {
		this.idAsignatura = idAsignatura;
	}
	public String getSigla() {
		return sigla;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	public String getTurno() {
		return turno;
	}
	public void setTurno(String turno) {
		this.turno = turno;
	}
	public String getSeccion() {
		return seccion;
	}
	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}
	
}

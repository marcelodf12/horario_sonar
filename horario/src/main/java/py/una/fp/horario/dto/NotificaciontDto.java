package py.una.fp.horario.dto;

public class NotificaciontDto {
	private String profesorTitulo;
	private String profesorNombre;
	private String profesorApellido;
	private Integer profesorId;
	private String profesorCorreo;
	private Integer asignaturaId;
	private String asignaturaNombre;
	private String asignaturaSeccion;
	private String asignaturaTurno;
	private String asignaturaAula;
	private Integer start;
	private Integer end;
	private Integer anho;
	private Integer carreraId;
	private String carreras;
	private String dia;
	private String inicio;
	private String fin;
	private String semestre;
	private String parcial1;
	private String parcial2;
	private String final1;
	private String final2;
	
	public String getProfesorTitulo() {
		return profesorTitulo;
	}
	public void setProfesorTitulo(String profesorTitulo) {
		this.profesorTitulo = profesorTitulo;
	}
	public String getProfesorNombre() {
		return profesorNombre;
	}
	public void setProfesorNombre(String profesorNombre) {
		this.profesorNombre = profesorNombre;
	}
	public String getProfesorApellido() {
		return profesorApellido;
	}
	public void setProfesorApellido(String profesorApellido) {
		this.profesorApellido = profesorApellido;
	}
	public Integer getProfesorId() {
		return profesorId;
	}
	public void setProfesorId(Integer profesorId) {
		this.profesorId = profesorId;
	}
	public String getProfesorCorreo() {
		return profesorCorreo;
	}
	public void setProfesorCorreo(String profesorCorreo) {
		this.profesorCorreo = profesorCorreo;
	}
	public Integer getAsignaturaId() {
		return asignaturaId;
	}
	public void setAsignaturaId(Integer asignaturaId) {
		this.asignaturaId = asignaturaId;
	}
	public String getAsignaturaNombre() {
		return asignaturaNombre;
	}
	public void setAsignaturaNombre(String asignaturaNombre) {
		this.asignaturaNombre = asignaturaNombre;
	}
	public String getAsignaturaSeccion() {
		return asignaturaSeccion;
	}
	public void setAsignaturaSeccion(String asignaturaSeccion) {
		this.asignaturaSeccion = asignaturaSeccion;
	}
	public String getAsignaturaTurno() {
		return asignaturaTurno;
	}
	public void setAsignaturaTurno(String asignaturaTurno) {
		this.asignaturaTurno = asignaturaTurno;
	}
	public String getAsignaturaAula() {
		return asignaturaAula;
	}
	public void setAsignaturaAula(String asignaturaAula) {
		this.asignaturaAula = asignaturaAula;
	}
	public Integer getStart() {
		return start;
	}
	public void setStart(Integer start) {
		this.start = start;
	}
	public Integer getEnd() {
		return end;
	}
	public void setEnd(Integer end) {
		this.end = end;
	}
	public Integer getAnho() {
		return anho;
	}
	public void setAnho(Integer anho) {
		this.anho = anho;
	}
	public String getCarreras() {
		return carreras;
	}
	public void setCarreras(String carreras) {
		this.carreras = carreras;
	}
	public String getDia() {
		return dia;
	}
	public void setDia(String dia) {
		this.dia = dia;
	}
	public String getInicio() {
		return inicio;
	}
	public void setInicio(String inicio) {
		this.inicio = inicio;
	}
	public String getFin() {
		return fin;
	}
	public void setFin(String fin) {
		this.fin = fin;
	}
	public String getSemestre() {
		return semestre;
	}
	public void setSemestre(String semestre) {
		this.semestre = semestre;
	}
	public String getParcial1() {
		return parcial1;
	}
	public void setParcial1(String parcial1) {
		this.parcial1 = parcial1;
	}
	public String getParcial2() {
		return parcial2;
	}
	public void setParcial2(String parcial2) {
		this.parcial2 = parcial2;
	}
	public String getFinal1() {
		return final1;
	}
	public void setFinal1(String final1) {
		this.final1 = final1;
	}
	public String getFinal2() {
		return final2;
	}
	public void setFinal2(String final2) {
		this.final2 = final2;
	}
	public Integer getCarreraId() {
		return carreraId;
	}
	public void setCarreraId(Integer carreraId) {
		this.carreraId = carreraId;
	}
	
	
}

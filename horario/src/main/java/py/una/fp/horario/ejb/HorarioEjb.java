package py.una.fp.horario.ejb;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.ListIterator;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import py.una.fp.horario.bd.DataBaseConfiguration;
import py.una.fp.horario.dto.AsignaturaDto;
import py.una.fp.horario.dto.AulaAgrupacionStringDto;
import py.una.fp.horario.dto.AulaDto;
import py.una.fp.horario.dto.BorradorDto;
import py.una.fp.horario.dto.ConflictosDto;
import py.una.fp.horario.dto.DivisionDto;
import py.una.fp.horario.dto.FilaHorarioDto;
import py.una.fp.horario.dto.HorarioDto;
import py.una.fp.horario.dto.ProfesorDto;
import py.una.fp.horario.parametros.Horario;
import py.una.fp.horario.parametros.Seccion;
import py.una.fp.horario.parametros.Slot;
import py.una.fp.horario.util.Configuracion;
import py.una.fp.horario.util.Mensaje;
import py.una.fp.horario.util.Respuesta;

@Stateless
public class HorarioEjb {
	@EJB
	DataBaseConfiguration dbc;
	
	@EJB
	Configuracion conf;
	
	@EJB
	NotificacionesEjb notificacionesEjb;
	
	Logger log = Logger.getLogger(HorarioEjb.class);
	
	public void generarBorrador(Integer periodo){
		
	}
	
	public ArrayList<FilaHorarioDto> getHorario(Integer idHorario){
		SqlSession session = dbc.getSession();
		HashMap<String, Object> parameters = new HashMap<>();
		parameters.put("id_horario", idHorario);
		List<FilaHorarioDto> list = null;
		if(idHorario !=null)
			list = session.selectList("py.una.fp.mapper.Horario.getHorario", parameters);
		else
			list = session.selectList("py.una.fp.mapper.Horario.getUltimoHorario", parameters);
		session.close();
		if(list.size()==0)
			return null;
		return (ArrayList<FilaHorarioDto>) list;	
	};
	
	public Long getVersion(){
		SqlSession session = dbc.getSession();
		List<Date> list = session.selectList("py.una.fp.mapper.Horario.getVersion");
		session.close();
		if(list.size()==0)
			return null;
		return list.get(0).getTime();	
	};
	
	public ArrayList<HorarioDto> listHorarios(Integer anho){
		SqlSession session = dbc.getSession();
		HashMap<String, Object> parameters = new HashMap<>();
		parameters.put("anho", anho);
		List<HorarioDto> list = session.selectList("py.una.fp.mapper.Horario.getHorarios", parameters);
		session.close();
		if(list.size()==0)
			return null;
		return (ArrayList<HorarioDto>) list;	
	};
	
	public ArrayList<HorarioDto> listPapelera(){
		SqlSession session = dbc.getSession();
		HashMap<String, Object> parameters = new HashMap<>();
		List<HorarioDto> list = session.selectList("py.una.fp.mapper.Horario.getPapelera", parameters);
		session.close();
		if(list.size()==0)
			return null;
		return (ArrayList<HorarioDto>) list;	
	};
	
	public ArrayList<BorradorDto> listBorrador(Integer id, String dpto, String carrera, String profesor, String asignatura, Boolean order, String column, Integer offset){
		SqlSession session = dbc.getSession();
		HashMap<String, Object> parameters = new HashMap<>();
		if(profesor!=null)
			profesor = profesor.replace(' ', '|');
		parameters.put("id", id);
		parameters.put("dpto", dpto);
		parameters.put("carrera", carrera);
		parameters.put("asignatura", asignatura);
		parameters.put("profesor", profesor);
		parameters.put("offset", offset);
		parameters.put("order", order);
		parameters.put("column", column);
		List<BorradorDto> list = session.selectList("py.una.fp.mapper.Horario.getBorrador", parameters);
		session.close();
		if(list.size()==0)
			return null;
		return (ArrayList<BorradorDto>) list;
	}
	
	public Boolean updateBorrador(Integer id, Boolean s, Boolean d) {
		SqlSession session = dbc.getSession();
		boolean success = true;
		try{
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("id", id);
			parameters.put("selected", s);
			parameters.put("def", d);
			session.selectList("py.una.fp.mapper.Horario.actualizarBorrador", parameters);
		}catch(Exception e){
			success = false;
			log.error("Failed! ",e);
		}finally {
			session.close();
		}
		return success;	
	}
	
	public Boolean eliminar(Integer id) {
		SqlSession session = dbc.getSession();
		boolean success = true;
		try{
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("id", id);
			session.selectList("py.una.fp.mapper.Horario.delete", parameters);
		}catch(Exception e){
			success = false;
			log.error("Failed! ",e);
		}finally {
			session.close();
		}
		return success;	
	}
	
	public Boolean nuevoHorario(Integer periodo, Integer anho){
		SqlSession session = dbc.getSession();
		boolean success = true;
		try{
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("periodo", periodo);
			parameters.put("anho", anho);
			session.selectList("py.una.fp.mapper.Horario.nuevoBorrador", parameters);
		}catch(Exception e){
			success = false;
			log.error("Failed! ",e);
		}finally {
			session.close();
		}
		return success;			
	}
	
	public ArrayList<ConflictosDto> calcularConflictos(Integer id){
		List<ConflictosDto> conflictos = null;
		try{
			SqlSession session = dbc.getSession();
			HashMap<String, Object> parameters = new HashMap<>();
			parameters = new HashMap<>();
			parameters.put("idHorario", id);
			conflictos = session.selectList("py.una.fp.mapper.Horario.getConflictosHorario", parameters);
			if(conflictos== null)
				return new ArrayList<ConflictosDto>();
		}catch(Exception e){
			log.error(e.getCause());
			e.printStackTrace();
			return null;
		}
		return (ArrayList<ConflictosDto>) conflictos;
	}
	
	public Horario toHorarioFormat(Integer id, List<Integer> aRecalcular){
		SqlSession session = dbc.getSession();
		HashMap<String, Object> parameters = new HashMap<>();
		parameters.put("idHorario", id);
		List<AsignaturaDto> asignaturas;
		if(aRecalcular==null || aRecalcular.isEmpty()){
			asignaturas = session.selectList("py.una.fp.mapper.Asignatura.getByBorrador", parameters);
		}else{
			log.debug(aRecalcular.size());
			parameters.put("aRecalcular", aRecalcular);
			asignaturas = session.selectList("py.una.fp.mapper.Asignatura.getRecalcular", parameters);
		}
		log.debug(new Gson().toJson(asignaturas));
		Horario h = new Horario();
		h.setSecciones(new ArrayList<Seccion>());
		Integer idDiv=0;
		for(AsignaturaDto asig: asignaturas){ // Aca se itera por las asignaturas
			parameters = new HashMap<>();
			parameters.put("id", asig.getIdAsignatura());
			log.debug(asig.getAsignatura());//getIdProfesorByAsignatura
			List<DivisionDto> divisiones = session.selectList("py.una.fp.mapper.Asignatura.getDivisiones", parameters);
			List<Integer> profes = session.selectList("py.una.fp.mapper.Asignatura.getIdProfesorByAsignatura", parameters);
			int [] idProfesores = new int[profes.size()];
		    int u = 0;
		    for (Integer e : profes)  
		    	idProfesores[u++] = e.intValue();
		    int count = 0;
			for(DivisionDto d: divisiones){ // Iteracion por division
					// Seccion es sinonimo de division.
					Seccion sec = new Seccion();
					sec.idDivision = idDiv++;
					sec.idDivisionDB = d.getIdDivision();
					sec.slots = new ArrayList<Slot>();
					sec.idProfesores = idProfesores;
					sec.idSeccion = asig.getIdAsignatura();
					sec.duracion = d.getDuracion();
					sec.tipoAula = d.getTipoAula();
					sec.auxGrupo = count++;//Este sirve para agrupar una division con una sola div de la otra asignatura
					sec.modo = d.getModo();
					sec.hora = d.getHora();
					

					//--------- ASIGNACION DE SLOTS ----------//
					if(asig.getSlots()== null || asig.getSlots().compareTo("")==0)
						asig.setSlots("");
					sec.slot_seccion = new int[Configuracion.MAX_SLOT];
					if(sec.modo.trim().toUpperCase().compareTo("F")==0){
						for(int i=(sec.hora.intValue()-1); i<(sec.hora.intValue()+sec.duracion-1); i++)
							sec.slot_seccion[i] = 1;
					}else{
						sec.slot_seccion = stringToSlot(asig.getSlots(), sec.slot_seccion);
					}
					//---------------------------------------------------------//
					
					h.getSecciones().add(sec);

			}// FIN DE ITERACION POR DIVISION
		}// FIN DE ITERACION POR SECCION
		int divisionesEstaticas = idDiv;
		if(aRecalcular!=null && !aRecalcular.isEmpty()){
			parameters.put("idHorario", id);
			parameters.put("aRecalcular", aRecalcular);
			List<Seccion> seccionesFijas = session.selectList("py.una.fp.mapper.Asignatura.getDivisionesFijas", parameters);
			log.debug(new Gson().toJson(seccionesFijas));
			for(Seccion s: seccionesFijas){
				s.slot_seccion = new int[Configuracion.MAX_SLOT];
				s.slots = new ArrayList<Slot>();
				for(int i=(s.hora.intValue()-1); i<(s.hora.intValue()+s.duracion-1); i++)
					s.slot_seccion[i] = 1;
				s.idDivision = idDiv++;
				h.getSecciones().add(s);
			}
		}
		
		parameters = new HashMap<>();
		for(Seccion sec1: h.getSecciones()){
			parameters.put("id", sec1.idSeccion);
			parameters.put("coiciden", false);
			List<AsignaturaDto> ex = session.selectList("py.una.fp.mapper.Asignatura.getAllRelaciones", parameters);
			parameters.put("coiciden", true);
			List<AsignaturaDto> grupo = session.selectList("py.una.fp.mapper.Asignatura.getAllRelaciones", parameters);
			sec1.conflictos = new ArrayList<Integer>();
			sec1.combinar = new ArrayList<Integer>();
			for(Seccion sec2: h.getSecciones()){
				if(sec1.idDivision<sec2.idDivision){
					
						boolean esGrupo = existeAsigEnArray(sec2, grupo);
						
						// idSeccion es idAsignatura - Si dos divisiones pertenecen a la misma
						// asignatura, entonces tienen un conflicto
						if(sec1.idSeccion == sec2.idSeccion){
							sec1.conflictos.add(sec2.idDivision);
						}else{
							boolean esConflicto = existeAsigEnArray(sec2, ex);
							// Divisiones con conflicto
							if(esConflicto){
								sec1.conflictos.add(sec2.idDivision);
							}
							if(sec1.idProfesores!=null && sec2.idProfesores!=null){
								boolean coicidenProf = false;
								for(int p1 : sec1.idProfesores){
									for(int p2 :sec2.idProfesores){
										if(p1==p2){
											coicidenProf = true;
											break;
										}
									}
									if(coicidenProf)
										break;
								}
								// Si no es un grupo, no es la misma asignatura y es el mismo profesor.
								if(!esGrupo && coicidenProf){
									sec1.conflictos.add(sec2.idDivision);
								}
							}
						}
					
						// grupo
						if(esGrupo && sec1.auxGrupo == sec2.auxGrupo && sec1.idDivision<=divisionesEstaticas){
							sec1.combinar.add(sec2.idDivision);
						}
						
						
					}
			}
		}
		List<ProfesorDto> profesores = null;
		profesores = session.selectList("py.una.fp.mapper.Profesor.getAllProfe");
		for(ProfesorDto p: profesores){
			if(p.getSlot()== null || p.getSlot().compareTo("")==0)
				p.setSlot("85-267;373-555;661-843;949-1131;1237-1419;1525-1656");
			int [] slot_profesor = new int[Configuracion.MAX_SLOT];
			slot_profesor = stringToSlot(p.getSlot(), slot_profesor);
			for(Seccion sec: h.getSecciones()){
				if(sec.idProfesores!=null)
				for(int p1 : sec.idProfesores)
					if(p1==p.getIdProfesor()){
						for(int k = 0; k<sec.slot_seccion.length; k++){
							// Slot_Division AND Slot_Profesor
							sec.slot_seccion[k]= sec.slot_seccion[k]*slot_profesor[k];
						}
					}
			}
		}
		
		for(Seccion sec: h.getSecciones()){
			int ultimoValor = 0;
			Slot slot=null;
			for(int k = 0; k<sec.slot_seccion.length; k++){
				if(ultimoValor == 0 && sec.slot_seccion[k]==1){
					slot = new Slot();
					slot.start = k+1;
				}
				if(ultimoValor == 1 && sec.slot_seccion[k]==0){
					slot.end = k-sec.duracion+1;
					if(slot.end>=slot.start)
						sec.slots.add(slot);
				}
				if(ultimoValor == 1 && k==sec.slot_seccion.length-1){
					slot.end = k-sec.duracion+1;
					if(slot.end>=slot.start)
						sec.slots.add(slot);
				}
				ultimoValor = sec.slot_seccion[k];
			}
		}
		

		session.close();
		log.debug("CANTIDAD DE SECCIONES EXTRAIDAS: "+h.getSecciones().size());
		return h;
	}
	
	private boolean existeAsigEnArray(Seccion s, List<AsignaturaDto> array){
		for(AsignaturaDto a : array){
			if(s.idSeccion == a.getIdAsignatura().intValue())
				return true;
		}
		return false;
	}
	
	
	public String calcular(Integer id, String algoritmo, ArrayList<Integer> aRecalcular) throws IOException{
		
		Horario h=null;
		Gson gson = new Gson();
		String estadoNuevo = "E";

		if(algoritmo.compareTo("AG")==0 || algoritmo.compareTo("CH")==0){
			h = this.toHorarioFormat(id, aRecalcular);
			ListIterator<Seccion> listIterator = h.getSecciones().listIterator();
			Hashtable<Integer, Integer> numbers = new Hashtable<Integer, Integer>();
			ArrayList<Integer> eliminados = new ArrayList<Integer>();
			log.debug("Descartar secciones sin slots");
			while(listIterator.hasNext()){
				Seccion c = (Seccion) listIterator.next();
				if(c.slots.size()<1){
					numbers.put(c.idSeccion, c.idSeccion);
					log.debug(c.idSeccion);
					//listIterator.remove();
				}
			}
			Enumeration<Integer> llaves = numbers.keys();
			while (llaves.hasMoreElements()) {
			  eliminados.add(llaves.nextElement());
			}
			log.debug("------------------------------");
			
			if(!eliminados.isEmpty()){
				Respuesta<List<AsignaturaDto>> r = new Respuesta<List<AsignaturaDto>>();
				SqlSession session = dbc.getSession();
				HashMap<String, Object> parameters = new HashMap<>();
				parameters.put("list", eliminados);
				List<AsignaturaDto> list = session.selectList("py.una.fp.mapper.Asignatura.getAsignaturasByList",parameters);
				r.setData(list);
				r.setSuccess(false);
				r.setMessage("Debe revisar las condiciones de las asignaturas y profesores.");
				r.setType("error");
				r.setReason("Existen asignaturas imposibles de ubicar");
				return gson.toJson(r);
			}
		}else if(algoritmo.compareTo("CA")==0){
			h = this.toAulaFormat(id);
			estadoNuevo = "C";
			if(h==null){
				Respuesta<Object> r = new Respuesta<Object>();
				r.setSuccess(false);
				r.setMessage("Favor verificar que todas las asignaturas tengan asignado un tamaño de aula requerida");
				r.setType("error");
				r.setReason("Existen asignaturas sin tamaño de aula requerida");
				return gson.toJson(r);
			}
		}else{
			Respuesta<Object> r = new Respuesta<Object>();
			r.setSuccess(false);
			r.setMessage("No existe el algoritmo " + algoritmo);
			r.setType("error");
			r.setReason("Verifique los parametros de entrada.");
			return gson.toJson(r);
		}
		
		
		h.setTimeMaxMin(new Integer(conf.getValor("time_max")));
		if(aRecalcular!=null && aRecalcular.size()>0)
			h.setTimeMaxMin(Math.round(aRecalcular.size()*aRecalcular.size()/10+10));

		Date now = new Date();
		String salt = ((long)(now.getTime()/1000000))+"";
		log.debug("salt cliente: "+salt);
		String apiKey = conf.getValor("api_key");
		String token = DigestUtils.sha256Hex(apiKey + salt);
		String idClient = conf.getValor("id_client");
		String ip = conf.getValor("core_ip");
		String port = conf.getValor("core_port");
		String contextRoot = conf.getValor("context_root");
		String uri ="http://"+ip+":"+port+"/"+contextRoot+"/rest/calc/horario/"+idClient+"/"+token+"/"+id+"/"+algoritmo;
        
		log.debug(uri);
		
		
		String horarioJson = gson.toJson(h);
		log.debug(horarioJson);
		byte[] horarioByte = horarioJson.getBytes();
		
        URL url = new URL(uri);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("content-type", "text/json; charset=UTF-8");
        connection.setDoOutput(true);
        
        connection.setRequestProperty("Content-Length", Integer.toString(horarioByte.length));
        connection.getOutputStream().write(horarioByte);
        connection.getOutputStream().flush();
        
        if (connection.getResponseCode() != 200) {
        	throw new RuntimeException("Failed : HTTP error code : "
        	    + connection.getResponseCode());
        	}
        SqlSession session = dbc.getSession();
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put("estado", estadoNuevo);
		parameters.put("id", id);
		session.update("py.una.fp.mapper.Horario.cambiarEstado", parameters);
        
        BufferedReader br = new BufferedReader(new InputStreamReader(
        	    (connection.getInputStream())));
        String output;
        String response = "";
        while ((output = br.readLine()) != null) {
        	response += output;
        }
        log.debug(response);
        Respuesta<Integer> r = gson.fromJson(response, new TypeToken<Respuesta<Integer>>(){}.getType());
        parameters.put("idProcess", r.getData());
        session.update("py.una.fp.mapper.Horario.setProcess", parameters);
        session.close();
        log.debug(response);
        return response;
	};
	
	private Horario toAulaFormat(Integer id) {
		Gson g = new Gson();
		SqlSession session = null;
		Horario h = null;
		try {
			session = dbc.getSession();
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("idHorario", id);
			List<AulaAgrupacionStringDto> agrupacion = session.selectList("py.una.fp.mapper.AgrupacionAulas.selectAgrupacionAulaConSlots", parameters);
			log.debug("Recuperados " + agrupacion.size() + " agrupaciones");
			for(AulaAgrupacionStringDto a: agrupacion){
				log.debug(g.toJson(a));
				if(a.cantidadAlumnos==null || a.cantidadAlumnos<=0)
					return null;
			}
			h = new Horario();
			h.setAgrupacion(agrupacion);
			List<AulaDto> aulas = session.selectList("py.una.fp.mapper.Aula.getAllAulas", parameters);
			log.debug("Recuperados " + aulas.size() + " aulas");
			h.setAulas(aulas);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if(session!=null)
				session.close();
		}
		return h;
		
	}

	public static int[] stringToSlot(String slot, int[] r){
		
		String[] slots = slot.split(";");
		for(String s: slots){
			String[] limites = s.split("-");
			Integer inicio = new Integer(limites[0]) - 1;
			Integer fin = new Integer(limites[1]);
			for(int n=inicio; n<fin;n++){
				r[n]=1;
			}
		}
		return r;
	}

	public Respuesta<Object> publicar(Integer idHorario) {
		SqlSession session = null;
		String mensaje=Mensaje.PUBLICAR_HORARIO_EXITOSO;
		Boolean success = true;
		String reason = Mensaje.INFO;
		String type = "success";
		Respuesta<Object> r;
		try {
			session = dbc.getSession();
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("id", idHorario);
			session.update("py.una.fp.mapper.Horario.publicar", parameters);
			notificacionesEjb.notificar(idHorario);
			log.info("Lanzado el proceso de Notificación");
		} catch (Exception e) {
			mensaje=Mensaje.EDICION_HORARIO_NO_EXITOSO;
			success = false;
			reason = Mensaje.ERROR;
			type = "error";
		} finally {
			if(session!=null)
				session.close();
		}
		r = new Respuesta<Object>(success,mensaje,reason,type);
		return r;
	};
	
	public Respuesta<Object> restaurar(Integer idHorario) {
		SqlSession session = null;
		String mensaje=Mensaje.EDICION_HORARIO_EXITOSO;
		Boolean success = true;
		String reason = Mensaje.INFO;
		String type = "success";
		Respuesta<Object> r;
		try {
			session = dbc.getSession();
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("id", idHorario);
			session.update("py.una.fp.mapper.Horario.restaurar", parameters);
		} catch (Exception e) {
			mensaje=Mensaje.EDICION_HORARIO_NO_EXITOSO;
			success = false;
			reason = Mensaje.ERROR;
			type = "error";
		} finally {
			if(session!=null)
				session.close();
		}
		r = new Respuesta<Object>(success,mensaje,reason,type);
		return r;
	};
	
	public Respuesta<Object> descartar(Integer idHorario) {
		SqlSession session = null;
		String mensaje=Mensaje.EDICION_HORARIO_EXITOSO;
		Boolean success = true;
		String reason = Mensaje.INFO;
		String type = "success";
		Respuesta<Object> r;
		try {
			session = dbc.getSession();
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("id", idHorario);
			session.update("py.una.fp.mapper.Horario.descartar", parameters);
		} catch (Exception e) {
			mensaje=Mensaje.EDICION_HORARIO_NO_EXITOSO;
			success = false;
			reason = Mensaje.ERROR;
			type = "error";
		} finally {
			if(session!=null)
				session.close();
		}
		r = new Respuesta<Object>(success,mensaje,reason,type);
		return r;
	};
	
	public ArrayList<FilaHorarioDto> getHorarioResultado(Integer idHorario){
		SqlSession session = dbc.getSession();
		HashMap<String, Object> parameters = new HashMap<>();
		parameters.put("id_horario", idHorario);
		List<FilaHorarioDto> list = null;
		if(idHorario !=null)
			list = session.selectList("py.una.fp.mapper.Horario.getHorarioResultado", parameters);
		else
			return null;
		session.close();
		if(list.size()==0)
			return null;
		return (ArrayList<FilaHorarioDto>) list;	
	};
	
	public Boolean updateExamen(FilaHorarioDto f, Integer idHorarioResultado){
		if(f==null)
			return false;
		SqlSession session = dbc.getSession();
		HashMap<String, Object> parameters = new HashMap<>();
		parameters.put("f1", f.getF1());
		parameters.put("f2", f.getF2());
		parameters.put("p1", f.getP1());
		parameters.put("p2", f.getP2());
		parameters.put("id", idHorarioResultado);
		try {
			session.update("py.una.fp.mapper.Horario.updateExamen", parameters);
		} catch (Exception e) {
			log.error(e.getMessage());
			return false;
		}
		return true;
	}
	
	public Boolean updateCantidadAlumnos(FilaHorarioDto f){
		if(f==null)
			return false;
		SqlSession session = dbc.getSession();
		HashMap<String, Object> parameters = new HashMap<>();
		parameters.put("ca", f.getCa());
		parameters.put("id", new Integer(f.getId()));
		try {
			session.update("py.una.fp.mapper.Horario.updateCa", parameters);
		} catch (Exception e) {
			log.error(e.getMessage());
			return false;
		}
		return true;
	}


}

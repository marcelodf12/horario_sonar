package py.una.fp.horario.dto;

public class AulaAgrupacionStringDto {
	
		public Integer id;
		public String start;
		public String end;
		public String profesor;
		public Integer cantidadAlumnos;
		public Integer tipoAula;
		public String grupo;
		public Integer idHorario;

}

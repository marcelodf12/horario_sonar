package py.una.fp.horario.ejb;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import py.una.fp.horario.bd.DataBaseConfiguration;
import py.una.fp.horario.dto.AsignaturaDto;
import py.una.fp.horario.dto.PerfilDto;
import py.una.fp.horario.dto.UsuarioDto;
import py.una.fp.horario.dto.UsuarioPerfilDto;

@Stateless
public class UsuarioEjb {

	@EJB
	DataBaseConfiguration dbc;

	Logger log = Logger.getLogger(UsuarioEjb.class);
	
	public ArrayList<UsuarioDto>getAll(String nombre, String column, Boolean order, Integer offset){
		SqlSession session = dbc.getSession();
		HashMap<String, Object> parameters = new HashMap<>();
		parameters.put("nombre", nombre);
		parameters.put("column", column);
		parameters.put("order", order);
		parameters.put("offset", offset);
		List<UsuarioDto> list = session.selectList("py.una.fp.mapper.Usuario.getAll", parameters);
		session.close();
		if(list.size()==0)
			return null;		
		return (ArrayList<UsuarioDto>) list;
	}

	public String login(String nombreUser, String password) {
		SqlSession session = dbc.getSession();
		HashMap<String, Object> parameters = new HashMap<>();
		parameters.put("user", nombreUser);
		List<UsuarioDto> list = session.selectList("py.una.fp.mapper.Usuario.getByName", parameters);
		if (list != null)
			if (!list.isEmpty()) {
				UsuarioDto user = list.get(0);
				parameters.put("id", user.getIdUsuario());
				log.debug("expirado=" + isBloqueado(user.getLastLogin()));
				if(user.getIntentosFallidos()<3 || !isBloqueado(user.getLastLogin())){
					String sha256hex = list.get(0).getPassword();
					log.debug(sha256hex);
					if (sha256hex.compareTo(password) == 0) {
						Double random = new Random().nextDouble();
						String token = DigestUtils.sha256Hex(random.toString());
						parameters.put("token", token);
						session.selectList("py.una.fp.mapper.Usuario.setToken", parameters);
						return token + ";" + user.getIdProfesor() + ";" + user.getIdUsuario();
					}
				}
				session.selectList("py.una.fp.mapper.Usuario.errorLogin", parameters);
			}
		session.close();
		return "ERROR";
	}
	public ArrayList<PerfilDto> getPefilByToken(String token) {
		SqlSession session = dbc.getSession();
		HashMap<String, Object> parameters = new HashMap<>();
		parameters.put("token", token);
		List<PerfilDto> list = session.selectList("py.una.fp.mapper.Usuario.getPerfilByToken", parameters);
		session.close();
		if(list!=null && list.size()==0)
			return null;
		return (ArrayList<PerfilDto>) list;
	}
	
	public boolean cambiarPassword(Integer id, String nombreUser, String password, String passwordNuevo){
				
		SqlSession session = dbc.getSession();
		HashMap<String, Object> parameters = new HashMap<>();
		parameters.put("user", nombreUser);
		List<UsuarioDto> list = session.selectList("py.una.fp.mapper.Usuario.getByName", parameters);
		if (list != null){
			if (!list.isEmpty()) {
				UsuarioDto user = list.get(0);
				String sha256hex = user.getPassword();
				
				if (sha256hex.compareTo(password) == 0) {
					parameters.put("id", user.getIdUsuario());
					parameters.put("password", passwordNuevo);
					session.selectList("py.una.fp.mapper.Usuario.updatePassword", parameters);
					return true;
				} else {
					System.out.println("pass no coincide..");
				}
			}
		}
		session.close();
		
		return false;
	}

	private boolean isBloqueado(Date lastLogin) {
		if(lastLogin==null)
			return true;
		Date now = new Date();
		System.out.println(now);
		System.out.println(lastLogin);
		return (lastLogin.getTime()+300000) > now.getTime();

	}
	
	public boolean create(UsuarioDto u){
		SqlSession session = dbc.getSession();
		u.setPassword("8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918");
		u.setExpirate(new Date());
		boolean success = true;
		try{
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("nombre", u.getNombre());
			parameters.put("id_profesor", u.getIdProfesor());
			parameters.put("nombre_profesor", u.getNombreProfesor());
			parameters.put("password", u.getPassword());
			parameters.put("expirate", u.getExpirate());
			session.selectList("py.una.fp.mapper.Usuario.insert", parameters);
		}catch(Exception e){
			success = false;
			log.error("Failed! ",e);
		}finally {
			session.close();
		}
		return success;
	}

	public ArrayList<PerfilDto> getPerfiles(Integer idProfesor) {
		SqlSession session = dbc.getSession();
		HashMap<String, Object> parameters = new HashMap<>();
		parameters.put("idProfesor", idProfesor);
		List<PerfilDto> list = session.selectList("py.una.fp.mapper.Usuario.getPerfilesUsuarioProfesor", parameters);
		session.close();		
		return (ArrayList<PerfilDto>) list;
	}
	
	public ArrayList<PerfilDto> getPermisos() {
		SqlSession session = dbc.getSession();
		List<PerfilDto> list = session.selectList("py.una.fp.mapper.Usuario.getAllPermisos");
		session.close();
		if(list.size()==0)
			return null;		
		return (ArrayList<PerfilDto>) list;
	}
	
	public boolean deletePermiso(Integer idUsuario, Integer idPerfil){
		SqlSession session = dbc.getSession();
		boolean success = true;
		try{
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("idPerfil", idPerfil);
			parameters.put("idUsuario", idUsuario);
			session.selectList("py.una.fp.mapper.Usuario.deleteUsuarioPerfil", parameters);
		}catch(Exception e){
			success = false;
			log.error("Failed! ",e);
		}finally {
			session.close();
		}
		return success;
	}

	public Boolean createUsuarioPerfil(UsuarioPerfilDto permiso) {

		SqlSession session = dbc.getSession();
		boolean success = true;
		try{
				
			HashMap<String, Object> parameters1 = new HashMap<>();
			parameters1.put("idProfesor", permiso.getIdUsuario());
			List<UsuarioDto> list = session.selectList("py.una.fp.mapper.Usuario.getUsuarioPorIdProfesor", parameters1);
			
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("idUsuario", list.get(0).getIdUsuario());
			parameters.put("idPerfil", permiso.getIdPerfil());
			session.selectList("py.una.fp.mapper.Usuario.insertUsuarioPerfil", parameters);
		}catch(Exception e){
			success = false;
			log.error("Failed! ",e);
		}finally {
			session.close();
		}
		return success;

	}
	
	
	public boolean getLoginExistente(String loginNombre){
		SqlSession session = dbc.getSession();
		HashMap<String, Object> parameters = new HashMap<>();
		parameters.put("nombre", loginNombre);
		
		List<UsuarioDto> list = session.selectList("py.una.fp.mapper.Usuario.getLoginExistente", parameters);
		session.close();
		boolean existe = false;
		if(list.size()>0)
			existe = true;		
		return existe;
	}
	

}

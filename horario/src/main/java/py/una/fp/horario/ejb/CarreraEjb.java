package py.una.fp.horario.ejb;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import py.una.fp.horario.bd.DataBaseConfiguration;
import py.una.fp.horario.dto.CarreraDto;

@Stateless
public class CarreraEjb {
	
	@EJB
	DataBaseConfiguration dbc;
	
	Logger log = Logger.getLogger(CarreraEjb.class);
	
	public ArrayList<CarreraDto>getAll(String nombre, String sigla, Integer offset, String column, Boolean order){
		SqlSession session = dbc.getSession();
		HashMap<String, Object> parameters = new HashMap<>();
		parameters.put("nombre", nombre);
		parameters.put("sigla", sigla);
		parameters.put("offset", offset);
		parameters.put("column", column);
		parameters.put("order", order);
		List<CarreraDto> list = session.selectList("py.una.fp.mapper.Carrera.getAll", parameters);
		session.close();
		if(list.size()==0)
			return null;
		return (ArrayList<CarreraDto>) list;
	}

	public CarreraDto get(Integer idCarrera) {
		SqlSession session = dbc.getSession();
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("idCarrera", idCarrera);
			List<CarreraDto> list = session.selectList("py.una.fp.mapper.Carrera.getById", parameters);
			session.close();
			if(list==null)
				return null;
			CarreraDto c = list.get(0);
			return c;
	}
	
	public boolean delete(Integer id){
		SqlSession session = dbc.getSession();
		boolean success = true;
		try{
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("id", id);
			session.selectList("py.una.fp.mapper.Carrera.delete", parameters);
		}catch(Exception e){
			success = false;
			log.error("Failed! ",e);
		}finally {
			session.close();
		}
		return success;
	}
	
	public boolean create(CarreraDto carrera){
		SqlSession session = dbc.getSession();
		boolean success = true;
		try{
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("nombre", carrera.getNombre());
			parameters.put("sigla", carrera.getSigla());
			parameters.put("cod_car_sec", carrera.getCodCarSec());
			session.selectList("py.una.fp.mapper.Carrera.insert", parameters);
		}catch(Exception e){
			success = false;
			log.error("Failed! ",e);
		}finally {
			session.close();
		}
		return success;
	}
	
	public boolean edit(CarreraDto carrera){
		SqlSession session = dbc.getSession();
		boolean success = true;
		try{
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("id", carrera.getIdCarrera());
			parameters.put("nombre", carrera.getNombre());
			parameters.put("sigla", carrera.getSigla());
			parameters.put("cod_car_sec", carrera.getCodCarSec());
			session.selectList("py.una.fp.mapper.Carrera.update", parameters);
		}catch(Exception e){
			success = false;
			log.error("Failed! ",e);
		}finally {
			session.close();
		}
		return success;
	}

	public List<CarreraDto> getAllAll() {
		SqlSession session = dbc.getSession();
		List<CarreraDto> list = session.selectList("py.una.fp.mapper.Carrera.getAllAll");
		session.close();
		if(list.size()==0)
			return null;
		return (ArrayList<CarreraDto>) list;
	}

	
}

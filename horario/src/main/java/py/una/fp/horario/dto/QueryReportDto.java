package py.una.fp.horario.dto;

import java.util.List;

import com.google.gson.Gson;

public class QueryReportDto {

	private List<ElementsDto> elements;
	
	private List<ElementsDto> profesores;
	
	private List<ElementsDto> aulas;
	
	private List<ElementsDto> carreras;
	
	private String identificador;
	
	private String prefijo;
	
	private String ruta;
	
	public QueryReportDto() {
		super();
	}
	
	public QueryReportDto(String s) {
		Gson g = new Gson();
		QueryReportDto obj = g.fromJson(s, QueryReportDto.class);
		this.elements = obj.getElements();
		this.profesores = obj.getProfesores();
		this.aulas = obj.getAulas();
		this.carreras = obj.getCarreras();
		this.identificador = obj.getIdentificador();
		this.ruta = obj.getRuta();
		this.prefijo = obj.getPrefijo();
	}

	public List<ElementsDto> getElements() {
		return elements;
	}

	public void setElements(List<ElementsDto> elements) {
		this.elements = elements;
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public String getPrefijo() {
		return prefijo;
	}

	public void setPrefijo(String prefijo) {
		this.prefijo = prefijo;
	}

	public String getRuta() {
		return ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	public List<ElementsDto> getProfesores() {
		return profesores;
	}

	public void setProfesores(List<ElementsDto> profesores) {
		this.profesores = profesores;
	}

	public List<ElementsDto> getAulas() {
		return aulas;
	}

	public void setAulas(List<ElementsDto> aulas) {
		this.aulas = aulas;
	}

	public List<ElementsDto> getCarreras() {
		return carreras;
	}

	public void setCarreras(List<ElementsDto> carreras) {
		this.carreras = carreras;
	}
	
	
}

/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package py.una.fp.horario.rest;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * A class extending {@link Application} and annotated with @ApplicationPath is the Java EE 7 "no XML" approach to activating
 * JAX-RS.
 * 
 * <p>
 * Resources are served relative to the servlet path specified in the {@link ApplicationPath} annotation.
 * </p>
 */
@ApplicationPath("/rest")
public class JaxRsActivator extends Application {
    @Override
    public Set<Class<?>> getClasses() {
    	final Set<Class<?>> resources = new HashSet<Class<?>>();

        resources.add(AsignaturaRest.class);
        resources.add(AulasRest.class);
        resources.add(AutenticacionRest.class);
        resources.add(CarrerasRest.class);
        resources.add(CORSFilter.class);
        resources.add(DepartamentoRest.class);
        resources.add(ExamenRest.class);
        resources.add(FileRest.class);
        resources.add(HorarioRest.class);
        resources.add(ParametrosRest.class);
        resources.add(ProfesorRest.class);
        resources.add(ReportesRest.class);
        resources.add(UsuarioRest.class);
        

        resources.add(io.swagger.jaxrs.listing.ApiListingResource.class);
        resources.add(io.swagger.jaxrs.listing.SwaggerSerializers.class);

        return resources;
    }
	 
}

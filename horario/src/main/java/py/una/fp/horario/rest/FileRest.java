package py.una.fp.horario.rest;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import io.swagger.annotations.Api;
import py.una.fp.horario.ejb.AulaEjb;
import py.una.fp.horario.ejb.ImportEjb;
import py.una.fp.horario.util.Configuracion;
import py.una.fp.horario.util.Mensaje;
import py.una.fp.horario.util.Respuesta;

@Stateless
@Path("/file/upload")
public class FileRest {
	
	@EJB
	Configuracion conf;
	
	@EJB
	AulaEjb aulaEjb;
	
	@EJB
	ImportEjb importEjb;
	
	Logger log = Logger.getLogger(FileRest.class);

	@POST
	@Path("/asignaturas/conflictos/")
	@Consumes("multipart/form-data")
	@Produces(MediaType.APPLICATION_JSON)
	public Respuesta<String> uploadFileAsignaturas(MultipartFormDataInput input) {

		String mensaje=Mensaje.UPDATE_ASIG_EXITOSO;
		String reason = Mensaje.INFO;
		String type = "success";
		String data = "";
		Boolean success = true;
		
		String fileName = "";

		Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
		List<InputPart> inputParts = uploadForm.get("uploadedFile");

		for (InputPart inputPart : inputParts) {

		 try {

			MultivaluedMap<String, String> header = inputPart.getHeaders();
			fileName = getFileName(header);
			data = fileName;
			mensaje += fileName;
			//convert the uploaded file to inputstream
			InputStream inputStream = inputPart.getBody(InputStream.class,null);

			byte [] bytes = IOUtils.toByteArray(inputStream);
			
			boolean exito = importEjb.actualizarPorArchivo(bytes);
			if(!exito){
				mensaje = Mensaje.UPDATE_ASIG_NO_EXITOSO;
				reason= Mensaje.ERROR;
				type = "error";
				success = false;
			}

			log.debug(exito);
			log.debug(new String(bytes));

		  } catch (IOException e) {
			mensaje = Mensaje.ERROR_IO_FILE;
			reason= Mensaje.ERROR;
			type = "error";
			success = false;
			e.printStackTrace();
		  }

		}

		return new Respuesta<String>(success, mensaje, reason, type, data);

	}

	
	
	@POST
	@Path("/alumnos/horario/{id:[0-9][0-9]*}")
	@Consumes("multipart/form-data")
	public Respuesta<String> uploadFile(MultipartFormDataInput input, @PathParam("id") Integer id) {

		String mensaje=Mensaje.UPDATE_INSCRIPTOS_EXITOSO;
		String reason = Mensaje.INFO;
		String type = "success";
		String data = "";
		Boolean success = true;
		
		String fileName = "";

		Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
		List<InputPart> inputParts = uploadForm.get("uploadedFile");

		for (InputPart inputPart : inputParts) {

		 try {

			MultivaluedMap<String, String> header = inputPart.getHeaders();
			fileName = getFileName(header);
			data = fileName;
			mensaje += fileName;
			//convert the uploaded file to inputstream
			InputStream inputStream = inputPart.getBody(InputStream.class,null);

			byte [] bytes = IOUtils.toByteArray(inputStream);
			
			boolean exito = aulaEjb.setCantidadAlumnos(id, bytes);
			if(!exito){
				mensaje = Mensaje.UPDATE_INSCRIPTOS_NO_EXITOSO;
				reason= Mensaje.ERROR;
				type = "error";
				success = false;
			}

			log.debug(exito);
			log.debug(new String(bytes));

		  } catch (IOException e) {
			mensaje = Mensaje.ERROR_IO_FILE;
			reason= Mensaje.ERROR;
			type = "error";
			success = false;
			e.printStackTrace();
		  }

		}

		return new Respuesta<String>(success, mensaje, reason, type, data);

	}

	/**
	 * header sample
	 * {
	 * 	Content-Type=[image/png],
	 * 	Content-Disposition=[form-data; name="file"; filename="filename.extension"]
	 * }
	 **/
	//get uploaded filename, is there a easy way in RESTEasy?
	private String getFileName(MultivaluedMap<String, String> header) {

		String[] contentDisposition = header.getFirst("Content-Disposition").split(";");

		for (String filename : contentDisposition) {
			if ((filename.trim().startsWith("filename"))) {

				String[] name = filename.split("=");

				String finalFileName = name[1].trim().replaceAll("\"", "");
				return finalFileName;
			}
		}
		return "unknown";
	}

	/*/save to somewhere
	private void writeFile(byte[] content, String filename) throws IOException {

		File file = new File(filename);

		if (!file.exists()) {
			file.createNewFile();
		}

		FileOutputStream fop = new FileOutputStream(file);

		fop.write(content);
		fop.flush();
		fop.close();

	}*/
}

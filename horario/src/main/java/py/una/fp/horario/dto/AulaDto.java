package py.una.fp.horario.dto;

import java.io.Serializable;

/**
 *  DTO AULA
 */
public class AulaDto implements Serializable {
	
	private Integer idAula;
	private String nombre;
	private Integer capacidad;
	private Integer idTipoAula;
	private String descripcionTipoAula;


	public String getDescripcionTipoAula() {
		return descripcionTipoAula;
	}
	public void setDescripcionTipoAula(String descripcionTipoAula) {
		this.descripcionTipoAula = descripcionTipoAula;
	}
	public Integer getIdAula() {
		return idAula;
	}
	public void setIdAula(Integer idAula) {
		this.idAula = idAula;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Integer getCapacidad() {
		return capacidad;
	}
	public void setCapacidad(Integer capacidad) {
		this.capacidad = capacidad;
	}
	public Integer getIdTipoAula() {
		return idTipoAula;
	}
	public void setIdTipoAula(Integer idTipoAula) {
		this.idTipoAula = idTipoAula;
	}
	@Override
	public String toString(){
		return nombre;
	}
}

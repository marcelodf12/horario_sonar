package py.una.fp.horario.dto.resultado;

import java.util.ArrayList;

public class TipoAulaHorarioResultadoDto {
	
	public Integer id;
	public Integer idHorarioResultado;
	public Integer idTipoAula;
	public String aula;
	public ArrayList<DivisionHorarioResultadoDto> divisiones;

}

package py.una.fp.horario.dto;

public class ConflictosDto {
	
	public String asignatura1;
	public String asignatura2;
	public String seccion1;
	public String seccion2;
	public String turno1;
	public String turno2;
	public String periodo1;
	public String periodo2;
	public String startSlot1;
	public String endSlot1;
	public String startSlot2;
	public String endSlot2;

}

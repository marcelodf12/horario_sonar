package py.una.fp.horario.dto;

public class ImportDto {

	private String grupoConflicto;
	private String turno;
	private String enfasis;
	private String plan;
	private String seccion;
	private String codigoAsig;
	private String nombreAsignatura;
	private Integer idAsignaturaCarrera;
    private Integer idCarrera;
    private Integer semestre;
    private Integer nivel;
    
    
    
	public ImportDto(String grupoConflicto, String turno, String enfasis, String plan, String seccion,
			String codigoAsig, String nombreAsignatura, Integer idAsignaturaCarrera, Integer idCarrera,
			Integer semestre, Integer nivel) {
		super();
		this.grupoConflicto = grupoConflicto;
		this.turno = turno;
		this.enfasis = enfasis;
		this.plan = plan;
		this.seccion = seccion;
		this.codigoAsig = codigoAsig;
		this.nombreAsignatura = nombreAsignatura;
		this.idAsignaturaCarrera = idAsignaturaCarrera;
		this.idCarrera = idCarrera;
		this.semestre = semestre;
		this.nivel = nivel;
	}
	
	
	
	public ImportDto() {
		super();
	}



	public String getGrupoConflicto() {
		return grupoConflicto;
	}
	public void setGrupoConflicto(String grupoConflicto) {
		this.grupoConflicto = grupoConflicto;
	}
	public String getTurno() {
		return turno;
	}
	public void setTurno(String turno) {
		this.turno = turno;
	}
	public String getEnfasis() {
		return enfasis;
	}
	public void setEnfasis(String enfasis) {
		this.enfasis = enfasis;
	}
	public String getPlan() {
		return plan;
	}
	public void setPlan(String plan) {
		this.plan = plan;
	}
	public String getSeccion() {
		return seccion;
	}
	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}
	public String getCodigoAsig() {
		return codigoAsig;
	}
	public void setCodigoAsig(String codigoAsig) {
		this.codigoAsig = codigoAsig;
	}
	public String getNombreAsignatura() {
		return nombreAsignatura;
	}
	public void setNombreAsignatura(String nombreAsignatura) {
		this.nombreAsignatura = nombreAsignatura;
	}
	public Integer getIdAsignaturaCarrera() {
		return idAsignaturaCarrera;
	}
	public void setIdAsignaturaCarrera(Integer idAsignaturaCarrera) {
		this.idAsignaturaCarrera = idAsignaturaCarrera;
	}
	public Integer getIdCarrera() {
		return idCarrera;
	}
	public void setIdCarrera(Integer idCarrera) {
		this.idCarrera = idCarrera;
	}
	public Integer getSemestre() {
		return semestre;
	}
	public void setSemestre(Integer semestre) {
		this.semestre = semestre;
	}
	public Integer getNivel() {
		return nivel;
	}
	public void setNivel(Integer nivel) {
		this.nivel = nivel;
	}
    
}

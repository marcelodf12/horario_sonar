package py.una.fp.horario.ejb;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.google.gson.Gson;

import py.una.fp.horario.bd.DataBaseConfiguration;
import py.una.fp.horario.dto.ConflictosDto;
import py.una.fp.horario.dto.ImportDto;

@Stateless
public class ImportEjb {

	@EJB
	DataBaseConfiguration dbc;
	
	Logger log = Logger.getLogger(ImportEjb.class);
	
	public boolean actualizarPorArchivo(byte [] bytes){
		SqlSession session = dbc.getSession();
		boolean success = true;
		try{
			Reader in = new StringReader(new String(bytes));
			Iterable<CSVRecord> records = CSVFormat.EXCEL.withHeader().withDelimiter(';').parse(in);
			ArrayList<ImportDto> datos = new ArrayList<ImportDto>();
			for (CSVRecord record : records) {
			    String grupoConflicto = record.get("Grupo").trim();
			    String turno = record.get("Turno").trim();
			    String enfasis = record.get("Enfasis").trim();
			    String plan = record.get("Plan").trim();
			    String seccion = record.get("Seccion").trim();
			    String codigoAsig = record.get("CodAsig").trim();
			    String nombreAsignatura = record.get("Asignatura").trim();
			    Integer idAsignaturaCarrera = null;
			    try {
			    	idAsignaturaCarrera = new Integer(record.get("CodAsigCarrera").trim());
			    	log.debug("Asignatura_id "+idAsignaturaCarrera);
			    }catch (Exception e) {
					log.error("Asignatura_id NULL");
				}
			    Integer idCarrera = null;
			    try {
			    	idCarrera = new Integer(record.get("idCarrera").trim());
			    	log.debug("Carrera_id "+idCarrera);
			    }catch (Exception e) {
			    	log.error("Carrera_id NULL");
				}
			    Integer semestre = null;
			    try {
			    	semestre = new Integer(record.get("semestre").trim());
			    	log.debug("Semestre "+semestre);
			    }catch (Exception e) {
			    	log.error("Semestre NULL");
				}
			    Integer nivel = null;
			    try {
			    	nivel = new Integer(record.get("nivel").trim());
			    	log.debug("Nivel "+nivel);
			    }catch (Exception e) {
			    	log.error("Nivel NULL");
				}
			    ImportDto d = 
			    		new ImportDto(grupoConflicto, turno, enfasis, plan, seccion, codigoAsig, nombreAsignatura, idAsignaturaCarrera, idCarrera, semestre, nivel);
			    datos.add(d);
			}
			actualizarPorArray(datos);
		}catch(Exception e){
			success = false;
			log.error("Failed! ",e);
		}finally {
			if(session != null)
				session.close();
		}
		return success;
	}
	
	private void actualizarRestricciones() {
		try {
			SqlSession session = dbc.getSession();
			session.insert("py.una.fp.mapper.Importar.getConflictosFromGrupos");
			session.close();
		} catch (Exception e) {
			log.error("failed!",e);
		}
		
	}
	
	public boolean actualizarPorArray(List<ImportDto> datos) {
	    log.debug((new Gson()).toJson(datos));
		boolean error=false;
		for(ImportDto i: datos) {
			if(!isValid(i)) {
				error=true;
				break;
			}
		}
		log.debug("Hubo error: " + error);
		if(!error) {
			for(ImportDto i: datos) {
				agregarGrupo(i);
			}
		}
		actualizarRestricciones();
		return !error;
	}

	public void agregarGrupo(ImportDto datos) {
		if(datos!=null && datos.getGrupoConflicto()!=null && datos.getGrupoConflicto().trim().compareTo("")!=0) {
			SqlSession session = dbc.getSession();
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("codAsig", datos.getCodigoAsig());
			parameters.put("seccion", datos.getSeccion());
			parameters.put("turno", datos.getTurno());
			parameters.put("idAsignaturaCarrera", datos.getIdAsignaturaCarrera());
			parameters.put("nombre", datos.getNombreAsignatura());
			parameters.put("semestre",datos.getSemestre());
			parameters.put("nivel", datos.getNivel());
			parameters.put("enfasis", datos.getEnfasis());
			parameters.put("plan", datos.getPlan());
			parameters.put("id_carrera", datos.getIdCarrera());
			if(datos.getIdAsignaturaCarrera() == null) {
				log.debug("ID_CARRERA: " + datos.getIdCarrera());
				session.insert("py.una.fp.mapper.Asignatura.insertCarrera", parameters);
			}
			List<Integer> listAsigCarrera = session.selectList("py.una.fp.mapper.Importar.getAsignaturaCarrera", parameters);
			if(listAsigCarrera!=null && listAsigCarrera.size()>0) {
				parameters.put("grupoConflicto", datos.getGrupoConflicto());
				List<Integer> listGrupoConf = session.selectList("py.una.fp.mapper.Importar.getGrupoConflictoByName", parameters);
				if(listGrupoConf==null || listGrupoConf.size()==0) {
					session.insert("py.una.fp.mapper.Importar.nuevoGrupoConflicto", parameters);
					listGrupoConf = session.selectList("py.una.fp.mapper.Importar.getGrupoConflictoByName", parameters);
				}
				if(listGrupoConf!=null && listGrupoConf.size()>0) {
					parameters.put("id_grupo_conflicto", listGrupoConf.get(0));
					parameters.put("id_asignatura_carrera", listAsigCarrera.get(0));
					session.insert("py.una.fp.mapper.Importar.cambiarGrupo", parameters);
				}
			}
		}
	}
	
	public ArrayList<ImportDto> recuperarGrupos(Integer idCarrera){
		SqlSession session = dbc.getSession();
		HashMap<String, Object> parameters = new HashMap<>();
		parameters.put("id_carrera", idCarrera);
		List<ImportDto> list = session.selectList("py.una.fp.mapper.Importar.getGrupos", parameters);
		return (ArrayList<ImportDto>) list;
	}
	
	private boolean isValid(ImportDto i) {
		boolean r=false;
		try {
			r = //i.getGrupoConflicto().trim().compareTo("")!=0 &&
					i.getTurno().trim().compareTo("")!=0 &&
					//enfasis.trim().compareTo("")!=0 &&
					//plan.trim().compareTo("")!=0 &&
					i.getSeccion().trim().compareTo("")!=0 &&
					i.getCodigoAsig().trim().compareTo("")!=0 &&
					i.getNombreAsignatura().trim().compareTo("")!=0 &&
					//idAsignaturaCarrera.trim().compareTo("")!=0 &&
				    i.getIdCarrera()!=null;
				    //semestre.trim().compareTo("")!=0 &&
				    //nivel.trim().compareTo("")!=0;
		} finally {
			// TODO: handle finally clause
		}
		return r;	
	}
}

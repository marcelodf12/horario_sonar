package py.una.fp.horario.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import py.una.fp.horario.dto.ProfesorDto;
import py.una.fp.horario.dto.SlotDto;
import py.una.fp.horario.ejb.ProfesorEjb;
import py.una.fp.horario.secured.Secured;
import py.una.fp.horario.util.Mensaje;
import py.una.fp.horario.util.Respuesta;

@Stateless
@Path("/profesor")
@Api(
	    value = "/profesor", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON
	)
public class ProfesorRest {
	
	@EJB
	ProfesorEjb profesorEjb;

	Logger log = Logger.getLogger(ProfesorRest.class);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Listar Profesores",
	        notes = "Servicio para listar y filtrar profesores"
	    )
	public Respuesta<ArrayList<ProfesorDto>> listAll(
			@QueryParam("nombre") String nombre,
			@QueryParam("apellido") String apellido,
			@QueryParam("cedula") String cedula,
			@QueryParam("column") String column,
			@QueryParam("offset") String offset,
			@QueryParam("order") Boolean order
			){
		List<ProfesorDto> list = null;
		String mensaje="";
		Boolean success = true;
		String reason = Mensaje.CONSULTA_EXITOSA;
		String type = "success";
		try {
			list = profesorEjb.getAll(nombre,apellido, cedula, column, order,new Integer(offset));
		} catch (Exception e) {
			log.error("Failed! ",e);
			mensaje = Mensaje.ERROR_DATA_BASE;
			reason = e.getCause().getMessage();
			success = false;
			type = "error";
		}
		if(list != null){
			mensaje = (list.size()==11?offset:"ultima");
			if(list.size()==11){
				list.remove(10);
			}
		}else{
			mensaje = Mensaje.WARN_PROFESOR_CONSULTA_VACIA;
			success = false;
			type = "info";
			reason = Mensaje.WARN;
		}
		Respuesta<ArrayList<ProfesorDto>> r = new Respuesta<ArrayList<ProfesorDto>>(success, mensaje, reason, type, (ArrayList<ProfesorDto>) list);
		return r;
	}
	
	@GET
	@Path("/sinUsuario")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Listar Profesores",
	        notes = "Servicio para listar y filtrar profesores sin información de usuario"
	    )
	public Respuesta<ArrayList<ProfesorDto>> listAllSinsuario(
			@QueryParam("nombre") String nombre,
			@QueryParam("apellido") String apellido,
			@QueryParam("cedula") String cedula,
			@QueryParam("column") String column,
			@QueryParam("offset") String offset,
			@QueryParam("order") Boolean order
			){
		List<ProfesorDto> list = null;
		String mensaje="";
		Boolean success = true;
		String reason = Mensaje.CONSULTA_EXITOSA;
		String type = "success";
		try {
			list = profesorEjb.getAllSinUsuario(nombre,apellido, cedula, column, order,new Integer(offset));
		} catch (Exception e) {
			log.error("Failed! ",e);
			mensaje = Mensaje.ERROR_DATA_BASE;
			reason = e.getCause().getMessage();
			success = false;
			type = "error";
		}
		if(list != null){
			mensaje = (list.size()==11?offset:"ultima");
			if(list.size()==11){
				list.remove(10);
			}
		}else{
			mensaje = Mensaje.WARN_PROFESOR_CONSULTA_VACIA;
			success = false;
			type = "info";
			reason = Mensaje.WARN;
		}
		Respuesta<ArrayList<ProfesorDto>> r = new Respuesta<ArrayList<ProfesorDto>>(success, mensaje, reason, type, (ArrayList<ProfesorDto>) list);
		return r;
	}

	@GET
	@Path("/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Consultar Profesor",
	        notes = "Servicio para obtener la información de un profesor apartir de su identificador"
	    )
	public Respuesta<ProfesorDto> get(@PathParam("id") Integer id){
		String mensaje="";
		Boolean success = true;
		String reason = "";
		String type = "success";
		ProfesorDto c = null;
		try {
			c = profesorEjb.get(id);
			if(c == null){
				mensaje = Mensaje.ERROR_PROFESOR_NO_EXISTE;
				success = false;
				type = "info";
			}
		} catch (Exception e) {
			log.error("Failed! ",e);
			mensaje = Mensaje.ERROR_DATA_BASE;
			success = false;
			type = "error";
		}
		Respuesta<ProfesorDto> r = new Respuesta<ProfesorDto>(success,mensaje,reason, type, c);
		return r;
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id:[0-9][0-9]*}")
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Actualizar profesor",
	        notes = "Servicio para actualizar los datos de un profesor"
	    )
	public Respuesta<ProfesorDto> update(ProfesorDto profesor, @PathParam("id") Integer id){
		String mensaje=Mensaje.EDICION_PROFESOR_EXITOSO;
		Boolean success = profesorEjb.edit(id,profesor);
		String reason = "";
		String type = "success";
		if(!success){
			mensaje = Mensaje.EDICION_PROFESOR_NO_EXITOSO;
			type = "error";
			reason = Mensaje.ERROR;
		}
		Respuesta<ProfesorDto> r = new Respuesta<ProfesorDto>(success, mensaje, reason,type);
		return r;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Crear profesor",
	        notes = "Servicio para dar de alta un nuevo profesor"
	    )
	public Respuesta<ProfesorDto> create(ProfesorDto profesor){
		String mensaje=Mensaje.CREACION_PROFESOR_EXITOSO;
		Boolean success = profesorEjb.create(profesor);
		String reason = "";
		String type = "success";
		if(!success){
			mensaje = Mensaje.CREACION_PROFESOR_NO_EXITOSO;
			reason = Mensaje.ERROR;
			type = "error";
		}
		Respuesta<ProfesorDto> r = new Respuesta<ProfesorDto>(success, mensaje, reason,type);
		return r;
	}
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id:[0-9][0-9]*}")
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Eliminar Profesor",
	        notes = "Servicio para eliminar un profesor"
	    )
	public Respuesta<ProfesorDto> delete(@PathParam("id") Integer id){
		String mensaje=Mensaje.ELIMINACION_PROFESOR_EXITOSO;
		String reason = Mensaje.INFO;
		String type = "success";
		
		Boolean success = profesorEjb.delete(id);
		if(!success){
			mensaje = Mensaje.ELIMINACION_PROFESOR_NO_EXITOSO;
			type = "error";
			reason = Mensaje.ERROR;
		}
		Respuesta<ProfesorDto> r = new Respuesta<ProfesorDto>(success, mensaje, reason, type);
		return r;
	}
	
	@GET
	@Path("/{idProfesor:[0-9][0-9]*}/slot/{idSlot:-?[0-9][0-9]*}")
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Consultar disponibilidad de profesor",
	        notes = "Servicio para consultar la configuración de slots de un profesor"
	    )
	public Respuesta<ProfesorDto> getSlot(@PathParam("idProfesor") Integer idProfesor,@PathParam("idSlot") Integer idSlot) {
		String mensaje="";
		Boolean success = true;
		String reason = "";
		String type = "success";
		ProfesorDto c = null;
		try {
			c = profesorEjb.getSlot(idProfesor,idSlot);
			if(c == null){
				mensaje = Mensaje.ERROR_PROFESOR_NO_EXISTE;
				success = false;
				type = "info";
			}
		} catch (Exception e) {
			log.error("Failed! ",e);
			mensaje = Mensaje.ERROR_DATA_BASE;
			success = false;
			type = "error";
		}
		Respuesta<ProfesorDto> r = new Respuesta<ProfesorDto>(success,mensaje,reason, type, c);
		return r;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id:[0-9][0-9]*}/slots/")
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Consultar disponibilidad de profesor (5 últimos)",
	        notes = "Servicio para consultar las últimas 5 configuraciones de slots de un profesor"
	    )
	public Respuesta<ArrayList<SlotDto>> listSlots(@QueryParam("idProfesor") Integer idProfesor){
		List<SlotDto> list = null;
		String mensaje="";
		Boolean success = true;
		String reason = Mensaje.CONSULTA_EXITOSA;
		String type = "success";
		try {
			list = profesorEjb.getAllSlot(idProfesor);
		} catch (Exception e) {
			log.error("Failed! ",e);
			mensaje = Mensaje.ERROR_DATA_BASE;
			reason = e.getCause().getMessage();
			success = false;
			type = "error";
		}
		if(list == null){
			mensaje = Mensaje.WARN_PROFESOR_CONSULTA_VACIA;
			success = false;
			type = "info";
			reason = Mensaje.WARN;
		}
		Respuesta<ArrayList<SlotDto>> r = new Respuesta<ArrayList<SlotDto>>(success, mensaje, reason, type, (ArrayList<SlotDto>) list);
		return r;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{idProfesor:[0-9][0-9]*}/slot/{idSlot:-?[0-9][0-9]*}")
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Modificar disponibilidad de profesor",
	        notes = "Servicio para modificar la configuración de slots de un profesor"
	    )
	public Respuesta<SlotDto> insertSlot(@PathParam("idProfesor") Integer idProfesor,@PathParam("idSlot") Integer idSlot, String slot){
		String mensaje=Mensaje.CREACION_PROFESOR_EXITOSO;
		Boolean success = profesorEjb.insertSlot(slot, 1, idProfesor);
		String reason = "";
		String type = "success";
		if(!success){
			mensaje = Mensaje.CREACION_PROFESOR_NO_EXITOSO;
			reason = Mensaje.ERROR;
			type = "error";
		}
		Respuesta<SlotDto> r = new Respuesta<SlotDto>(success, mensaje, reason,type);
		return r;
	}

}

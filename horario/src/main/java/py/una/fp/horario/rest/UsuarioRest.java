package py.una.fp.horario.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import py.una.fp.horario.dto.PerfilDto;
import py.una.fp.horario.dto.UsuarioDto;
import py.una.fp.horario.dto.UsuarioPerfilDto;
import py.una.fp.horario.ejb.UsuarioEjb;
import py.una.fp.horario.secured.Secured;
import py.una.fp.horario.util.Mensaje;
import py.una.fp.horario.util.Respuesta;


@Stateless
@Path("/usuario")
@Api(
	    value = "/usuario", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON
	)
public class UsuarioRest {
	
	@EJB
	UsuarioEjb usuarioEbj;
	

	Logger log = Logger.getLogger(UsuarioRest.class);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(perfil="2")
	@ApiOperation(
	        value = "Listar usuarios",
	        notes = "Servicio para listar y filtrar usuarios"
	    )
	public Respuesta<ArrayList<UsuarioDto>> listAll(
			@QueryParam("offset") String offset,
			@QueryParam("nombre") String nombre,
			@QueryParam("column") String column,
			@QueryParam("order") Boolean order
			){
		List<UsuarioDto> list = null;
		String mensaje="";
		Boolean success = true;
		String reason = Mensaje.CONSULTA_EXITOSA;
		String type = "success";
		try {
			list = usuarioEbj.getAll(nombre, column, order,new Integer(offset));
			
		} catch (Exception e) {
			log.error("Failed! ",e);
			mensaje = Mensaje.ERROR_DATA_BASE;
			reason = e.getCause().getMessage();
			success = false;
			type = "error";
		}
		if(list != null){
			mensaje = (list.size()==11?offset:"ultima");
			if(list.size()==11){
				list.remove(10);
			}
		}else{
			mensaje = Mensaje.WARN_USUARIO_CONSULTA_VACIA;
			success = false;
			type = "info";
			reason = Mensaje.WARN;
		}
		Respuesta<ArrayList<UsuarioDto>> r = new Respuesta<ArrayList<UsuarioDto>>(success, mensaje, reason, type, (ArrayList<UsuarioDto>) list);
		return r;
	}
	
	
	@GET
	@Path("/perfiles/{idProfesor:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(perfil="1")
	@ApiOperation(
	        value = "Consultar roles",
	        notes = "Servicio para consultar los permisos de un determinado usuario"
	    )
	public Respuesta<List<PerfilDto>> getPerfiles(@PathParam("idProfesor") Integer idProfesor){
		String mensaje="";
		Boolean success = true;
		String reason = "";
		String type = "success";
		List<PerfilDto> perfiles = null;
		try {
			perfiles = usuarioEbj.getPerfiles(idProfesor);
			if(perfiles.size() == 0){
				mensaje = Mensaje.ERROR_PERFILES_NO_EXISTE;
				success = false;
				type = "info";
			}
		} catch (Exception e) {
			log.error("Failed! ",e);
			mensaje = Mensaje.ERROR_DATA_BASE;
			success = false;
			type = "error";
		}
		Respuesta<List<PerfilDto>> r = new Respuesta<List<PerfilDto>>(success,mensaje,reason, type, perfiles);
		return r;
	}
	
	
	@GET
	@Path("/permisos")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(perfil="1")
	@ApiOperation(
	        value = "Listar permisos",
	        notes = "Servicio para listar todos los permisos disponibles"
	    )
	public Respuesta<List<PerfilDto>> getPermisos(){
		String mensaje="";
		Boolean success = true;
		String reason = "";
		String type = "success";
		List<PerfilDto> perfiles = null;
		try {
			perfiles = usuarioEbj.getPermisos();
			if(perfiles.size() == 0){
				mensaje = Mensaje.ERROR_PERFILES_NO_EXISTE;
				success = false;
				type = "info";
			}
		} catch (Exception e) {
			log.error("Failed! ",e);
			mensaje = Mensaje.ERROR_DATA_BASE;
			success = false;
			type = "error";
		}
		Respuesta<List<PerfilDto>> r = new Respuesta<List<PerfilDto>>(success,mensaje,reason, type, perfiles);
		return r;
	}
	
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("perfil/{idUsuario:[0-9][0-9]*}/{idPerfil:[0-9][0-9]*}")
	@Secured(perfil="2")
	@ApiOperation(
	        value = "Quitar permiso a usuario",
	        notes = "Servicio para eliminar un permiso a un determinado usuario"
	    )
	public Respuesta<PerfilDto> deleteUsuarioPerfil(@PathParam("idUsuario") Integer idUsuario, @PathParam("idPerfil") Integer idPerfil){
		String mensaje=Mensaje.ELIMINACION_PERMISO_EXITOSO;
		String reason = Mensaje.INFO;
		String type = "success";
		Boolean success = usuarioEbj.deletePermiso(idUsuario, idPerfil);
		if(!success){
			mensaje = Mensaje.ELIMINACION_PERMISO_NO_EXITOSO;
			type = "error";
			reason = Mensaje.ERROR;
		}
		Respuesta<PerfilDto> r = new Respuesta<PerfilDto>(success, mensaje, reason, type);
		return r;
	}
	

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("usuarioPerfil")
	@Secured(perfil="2")
	@ApiOperation(
	        value = "Agregar Permiso a usuario",
	        notes = "Servicio para agregar un nuevo permiso a un usuario determinado"
	    )
	public Respuesta<UsuarioPerfilDto> createUsuarioPerfil(UsuarioPerfilDto permiso){
		String mensaje=Mensaje.CREACION_USUARIO_PERFIL_EXITOSO;
		Boolean success = usuarioEbj.createUsuarioPerfil(permiso);
		String reason = "";
		String type = "success";
		if(!success){
			mensaje = Mensaje.CREACION_USUARIO_PERFIL_NO_EXITOSO;
			reason = Mensaje.ERROR;
			type = "error";
		}
		Respuesta<UsuarioPerfilDto> r = new Respuesta<UsuarioPerfilDto>(success, mensaje, reason,type);
		return r;
	}
	
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(perfil="2")
	@ApiOperation(
	        value = "Crear Usuario",
	        notes = "Servicio para dar de alta un nuevo usuario"
	    )
	public Respuesta<UsuarioDto> create(UsuarioDto u){
		String mensaje=Mensaje.CREACION_USUARIO_EXITOSO;
		String reason = Mensaje.INFO;
		String type = "success";
		
		Boolean existeLogin = usuarioEbj.getLoginExistente(u.getNombre());
		if(existeLogin){
			mensaje = Mensaje.EXISTE_LOGIN_USUARIO;
			type = "error";
			reason = Mensaje.ERROR;
			Respuesta<UsuarioDto> r = new Respuesta<UsuarioDto>(false, mensaje, reason, type);
			return r;
		}
		
		Boolean success = usuarioEbj.create(u);		
		if(!success){
			mensaje = Mensaje.CREACION_USUARIO_NO_EXITOSO;
			type = "error";
			reason = Mensaje.ERROR;
		}
		Respuesta<UsuarioDto> r = new Respuesta<UsuarioDto>(success, mensaje, reason, type);
		return r;
	}


}

package py.una.fp.horario.secured;

import java.util.ArrayList;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import py.una.fp.horario.dto.PerfilDto;
import py.una.fp.horario.ejb.UsuarioEjb;
import py.una.fp.horario.util.Respuesta;

@Secured
@Interceptor
public class SecureInterceptor {
	
	@EJB
	UsuarioEjb usuarioEjb;
	
	@Inject
    HttpServletRequest request;
	
	Logger log = Logger.getLogger(SecureInterceptor.class);
	
	@AroundInvoke
	public Object aroundInvoke(InvocationContext ic) throws Exception {
		String perfilNeed = ic.getMethod().getAnnotation(Secured.class).perfil();
		String token = request.getHeader("authorization");
		log.debug("Peticion interceptada, token:"+token);
		if(token==null || token == ""){
			Respuesta<Object> r = new Respuesta<Object>();
			r.setSuccess(false);
			r.setReason("Forbidden");
			r.setType("error");
			r.setMessage("Usted no esta autenticado. Favor loguearse");
			return r;
		}
		log.debug(perfilNeed);
		log.debug(token);
		
		ArrayList<PerfilDto> perfiles = usuarioEjb.getPefilByToken(token);
		
		boolean habilitado = false;
		
		if(perfiles!=null)
		for(PerfilDto p: perfiles){
			if(p.getCodigo().toString().compareTo(perfilNeed)==0){
				habilitado = true;
				break;
			}
		}
		if(habilitado){
			return ic.proceed();
		}else{
			Respuesta<Object> r = new Respuesta<Object>();
			r.setSuccess(false);
			r.setReason("Forbidden");
			r.setType("error");
			r.setMessage("No tiene permisos para realizar esta operación");
			return r;
		}
		
	}

}

package py.una.fp.horario.ejb;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import py.una.fp.horario.bd.DataBaseConfiguration;
import py.una.fp.horario.dto.AgrupacionAulasDto;
import py.una.fp.horario.dto.AulaDto;
import py.una.fp.horario.dto.TipoAulaDto;

@Stateless
public class AulaEjb {
	
	@EJB
	DataBaseConfiguration dbc;
	
	Logger log = Logger.getLogger(AulaEjb.class);
	
	public ArrayList<AulaDto>getAll(String nombre, Integer capacidad, Integer tipo, Integer offset, String column, Boolean order){
		SqlSession session = dbc.getSession();
		List<AulaDto> list = null;
		HashMap<String, Object> parameters = new HashMap<>();
		parameters.put("nombre", nombre);
		parameters.put("capacidad", capacidad);
		parameters.put("tipo", tipo);
		parameters.put("offset", offset);
		parameters.put("column", column);
		parameters.put("order", order);
		list = session.selectList("py.una.fp.mapper.Aula.getAll", parameters);
		session.close();
		if(list.size()==0)
			return null;
		return (ArrayList<AulaDto>) list;
	}
	
	
	public ArrayList<TipoAulaDto>getAllTipoAula(){
		SqlSession session = dbc.getSession();
		List<TipoAulaDto> list = null;
		list = session.selectList("py.una.fp.mapper.Aula.getAllTiposAulas");
		session.close();
		if(list.size()==0)
			return null;
		return (ArrayList<TipoAulaDto>) list;
	}
	
	
	public boolean delete(Integer id){
		SqlSession session = dbc.getSession();
		boolean success = true;
		try{
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("id", id);
			session.selectList("py.una.fp.mapper.Aula.delete", parameters);
		}catch(Exception e){
			success = false;
			log.error("Failed! ",e);
		}finally {
			session.close();
		}
		return success;
	}
	
	public boolean create(String nombre, Integer capacidad, Integer tipo){
		SqlSession session = dbc.getSession();
		boolean success = true;
		try{
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("nombre", nombre);
			parameters.put("capacidad", capacidad);
			parameters.put("tipo", tipo);
			session.selectList("py.una.fp.mapper.Aula.insert", parameters);
		}catch(Exception e){
			success = false;
			log.error("Failed! ",e);
		}finally {
			session.close();
		}
		return success;
	}
	
	public boolean edit(Integer id, String nombre, Integer capacidad, Integer tipo){
		SqlSession session = dbc.getSession();
		boolean success = true;
		try{
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("id", id);
			parameters.put("nombre", nombre);
			parameters.put("capacidad", capacidad);
			parameters.put("tipo", tipo);
			session.selectList("py.una.fp.mapper.Aula.update", parameters);
		}catch(Exception e){
			success = false;
			log.error("Failed! ",e);
		}finally {
			session.close();
		}
		return success;
	}
	
	public boolean setCantidadAlumnos(Integer idHorario, byte [] bytes){
		SqlSession session = dbc.getSession();
		boolean success = true;
		try{
			Reader in = new StringReader(new String(bytes));
			Iterable<CSVRecord> records = CSVFormat.EXCEL.withHeader().withDelimiter(';').parse(in);
			HashMap<String, Object> parameters = new HashMap<>();
			for (CSVRecord record : records) {
			    String conFirma = record.get("ConFirma");
			    String sinFirma = record.get("SinFirma");
			    String turno = record.get("Turno");
			    String seccion = record.get("Seccion");
			    String codigoAsig = record.get("CodAsig");
			    Integer tamanho = Integer.valueOf(sinFirma) + Integer.valueOf(conFirma);
			    parameters.put("tamanho", tamanho);
				parameters.put("idHorario", idHorario);
				parameters.put("turno", turno);
				parameters.put("seccion", seccion);
				parameters.put("codigoAsig", codigoAsig);
				session.selectList("py.una.fp.mapper.AgrupacionAulas.actualizarCantidadAlumnos", parameters);
			}
		}catch(Exception e){
			success = false;
			log.error("Failed! ",e);
		}finally {
			if(session != null)
				session.close();
		}
		return success;
	}
}

package py.una.fp.horario.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import py.una.fp.horario.dto.FilaHorarioDto;
import py.una.fp.horario.ejb.HorarioEjb;
import py.una.fp.horario.secured.Secured;
import py.una.fp.horario.util.Mensaje;
import py.una.fp.horario.util.Respuesta;

@Stateless
@Path("/examenes")
@Api(
	    value = "/examenes", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON
	)
public class ExamenRest {
	
	@EJB
	HorarioEjb horarioEbj;
	
	Logger log = Logger.getLogger(ExamenRest.class);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/horario/{id:[0-9][0-9]*}")
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Listar Exámenes",
	        notes = "Servicio para listar los exámenes de un determinado horario"
	    )
	public Respuesta<ArrayList<FilaHorarioDto>> getExamenes(@PathParam("id") String idHorario){
		List<FilaHorarioDto> list = null;
		String mensaje="";
		Boolean success = true;
		String reason = Mensaje.CONSULTA_EXITOSA;
		String type = "success";
		try {
			Integer id = null;
			id = new Integer(idHorario);
			list = horarioEbj.getHorarioResultado(id);
		} catch (Exception e) {
			log.error("Failed! ",e);
			mensaje = Mensaje.ERROR_DATA_BASE;
			reason = e.getCause().getMessage();
			success = false;
			type = "error";
		}
		if(list == null){
			mensaje = Mensaje.WARN_HORARIO_CONSULTA_VACIA;
			success = false;
			type = "info";
			reason = Mensaje.WARN;
		}
		Respuesta<ArrayList<FilaHorarioDto>> r = new Respuesta<ArrayList<FilaHorarioDto>>(success, mensaje, reason, type, (ArrayList<FilaHorarioDto>) list);
		return r;
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/horario/{id:[0-9][0-9]*}")
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Actualizar Examen",
	        notes = "Servicio para modificar la fecha y hora de un examen de un horario particular."
	    )
	public Respuesta<FilaHorarioDto> update(FilaHorarioDto f, @PathParam("id") Integer id){
		String mensaje=Mensaje.EDICION_HORARIO_EXITOSO;
		String reason = Mensaje.INFO;
		String type = "success";
		
		Boolean success = horarioEbj.updateExamen(f, id);
		if(!success){
			mensaje = Mensaje.EDICION_HORARIO_NO_EXITOSO;
			type = "error";
			reason = Mensaje.ERROR;
		}
		Respuesta<FilaHorarioDto> r = new Respuesta<FilaHorarioDto>(success, mensaje, reason, type);
		return r;
	}

}

package py.una.fp.horario.dto;

public class UsuarioPerfilDto {
	
//para hacer commit
	
	private Integer idUsuario;
	private Integer idPerfil;

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Integer getIdPerfil() {
		return idPerfil;
	}

	public void setIdPerfil(Integer idPerfil) {
		this.idPerfil = idPerfil;
	}


}

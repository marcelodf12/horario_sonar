package py.una.fp.horario.ejb;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import py.una.fp.horario.bd.DataBaseConfiguration;
import py.una.fp.horario.dto.NotificaciontDto;
import py.una.fp.horario.mail.GMail;
import py.una.fp.horario.reports.core.ReportExporter;
import py.una.fp.horario.reports.core.ReportGenerator;
import py.una.fp.horario.util.Configuracion;

@Stateless
public class NotificacionesEjb {
	@EJB
	DataBaseConfiguration dbc;
	
	@EJB
	HorarioEjb horarioEjb;
	
	@EJB
	GMail gmailEjb;
	
    @EJB
    Configuracion conf;
	
	Logger log = Logger.getLogger(NotificacionesEjb.class);
	
	private static final String directorySeparator = System.getProperty("file.separator");
	
	private String slotToHora(int slot) {
		slot = (slot-1)%288;
		int minTotal = slot*5;
		int hora = (int) Math.floor(minTotal/60);
		String horaStr;
		String minStr;
		horaStr = hora > 9 ? "" + hora: "0" + hora;
		int min = minTotal - hora*60;
		minStr = min > 9 ? "" + min: "0" + min;
		return horaStr+":"+minStr;
	}
	
	@Asynchronous
	public void notificar(Integer idHorario) {
		try {
			SqlSession session = dbc.getSession();
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("idHorario", idHorario);
			List<NotificaciontDto> list = null;
			if(idHorario !=null){
				list = session.selectList("py.una.fp.mapper.Profesor.getNotificados", parameters);
			}
			String dirPdf = conf.getValor("dir_pdf");
			session.close();
			String asunto = "Nuevo horario publicado (FPUNA)";
			log.debug("A notificar: " + list);
			HashMap<Integer, ArrayList<NotificaciontDto>> profesores = new HashMap<Integer, ArrayList<NotificaciontDto>>();
			String[] diasCombo = {"LUN","MAR","MIE","JUE","VIE","SAB","DOM"};
			for(NotificaciontDto n: list) {
				log.debug(n.getProfesorId());
				double i = n.getStart()/288;
				n.setDia(diasCombo[(int) Math.floor(i)]);
				n.setInicio(slotToHora(n.getStart()));
				n.setFin(slotToHora(n.getEnd()));
				ArrayList<NotificaciontDto> notificaciones = profesores.get(n.getProfesorId());
				if(notificaciones==null) {
					notificaciones = new ArrayList<NotificaciontDto>();
					profesores.put(n.getProfesorId(), notificaciones);
				}
				notificaciones.add(n);
			}
			for (Map.Entry<Integer, ArrayList<NotificaciontDto>> entry : profesores.entrySet()) {
				log.debug("Key = " + entry.getKey() + ", Value = " + entry.getValue());
			    List<NotificaciontDto> ds = entry.getValue();
			    String to = ds.get(0).getProfesorCorreo();
			    log.info("Preparando mail a: " + to);
			    if(to!=null && to.trim().compareTo("")!=0) {
				    String profesor = ds.get(0).getProfesorTitulo() + " " + ds.get(0).getProfesorNombre() + " " + ds.get(0).getProfesorApellido();
				    String fecha = "Última fecha de actualización: " + (new SimpleDateFormat("dd/MM/yyyy HH:mm")).format(new Date());
				    Map<String,Object> parametros = new HashMap<String,Object>();
				    parametros.put("titulo", profesor);
				    parametros.put("fecha", fecha);
				    parametros.put("subTitulo", "Horario de clases por profesor");
				    byte[] pdf = (new ReportGenerator<NotificaciontDto>()).toPDF("HorarioProfesor", parametros, ds);
				    String path = dirPdf + directorySeparator + idHorario + directorySeparator + "HorarioPorProfesor";
				    File folder = new File(path);
				    if(!folder.exists()) folder.mkdirs();
				    String file =  path + directorySeparator + "horario_profesor_" + ds.get(0).getProfesorId()+".pdf";
				    ReportExporter.gravarEmDisco(pdf, file);
				    log.debug("PDF generado");
				    String[] toArray = {to};
				    String[] fileArray = {file};
				    //send(        String from,         String[] recipientsTo,         String[] recipientsCC, String[] recipientsBCC, String subject, String body, String[] files)
				    gmailEjb.send("horario.fpuna.com", toArray, null, null, asunto, "Se ha publicado un nuevo horario. Para visualizarlo vea el archivo adjunto", fileArray);
				    log.info("Mail enviado");
			    }
			}
		} catch (Exception e) {
			log.error("ERROR AL NOTIFICAR PROFESORES");
			log.error( "failed!", e );
		}
		log.info("El proceso de notificación culminó correctamente");
	}

}

package py.una.fp.horario.dto;

public class DivisionDto {
	
	private Integer idDivision;
	private Integer idAsignatura;
	private Integer tipoAula;
	private Integer duracion;
	private String modo;
	private Integer hora;
	
	public Integer getIdDivision() {
		return idDivision;
	}
	public void setIdDivision(Integer idDivision) {
		this.idDivision = idDivision;
	}
	public Integer getIdAsignatura() {
		return idAsignatura;
	}
	public void setIdAsignatura(Integer idAsignatura) {
		this.idAsignatura = idAsignatura;
	}
	public Integer getTipoAula() {
		return tipoAula;
	}
	public void setTipoAula(Integer tipoAula) {
		this.tipoAula = tipoAula;
	}
	public Integer getDuracion() {
		return duracion;
	}
	public void setDuracion(Integer duracion) {
		this.duracion = duracion;
	}
	public String getModo() {
		return modo;
	}
	public void setModo(String modo) {
		this.modo = modo;
	}
	public Integer getHora() {
		return hora;
	}
	public void setHora(Integer hora) {
		this.hora = hora;
	}
}

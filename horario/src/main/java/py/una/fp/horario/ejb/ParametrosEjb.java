package py.una.fp.horario.ejb;

import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import py.una.fp.horario.bd.DataBaseConfiguration;
import py.una.fp.horario.dto.ConfiguracionDto;
import py.una.fp.horario.util.Configuracion;

@Stateless
public class ParametrosEjb {

	@EJB
	DataBaseConfiguration dbc;
	
	@EJB
	Configuracion conf;
	
	Logger log = Logger.getLogger(ParametrosEjb.class);
	
	public List<ConfiguracionDto> getAll(){
		SqlSession session = null;
		List<ConfiguracionDto> list = null;
		try{
			session = dbc.getSession();
			list = session.selectList("py.una.fp.mapper.Configuracion.getAll");
		}catch(Exception e){
			log.error(e.getMessage());
			log.error(e.getCause());
		}finally {
			if(session!=null)
				session.close();
		}
		return list;
	}
	
	public boolean edit(String clave, String valor){
		SqlSession session = null;
		boolean r = true;
		try{
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("clave", clave);
			parameters.put("valor", valor);
			session = dbc.getSession();
			session.update("py.una.fp.mapper.Configuracion.updateValor", parameters);
		}catch(Exception e){
			log.error(e.getMessage());
			log.error(e.getCause());
			r=false;
		}finally {
			if(session!=null)
				session.close();
		}
		return r;
	}
	
}

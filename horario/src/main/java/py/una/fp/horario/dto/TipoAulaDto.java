package py.una.fp.horario.dto;

public class TipoAulaDto {
	
	private Integer idTipoAula;
	
	private String descripcion;

	public Integer getIdTipoAula() {
		return idTipoAula;
	}

	public void setIdTipoAula(Integer idTipoAula) {
		this.idTipoAula = idTipoAula;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	
}

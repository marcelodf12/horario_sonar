package py.una.fp.horario.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import py.una.fp.horario.dto.DepartamentoDto;
import py.una.fp.horario.ejb.DepartamentoEjb;
import py.una.fp.horario.secured.Secured;
import py.una.fp.horario.util.Mensaje;
import py.una.fp.horario.util.Respuesta;


@Stateless
@Path("/departamento")
@Api(
	    value = "/departamento", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON
	)
public class DepartamentoRest {
	@EJB
	DepartamentoEjb departamentoEjb;

	Logger log = Logger.getLogger(DepartamentoRest.class);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/siglas")
	@ApiOperation(
	        value = "Listar siglas de departamentos",
	        notes = "Servicio para las siglas con sus nombres correspondientes de los departamentos"
	    )
	public Respuesta<ArrayList<DepartamentoDto>> listAll(
			@QueryParam("column") String column,
			@QueryParam("order") Boolean order
			){
		List<DepartamentoDto> list = null;
		String mensaje="";
		Boolean success = true;
		String reason = Mensaje.CONSULTA_EXITOSA;
		String type = "success";
		try {
			list = departamentoEjb.getAll(null, null, column, order);
			for(DepartamentoDto d: list)
				d.setIdDepartamento(null);
		} catch (Exception e) {
			log.error("Failed! ",e);
			mensaje = Mensaje.ERROR_DATA_BASE;
			reason = e.getCause().getMessage();
			success = false;
			type = "error";
		}
		if(list == null){
			mensaje = Mensaje.WARN_DEPARTAMENTO_CONSULTA_VACIA;
			success = false;
			type = "info";
			reason = Mensaje.WARN;
		}
		Respuesta<ArrayList<DepartamentoDto>> r = new Respuesta<ArrayList<DepartamentoDto>>(success, mensaje, reason, type, (ArrayList<DepartamentoDto>) list);
		return r;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Listar departamentos",
	        notes = "Servicio para listar y filtrar departamentos"
	    )
	public Respuesta<ArrayList<DepartamentoDto>> listAll(
			@QueryParam("nombre") String nombre,
			@QueryParam("sigla") String sigla,
			@QueryParam("column") String column,
			@QueryParam("order") Boolean order
			){
		List<DepartamentoDto> list = null;
		String mensaje="";
		Boolean success = true;
		String reason = Mensaje.CONSULTA_EXITOSA;
		String type = "success";
		try {
			list = departamentoEjb.getAll(nombre, sigla, column, order);
		} catch (Exception e) {
			log.error("Failed! ",e);
			mensaje = Mensaje.ERROR_DATA_BASE;
			reason = e.getCause().getMessage();
			success = false;
			type = "error";
		}
		if(list == null){
			mensaje = Mensaje.WARN_DEPARTAMENTO_CONSULTA_VACIA;
			success = false;
			type = "info";
			reason = Mensaje.WARN;
		}
		Respuesta<ArrayList<DepartamentoDto>> r = new Respuesta<ArrayList<DepartamentoDto>>(success, mensaje, reason, type, (ArrayList<DepartamentoDto>) list);
		return r;
	}
	
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Crear departamento",
	        notes = "Servicio para dar de alta un nuevo departamento"
	    )
	public Respuesta<DepartamentoDto> create(DepartamentoDto departamento){
		String mensaje=Mensaje.CREACION_DEPARTAMENTO_EXITOSO;
		String reason = Mensaje.INFO;
		String type = "success";
		
		Boolean success = departamentoEjb.create(departamento.getNombre(), departamento.getSigla());
		if(!success){
			mensaje = Mensaje.CREACION_DEPARTAMENTO_NO_EXITOSO;
			type = "error";
			reason = Mensaje.ERROR;
		}
		Respuesta<DepartamentoDto> r = new Respuesta<DepartamentoDto>(success, mensaje, reason, type);
		return r;
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id:[0-9][0-9]*}")
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Modificar departamento",
	        notes = "Servicio para modificar los datos de un departamento"
	    )
	public Respuesta<DepartamentoDto> update(DepartamentoDto departamento, @PathParam("id") Integer id){
		String mensaje=Mensaje.EDICION_DEPARTAMENTO_EXITOSO;
		String reason = Mensaje.INFO;
		String type = "success";
		
		Boolean success = departamentoEjb.edit(id,departamento.getNombre(), departamento.getSigla());
		if(!success){
			mensaje = Mensaje.EDICION_DEPARTAMENTO_NO_EXITOSO;
			type = "error";
			reason = Mensaje.ERROR;
		}
		Respuesta<DepartamentoDto> r = new Respuesta<DepartamentoDto>(success, mensaje, reason, type);
		return r;
	}
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id:[0-9][0-9]*}")
	@Secured(perfil="3")
	@ApiOperation(
	        value = "Eliminar departamento",
	        notes = "Servicio para eliminar un departamento"
	    )
	public Respuesta<DepartamentoDto> delete(@PathParam("id") Integer id){
		String mensaje=Mensaje.ELIMINACION_DEPARTAMENTO_EXITOSO;
		String reason = Mensaje.INFO;
		String type = "success";
		
		Boolean success = departamentoEjb.delete(id);
		if(!success){
			mensaje = Mensaje.ELIMINACION_DEPARTAMENTO_NO_EXITOSO;
			type = "error";
			reason = Mensaje.ERROR;
		}
		Respuesta<DepartamentoDto> r = new Respuesta<DepartamentoDto>(success, mensaje, reason, type);
		return r;
	}
}

package py.una.fp.horario.dto;

import java.util.Date;

public class HorarioDto {
	
	private Integer idHorario;
	private Integer anho;
	private Date fecha;
	private String estado;
	private Integer periodo;
	private Integer idHorarioAnterior;
	private Integer idProcess;
	private Integer progreso;
	private Boolean publicado;
	
	public Integer getIdHorario() {
		return idHorario;
	}
	public void setIdHorario(Integer idHorario) {
		this.idHorario = idHorario;
	}
	public Integer getAnho() {
		return anho;
	}
	public void setAnho(Integer anho) {
		this.anho = anho;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Integer getIdHorarioAnterior() {
		return idHorarioAnterior;
	}
	public void setIdHorarioAnterior(Integer idHorarioAnterior) {
		this.idHorarioAnterior = idHorarioAnterior;
	}
	public Integer getPeriodo() {
		return periodo;
	}
	public void setPeriodo(Integer periodo) {
		this.periodo = periodo;
	}
	public Integer getIdProcess() {
		return idProcess;
	}
	public void setIdProcess(Integer idProcess) {
		this.idProcess = idProcess;
	}
	public Integer getProgreso() {
		return progreso;
	}
	public void setProgreso(Integer progreso) {
		this.progreso = progreso;
	}
	public Boolean getPublicado() {
		return publicado;
	}
	public void setPublicado(Boolean publicado) {
		this.publicado = publicado;
	}
	
	
}

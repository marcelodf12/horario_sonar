package py.una.fp.horario.dto;

import java.util.ArrayList;

public class CarreraDto {
	
	private Integer idCarrera;
	private String nombre;
	private String sigla;
	private String codCarSec;

	private ArrayList<AsignaturaDto> asignaturas;
	public Integer getIdCarrera() {
		return idCarrera;
	}
	public void setIdCarrera(Integer idCarrera) {
		this.idCarrera = idCarrera;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getSigla() {
		return sigla;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public ArrayList<AsignaturaDto> getAsignaturas() {
		return asignaturas;
	}
	public void setAsignaturas(ArrayList<AsignaturaDto> asignaturas) {
		this.asignaturas = asignaturas;
	}
	public String getCodCarSec() {
		return codCarSec;
	}
	public void setCodCarSec(String codCarSec) {
		this.codCarSec = codCarSec;
	}

	
	

}

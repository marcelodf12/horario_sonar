package py.una.fp.horario.util;

import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import py.una.fp.horario.bd.DataBaseConfiguration;
import py.una.fp.horario.dto.ConfiguracionDto;

@Singleton
@Startup
public class Configuracion {
	
	@EJB
	DataBaseConfiguration dbc;
	
	public static final Integer MAX_SLOT = 2016;
	
	private SqlSession session;
	
	Logger log = Logger.getLogger(Configuracion.class);
	
	@PostConstruct
	void init()
	{
		session = dbc.getSession();
		
		/* Setear H_MAX
		HashMap<String, Object> parameters = new HashMap<>();
		parameters.put("clave", "h_max");
		List<ConfiguracionDto> conf = session.selectList("py.una.fp.mapper.Configuracion.getValor", parameters);
		H_MAX = new Integer(conf.get(0).getValor());
		
		// Setear H_MIN
		parameters = new HashMap<>();
		parameters.put("clave", "h_min");
		conf = session.selectList("py.una.fp.mapper.Configuracion.getValor", parameters);
		H_MIN = new Integer(conf.get(0).getValor());*/
		
	}
		
	public String getValor(String clave){
		log.debug(clave);
		HashMap<String, Object> parameters = new HashMap<>();
		parameters.put("clave", clave);
		List<ConfiguracionDto> conf = session.selectList("py.una.fp.mapper.Configuracion.getValor", parameters);
		return conf.get(0).getValor();
		
	}

	/*public static Integer getH_MAX() {
		return H_MAX;
	}

	public static Integer getH_MIN() {
		return H_MIN;
	}*/
	
	

}

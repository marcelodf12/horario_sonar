package py.una.fp.horario.ejb;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import py.una.fp.horario.bd.DataBaseConfiguration;
import py.una.fp.horario.dto.AsignaturaCarreraDto;
import py.una.fp.horario.dto.AsignaturaDto;
import py.una.fp.horario.dto.DivisionDto;
import py.una.fp.horario.dto.ProfesorDto;

@Stateless
public class AsignaturaEjb {
	
	@EJB
	DataBaseConfiguration dbc;
	
	Logger log = Logger.getLogger(AsignaturaEjb.class);
	
	
	public ArrayList<AsignaturaDto>getAll(String asignatura, String codAsig, Integer periodo, String turno, String dpto,String seccion, Integer offset, String column, Boolean order){
		SqlSession session = dbc.getSession();
		List<AsignaturaDto> list = null;
		HashMap<String, Object> parameters = new HashMap<>();
		parameters.put("asignatura", asignatura);
		parameters.put("cod_asig", codAsig);
		parameters.put("periodo", periodo);
		parameters.put("turno", turno);
		parameters.put("dpto",dpto);
		parameters.put("seccion",seccion);
		parameters.put("offset", offset);
		parameters.put("column", column);
		parameters.put("order", order);
		list = session.selectList("py.una.fp.mapper.Asignatura.getAll", parameters);
		if(list.size()==0)
			return null;
		for(AsignaturaDto a : list){
			parameters.put("id", a.getIdAsignatura());
			List<AsignaturaCarreraDto> c = session.selectList("py.una.fp.mapper.Carrera.getAsignaturaCarreraByAsignatura", parameters);
			for(AsignaturaCarreraDto as: c){
				if(as.getNombre()==null || as.getNombre().trim().compareTo("")==0){
					as.setNombre(a.getAsignatura());
				}
			}
			a.setCarreras((ArrayList<AsignaturaCarreraDto>)c);
		}
		session.close();
		return (ArrayList<AsignaturaDto>) list;
	}
	public ArrayList<AsignaturaDto>getAllRelaciones(Integer id, Boolean coiciden){
		SqlSession session = dbc.getSession();
		List<AsignaturaDto> list = null;
		HashMap<String, Object> parameters = new HashMap<>();
		parameters.put("id", id);
		parameters.put("coiciden", coiciden);
		list = session.selectList("py.una.fp.mapper.Asignatura.getAllRelaciones", parameters);
		session.close();
		if(list.size()==0)
			return null;
		return (ArrayList<AsignaturaDto>) list;
	}
	public ArrayList<AsignaturaDto>getNombre(Integer id){
		SqlSession session = dbc.getSession();
		List<AsignaturaDto> list = null;
		HashMap<String, Object> parameters = new HashMap<>();
		parameters.put("id", id);
		list = session.selectList("py.una.fp.mapper.Asignatura.getNombre", parameters);
		session.close();
		if(list.size()==0)
			return null;
		return (ArrayList<AsignaturaDto>) list;
	}
	public boolean create(AsignaturaDto a){
		//log.debug(new Gson().toJson(a));
		SqlSession session = dbc.getSession();
		boolean success = true;
		try{
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("id_departamento", a.getIdDpto());
			parameters.put("nombre", a.getAsignatura());
			parameters.put("cod_asig", a.getCodAsig());
			parameters.put("turno", a.getTurno());
			parameters.put("periodo", a.getPeriodo());
			parameters.put("seccion", a.getSeccion());
			parameters.put("slots", a.getSlots());
			session.selectList("py.una.fp.mapper.Asignatura.insert", parameters);
		}catch(Exception e){
			success = false;
			log.error("Failed! ",e);
		}finally {
			session.close();
		}
		return success;
	}
	
	public boolean edit(AsignaturaDto a){
		SqlSession session = dbc.getSession();
		boolean success = true;
		try{
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("id_asignatura", a.getIdAsignatura());
			parameters.put("id_departamento", a.getIdDpto());
			parameters.put("nombre", a.getAsignatura());
			parameters.put("cod_asig", a.getCodAsig());
			parameters.put("turno", a.getTurno());
			parameters.put("periodo", a.getPeriodo());
			parameters.put("seccion", a.getSeccion());
			parameters.put("slots", a.getSlots());
			session.selectList("py.una.fp.mapper.Asignatura.update", parameters);
			parameters = new HashMap<>();
			parameters.put("id_asignatura", a.getIdAsignatura());
			session.delete("py.una.fp.mapper.Asignatura.deleteDivisiones", parameters);
			session.delete("py.una.fp.mapper.Asignatura.deleteProfesores", parameters);
			session.delete("py.una.fp.mapper.Asignatura.deleteCarreras", parameters);
			for(DivisionDto d: a.getDivisiones()){
				parameters.put("modo", d.getModo());
				parameters.put("tipoAula", d.getTipoAula());
				parameters.put("hora", d.getHora());
				parameters.put("duracion", d.getDuracion());
				session.selectList("py.una.fp.mapper.Asignatura.insertDivision", parameters);
			}
			for(ProfesorDto p: a.getProfesores()){
				parameters.put("id_profesor", p.getIdProfesor());
				session.selectList("py.una.fp.mapper.Asignatura.insertProfesor", parameters);
			}
			for(AsignaturaCarreraDto c: a.getCarreras()){
				//#{id_asignatura}, #{nombre}, #{semestre}, #{nivel}, #{enfasis}, #{plan}, #{id_carrera}
				parameters.put("nombre", c.getNombre());
				parameters.put("semestre",c.getSemestre());
				parameters.put("nivel", c.getNivel());
				parameters.put("enfasis", c.getEnfasis());
				parameters.put("plan", c.getPlan());
				parameters.put("id_carrera", c.getIdCarrera());
				session.selectList("py.una.fp.mapper.Asignatura.insertCarrera", parameters);
			}
		}catch(Exception e){
			success = false;
			log.error("Failed! ",e);
		}finally {
			session.close();
		}
		return success;
	}
	
	public AsignaturaDto get(Integer idAsignatura){
		SqlSession session = dbc.getSession();
		List<AsignaturaDto> list = null;
		List<DivisionDto> divisiones = null;
		HashMap<String, Object> parameters = new HashMap<>();
		parameters.put("id", idAsignatura);
		list = session.selectList("py.una.fp.mapper.Asignatura.getById", parameters);
		if(list.size()==0)
			return null;
		AsignaturaDto a = list.get(0);
		List<ProfesorDto> p = session.selectList("py.una.fp.mapper.Profesor.getByAsignatura", parameters);
		List<AsignaturaCarreraDto> c = session.selectList("py.una.fp.mapper.Carrera.getAsignaturaCarreraByAsignatura", parameters);
		a.setProfesores((ArrayList<ProfesorDto>)p);
		a.setCarreras((ArrayList<AsignaturaCarreraDto>) c);
		parameters = new HashMap<>();
		parameters.put("id", idAsignatura);
		divisiones = session.selectList("py.una.fp.mapper.Asignatura.getDivisiones", parameters);
		list.get(0).setDivisiones((ArrayList<DivisionDto>)divisiones);
		session.close();
		return list.get(0);
	}
	
	public List<AsignaturaCarreraDto> getAsignaturasDeLaCarrera(Integer idCarrera){
		SqlSession session = dbc.getSession();
		List<AsignaturaCarreraDto> list = null;
				
		HashMap<String, Object> parameters = new HashMap<>();
		parameters.put("idCarrera", idCarrera);
		list = session.selectList("py.una.fp.mapper.AsignaturaCarrera.getAllAsignaturasDeLaCarrera", parameters);
		session.close();
		
		log.debug("LAS MATERIAS SON : "+ list.size());
		
		
		return list;
	}
	
	public List<AsignaturaCarreraDto> getAllAsignaturasParaRecalculo(String nombrePrincipal, String nombre, String carrera, String profesor, Integer offset, String column, Boolean order){
		SqlSession session = dbc.getSession();
		List<AsignaturaCarreraDto> list = null;			

		HashMap<String, Object> parameters = new HashMap<>();
		parameters.put("nombrePrincipal", nombrePrincipal);
		parameters.put("nombre", nombre);
		parameters.put("carrera", carrera);
		parameters.put("nombreProfesor", profesor);
		parameters.put("offset", offset);
		parameters.put("column", column);
		parameters.put("order", order);
		list = session.selectList("py.una.fp.mapper.AsignaturaCarrera.getAll", parameters);
		session.close();
		if(list.size()==0)
			return null;
				
		return list;
	}
	
	public boolean delete(Integer id){
		SqlSession session = dbc.getSession();
		boolean success = true;
		try{
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("id", id);
			session.selectList("py.una.fp.mapper.Asignatura.delete", parameters);
		}catch(Exception e){
			success = false;
			log.error("Failed! ",e);
		}finally {
			session.close();
		}
		return success;
	}
	
	public boolean addGrupo(Integer id1, Integer id2){
		SqlSession session = dbc.getSession();
		boolean success = true;
		try{
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("id_1", id1);
			parameters.put("id_2", id2);
			session.selectList("py.una.fp.mapper.Asignatura.addToGroup", parameters);
		}catch(Exception e){
			success = false;
			log.error("Failed! ",e);
		}finally {
			session.close();
		}
		return success;
	}
	
	public boolean quitar(Integer id1, Integer id2, Boolean coiciden){
		SqlSession session = dbc.getSession();
		boolean success = true;
		try{
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("id_1", id1);
			parameters.put("id_2", id2);
			parameters.put("coiciden", coiciden);
			if(coiciden)
				session.selectList("py.una.fp.mapper.Asignatura.quitarDeGrupo", parameters);
			else
				session.selectList("py.una.fp.mapper.Asignatura.quitarConflicto", parameters);
		}catch(Exception e){
			success = false;
			log.error("Failed! ",e);
		}finally {
			session.close();
		}
		return success;
	}
	public Boolean addConflicto(Integer id1, Integer id2) {
		SqlSession session = dbc.getSession();
		boolean success = true;
		try{
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("id_1", id1);
			parameters.put("id_2", id2);
			session.selectList("py.una.fp.mapper.Asignatura.addConflicto", parameters);
		}catch(Exception e){
			success = false;
			log.error("Failed! ",e);
		}finally {
			session.close();
		}
		return success;
	}

}

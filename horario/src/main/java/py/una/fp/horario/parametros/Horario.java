package py.una.fp.horario.parametros;

import java.util.ArrayList;
import java.util.List;

import py.una.fp.horario.dto.AulaAgrupacionStringDto;
import py.una.fp.horario.dto.AulaDto;

public class Horario {
	
	private ArrayList<Seccion> secciones;
	
	private List<AulaAgrupacionStringDto> agrupacion;
	
	private List<AulaDto> aulas;
	
	private int timeMaxMin;
	
	public ArrayList<Seccion> getSecciones() {
		return secciones;
	}

	public void setSecciones(ArrayList<Seccion> secciones) {
		this.secciones = secciones;
	}
	
	public List<AulaAgrupacionStringDto> getAgrupacion() {
		return agrupacion;
	}

	public void setAgrupacion(List<AulaAgrupacionStringDto> agrupacion) {
		this.agrupacion = agrupacion;
	}

	public int getTimeMaxMin() {
		return timeMaxMin;
	}

	public void setTimeMaxMin(int timeMaxMin) {
		this.timeMaxMin = timeMaxMin;
	}

	public List<AulaDto> getAulas() {
		return aulas;
	}

	public void setAulas(List<AulaDto> aulas) {
		this.aulas = aulas;
	}
	
	
}

package py.una.fp.horario.dto.resultado;

import java.util.ArrayList;

public class HorarioResultadoDto {
	
	public Integer id;
	public Integer idAsignatura;
	public Integer cantidadAlumnos;
	public Boolean nuevo;
	public ArrayList<TipoAulaHorarioResultadoDto> tipos;
}

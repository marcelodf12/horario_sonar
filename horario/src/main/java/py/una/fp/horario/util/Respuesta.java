package py.una.fp.horario.util;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Respuesta<T> {

	private Boolean success;

	private String message;

	private String reason;
	
	private String type;

	private T data;

	public Respuesta() {

	}

	public Respuesta(Boolean success, String message, String reason, String type) {
		this.success = success;
		this.message = message;
		this.reason = reason;
		this.type = type;
	}

	public Respuesta(Boolean success, String message, String reason, String type, T data) {
		this.success = success;
		this.message = message;
		this.reason = reason;
		this.type = type;
		this.data = data;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}

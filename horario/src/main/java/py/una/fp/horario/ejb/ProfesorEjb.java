package py.una.fp.horario.ejb;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import py.una.fp.horario.bd.DataBaseConfiguration;
import py.una.fp.horario.dto.AsignaturaDto;
import py.una.fp.horario.dto.ProfesorDto;
import py.una.fp.horario.dto.SlotDto;

@Stateless
public class ProfesorEjb {
	
	@EJB
	DataBaseConfiguration dbc;
	
	Logger log = Logger.getLogger(ProfesorEjb.class);
	
	public ArrayList<ProfesorDto>getAll(String nombre, String apellido, String cedula, String column, Boolean order, Integer offset){
		SqlSession session = dbc.getSession();
		HashMap<String, Object> parameters = new HashMap<>();
		parameters.put("nombre", nombre);
		parameters.put("apellido", apellido);
		parameters.put("cedula", cedula);
		parameters.put("column", column);
		parameters.put("order", order);
		parameters.put("offset", offset);
		List<ProfesorDto> list = session.selectList("py.una.fp.mapper.Profesor.getAll", parameters);
		session.close();
		if(list.size()==0)
			return null;
		return (ArrayList<ProfesorDto>) list;
	}
	
	public ArrayList<ProfesorDto>getAllSinUsuario(String nombre, String apellido, String cedula, String column, Boolean order, Integer offset){
		SqlSession session = dbc.getSession();
		HashMap<String, Object> parameters = new HashMap<>();
		parameters.put("nombre", nombre);
		parameters.put("apellido", apellido);
		parameters.put("cedula", cedula);
		parameters.put("column", column);
		parameters.put("order", order);
		parameters.put("offset", offset);
		List<ProfesorDto> list = session.selectList("py.una.fp.mapper.Profesor.getAllSinUsuario", parameters);
		session.close();
		if(list.size()==0)
			return null;
		return (ArrayList<ProfesorDto>) list;
	}
	
	public ProfesorDto get(Integer idProfesor) {
		SqlSession session = dbc.getSession();
		HashMap<String, Object> parameters = new HashMap<>();
		parameters.put("idProfesor", idProfesor);
		List<ProfesorDto> list = session.selectList("py.una.fp.mapper.Profesor.getById", parameters);
		ProfesorDto c = list.get(0);
		List<AsignaturaDto> asignaturas = session.selectList("py.una.fp.mapper.Asignatura.getAllByProfesor", parameters);
		c.setAsignaturas(asignaturas);
		session.close();
		return c;
	}

	public Boolean edit(Integer id, ProfesorDto p) {
		SqlSession session = dbc.getSession();
		boolean success = true;
		try{
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("id", id);
			parameters.put("nombre", p.getNombre());
			parameters.put("apellido", p.getApellido());
			parameters.put("titulo", p.getTitulo());
			parameters.put("cedula", p.getCedula());
			parameters.put("celular", p.getCelular());
			parameters.put("correo", p.getCorreo());
			parameters.put("notificar", p.getNotificar());
			session.selectList("py.una.fp.mapper.Profesor.update", parameters);
		}catch(Exception e){
			success = false;
			log.error("Failed! ",e);
		}finally {
			session.close();
		}
		return success;	
	}
	
	public Boolean create(ProfesorDto p) {
		SqlSession session = dbc.getSession();
		boolean success = true;
		try{
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("nombre", p.getNombre());
			parameters.put("apellido", p.getApellido());
			parameters.put("titulo", p.getTitulo());
			parameters.put("cedula", p.getCedula());
			parameters.put("celular", p.getCelular());
			parameters.put("correo", p.getCorreo());
			parameters.put("notificar", p.getNotificar());
			session.selectList("py.una.fp.mapper.Profesor.insert", parameters);
		}catch(Exception e){
			success = false;
			log.error("Failed! ",e);
		}finally {
			session.close();
		}
		return success;	
	}
	
	public boolean delete(Integer id){
		SqlSession session = dbc.getSession();
		boolean success = true;
		try{
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("id", id);
			session.selectList("py.una.fp.mapper.Profesor.delete", parameters);
		}catch(Exception e){
			success = false;
			log.error("Failed! ",e);
		}finally {
			session.close();
		}
		return success;
	}
	
	public ProfesorDto getSlot(Integer idProfesor, Integer idSlot) {
		SqlSession session = dbc.getSession();
		ProfesorDto p = null;
		try{
			HashMap<String, Object> parameters = new HashMap<>();
			List<ProfesorDto> list = null;
			if(idSlot != -1){
				parameters.put("idSlotProfesor", idSlot);
				list = session.selectList("py.una.fp.mapper.Profesor.getSlot", parameters);
			}else{
				parameters.put("idProfesor", idProfesor);
				list = session.selectList("py.una.fp.mapper.Profesor.getById", parameters);
			}
			if(list.size()>0)
				p = list.get(0);
		}catch(Exception e){
			log.error("Failed! ",e);
		}finally {
			session.close();
		}
		return p;
	}
	
	public ArrayList<SlotDto>getAllSlot(Integer idProfesor){
		SqlSession session = dbc.getSession();
		HashMap<String, Object> parameters = new HashMap<>();
		parameters.put("idProfesor", idProfesor);
		List<SlotDto> list = session.selectList("py.una.fp.mapper.Profesor.getAllSlot", parameters);
		session.close();
		if(list.size()==0)
			return null;
		return (ArrayList<SlotDto>) list;
	}
	
	public Boolean insertSlot(String slot, Integer idUsuario, Integer idProfesor) {
		SqlSession session = dbc.getSession();
		boolean success = true;
		try{
			HashMap<String, Object> parameters = new HashMap<>();
			parameters.put("slot", slot);
			parameters.put("idUsuario", idUsuario);
			parameters.put("idProfesor", idProfesor);
			session.selectList("py.una.fp.mapper.Profesor.insertSlot", parameters);
		}catch(Exception e){
			success = false;
			log.error("Failed! ",e);
		}finally {
			session.close();
		}
		return success;	
	}
	

}

package py.una.fp.horario.reports.core;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import py.una.fp.horario.reports.exceptions.JasperPOJOReportException;

public class ReportGenerator<T> {
	
	Logger log = Logger.getLogger(ReportGenerator.class);

	private JasperPrint generateJasper(String reportName, Map<String,Object> parameters, List<T> datasource) throws JRException {
		log.debug("Nombre del reporte: " + reportName);
		String reportSource = getClass().getClassLoader().getResource("reports/"+reportName+".jrxml").getPath();
		log.debug("Generando reporte desde la plantilla: " + reportSource);
		JasperDesign jd = JRXmlLoader.load(reportSource);
		JasperReport report = JasperCompileManager.compileReport(jd);
		JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(datasource);
		JasperPrint print = JasperFillManager.fillReport(report, parameters, ds);
		return print;
	}

	public byte[] toPDF(String reportName, Map<String,Object> parameters, List<T> datasource) throws JRException, JasperPOJOReportException {
		JasperPrint print = generateJasper(reportName, parameters, datasource);
		return ReportExporter.exportarParaPDF(print);

	}

}

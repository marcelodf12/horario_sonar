package py.una.fp.horario.bd;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;


@Singleton
@Startup
public class DataBaseConfiguration {

	private String configFile;
	
	private SqlSessionFactory sqlSessionFactory;
	
	@PostConstruct
	void init()
	{
		System.out.println("Inicio");
		configFile = "mybatis" + File.separator + "mybatis.xml";
		InputStream inputStream = null;
		try {
			inputStream = Resources.getResourceAsStream(configFile);
			sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);	
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("ERROR**********");
		}
		
	}
	
	public SqlSession getSession(){
		return sqlSessionFactory.openSession();
		
	}

}

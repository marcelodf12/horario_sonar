package py.una.fp.horario.dto;

import java.util.ArrayList;
import java.util.List;

public class AsignaturasPorSemestreDto {

	
	private String nombre = "";
	
	private List<AsignaturaCarreraDto> asignaturas = new ArrayList<AsignaturaCarreraDto>();
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<AsignaturaCarreraDto> getAsignaturas() {
		return asignaturas;
	}

	public void setAsignaturas(List<AsignaturaCarreraDto> asignaturas) {
		this.asignaturas = asignaturas;
	}
}

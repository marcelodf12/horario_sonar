package py.una.fp.horario.rest;

import java.io.File;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import org.apache.log4j.Logger;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import py.una.fp.horario.dto.QueryReportDto;
import py.una.fp.horario.ejb.ReportesEjb;
import py.una.fp.horario.secured.Secured;
import py.una.fp.horario.util.FileStreamingOutput;
import py.una.fp.horario.util.Mensaje;
import py.una.fp.horario.util.Respuesta;

@Stateless
@Path("/reportes")
@Api(
	    value = "/reportes", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON
	)
public class ReportesRest {

	Logger log = Logger.getLogger(ReportesRest.class);
	
	@EJB
	ReportesEjb reportesEjb;
	
	@GET
	@Path("/queries")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(perfil="1")
	@ApiOperation(
	        value = "Listar identificadores de Reportes",
	        notes = "Servicio para listar todos los identenficadores posibles para quitar reportes"
	    )
	public Respuesta<QueryReportDto> queryReport(){
		String mensaje="";
		Boolean success = true;
		String reason = Mensaje.CONSULTA_EXITOSA;
		String type = "success";
		QueryReportDto data = null;
		try {
			data = reportesEjb.getQueryReports();	
		} catch (Exception e) {
			log.error("Failed! ",e);
			mensaje = Mensaje.ERROR_DATA_BASE;
			reason = e.getCause().getMessage();
			success = false;
			type = "error";
		}
		Respuesta<QueryReportDto> r = new Respuesta<QueryReportDto>(success, mensaje, reason, type, data);
		return r;
	}
	
	@GET
	@Path("/horario/{id:[0-9][0-9]*}")
	@Produces("application/pdf")
	@ApiOperation(
	        value = "Descargar Reporte",
	        notes = "Servicio para descargar reportes"
	    )
	public StreamingOutput getFile(@QueryParam("query") QueryReportDto query, @PathParam("id") Integer idHorario) {
	  String pdfFileName = reportesEjb.getReports(query, idHorario);
	  
	  if (pdfFileName == null)
	        throw new WebApplicationException(Response.Status.BAD_REQUEST);
	    if (!pdfFileName.endsWith(".pdf")) pdfFileName = pdfFileName + ".pdf";

	    File pdf = new File(pdfFileName);
	    if (!pdf.exists())
	        throw new WebApplicationException(Response.Status.NOT_FOUND);

	    log.debug("Tamaño del archivo: " + pdf.length());
	    
	    return new FileStreamingOutput(pdf);
	}
	
	@POST
	@Path("/generar/{id:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(perfil="1")
	@ApiOperation(
	        value = "Actualizar Reportes",
	        notes = "Servicio para lanzar proceso de actualización de reportes"
	    )
	public Respuesta<Boolean> generarReports(@PathParam("id") Integer idHorario){
		String mensaje="";
		Boolean success = true;
		String reason = Mensaje.CONSULTA_EXITOSA;
		String type = "success";
		Boolean data = null;
		Long p = null;
		try {
			p = reportesEjb.getReportProcess();
			data = p==null;
			if(data) {
				reportesEjb.generateAllReports(idHorario);
			}else{
				success = false;
				reason = "Existen reportes generandonse";
				mensaje = "No puede lanzar procesos de reportes simultaneamente. El progreso del proceso actual es del " + p + "%.";
				type = "error";
			}
		} catch (Exception e) {
			log.error("Failed! ",e);
			mensaje = Mensaje.ERROR_DATA_BASE;
			reason = e.getCause().getMessage();
			success = false;
			type = "error";
		}
		Respuesta<Boolean> r = new Respuesta<Boolean>(success, mensaje, reason, type, data);
		return r;
	}
	
	@GET
	@Path("/progreso")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(perfil="1")
	@ApiOperation(
	        value = "Consultar progreso de actualización",
	        notes = "Servicio para consultar el progreso del proceso de actualización de reportes"
	    )
	public Respuesta<Long> getProgreso(){
		String mensaje="";
		Boolean success = true;
		String reason = Mensaje.CONSULTA_EXITOSA;
		String type = "success";
		Long data = null;
		try {
			data = reportesEjb.getReportProcess();	
		} catch (Exception e) {
			log.error("Failed! ",e);
			mensaje = Mensaje.ERROR_DATA_BASE;
			reason = e.getCause().getMessage();
			success = false;
			type = "error";
		}
		Respuesta<Long> r = new Respuesta<Long>(success, mensaje, reason, type, data);
		return r;
	}
	
}

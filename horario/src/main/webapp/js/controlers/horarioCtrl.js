var fp_app = angular.module("app");

fp_app.controller('horarioCtrl', ['$scope','HorarioService','mensajes','ngDialog','$location','BorradorService','$interval'
                         ,function($scope , HorarioService , mensajes , ngDialog , $location , BorradorService , $interval) {
	
	$scope.$parent.nuevaRuta("horarios", $location.url(), "Horarios", true);

	$scope.search_aux = {};
	var fecha = new Date();
	$scope.search_aux.a = fecha.getFullYear();
	
	$scope.generando = false;

	$scope.estados = [];
	$scope.estados['B']='Borrador';
	$scope.estados['E']='Asignando Horario';
	$scope.estados['S']='Sin Aulas';
	$scope.estados['C']='Asignando Aulas';
	$scope.estados['L']='Listo';
	$scope.estados['H']='Historico';
	$scope.estados['P']='Papelera';
	$scope.esperando = false;
	
	v = [];
	// B Borrador
	v['B&calcular']=1; v['B&configurar']=1;v['B&eliminar']=1;

	// E Espera
	v['E&refresh']=1;	

	// S Sin cantidad de alumnos
	v['S&ver']=1; v['S&descartar']=1;v['S&publicar']=1;v['S&recalcular']=1; v['S&examenes']=1;v['S&cargarAlumnos']=1;v['S&calcularAulas']=1;
	
	// C Calculando
	v['C&refresh']=1;
	
	// L Listo
	v['L&ver']=1; v['L&descartar']=1;v['L&publicar']=1;v['L&recalcular']=1;v['L&examenes']=1;
	
	// P Papelera
	v['P&restaurar']=1;v['P&eliminar']=1;
	
	// H Historico
	v['H&restaurar']=1;v['H&eliminar']=1;

//funciones auxiliares
	$scope.setIdHorarioRestaurar= function(index){
		$scope.idHorarioRestaurar = index;
		$scope.restaurarEstado = $scope.horarios[index].estado=='P'?$scope.estados['S']:$scope.estados['L'];
	};
	$scope.setIdHorarioEliminar= function(index){
		$scope.idHorarioEliminar = index
	};
	$scope.setIdHorarioDescartar= function(index){
		$scope.idHorarioDescartar = index
	};
// fin funciones
	
	$scope.iconos = function(estado, accion){
		permiso = false;
		try {
			permiso = v[estado+'&'+accion]==1;
		}catch(err) {/*pasar*/}
		return permiso;
	}
	
	$scope.filtrar = function(){
		$scope.horarios = [];
		$scope.esperando = true;
		$scope.error = false;
		if(!($scope.search_aux.a>0)){
			var fecha = new Date();
			$scope.search_aux.a = fecha.getFullYear();
		}
			HorarioService.getHorarios($scope.search_aux.a, function(response,status){
				$scope.esperando = false;
				if(response.success && status==200){
					$scope.horarios = response.data;
					$('[data-toggle="tooltip"]').tooltip()
				}else{
					$scope.error = true;
				}
			})
	};
	
	$scope.verPapelera = function(){
		$scope.horarios = [];
		$scope.esperando = true;
		$scope.error = false;
		HorarioService.getPapelera(function(response,status){
			$scope.esperando = false;
			if(response.success && status==200){
				$scope.horarios = response.data;
			}else{
				$scope.error = true;
			}
		});
	};
	
	$scope.ver = function(index){
		$location.path('horarioVer/' + $scope.horarios[index].idHorario);
	}
	
	$scope.calcAulas = function(index){
		$scope.idHorarioAula = index;
	}
	
	$scope.examenes = function(index){
		$location.path('examenes/horario/' + $scope.horarios[index].idHorario);
	}
	
	$scope.cantidadAlumnos = function(index){
		$location.path('horario/inscriptos/' + $scope.horarios[index].idHorario);
	}
	
	$scope.refresh = function(index){
		$scope.filtrar();
	}
	
	$scope.refresh_horario = $interval($scope.refresh, 20000);
	
	$scope.$on('$destroy', function() {
		console.log("Destruyendo interval horario");
		$interval.cancel($scope.refresh_horario);
      });
	
	
	$scope.errorSinAsignar = false;
	$scope.calcular = function(index, algoritmo){
		$scope.generando = true;
		$scope.errorSinAsignar = false;
		HorarioService.calcular($scope.horarios[index].idHorario, algoritmo, function(response,status){
			$scope.generando = false;
			if(status==200 && response.success){
				$scope.filtrar();
    		}else if(status==200){
				$scope.errorSinAsignar = true && algoritmo!='CA';
				$scope.asignaturasSinAsignar = response.data;
			}
		});
	}
	
	$scope.eliminar = function(index){
		  $scope.title_modal = "Eliminar Horario";
		  $scope.message_modal = "Está seguro que desea eliminar el Horario " + $scope.horarios[index].idHorario + ". Esta acción no puede revertirse";
		  $scope.id_horario = $scope.horarios[index].idHorario
		  try{
	        	HorarioService.eliminar($scope.id_horario, function(response,status){
	    			if(status==200){
	    				$scope.filtrar();
	        		}
	    			solicitarPagina(1);
	    		});
		  }
		  catch(err){
			  console.log($scope);
			  console.log(err);
		  };
	}
	
	$scope.descartar = function(index){
		$scope.generando = true;
		HorarioService.descartar($scope.horarios[index].idHorario, function(response,status){
			$scope.generando = false;
			if(status==200 && response.success){
				$scope.filtrar();
    		}else if(status==200){
				$scope.errorSinAsignar = true;
				$scope.asignaturasSinAsignar = response.data;
			}
		});		
	}
	
	$scope.verAulas = function(index){
		if($scope.horarios[index].estado == "P")
			$location.path('/verAulas/' + $scope.horarios[index].idHorario + '/0');
	}
	
	$scope.recalcular = function(index){
		idHorario = $scope.horarios[index].idHorario
		$location.path('/recalculo/'+idHorario);
	}
	
	$scope.configurar = function(index){
		if($scope.horarios[index].estado == "B")
			$location.path('/borrador/' + $scope.horarios[index].idHorario);
	}
	
	$scope.publicar = function(index){
		$scope.generando = true;
		HorarioService.publicar($scope.horarios[index].idHorario, function(response,status){
			$scope.generando = false;
			if(status==200 && response.success){
				$scope.filtrar();
    		}else if(status==200){
				$scope.errorSinAsignar = true;
				$scope.asignaturasSinAsignar = response.data;
			}
		});
	};
	
	$scope.restaurar = function(index){
		$scope.generando = true;
		HorarioService.restaurar($scope.horarios[index].idHorario, function(response,status){
			$scope.generando = false;
			if(status==200 && response.success){
				$scope.filtrar();
    		}else if(status==200){
				$scope.errorSinAsignar = true;
				$scope.asignaturasSinAsignar = response.data;
			}
		});
	};
	
	$scope.fecha = function timeConverter(UNIX_timestamp){
		  var a = new Date(UNIX_timestamp);
		  var months = ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
		  var year = a.getFullYear();
		  var month = months[a.getMonth()];
		  var date = a.getDate();
		  var hour = a.getHours();
		  var min = a.getMinutes();
		  var sec = a.getSeconds();
		  var time = date + ' ' + month + ' ' + year;
		  return time;
		}
	
	$scope.filtrar();
	
	// Generar Borrador
	$scope.nuevoHorario = function(){
		$scope.borrador = {'anho':$scope.search_aux.a, 'periodo':1};
		$scope.title_modal = "Generar Nuevo Horario";
	};
	
	$scope.generarBorrador = function(){
		//console.log($scope.borrador);
		var form = document.getElementById("horarioAdmModal");
		
		if($scope.borrador.anho == null || $scope.borradoranho < 0 || $scope.borrador.periodo == null  || $scope.borrador.periodo < 0){
			  form.classList.add("was-validated");
		} else {
			BorradorService.nuevo($scope.borrador, function(response, status){
				if(status==200){
					$scope.filtrar();
					$('#horarioAdmModal').modal('hide'); // cerramos el modal si esta todo OK
				}
			});
		}
	};
	// Fin Generar Borrador
	
}]);
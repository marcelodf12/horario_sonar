var fp_app = angular.module("app");

fp_app.controller('cantidadAlumnosCtrl', ['$scope','ngDialog','$location','$routeParams','toaster','mensajes','HorarioService','UtilService'
                         , function($scope , ngDialog , $location , $routeParams , toaster , mensajes , HorarioService , UtilService) {
	
	
	if(typeof($routeParams.id)!='undefined'){
		$scope.idHorario = $routeParams.id.toString();
	}else{
		$scope.idHorario = "";
	}
	
	$scope.$parent.nuevaRuta("cant_alumnos", $location.url(), "Cargar Inscriptos", false);
	
// PAGINACION y FILTRADO
	$scope.currentPage = 0;
	$scope.pageSize = 20;
	$scope.anterior = function(){
		$scope.currentPage = $scope.currentPage==0?0:$scope.currentPage-1;
	}
	$scope.siguiente = function(){
		$scope.currentPage = $scope.currentPage+1;
	}
	$scope.setPageSize = function(n){
		$scope.pageSize = n;
	}
	$scope.verExa = false;
	$scope.cambiarVistaExa = function(){
		$scope.verExa = !$scope.verExa;
	};
	$scope.search_aux = {};
	$scope.filtrar = function(){
		$scope.currentPage = 0;
		$scope.search = angular.fromJson(angular.toJson($scope.search_aux));;
	};
	// Para redondear en la vista
	$scope.redondeoMax = window.Math.ceil;
// FIN PAGINACION y FILTRADO
	
	$scope.actualizar = function(){
		$scope.esperando = true;
		$scope.error = false;
		HorarioService.getExamenes($scope.idHorario, function(data, status){
			if(data.success){
				$scope.examenes = data.data;
				$scope.cancelar();
			}else{
				$scope.error = true;
				
			}
			$scope.esperando = false;
		});
	};
	$scope.actualizar();
	
// OPERACIONES
	$scope.cancelar= function(){
		$scope.examenes.forEach(function(item, index){
			item.ca_aux = item.ca;
		});
	};
	
	// levantar archivo
	$scope.confirmUpload = function(){
		HorarioService.cargarAulas($scope.inscriptosFile,$scope.idHorario, function(response,status){
			console.log("Subiendo archivo.");
		});
	};
	
	$scope.updateCantidadAlumnos = function(e){
		$scope.esperando = true;
		$scope.error = false;
		$scope.update = [];
		$scope.examenes.forEach(function(item, index){
			if(!(item.ca == item.ca_aux))
				$scope.update.push({ca:item.ca_aux, id:item.id+""});
		});
		HorarioService.updateCantidadAlumnos($scope.update, function(data, status){
			if(data.success){
				$scope.actualizar();
			}else{
				$scope.cancelar();
				
			}
			$scope.esperando = false;
		});
	}
// FIN OPERACIONES

}]);


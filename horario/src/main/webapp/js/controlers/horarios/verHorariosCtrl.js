var fp_app = angular.module("app");
fp_app.controller('verHorarioCtrl', ['$scope','$http','$routeParams','HorarioService','toaster','$location','UtilService'
                           , function($scope , $http , $routeParams , HorarioService , toaster , $location , UtilService) {
	
	if(typeof($routeParams.id)!='undefined'){
		$scope.idHorario = parseInt($routeParams.id.toString());
	}else{
		$scope.idHorario = -1;
	}
	
	$scope.$parent.nuevaRuta("horario", $location.url(), "Ver Horario", false);
	
	$scope.search_aux = {};
	
	//Prototipo para eliminar elementos duplicados de un array
	Array.prototype.unique=function(a){
		  return function(){return this.filter(a)}}(function(a,b,c){return c.indexOf(a,b+1)<0
		});
	//fin prototipp
	
	$scope.verExa = false;
	$scope.cambiarVistaExa = function(){
		$scope.verExa = !$scope.verExa;
	};
	
	$scope.currentPage = 0;
	$scope.pageSize = 10;
	
	$scope.setPageSize = function(n){
		$scope.pageSize = n;
	}
	
	$scope.anterior = function(){
		$scope.currentPage = $scope.currentPage==0?0:$scope.currentPage-1;
	}
	$scope.siguiente = function(){
		$scope.currentPage = $scope.currentPage+1;
	}
	
	
	$scope.actualizarHorario = function(){
		HorarioService.getHorarioEspecifico($scope.idHorario, function(response, status){
			if(response.success==true && status==200){
				$scope.horario = response.data;
				$scope.carreras = [];
				$scope.carreras.push("");
				for (i = 0; i < response.data.length; ++i) {
					$scope.carreras.push(response.data[i].c);
				}
				$scope.carreras = $scope.carreras.unique();
			}else{
				toaster.pop({	type : "error",
					title : "Error",
					body : response.message
					});
				if(status==404){
					$location.path('horarios/');
				}
			}
	
		});
	};
	
	$scope.actualizarHorario();
	
	$scope.filtrar = function(){
		$scope.currentPage = 0;
		$scope.search = angular.fromJson(angular.toJson($scope.search_aux));;
	};
	$scope.mostrarSeleccionados = function(){
		$scope.search = {selected : true}
	};
	
	$scope.conflictosResultado=[];
	$scope.conflictos = false;
	$scope.mostrarConflictos= function(){
		HorarioService.getConflictos($scope.idHorario, function(response, status){
			if(response.success==true && status==200){
				$scope.conflictosResultado = response.data;
				for (var k = 0; k < response.data.length; k++) {
					s1_s=+response.data[k].startSlot1;
					s1_e=+response.data[k].endSlot1;
					s2_s=+response.data[k].startSlot2;
					s2_e=+response.data[k].endSlot2;
					time = (Math.abs((s2_s>s1_e? s2_e-s1_s+1:s1_e-s2_s+1)*5))*60;
					var hours = ('0' + (Math.floor( time / 3600 ))).slice(-2).toString();  
					var minutes = ('0' + (Math.floor( (time % 3600) / 60 ))).slice(-2).toString();
					var result = hours + ":" + minutes;
					response.data[k].min = result;
				}
				
				$scope.conflictos = true;
			}
		});
		
	};
	$scope.refresh= function(){
		HorarioService.cleanCache();
		$scope.actualizarHorario();
	};
	
	$scope.downloadExcel = function(){
		filename = 'Horario ID='+$scope.idHorario+' ' + UtilService.fecha(new Date().getTime());
		return UtilService.tableToExcel()(document.getElementById('tableForExcel'),'Horario',filename );
		
	}
	
	$scope.toReportes = function(){
		$location.path('horarios/'+$routeParams.id+'/reportes');
	}
	
	// Para redondear en la vista
	$scope.redondeoMax = window.Math.ceil;
	
}]);



fp_app.filter('slice', function() {
	  return function(arr, start, end) {
	    return (arr || []).slice(start, end);
	  };
	});


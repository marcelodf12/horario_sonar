var fp_app = angular.module("app");

fp_app.controller('recalculoCtrl', ['$scope','$http','$routeParams','AsignaturaService','toaster','$location','HorarioService'
                                   , function($scope , $http , $routeParams , AsignaturaService , toaster , $location,HorarioService) {
	
	if(typeof($routeParams.id)!='undefined'){
		$scope.idHorario = $routeParams.id.toString();
	}else{
		$scope.idHorario = "";
		$location.path("/404");
	}
		
	$scope.$parent.nuevaRuta("recalculo", $location.url(), "Recalculo", false);
	
	$scope.search_aux = {};
	
	$scope.asignaturas;
	$scope.asignaturasRecalcular;
	
	
	$scope.busqueda_codigo;
	$scope.busqueda_nombre_principal;
	$scope.busqueda_dpto;
	$scope.busqueda_seccion;
	
	$scope.buscar = function(nombre, dpto, codigo, periodo){
		$scope.busqueda_nombre_principal= nombre;
		$scope.busqueda_codigo = codigo;
		$scope.busqueda_dpto = dpto;
		$scope.busqueda_periodo = periodo;
		$scope.page = 1;
		solicitarPagina($scope.page);
	};
	
	$scope.calcular = function(algoritmo){
		if($scope.asignaturasRecalcular.length>0){
			$scope.generando = true;
			$scope.errorSinAsignar = false;
			asignaturas = [];
			for(i=0; i<$scope.asignaturasRecalcular.length; i++){
				asignaturas.push($scope.asignaturasRecalcular[i].idAsignatura);
			}
			console.log(asignaturas);
			HorarioService.reCalcular($scope.idHorario, algoritmo, asignaturas, function(response,status){
				$scope.generando = false;
				if(status==200 && response.success){
					toaster.pop({	type : response.type,
									title : response.reason,
									body : response.message
					});
	    		}else if(status==200){
	    			toaster.pop({	type : response.type,
						title : response.reason,
						body : response.message
	    			});
					$scope.errorSinAsignar = true;
					$scope.asignaturasSinAsignar = response.data;
				}else{
					toaster.pop({	type : "error",
						title : "Error",
						body : mensajes.errorConexion
					});
				}
				$location.path("/horarios/");
			});
		}
	};
	
	solicitarPagina = function(pagina){
		$scope.asignaturas = [];
		$scope.esperando = true;
		$scope.error = false;
		$scope.vacio = false;
		$scope.ultima = false;
		offset = ((pagina-1)*5);
		
		//servicesAsignaturas.list = function (offset, asignatura, codAsig, turno, dpto, seccion, periodo, column, order, callbackFunction) {
		AsignaturaService.list(offset.toString(),$scope.busqueda_nombre_principal ,$scope.busqueda_codigo,$scope.busqueda_turno,$scope.busqueda_dpto,$scope.busqueda_seccion,$scope.busqueda_periodo , $scope.order, $scope.orderAsc, function(response,status){
			
			if(status==200 && response.success){
				$scope.asignaturas = response.data;
				$scope.ultima = response.message == "ultima";
				$scope.esperando = false;
				if(response.data.length==0)
					$scope.vacio = true;

			}else{
				$scope.page = 1;
				$scope.esperando = false;
				$scope.error = true;
			}
			if(status==200){
				if(!response.success)
					toaster.pop({	type : response.type,
									title : response.reason,
									body : response.message
					});
			}else{
				toaster.pop({	type : "error",
								title : "Error",
								body : mensajes.errorConexion
				});
			}
		});
	};
	
	
	$scope.anterior = function(){
		$scope.page = $scope.page>1?$scope.page-1:1;
		solicitarPagina($scope.page);
	};
	
	$scope.siguiente = function(){
		$scope.page = $scope.ultima?$scope.page:$scope.page+1;
		solicitarPagina($scope.page);
	};

	
	$scope.selectedAsignatura = function(index){
		esta = false
		if($scope.asignaturasRecalcular==null)
			$scope.asignaturasRecalcular = [];
		for(i=0; i < $scope.asignaturasRecalcular.length ; i++){
			if($scope.asignaturasRecalcular[i].idAsignatura == $scope.asignaturas[index].idAsignatura){
				esta=true;
				break;
			}
		}
		if(!esta){
			$scope.asignaturasRecalcular.push(JSON.parse(JSON.stringify($scope.asignaturas[index])));
			//limpiarFiltros();
		}
	};	
	
	limpiarFiltros = function(){
		$scope.asignaturas = [];
		$scope.busqueda_codigo = "";
		$scope.busqueda_nombre_principal = "";
		$scope.busqueda_dpto = "";
		$scope.busqueda_seccion = "";
		$scope.busqueda_turno = "";
		$scope.busqueda_periodo = "";
	}
	
	$scope.eliminarAsig = function(index){
		$scope.asignaturasRecalcular.splice(index, 1);
	};
	
	
}]);

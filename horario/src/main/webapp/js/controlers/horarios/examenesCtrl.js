var fp_app = angular.module("app");

fp_app.controller('examenesCtrl', ['$scope','ngDialog','$location','$routeParams','toaster','mensajes','HorarioService','UtilService'
                         , function($scope , ngDialog , $location , $routeParams , toaster , mensajes , HorarioService , UtilService) {
	
	
	if(typeof($routeParams.id)!='undefined'){
		$scope.idHorario = $routeParams.id.toString();
	}else{
		$scope.idHorario = "";
	}
	
	$scope.$parent.nuevaRuta("examenes", $location.url(), "Exámenes", false);
	
// PAGINACION y FILTRADO
	$scope.currentPage = 0;
	$scope.pageSize = 20;
	$scope.anterior = function(){
		$scope.currentPage = $scope.currentPage==0?0:$scope.currentPage-1;
	}
	$scope.siguiente = function(){
		$scope.currentPage = $scope.currentPage+1;
	}
	$scope.setPageSize = function(n){
		$scope.pageSize = n;
	}
	$scope.verExa = false;
	$scope.cambiarVistaExa = function(){
		$scope.verExa = !$scope.verExa;
	};
	$scope.search_aux = {};
	$scope.filtrar = function(){
		$scope.currentPage = 0;
		$scope.search = angular.fromJson(angular.toJson($scope.search_aux));;
	};
	// Para redondear en la vista
	$scope.redondeoMax = window.Math.ceil;
// FIN PAGINACION y FILTRADO
	
	$scope.esperando = true;
	$scope.error = false;
	HorarioService.getExamenes($scope.idHorario, function(data, status){
		if(data.success){
			$scope.examenes = data.data;
		}else{
			$scope.error = true;
			
		}
		$scope.esperando = false;
	});
	
// OPERACIONES
	$scope.edit = function(e){
		e.p1_bk = e.p1;
		e.p2_bk = e.p2;
		e.f1_bk = e.f1;
		e.f2_bk = e.f2;
		e.edit = true;
	};
	$scope.erase = function(e){
		e.p1 = null;
		e.p2 = null;
		e.f1 = null;
		e.f2 = null;
		updateExamenes(e);
	};
	$scope.save = function(e){
		updateExamenes(e);
	};
	$scope.cancel = function(e){
		e.p1 = e.p1_bk;
		e.p2 = e.p2_bk;
		e.f1 = e.f1_bk;
		e.f2 = e.f2_bk;
		e.edit = false;
		
	};
	updateExamenes = function(e){
		$scope.esperando = true;
		$scope.error = false;
		HorarioService.updateExamenes(e , e.id, function(data, status){
			if(data.success){
				e.edit = false;
			}else{
				$scope.cancel(e);
				
			}
			toaster.pop({	type : data.type,
				title : data.reason,
				body : data.message
			});
			$scope.esperando = false;
		});
	}
// FIN OPERACIONES
	
}]);


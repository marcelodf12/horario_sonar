var fp_app = angular.module("app");

fp_app.controller('verAulasCtrl', ['$scope','$http','$routeParams','HorarioService','toaster','$location','UtilService'
                                   , function($scope , $http , $routeParams , HorarioService , toaster , $location, UtilService) {
	
	if(typeof($routeParams.id)!='undefined'){
		$scope.idHorario = parseInt($routeParams.id.toString());
	}else{
		$scope.idHorario = -1;
	}
	if(typeof($routeParams.dia)!='undefined'){
		$scope.dia = parseInt($routeParams.dia.toString());
	}else{
		$scope.dia = 0;
	}
	
	$scope.$parent.nuevaRuta("aulas", $location.url(), "Ver Aulas", false);
	
	$scope.search_aux = {};
	
	//Prototipo para eliminar elementos duplicados de un array
	Array.prototype.unique=function(a){
		  return function(){return this.filter(a)}}(function(a,b,c){return c.indexOf(a,b+1)<0
		});
	//fin prototipp
	
	$scope.verDia = function(dia){
		$location.path('/verAulas/'+$scope.idHorario+'/'+dia);
	}
	

	
	HorarioService.getHorarioEspecifico($scope.idHorario, function(response, status){
			if(response.success==true && status==200){
				$scope.horario = response.data;
				  google.charts.load("current", {packages:["timeline"]});
				  google.charts.setOnLoadCallback(drawChart);
				  function drawChart() {
				    var container = document.getElementById('example2.1');
				    var chart = new google.visualization.Timeline(container);
				    var dataTable = new google.visualization.DataTable();

				    dataTable.addColumn({ type: 'string', id: 'Term' });
				    dataTable.addColumn({ type: 'string', id: 'Name' });
				    dataTable.addColumn({ type: 'date', id: 'Start' });
				    dataTable.addColumn({ type: 'date', id: 'End' });

				    rows = [];
				    for (i = 0; i < response.data.length; ++i) {
						var obj = $scope.horario[i].slots;
						for (k = 0; k < obj.length; ++k) {
							h=obj[k].hora.split(" ");
							a=obj[k].aula;
							if(a.trim()!="" && obj[k].dia == UtilService.diasComboAbr()[$scope.dia]){
								rows.push([a , $scope.horario[i].a,
								           new Date(0, 0, 0, h[0].split(":")[0],h[0].split(":")[1]),
								           new Date(0, 0, 0, h[1].split(":")[0],h[1].split(":")[1]) ]);
							}
						}
					}
				    dataTable.addRows(rows);
				    /*dataTable.addRows([
				      [ '1', 'George Washington', new Date(1789, 3, 30), new Date(1797, 2, 4) ],
				      [ '2', 'John Adams',        new Date(1797, 2, 4),  new Date(1801, 2, 4) ],
				      [ '3', 'Thomas Jefferson',  new Date(1801, 2, 4),  new Date(1809, 2, 4) ]]);*/

				    chart.draw(dataTable);
				  }
				
			}else{
				toaster.pop({	type : "error",
					title : "Error",
					body : response.message
					});
				if(status==404){
					$location.path('horarios/');
				}
			}

	});
	
	
	
	
	
	
	
	

	
	
}]);

var fp_app = angular.module("app");

fp_app.controller('editHorarioCtrl', ['$scope','BorradorService','$location','toaster','mensajes','ngDialog','$routeParams'
                            ,function( $scope , BorradorService , $location , toaster , mensajes , ngDialog , $routeParams) {
	
	if(typeof($routeParams.id)!='undefined'){
		$scope.idBorrador = $routeParams.id.toString();
	}else{
		$scope.idBorrador = "";
	}
	$scope.$parent.nuevaRuta("horario", $location.url(), "Configurar Borrador", false);
	
	$scope.mayorCero = function(e){return e.cantidadAlumnos>0};
	
	$scope.borrador = [];
	$scope.page = 1;
	$scope.ultima = false;
	busqueda_carrera = "";
	busqueda_dpto = "";
	busqueda_asignatura = "";
	busqueda_profesor = "";
	$scope.esperando = false;
	$scope.error = false;
	$scope.vacio = false;

	
	solicitarPagina = function(pagina){
		$scope.borrador = [];
		$scope.esperando = true;
		$scope.error = false;
		$scope.vacio = false;
		$scope.carrera = busqueda_carrera;
		$scope.dpto = busqueda_dpto;
		$scope.asignatura = busqueda_asignatura;
		$scope.profesor = busqueda_profesor;
		offset = ((pagina-1)*25);
					//function (id, carrera, dpto,  asignatura, profesor, order, column, offset, callbackFunction)
		BorradorService.list($scope.idBorrador, busqueda_carrera, busqueda_dpto, busqueda_asignatura, busqueda_profesor, true, 'asignatura', offset, function(response,status){
			if(status==200 && response.success){
				$scope.borrador = response.data;
				$scope.ultima = response.message == "ultima";
				$scope.esperando = false;
				if(response.data.length==0)
					$scope.vacio = true;
			}else{
				$scope.page = 1;
				$scope.esperando = false;
				$scope.error = true;
			}
			if(status==200){
				if(!response.success)
					toaster.pop({	type : response.type,
									title : response.reason,
									body : response.message
					});
			}else{
				toaster.pop({	type : "error",
								title : "Error",
								body : mensajes.errorConexion
				});
			}
		});
	};
	
	$scope.anterior = function(){
		$scope.page = $scope.page>1?$scope.page-1:1;
		solicitarPagina($scope.page);
	}
	
	$scope.siguiente = function(){
		$scope.page = $scope.ultima?$scope.page:$scope.page+1;
		solicitarPagina($scope.page);
	}
	
	$scope.buscar = function(){
		busqueda_carrera = $scope.carrera;
		busqueda_asignatura = $scope.asignatura;
		busqueda_profesor = $scope.profesor;
		busqueda_dpto = $scope.dpto;
		$scope.page = 1;
		solicitarPagina($scope.page);
	}

	solicitarPagina(1);
	
	$scope.secciones_auxiliar = [];
	  
	$scope.guardar = function(index, id,selected,def) {
	  $scope.borrador[index].selected=selected;
	  $scope.borrador[index].def=def;
	  BorradorService.guardar({id:id,selected:selected,def:def}, function(response,status){
			  if(status==200){
    				toaster.pop({	type : response.type,
									title : response.reason,
									body : response.message
					});
    			}else{
    				toaster.pop({	type : "error",
    								title : "Error",
			    					body : mensajes.errorConexion
    				});
    			}
    			solicitarPagina(1);
    		});
	};
	  
	  
		
    }]);
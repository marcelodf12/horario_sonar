var fp_app = angular.module("app");

fp_app.controller('configuracionCtrl', ['$scope','ConfiguracionService','ngDialog','toaster','mensajes','$location'
                      , function($scope , ConfiguracionService , ngDialog , toaster , mensajes , $location) {
	
	$scope.$parent.nuevaRuta("config", $location.url(), "Configuracion", true);
	
	$scope.params = [];
	ConfiguracionService.listAll(function(response,status){
		if(status==200){
			$scope.params = response.data;;
		}
	});
	
	$scope.guardar = function(clave, valor){
		$scope.clave=clave;
		$scope.valor=valor;
	};
	
	$scope.confirmGuardar = function(){
		ConfiguracionService.guardar($scope.clave, $scope.valor,function(response,status){
			if(status==200){
				toaster.pop({	type : response.type,
					title : response.reason,
					body : response.message
				});
			}else{
				toaster.pop({	type : "error",
		    					title : "Error",
		    					body : mensajes.errorConexion
				});
			}
		});
	};
	
}]);

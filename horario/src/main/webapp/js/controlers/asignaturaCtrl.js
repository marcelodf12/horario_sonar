var fp_app = angular.module("app");

fp_app.controller('asignaturaCtrl', ['$scope','AsignaturaService','ngDialog','$routeParams','toaster','mensajes','$location','$sessionStorage'
                           , function($scope , AsignaturaService , ngDialog , $routeParams , toaster , mensajes , $location , $sessionStorage) {

	if($location.url() == "/asignatura/--any--/--any--/dpto/--any--/turno/--any--/codAsig/--any--/seccion/--any--/periodo/--any--/pag/--any--/order/true/column/asignatura/vista/--any--"){
		$scope.$parent.nuevaRuta("asignaturas", $location.url(), "Asignaturas", true);
	}else{
		$scope.$parent.nuevaRuta("asignaturas", $location.url(), "Asignaturas", false);
	};
	
	//URL EJEMPLO: #/asignatura/--any--/--any--/dpto/--any--/turno/--any--/codAsig/--any--/nivel/--any--/seccion/--any--/profesor/--any--/pag/--any--/order/ASC
	var any = "--any--";
	
	$scope.esperando = true;
	
	$scope.encontrado = false;
	$scope.idAsignatura = $routeParams.idAsignatura.toString();
	idAsignatura = $routeParams.idAsignatura.toString();
	$scope.busqueda_asignatura = $routeParams.fAsignatura.toString();
	$scope.busqueda_dpto = $routeParams.fDepartamento.toString();
	$scope.busqueda_turno = $routeParams.fTurno.toString();
	$scope.busqueda_codAsig = $routeParams.fCodAsig.toString();
	$scope.busqueda_seccion = $routeParams.fSeccion.toString();
	$scope.busqueda_periodo = $routeParams.fPeriodo.toString();
	$scope.busqueda_pagina = $routeParams.fPagina.toString();
	$scope.busqueda_order = $routeParams.order.toString();
	$scope.busqueda_column = $routeParams.column.toString();
	$scope.visibleConflicto = false;
	$scope.visibleGrupo = false;

	$scope.vista = $routeParams.vista.toString();
	
	if( $scope.busqueda_asignatura == any) $scope.busqueda_asignatura="";
	if( $scope.busqueda_dpto == any) $scope.busqueda_dpto="";
	if( $scope.busqueda_turno == any) $scope.busqueda_turno ="";
	if( $scope.busqueda_codAsig == any) $scope.busqueda_codAsig ="";
	if( $scope.busqueda_seccion == any) $scope.busqueda_seccion ="";
	if( $scope.busqueda_periodo == any) $scope.busqueda_periodo ="";
	if( $scope.busqueda_pagina == any) $scope.busqueda_pagina ="0";
	$scope.orderAsc = $scope.busqueda_order == "true"?true:false;
	
	$scope.orderDpto=false;
	$scope.orderAsignatura=false;
	$scope.orderTurno=false;
	$scope.orderPeriodo=false;
	$scope.orderCodAsig=false;
	$scope.orderSeccion=false;
	if($scope.busqueda_column=="dpto") $scope.orderDpto = true;
	if($scope.busqueda_column=="asignatura") $scope.orderAsignatura = true;
	if($scope.busqueda_column=="turno") $scope.orderTurno = true;
	if($scope.busqueda_column=="periodo") $scope.orderPeriodo = true;
	if($scope.busqueda_column=="codAsig") $scope.orderCodAsig = true;
	if($scope.busqueda_column=="seccion") $scope.orderSeccion = true;

	$scope.page = parseInt($scope.busqueda_pagina)/10 + 1;
	$scope.ultima = false;
	
	$scope.recargar = function(){
		AsignaturaService.list($scope.busqueda_pagina, $scope.busqueda_asignatura,
								$scope.busqueda_codAsig, $scope.busqueda_turno, $scope.busqueda_dpto,
								$scope.busqueda_seccion,
								$scope.busqueda_periodo, $scope.busqueda_column, $scope.orderAsc,
								function(response,status){
			$scope.esperando = false;
			if(status==200 && response.success){
				$scope.asignaturas = response.data;
				for (var i = 0; i < $scope.asignaturas.length; i++) {
					$scope.asignaturas[i].selected = false;
					$scope.asignaturas[i].verGrupo = false;
					$scope.asignaturas[i].verConflicto = false;
				}
				if(response.message == "ultima")
					$scope.ultima = true;
			}else if(status==200){
				$scope.vacio = true;
				toaster.pop({ 	type : response.type,
					title : response.reason,
					body : response.message
				});
			}else{
				$scope.error = true;
				toaster.pop({ 	type : "error",
								title : "Error",
								body : mensajes.errorConexion
							});
			}
		});
	};
	$scope.recargar();
	$scope.ordenarDpto = function(){
		order = !$scope.orderAsc?"true":"false";
		$location.path('/asignatura/' + $routeParams.idAsignatura.toString() + 
		'/' + $routeParams.fAsignatura.toString() +
		'/dpto/' + $routeParams.fDepartamento.toString() +
		'/turno/' + $routeParams.fTurno.toString() +
		'/codAsig/' + $routeParams.fCodAsig.toString() +
		'/seccion/' + $routeParams.fSeccion.toString() +
		'/periodo/' + $routeParams.fPeriodo.toString() +
		'/pag/' + $routeParams.fPagina.toString() +
		'/order/' + order +
		'/column/' + 'dpto' +
		'/vista/' + $routeParams.vista.toString());
	};
	$scope.ordenarAsignatura = function(){
		order = !$scope.orderAsc?"true":"false";
		$location.path('/asignatura/' + $routeParams.idAsignatura.toString() + 
		'/' + $routeParams.fAsignatura.toString() +
		'/dpto/' + $routeParams.fDepartamento.toString() +
		'/turno/' + $routeParams.fTurno.toString() +
		'/codAsig/' + $routeParams.fCodAsig.toString() +
		'/seccion/' + $routeParams.fSeccion.toString() +
		'/periodo/' + $routeParams.fPeriodo.toString() +
		'/pag/' + $routeParams.fPagina.toString() +
		'/order/' + order +
		'/column/' + 'asignatura' +
		'/vista/' + $routeParams.vista.toString());
	};
	$scope.ordenarTurno = function(){
		order = !$scope.orderAsc?"true":"false";
		$location.path('/asignatura/' + $routeParams.idAsignatura.toString() + 
		'/' + $routeParams.fAsignatura.toString() +
		'/dpto/' + $routeParams.fDepartamento.toString() +
		'/turno/' + $routeParams.fTurno.toString() +
		'/codAsig/' + $routeParams.fCodAsig.toString() +
		'/seccion/' + $routeParams.fSeccion.toString() +
		'/periodo/' + $routeParams.fPeriodo.toString() +
		'/pag/' + $routeParams.fPagina.toString() +
		'/order/' + order +
		'/column/' + 'turno' +
		'/vista/' + $routeParams.vista.toString());
	};
	$scope.ordenarPeriodo = function(){
		order = !$scope.orderAsc?"true":"false";
		$location.path('/asignatura/' + $routeParams.idAsignatura.toString() + 
		'/' + $routeParams.fAsignatura.toString() +
		'/dpto/' + $routeParams.fDepartamento.toString() +
		'/turno/' + $routeParams.fTurno.toString() +
		'/codAsig/' + $routeParams.fCodAsig.toString() +
		'/seccion/' + $routeParams.fSeccion.toString() +
		'/periodo/' + $routeParams.fPeriodo.toString() +
		'/pag/' + $routeParams.fPagina.toString() +
		'/order/' + order +
		'/column/' + 'periodo' +
		'/vista/' + $routeParams.vista.toString());
	};
	$scope.ordenarCodAsig = function(){
		order = !$scope.orderAsc?"true":"false";
		$location.path('/asignatura/' + $routeParams.idAsignatura.toString() + 
		'/' + $routeParams.fAsignatura.toString() +
		'/dpto/' + $routeParams.fDepartamento.toString() +
		'/turno/' + $routeParams.fTurno.toString() +
		'/codAsig/' + $routeParams.fCodAsig.toString() +
		'/seccion/' + $routeParams.fSeccion.toString() +
		'/periodo/' + $routeParams.fPeriodo.toString() +
		'/pag/' + $routeParams.fPagina.toString() +
		'/order/' + order +
		'/column/' + 'codAsig' +
		'/vista/' + $routeParams.vista.toString());
	};

	$scope.ordenarSeccion = function(){
		order = !$scope.orderAsc?"true":"false";
		$location.path('/asignatura/' + $routeParams.idAsignatura.toString() + 
		'/' + $routeParams.fAsignatura.toString() +
		'/dpto/' + $routeParams.fDepartamento.toString() +
		'/turno/' + $routeParams.fTurno.toString() +
		'/codAsig/' + $routeParams.fCodAsig.toString() +
		'/seccion/' + $routeParams.fSeccion.toString() +
		'/periodo/' + $routeParams.fPeriodo.toString() +
		'/pag/' + $routeParams.fPagina.toString() +
		'/order/' + order +
		'/column/' + 'seccion' +
		'/vista/' + $routeParams.vista.toString());
	};

	$scope.siguiente = function(){
		pagina = (parseInt($scope.busqueda_pagina) + 10).toString();
		$location.path('/asignatura/' + $routeParams.idAsignatura.toString() + 
		'/' + $routeParams.fAsignatura.toString() +
		'/dpto/' + $routeParams.fDepartamento.toString() +
		'/turno/' + $routeParams.fTurno.toString() +
		'/codAsig/' + $routeParams.fCodAsig.toString() +
		'/seccion/' + $routeParams.fSeccion.toString() +
		'/periodo/' + $routeParams.fPeriodo.toString() +
		'/pag/' + pagina +
		'/order/' + $routeParams.order.toString() +
		'/column/' + $routeParams.column.toString() +
		'/vista/' + $routeParams.vista.toString());
	};
	$scope.anterior = function(){
		pagina = (parseInt($scope.busqueda_pagina) - 10).toString();
		$location.path('/asignatura/' + $routeParams.idAsignatura.toString() + 
		'/' + $routeParams.fAsignatura.toString() +
		'/dpto/' + $routeParams.fDepartamento.toString() +
		'/turno/' + $routeParams.fTurno.toString() +
		'/codAsig/' + $routeParams.fCodAsig.toString() +
		'/seccion/' + $routeParams.fSeccion.toString() +
		'/periodo/' + $routeParams.fPeriodo.toString() +
		'/pag/' + pagina +
		'/order/' + $routeParams.order.toString() +
		'/column/' + $routeParams.column.toString() +
		'/vista/' + $routeParams.vista.toString());
	};
	$scope.buscar = function(){
		asignatura = $scope.busqueda_asignatura == ""?any:$scope.busqueda_asignatura;
		dpto = $scope.busqueda_dpto == ""?any:$scope.busqueda_dpto;
		turno = $scope.busqueda_turno == ""?any:$scope.busqueda_turno;
		codAsig = $scope.busqueda_codAsig == ""?any:$scope.busqueda_codAsig;
		seccion = $scope.busqueda_seccion == ""?any:$scope.busqueda_seccion;
		periodo = $scope.busqueda_periodo == ""?any:$scope.busqueda_periodo;
		
		$location.path('/asignatura/' + $routeParams.idAsignatura.toString() + 
		'/' + asignatura +
		'/dpto/' + dpto +
		'/turno/' + turno.toString() +
		'/codAsig/' + codAsig.toString() +
		'/seccion/' + seccion.toString() +
		'/periodo/' + periodo.toString() +
		'/pag/0'  +
		'/order/' + $routeParams.order.toString() +
		'/column/' + $routeParams.column.toString() +
		'/vista/' + $routeParams.vista.toString());
	};
	
	$scope.nuevo = function(){
		$sessionStorage.ultimaURL = $location.url();
		$location.path('/asignaturaNuevo/');
	};
	$scope.editar = function(id){
		$sessionStorage.ultimaURL = $location.url();
		$location.path('/editarAsignatura/' + id);
	};
	$scope.addRestriccion = function(a){
		id2 = a.idAsignatura;
		if($scope.encontrado){
			if($routeParams.vista.toString()=="grupo"){
				AsignaturaService.addGrupo($routeParams.idAsignatura.toString(), id2, function(response,status){
					if(status==200){
						if(response.success){
							if(typeof($scope.grupos)=='undefined' || $scope.grupos==null)
								$scope.grupos = [];
							$scope.grupos.push(a);
						}
						toaster.pop({ 	type : response.type,
							title : response.reason,
							body : response.message
						});
					}else{
						$scope.error = true;
						toaster.pop({ 	type : "error",
										title : "Error",
										body : mensajes.errorConexion
									});
					}
				});
			}else{
				AsignaturaService.addConflicto($routeParams.idAsignatura.toString(), id2, function(response,status){
					if(status==200){
						if(response.success){
							if(typeof($scope.conflictos)=='undefined' || $scope.conflictos==null)
								$scope.conflictos = [];
							$scope.conflictos.push(a);
						}
						toaster.pop({ 	type : response.type,
							title : response.reason,
							body : response.message
						});
					}else{
						$scope.error = true;
						toaster.pop({ 	type : "error",
										title : "Error",
										body : mensajes.errorConexion
									});
					}
				});
			}
		}
	}
	
	$scope.quitar = function(a, coiciden){
		id2 = a.idAsignatura;
		AsignaturaService.quitar($routeParams.idAsignatura.toString(), id2,coiciden, function(response,status){
			if(status==200){
				if(response.success){
					$scope.buscarRelaciones();
				}
				toaster.pop({ 	type : response.type,
					title : response.reason,
					body : response.message
				});
			}else{
				$scope.error = true;
				toaster.pop({ 	type : "error",
								title : "Error",
								body : mensajes.errorConexion
							});
			}
		});
	}
	
	$scope.buscarRelaciones = function(i, vista){
		$scope.asignaturas[i].buscandoRelacion = true;
		isGrupo=vista=="grupo";
		AsignaturaService.listRestricciones($scope.asignaturas[i].idAsignatura.toString(), isGrupo, function(response,status){
			if(status==200 && response.success){
				if(isGrupo){
					$scope.asignaturas[i].grupos = response.data;
				}else{
					$scope.asignaturas[i].conflictos = response.data;
				}
				$scope.asignaturas[i].buscandoRelacion = false;
			}else if(status==200){
				$scope.vacio = true;
				toaster.pop({ 	type : response.type,
					title : response.reason,
					body : response.message
				});
			}else{
				$scope.error = true;
				toaster.pop({ 	type : "error",
								title : "Error",
								body : mensajes.errorConexion
							});
			}
		});
	};
	
	$scope.seleccionar = function(i){
		$scope.asignaturas[i].selected = !$scope.asignaturas[i].selected;
		$scope.asignaturas[i].verGrupo = false;
		$scope.asignaturas[i].verConflicto = false;
	};
		
	$scope.verGrupo = function(i){
		$scope.buscarRelaciones(i, "grupo");
		$scope.asignaturas[i].selected = false;
		$scope.asignaturas[i].verGrupo = !$scope.asignaturas[i].verGrupo;
		$scope.asignaturas[i].verConflicto = false;
	};
	
	$scope.verConflictos = function(i){
		$scope.buscarRelaciones(i, "conflicto");
		$scope.asignaturas[i].selected = false;
		$scope.asignaturas[i].verGrupo = false;
		$scope.asignaturas[i].verConflicto = !$scope.asignaturas[i].verConflicto;
	};
	
	$scope.agregarRelacion = function(i){
		AsignaturaService.addRelacion($scope.ultimoIdAsig, $scope.asignaturasModal[i].idAsignatura.toString(), $scope.coicide, function(response,status){
			toaster.pop({ 	type : response.type,
				title : response.reason,
				body : response.message
			});
		});
	};
	
	$scope.quitarRelacion = function(id1, id2, coicide){
		AsignaturaService.quitar(id1, id2, coicide, function(response,status){
			toaster.pop({ 	type : response.type,
				title : response.reason,
				body : response.message
			});
		});
	};
	$scope.confirmEliminarAsig = function(i){
		$scope.deleteId = i;
		$scope.title_modal = "Desea eliminar la asignatura " + $scope.asignaturas[i].asignatura
		$scope.message_modal = "Está apunto de eliminar una asignatura, desea continuar?";
		$('#confirmModal').modal('toggle')
	};
	
	$scope.eliminarAsig = function(){
		AsignaturaService.eliminar($scope.asignaturas[$scope.deleteId].idAsignatura.toString(), function(response,status){
			if(status==200 && response.success){
				$scope.recargar();
			}
			toaster.pop({ 	type : response.type,
				title : response.reason,
				body : response.message
			});
		});
	};
	
	$scope.ultimoSeleccionado = function(i, c){
		$scope.coicide = c;
		$scope.ultimoIdAsig = $scope.asignaturas[i].idAsignatura.toString();
		$('#addRelacionModal').modal('toggle')
	};
	
	
	/*************   MODAL   ******************/
	$scope.buscarModal = function(nombre, dpto, codigo, periodo){
		$scope.busqueda_nombre_principal= nombre;
		$scope.busqueda_codigo = codigo;
		$scope.busqueda_dpto = dpto;
		$scope.busqueda_periodo = periodo;
		$scope.pageModal = 1;
		solicitarPaginaModal($scope.page);
	};
	
	solicitarPaginaModal = function(paginaModal){
		$scope.asignaturasModal = [];
		$scope.esperandoModal = true;
		$scope.errorModal = false;
		$scope.vacioModal = false;
		$scope.ultimaModal = false;
		offsetModal = ((paginaModal-1)*5);
		
		//servicesAsignaturas.list = function (offset, asignatura, codAsig, turno, dpto, seccion, periodo, column, order, callbackFunction) {
		AsignaturaService.list(offsetModal.toString(),$scope.busqueda_nombre_principal ,$scope.busqueda_codigo,$scope.busqueda_turno,$scope.busqueda_dpto,$scope.busqueda_seccion,$scope.busqueda_periodo , $scope.orderModal, null, function(response,status){
			
			if(status==200 && response.success){
				$scope.asignaturasModal = response.data;
				$scope.ultimaModal = response.message == "ultima";
				$scope.esperandoModal = false;
				if(response.data.length==0)
					$scope.vacioModal = true;

			}else{
				$scope.pageModal = 1;
				$scope.esperandoModal = false;
				$scope.errorModal = true;
			}
			if(status==200){
				if(!response.success)
					toaster.pop({	type : response.type,
									title : response.reason,
									body : response.message
					});
			}else{
				toaster.pop({	type : "error",
								title : "Error",
								body : mensajes.errorConexion
				});
			}
		});
	};
	
	$scope.anteriorModal = function(){
		$scope.pageModal = $scope.pageModal>1?$scope.pageModal-1:1;
		solicitarPaginaModal($scope.pageModal);
	};
	
	$scope.siguienteModal = function(){
		$scope.pageModal = $scope.ultimaModal?$scope.pageModal:$scope.pageModal+1;
		solicitarPaginaModal($scope.pageModal);
	};
 
}]);
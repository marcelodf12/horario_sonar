var fp_app = angular.module("app");

fp_app.controller('miCuentaCtrl', ['$scope','$http','MiCuentaService','$location','toaster','mensajes'
                         ,function( $scope,$http , MiCuentaService , $location , toaster , mensajes ) {
	

	$scope.passwordActual="";
	$scope.passwordNuevo="";
	$scope.passwordNuevoVerif="";
	$scope.$parent.nuevaRuta("mi_cuenta", $location.url(), "Mi cuenta", true);
	
	$scope.cambiarPass = function(){
		var form = document.getElementById("miCuenta");
		
		if($scope.passwordNuevoVerif.localeCompare($scope.passwordNuevo) == 0){
			//si los nuevos pass son iguales
			MiCuentaService.cambiarPass($scope.passwordActual, $scope.passwordNuevo, function(response,status){
				
				  if(status==200){
	  				toaster.pop({	type : response.type,
										title : response.reason,
										body : response.message
						});

	  				$scope.passwordActual="";
	  				$scope.passwordNuevo="";
	  				$scope.passwordNuevoVerif="";
	  				
	  			}else{
	  				toaster.pop({	type : "error",
				    					title : "Error",
				    					body : mensajes.errorConexion
	  				});
	  			}	  		
			});
	
		} else {
			  form.classList.add("was-validated");}		
	};
	
	$scope.cancelar = function(){
		$scope.passwordActual = "";
		$scope.passwordNuevo = "";
		$scope.passwordNuevoVerif = "";
	}
	
	
}]);
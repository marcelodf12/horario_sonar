var fp_app = angular.module("app");

fp_app.controller('departamentoCtrl', ['$scope','DepartamentoService','$location','toaster','mensajes','ngDialog','$sessionStorage'
                              ,function($scope , DepartamentoService , $location , toaster , mensajes , ngDialog , $sessionStorage) {

	$scope.$parent.nuevaRuta("dptos", $location.url(), "Departamentos", true);
	
	solicitarPagina = function(pagina){
		DepartamentoService.list('','','',true,function(response,status){
				if(status==200 && response.success){
					$scope.departamentos = response.data;
					$scope.esperando = false;
					if(response.data.length==0)
						$scope.vacio = true;
				}else{
					$scope.esperando = false;
					$scope.error = true;
				}
				if(status==200){
					if(!response.success)
						toaster.pop({	type : response.type,
										title : response.reason,
										body : response.message
						});
				}else{
					toaster.pop({	type : "error",
									title : "Error",
									body : mensajes.errorConexion
					});
				}
		})
	};
	
	solicitarPagina(1);
	
	$scope.select = function(index){
		url = 'asignatura/--any--/--any--/dpto/'+$scope.departamentos[index].sigla+'/turno/--any--/codAsig/--any--/seccion/--any--/periodo/--any--/pag/--any--/order/true/column/asignatura/vista/--any--';
		$scope.$parent.nuevaRuta("asignaturas", url, "Asignaturas", false);
		$location.path(url);
	};
	
	
	
	  $scope.open = function() {
		  $scope.title_modal = "Nuevo Departamento";
		  $scope.nombre_dpto = "";
		  $scope.sigla_dpto = "";
		  $scope.id_dpto = -1;
	  };
	  
	  
	  
	  
	  $scope.edit = function(index) {
		  $scope.title_modal = "Editar Departamento";
		  $scope.nombre_dpto = $scope.departamentos[index].nombre;
		  $scope.sigla_dpto= $scope.departamentos[index].sigla;
		  $scope.id_dpto = $scope.departamentos[index].idDepartamento;
		  };
		  
	  $scope.eliminar = function(index) {
		  $scope.title_modal = "Eliminar Departamento";
		  $scope.message_modal = "Está seguro que desea eliminar el Departamento " + $scope.departamentos[index].nombre;
		  $scope.id_departamento = $scope.departamentos[index].idDepartamento;
	  };
	  
	  $scope.confirmDelete = function(){
      	DepartamentoService.eliminar($scope.id_departamento, function(response,status){
			if(status==200){
				toaster.pop({	type : response.type,
								title : response.reason,
								body : response.message
				});
    		}else{
				toaster.pop({	type : "error",
		    					title : "Error",
		    					body : mensajes.errorConexion
				});
			}
			solicitarPagina(1);
		});
	  };
	  
	  $scope.guardar = function(id, nombre, sigla) {
		  
		var form = document.getElementById("dptoModal");
			
		if(nombre.length == 0 || sigla.length == 0){
			  form.classList.add("was-validated");
		} else {
		  
			  if($scope.id_dpto == -1){
				  DepartamentoService.nuevo(nombre, sigla, function(response,status){
					  if(status==200){
		    				toaster.pop({	type : response.type,
											title : response.reason,
											body : response.message
							});

							$('#dptoModal').modal('hide'); // cerramos el modal si esta todo OK
		    			}else{
		    				toaster.pop({	type : "error",
					    					title : "Error",
					    					body : mensajes.errorConexion
		    				});
		    			}
		    			solicitarPagina(1);
		    		});
			  } else{
				  DepartamentoService.guardar(id, nombre, sigla, function(response,status){
					  if(status==200){
		    				toaster.pop({	type : response.type,
											title : response.reason,
											body : response.message
							});
							$('#dptoModal').modal('hide'); // cerramos el modal si esta todo OK
		    			}else{
		    				toaster.pop({	type : "error",
					    					title : "Error",
					    					body : mensajes.errorConexion
		    				});
		    			}
		    			solicitarPagina(1);
		    		});
			  }
		  }
	  };
	
}]);
var fp_app = angular.module("app");

fp_app.controller('carrerasConflictosCtrl', ['$scope','CarreraService','ngDialog','$location','toaster','mensajes','$routeParams'
                                    ,function($scope , CarreraService , ngDialog , $location , toaster , mensajes , $routeParams) {

    if(typeof($routeParams.id)!='undefined'){
        $scope.idCarrera = parseInt($routeParams.id.toString());
    }else{
        $scope.idCarrera = -1;
    }
    
    $scope.currentPage = 0;
    $scope.pageSize = 10;
    $scope.setPageSize = function(n){
        $scope.pageSize = n;
    }
    $scope.anterior = function(){
        $scope.currentPage = $scope.currentPage==0?0:$scope.currentPage-1;
    }
    $scope.siguiente = function(){
        $scope.currentPage = $scope.currentPage+1;
    }
    
    $scope.error = false;
	$scope.esperando = true;
    CarreraService.listCarreraConflictos($scope.idCarrera, function(response, status){
        $scope.esperando = false;
        if(response.success==true && status==200){
            $scope.carrerasConflicto = response.data;
            for(i=0;i<$scope.carrerasConflicto.length; i++){
                if($scope.carrerasConflicto[i].grupoConflicto==null) $scope.carrerasConflicto[i].grupoConflicto = "";
                $scope.carrerasConflicto[i].grupoConflictoAux = $scope.carrerasConflicto[i].grupoConflicto;
                $scope.carrerasConflicto[i].edited = false;
                $scope.carrerasConflicto[i].turno =  $scope.carrerasConflicto[i].turno==null? "":$scope.carrerasConflicto[i].turno;
                $scope.carrerasConflicto[i].seccion =  $scope.carrerasConflicto[i].seccion==null? "":$scope.carrerasConflicto[i].seccion;
                $scope.carrerasConflicto[i].enfasis =  $scope.carrerasConflicto[i].enfasis==null? "":$scope.carrerasConflicto[i].enfasis;
                $scope.carrerasConflicto[i].plan =  $scope.carrerasConflicto[i].plan==null? "":$scope.carrerasConflicto[i].plan;
                $scope.carrerasConflicto[i].semestre =  $scope.carrerasConflicto[i].semestre==null? "":$scope.carrerasConflicto[i].semestre;
                $scope.carrerasConflicto[i].nivel =  $scope.carrerasConflicto[i].nivel==null? "":$scope.carrerasConflicto[i].nivel;
            }
        }else{
            $scope.error = true;
        }

    });

    $scope.guardar = function(){;
		$scope.title_modal = "Desea guardar la configuración";
		$scope.message_modal = "Está apunto de reconfigurar las restricciones de conflicto. Está seguro?. Esta operción no pude ser revertida";
		$('#confirmModal').modal('toggle');
    };
    
    $scope.autoConfigurar = function(){
        conflictos = [];
        for(i=0;i<$scope.carrerasConflicto.length; i++){
            if($scope.carrerasConflicto[i].grupoConflictoAux!=$scope.carrerasConflicto[i].grupoConflicto){
                c = {};
                c.codigoAsig =  $scope.carrerasConflicto[i].codigoAsig==""? null:$scope.carrerasConflicto[i].codigoAsig;
                c.nombreAsignatura =  $scope.carrerasConflicto[i].nombreAsignatura==""? null:$scope.carrerasConflicto[i].nombreAsignatura;
                c.grupoConflicto =  $scope.carrerasConflicto[i].grupoConflictoAux==""? null:$scope.carrerasConflicto[i].grupoConflictoAux;
                c.turno =  $scope.carrerasConflicto[i].turno==""? null:$scope.carrerasConflicto[i].turno;
                c.seccion =  $scope.carrerasConflicto[i].seccion==""? null:$scope.carrerasConflicto[i].seccion;
                c.enfasis =  $scope.carrerasConflicto[i].enfasis==""? null:$scope.carrerasConflicto[i].enfasis;
                c.plan =  $scope.carrerasConflicto[i].plan==""? null:$scope.carrerasConflicto[i].plan;
                c.semestre =  $scope.carrerasConflicto[i].semestre==""? null:$scope.carrerasConflicto[i].semestre;
                c.nivel =  $scope.carrerasConflicto[i].nivel==""? null:$scope.carrerasConflicto[i].nivel;
                c.idAsignaturaCarrera = $scope.carrerasConflicto[i].idAsignaturaCarrera;
                c.idCarrera =  $scope.carrerasConflicto[i].idCarrera;
                conflictos.push(c);
            }
        }
        $scope.esperando = true;
        CarreraService.updateCarreraConflictos($scope.idCarrera, conflictos, function(response, status){
            $scope.esperando = false;
            $('#confirmModal').modal('toggle');
            $('.modal-backdrop').remove();
            $location.path("carreras/");
        });

    };

}]);
fp_app.filter('slice', function() {
    return function(arr, start, end) {
      return (arr || []).slice(start, end);
    };
  });
var fp_app = angular.module("app");

fp_app.controller('asignaturaNuevoCtrl', ['$scope','ngDialog','AsignaturaService','ProfesorService','DepartamentoService','CarreraService','$location','$routeParams','toaster','mensajes','$sessionStorage','AulaService','UtilService'
                                , function($scope , ngDialog , AsignaturaService , ProfesorService , DepartamentoService , CarreraService , $location , $routeParams , toaster , mensajes , $sessionStorage , AulaService , UtilService) {

	
	$scope.guardar = function(){
		$scope.title_modal = "Edición de Asignatura";
		$scope.message_modal = "¿Está seguro que desea guardar los cambios?"
	  };
	  
	  $scope.confirmGuardar = function(){
      	if($scope.dpto == null || typeof($scope.dpto)==undefined){
    		toaster.pop({ 	type : "error",
				title : "Faltan Datos obligatorios",
				body : "El departamento es obligatorio"
			});
    	}else if($scope.a.carreras == null || typeof($scope.a.carreras)==undefined || $scope.a.carreras.length == 0){
        		toaster.pop({ 	type : "error",
					title : "Faltan Datos obligatorios",
					body : "Debe agregar al menos una carrera"
				});
    	}else{
    		$scope.a.slots = UtilService.schedulerToSlot($("#schedule").jqs('export'));
    		console.log($scope.a.slots);
        	$scope.a.idDpto = $scope.dpto.idDepartamento;
        	AsignaturaService.guardar($scope.a, function(response, status){
        		if(status==200){
        			
        			toaster.pop({ 	type : response.type,
						title : response.reason,
						body : response.message
					});
        		}else{
        			toaster.pop({ 	type : 'error',
						title : 'Problemas de Conexión',
						body : 'No se puede conectar con el servidor'
					});
        		}
        	})
        	$('#confirmModal').modal('toggle');
        	//$location.path($sessionStorage.ultimaURL);
    	}
    };
	 
	$scope.cancelar = function(){
		$scope.title_modal = "Edición de Asignatura";
		$scope.message_modal = "¿Está seguro que desea descartar los cambios?"
		ngDialog.openConfirm({
            scope: $scope,
            template: 'view/modal/confirm_modal.html'
        }).then(function (confirm) {
        	$location.path($sessionStorage.ultimaURL);
        }, function(reject) {
          
        });
	};
	 
	 
	$scope.esperando = true;
	$scope.carreras = [];
	DepartamentoService.list('', '', 'sigla', true, function(response, status){
		if(status==200 && response.success){
			$scope.dptos = response.data;
			if(typeof($routeParams.id)!='undefined'){
				idCarrera = parseInt($routeParams.id.toString());
				AsignaturaService.get(idCarrera, function(response, status){
					$scope.esperando = false;
					if(status==200 && response.success){
						response.data.slot = response.data.slots;
						$scope.copia = angular.fromJson(angular.toJson(response.data));
						$scope.a = response.data;
						$scope.title = $scope.a.asignatura + "";
						for(d in $scope.dptos){
							if($scope.dptos[d].idDepartamento == $scope.a.idDpto){
								$scope.dpto = $scope.dptos[d];
							}
						}
						$scope.editar = true;
						$scope.$parent.nuevaRuta("editar_asignatura", $location.url(), $scope.a.asignatura, false);
						
						$scope.modos = UtilService.modos;
						
						$scope.tipos = {};
						
						AulaService.listTiposAula(function(response,status){
							if(status==200){
								$scope.tipos = response.data;
								for(i=0; i<$scope.a.divisiones.length ; i++){
									$scope.a.divisiones[i].modo = UtilService.modoToString($scope.a.divisiones[i].modo);
									for(t in $scope.tipos){
										if($scope.a.divisiones[i].tipoAula == $scope.tipos[t].idTipoAula){
											$scope.a.divisiones[i].tipoAula = $scope.tipos[t];
										};
									};
									$scope.a.divisiones[i].hora = UtilService.slotToDate($scope.a.divisiones[i].hora);
									$scope.a.divisiones[i].duracion = $scope.a.divisiones[i].duracion / 3;
								}
							}else{
								toaster.pop({	type : "error",
						    					title : "Error",
						    					body : mensajes.errorConexion
								});
							}
						});
					}else{
						$scope.error = true;
						toaster.pop({ 	type : response.type,
							title : response.reason,
							body : response.message
						});
					}
				});	
			}else{
				$scope.esperando = false;
				$scope.title = "Nueva Asignatura";
				$scope.editar = true;
				$scope.$parent.nuevaRuta("nueva_asignatura", $location.url(), "Nueva Asignatura", false);
			};
		}else{
			$scope.error = true;
			toaster.pop({ 	type : response.type,
				title : response.reason,
				body : response.message
			});
		}
	});
	

	
	$scope.nuevoProf = function(){
		$scope.profesores = [];
		$scope.page = 1;
		$scope.ultima = false;
		$scope.esperando = false;
		$scope.error = false;
		$scope.vacio = false;
		$scope.profesorSeleccionado = false;
		// ORDENACION
		$scope.orderAsc = true;
		$scope.order = "nombre";
		// END ORDENACION
		$scope.title_modal = "Asignar Profesor";
			ngDialog
			.open({
				template : 'view/modal/select_profesor_modal.html',
				scope : $scope
			});
	};
	
	solicitarPagina = function(pagina){
		$scope.profesores = [];
		$scope.esperando = true;
		$scope.error = false;
		$scope.vacio = false;
		offset = ((pagina-1)*10);
		//servicesProfesor.list = function (offset, 			nombre,           		apellido, 		cedula,               		column,      	order,      	callbackFunction)
		ProfesorService.list(				offset.toString(), 	$scope.busqueda_nombre, "", 			$scope.busqueda_cedula,		$scope.order, 	$scope.orderAsc, function(response,status){
			if(status==200 && response.success){
				$scope.profesores = response.data;
				$scope.ultima = response.message == "ultima";
				$scope.esperando = false;
				if(response.data.length==0)
					$scope.vacio = true;
			}else{
				$scope.page = 1;
				console.log(status);
				console.log(response);
				$scope.esperando = false;
				$scope.error = true;
			}
			if(status==200){
				if(!response.success)
					toaster.pop({	type : response.type,
									title : response.reason,
									body : response.message
					});
			}else{
				toaster.pop({	type : "error",
								title : "Error",
								body : mensajes.errorConexion
				});
			}
		});
	};
	
	solicitarPaginaCarrera = function(pagina){
		$scope.carreras = [];
		$scope.esperando = true;
		$scope.error = false;
		$scope.vacio = false;
		offset = ((pagina-1)*10);
		// servicesCarrera.list = function (offset, nombre, sigla, column, order, callbackFunction) {
		CarreraService.list(				offset.toString(), 	$scope.busqueda_carrera_nombre, "",	"nombre", 	$scope.orderAsc, function(response,status){
			if(status==200 && response.success){
				$scope.carreras = response.data;
				$scope.ultimaCarrera = response.message == "ultima";
				$scope.esperando = false;
				if(response.data.length==0)
					$scope.vacio = true;
			}else{
				$scope.pageCarrera = 1;
				console.log(status);
				console.log(response);
				$scope.esperando = false;
				$scope.error = true;
			}
			if(status==200){
				if(!response.success)
					toaster.pop({	type : response.type,
									title : response.reason,
									body : response.message
					});
			}else{
				toaster.pop({	type : "error",
								title : "Error",
								body : mensajes.errorConexion
				});
			}
		});
	};
	
	$scope.anterior = function(){
		$scope.page = $scope.page>1?$scope.page-1:1;
		solicitarPagina($scope.page);
	};
	
	$scope.siguiente = function(){
		$scope.page = $scope.ultima?$scope.page:$scope.page+1;
		solicitarPagina($scope.page);
	};
	
	$scope.buscar = function(nombre, cedula){
		$scope.busqueda_nombre = nombre;
		$scope.busqueda_cedula = cedula;
		$scope.page = 1;
		solicitarPagina($scope.page);
	};
	
	$scope.anteriorCarrera = function(){
		$scope.pageCarrera = $scope.pageCarrera>1?$scope.pageCarrera-1:1;
		solicitarPaginaCarrera($scope.pageCarrera);
	};
	
	$scope.siguienteCarrera = function(){
		$scope.pageCarrera = $scope.ultimaCarrera?$scope.pageCarrera:$scope.pageCarrera+1;
		solicitarPaginaCarrera($scope.pageCarrera);
	};
	
	$scope.buscarCarrera = function(nombre){
		$scope.busqueda_carrera_nombre = nombre;
		$scope.pageCarrera = 1;
		solicitarPaginaCarrera($scope.pageCarrera);
	};
	
	$scope.selectedProfesor = function(index){
		esta = false
		if($scope.a.profesores==null)
			$scope.a.profesores = [];
		for(i=0; i < $scope.a.profesores.length ; i++){
			if($scope.a.profesores[i].idProfesor == $scope.profesores[index].idProfesor){
				esta=true;
				break;
			}
		}
		if(!esta){
			$scope.a.profesores.push(JSON.parse(JSON.stringify($scope.profesores[index])));
		}
	};
	
	$scope.selectedCarrera = function(index){
		if($scope.a.carreras==null)
			$scope.a.carreras = [];
		
		$scope.a.carreras.push({sigla: $scope.carreras[index].sigla, idCarrera: $scope.carreras[index].idCarrera});
	};
	
	// Divisiones
	
	$scope.minutosCombo = UtilService.minutosCombo();
	$scope.horasCombo =  UtilService.horasCombo();
	$scope.diasCombo = UtilService.diasCombo();
	
	$scope.nuevaDiv = function(){
		if($scope.a.divisiones == null || typeof($scope.a.divisiones) == undefined)
			$scope.a.divisiones = [];
		division = {};
		division.tipoAula = "";
		division.duracion = 0;
		division.hora = "";
		division.modo = "";
		$scope.a.divisiones.push(division);
	};
	
	$scope.eliminarDiv = function(index){
		$scope.a.divisiones.splice(index, 1);
	};
	$scope.eliminarProf = function(index){
		$scope.a.profesores.splice(index, 1);
	};
	$scope.eliminarCarrera = function(index){
		$scope.a.carreras.splice(index, 1);
	};
	
	$scope.ocultar_todo = function(){
		$scope.asignatura_hide = true;
		$scope.restricciones_hide = true;
		$scope.carreras_hide = true;
		$scope.profesores_hide = true;
		$scope.divisiones_hide = true;
	}
	$scope.ocultar_todo();
	
	$scope.mostrar_tab = function(menu){
		$scope.ocultar_todo();
		if(menu=='a')
			$scope.asignatura_hide = false;
		if(menu=='p')
			$scope.profesores_hide = false;
		if(menu=='r')
			$scope.restricciones_hide = false;
		if(menu=='c')
			$scope.carreras_hide = false;
		if(menu=='d')
			$scope.divisiones_hide = false;
		
	}
	$scope.mostrar_tab('a');
		
}]);
	
var fp_app = angular.module("app");

fp_app.controller('profesoresCtrl', ['$scope','ProfesorService','$location','toaster','mensajes','ngDialog','$routeParams'
                           ,function( $scope , ProfesorService , $location , toaster , mensajes , ngDialog , $routeParams) {
	
	$scope.$parent.nuevaRuta("profesores", $location.url(), "Profesores", true);
	
	$scope.profesores = [];
	$scope.ultima = false;
	$scope.esperando = false;
	$scope.error = false;
	$scope.vacio = false;
	var any = "--any--";
	
	
	$scope.busqueda_nombre = $routeParams.fnombre.toString();
	$scope.busqueda_apellido = $routeParams.fapellido.toString();
	$scope.busqueda_cedula = $routeParams.fcedula.toString();
	$scope.busqueda_order = $routeParams.forder.toString();
	$scope.busqueda_column = $routeParams.fcolumn.toString();
	$scope.busqueda_pagina = $routeParams.fpag.toString();
	if( $scope.busqueda_nombre == any) $scope.busqueda_nombre="";
	if( $scope.busqueda_apellido == any) $scope.busqueda_apellido="";
	if( $scope.busqueda_cedula == any) $scope.busqueda_cedula="";
	if( $scope.busqueda_order == any) $scope.busqueda_order="true";
	if( $scope.busqueda_column == any) $scope.busqueda_column="nombre";
	if( $scope.busqueda_pagina == any) $scope.busqueda_pagina="0";
	
	$scope.page = parseInt($scope.busqueda_pagina)/10 + 1;
	$scope.orderAsc=false;
	if( $scope.busqueda_order == "true") $scope.orderAsc=true;
	$scope.order = $scope.busqueda_column;
	
	$scope.ordenar = function(columna){
		order = !$scope.orderAsc?"true":"false";
		$location.path(
				'/profesores/nombre/' + $routeParams.fnombre.toString() +
				'/apellido/' + $routeParams.fapellido.toString() +
				'/cedula/' + $routeParams.fcedula.toString() +
				'/pag/' + $routeParams.fpag.toString() +
				'/column/' + columna +
				'/order/' + order
		);
	}
	// END ORDENACION

	
	solicitarPagina = function(){
		$scope.profesores = [];
		$scope.secciones = [];
		$scope.Nombreprofesor = "";
		$scope.esperando = true;
		$scope.error = false;
		$scope.vacio = false;
		//servicesProfesor.list = function (offset, nombre, apellido, cedula, column, order, callbackFunction)
		ProfesorService.list($scope.busqueda_pagina, $scope.busqueda_nombre, $scope.busqueda_apellido, $scope.busqueda_cedula, $scope.busqueda_column, $scope.busqueda_order, function(response,status){
			if(status==200 && response.success){
				$scope.profesores = response.data;
				$scope.ultima = response.message == "ultima";
				$scope.esperando = false;
				if(response.data.length==0)
					$scope.vacio = true;
			}else{
				$scope.esperando = false;
				$scope.error = true;
			}
			if(status==200){
				if(!response.success)
					toaster.pop({	type : response.type,
									title : response.reason,
									body : response.message
					});
			}else{
				toaster.pop({	type : "error",
								title : "Error",
								body : mensajes.errorConexion
				});
			}
		});
	};
	
	$scope.anterior = function(){
		pagina = (parseInt($scope.busqueda_pagina) - 10).toString();
		$location.path(
				'/profesores/nombre/' + $routeParams.fnombre.toString() +
				'/apellido/' + $routeParams.fapellido +
				'/cedula/' + $routeParams.fcedula.toString() +
				'/pag/' + pagina +
				'/column/' + $routeParams.fcolumn.toString() +
				'/order/' + $routeParams.forder.toString()
		);
	}
	
	$scope.siguiente = function(){
		pagina = (parseInt($scope.busqueda_pagina) + 10).toString();
		$location.path(
				'/profesores/nombre/' + $routeParams.fnombre.toString() +
				'/apellido/' + $routeParams.fapellido +
				'/cedula/' + $routeParams.fcedula.toString() +
				'/pag/' + pagina +
				'/column/' + $routeParams.fcolumn.toString() +
				'/order/' + $routeParams.forder.toString()
		);
	}
	
	$scope.buscar = function(){
		nombre = $scope.busqueda_nombre == ""?any:$scope.busqueda_nombre;
		apellido = $scope.busqueda_apellido == ""?any:$scope.busqueda_apellido;
		cedula = $scope.busqueda_cedula == ""?any:$scope.busqueda_cedula;
		$location.path(
				'/profesores/nombre/' + nombre +
				'/apellido/' + apellido +
				'/cedula/' + cedula +
				'/pag/' + any +
				'/column/' + $routeParams.fcolumn.toString() +
				'/order/' + $routeParams.forder.toString()
		);
	}

	solicitarPagina();
	
	  $scope.open = function() {
		  $scope.title_modal = "Nuevo Profesor";
		  $scope.p = {};
		  $scope.p.titulo = "";
		  $scope.p.nombre = "";
		  $scope.p.apellido = "";
		  $scope.p.cedula = "";
		  $scope.p.celular = "";
		  $scope.p.correo = "";
		  $scope.p.notificar=false;
	  };
		  
  
	  $scope.eliminar = function(index) {
		  $scope.title_modal = "Eliminar Profesor";
		  $scope.message_modal = "Está seguro que desea eliminar el Profesor " + $scope.profesores[index].nombre;
		  $scope.id_profesor = $scope.profesores[index].idProfesor;
	  };
	  
	  $scope.confirmDelete = function(){
		  ProfesorService.eliminar($scope.id_profesor, function(response,status){
			if(status==200){
				toaster.pop({	type : response.type,
								title : response.reason,
								body : response.message
				});
    		}else{
				toaster.pop({	type : "error",
		    					title : "Error",
		    					body : mensajes.errorConexion
				});
			}
			solicitarPagina(1);
		});
	  };
	  
	  
	  $scope.gotoProfesor = function(index){
		  $location.path('profesor/' + $scope.profesores[index].idProfesor);
	  };
	  
	  $scope.gotoSlotProfesor = function(index){
		  if(typeof($scope.profesores[index].idSlot)!=undefined && $scope.profesores[index].idSlot != null){
			  $location.path('profesor/'+ $scope.profesores[index].idProfesor +'/slot/' + $scope.profesores[index].idSlot);
		  }else{
			  $location.path('profesor/'+ $scope.profesores[index].idProfesor + '/slot/-1');
		  }
			  
		  
	  };
	  
	  $scope.guardar = function() {
		  var form = document.getElementById("profModal");
			
			if($scope.p.titulo.length == 0 || $scope.p.nombre.length == 0 || $scope.p.apellido.length == 0){
				  form.classList.add("was-validated");
			} else {
					  
				  ProfesorService.nuevo($scope.p, function(response,status){
						  if(status==200){
			    				toaster.pop({	type : response.type,
												title : response.reason,
												body : response.message
								});
								$('#profModal').modal('hide'); // cerramos el modal si esta todo OK
			    			}else{
			    				toaster.pop({	type : "error",
						    					title : "Error",
						    					body : mensajes.errorConexion
			    				});
			    			}
			    			solicitarPagina(1);
	    		});
			}
	  };
		
    }]);

fp_app.controller('profesorCtrl', ['$scope','ProfesorService', 'PerfilService','$location','$routeParams','toaster','mensajes','$sessionStorage'
                         ,function( $scope , ProfesorService, PerfilService , $location , $routeParams , toaster , mensajes , $sessionStorage) {
	
	$scope.editar = false;
	profesor = {};
	$scope.perfiles = [];
	$scope.permisos=[];
	
	if(typeof($routeParams.id)!='undefined'){
		$scope.idProfesor = parseInt($routeParams.id.toString());
	}else{
		$scope.idProfesor = -1;
	};

	$scope.permisoGestionDeUsuario=$scope.$parent.permisos['GESTION DE USUARIOS'];

	
	$scope.editarAsignatura = function(i){
		$sessionStorage.ultimaURL = $location.url();
		$location.path('/editarAsignatura/' + $scope.profesor.asignaturas[i].idAsignatura);
	};
	
	ProfesorService.get($scope.idProfesor, function(response,status){
		if(status==200 && response.success){
			$scope.profesor = response.data;
			$scope.nombreProfesor=$scope.profesor.titulo + " " + $scope.profesor.nombre + " " + $scope.profesor.apellido;
			isRoot = $scope.$parent.ultimoLabel()!="profesores" && $scope.$parent.ultimoLabel()!="usuarios";
			$scope.$parent.nuevaRuta("editar_profesor", $location.url(), $scope.nombreProfesor, isRoot);
		}else{
			$scope.error = true;
		}
		if(status==200){
			if(!response.success)
				toaster.pop({	type : response.type,
								title : response.reason,
								body : response.message
				});
		}else{
			toaster.pop({	type : "error",
							title : "Error",
							body : mensajes.errorConexion
			});
		}
		$scope.esperando = false;
	});
	
	
	$scope.edit = function(){
		$scope.editar = true;
		profesor.idProfesor = $scope.profesor.idProfesor;
		profesor.titulo = $scope.profesor.titulo;
		profesor.nombre = $scope.profesor.nombre;
		profesor.apellido = $scope.profesor.apellido;
		profesor.cedula = $scope.profesor.cedula;
		profesor.celular = $scope.profesor.celular;
		profesor.correo = $scope.profesor.correo;
		profesor.notificar = $scope.profesor.notificar;
	}
	$scope.cancelar = function(){
		$scope.editar = false;
		$scope.profesor.titulo = profesor.titulo;
		$scope.profesor.nombre = profesor.nombre;
		$scope.profesor.apellido = profesor.apellido;
		$scope.profesor.cedula = profesor.cedula;
		$scope.profesor.celular = profesor.celular;
		$scope.profesor.correo = profesor.correo;
		$scope.profesor.notificar = profesor.notificar;
	}
	
	$scope.guardar = function(){
		var form = document.getElementById("cardInformacion");
		
		if($scope.profesor.titulo.length == 0 || $scope.profesor.nombre.length == 0 || $scope.profesor.apellido.length == 0 ){
			  form.classList.add("was-validated");
		} else {
			$scope.editar = false;
			ProfesorService.put($scope.profesor, function(response,status){
				if(status==200 && response.success){
					$scope.nombreProfesor=$scope.profesor.titulo + " " + $scope.profesor.nombre;
					$scope.esperando = false;
				}else{
					$scope.cancelar();
					$scope.esperando = false;
					$scope.error = true;
				}
				if(status==200){
						toaster.pop({	type : response.type,
										title : response.reason,
										body : response.message
						});
				}else{
					toaster.pop({	type : "error",
									title : "Error",
									body : mensajes.errorConexion
					});
				}
			});
		}
	}
	
	obtenerPerfiles = function(){
		$scope.esperandoPerfil = true;
		$scope.errorPerfil = false;
		$scope.vacioPerfiles = false;
		PerfilService.getPerfiles($scope.idProfesor, function(response,status){
			if(status==200 && response.success){
				$scope.perfiles = response.data;
				$scope.vacioPerfiles = $scope.perfiles.length<=0;
			}else{
				$scope.errorPerfil = true;
			}
			if(status==200){
				if(!response.success)
					toaster.pop({	type : response.type,
									title : response.reason,
									body : response.message
					});
			}else{
				toaster.pop({	type : "error",
								title : "Error",
								body : mensajes.errorConexion
				});
			}
			$scope.esperandoPerfil = false;
		});
	};
	
	  obtenerPermisos = function(){
			$scope.esperandoPermisos = true;
			$scope.errorPermisos = false;
			$scope.vacioPermisos = false;
			PerfilService.getPermisos(function(response,status){
				if(status==200 && response.success){
					$scope.permisos = response.data;
					$scope.vacioPermisos = $scope.permisos.length<=0;				
				}else{
					
					$scope.errorPermisos = true;
				}
				if(status==200){
					if(!response.success)
						toaster.pop({	type : response.type,
										title : response.reason,
										body : response.message
						});
				}else{
					toaster.pop({	type : "error",
									title : "Error",
									body : mensajes.errorConexion
					});
				}
				$scope.esperandoPermisos = false;
			});
		};
		
	  
	
	obtenerPerfiles();
	obtenerPermisos();
	
	$scope.eliminarPermiso = function(index) {
		  $scope.title_modal = "Eliminar Permiso";
		  $scope.message_modal = "Está seguro que desea eliminar el Permiso " + $scope.perfiles[index].nombre;
		  $scope.perfildelete = $scope.perfiles[index];
	  };
	  
	  $scope.confirmDelete = function(){
		  PerfilService.eliminarPerfil($scope.perfildelete, function(response,status){
			if(status==200){
				toaster.pop({	type : response.type,
								title : response.reason,
								body : response.message
				});
  		}else{
				toaster.pop({	type : "error",
		    					title : "Error",
		    					body : mensajes.errorConexion
				});
			}
			obtenerPerfiles();
		});
	  };
	
	
		
	  
	  $scope.nuevoPermiso = function(){
		  $scope.permiso=null;
		  $scope.usuarioPerfil={};
		  obtenerPermisos();
	  }
	  
	  
	  $scope.confirmAgregar = function(){
		  var form = document.getElementById("permisoModal");
			
		if($scope.permiso == null){
			  form.classList.add("was-validated");
		} else {

			  $scope.usuarioPerfil.idPerfil = $scope.permiso.codigo;
			  $scope.usuarioPerfil.idUsuario = $scope.idProfesor;
			  PerfilService.asignarPermiso($scope.usuarioPerfil, function(response,status){
				if(status==200){
					toaster.pop({	type : response.type,
									title : response.reason,
									body : response.message
					});
					$('#permisoModal').modal('hide'); // cerramos el modal si esta todo OK
	  		}else{
					toaster.pop({	type : "error",
			    					title : "Error",
			    					body : mensajes.errorConexion
					});
				}
				obtenerPerfiles();
			});
		}
	  };
	
}]);


fp_app.controller('profesorSlotsCtrl', ['$scope','ProfesorService','$routeParams','toaster','mensajes','ngDialog','$location','$sessionStorage','UtilService'
                              ,function( $scope , ProfesorService , $routeParams , toaster , mensajes , ngDialog , $location , $sessionStorage , UtilService) {
	
	$scope.idSlot = parseInt($routeParams.id.toString());
	$scope.idProfesor = parseInt($routeParams.idProfesor.toString());
	$scope.editar = false;
	$scope.error = false;
	$scope.esperando = true;
	$scope.copia = {};
	$scope.slot = {};
	
	ProfesorService.getSlot($scope.idSlot,$scope.idProfesor, function(response, status){
		if(status==200 && response.success){
			if(typeof(response.data.slot)==undefined || response.data.slot == null) {
				response.data.idSlot = -1;
			}
			$scope.copia = angular.fromJson(angular.toJson(response.data));
			$scope.profesor = response.data;
			$scope.nombreProfesor=$scope.profesor.titulo + " " + $scope.profesor.nombre + " " + $scope.profesor.apellido;
			$scope.esperando = false;
			$scope.error = false;
			isRoot = $scope.$parent.ultimoLabel()!="profesores"
			if(isRoot){
				$scope.$parent.nuevaRuta("slot_profesor", $location.url(), "Mi Disponibilidad", isRoot);
			}else{
				$scope.$parent.nuevaRuta("slot_profesor", $location.url(), "Disponibilidad", isRoot);
			}
			
		}else{
			$scope.esperando = false;
			$scope.error = true;
		}
		if(status==200){
			if(!response.success)
				toaster.pop({	type : response.type,
								title : response.reason,
								body : response.message
				});
		}else{
			toaster.pop({	type : "error",
							title : "Error",
							body : mensajes.errorConexion
			});
		}
	})
	
	
	$scope.cancelar = function(){
		$scope.profesor = angular.fromJson(angular.toJson($scope.copia));
		toaster.pop({	type : 'info',
						title : 'Información',
						body : mensajes.noGuardado
		});
	};
	
	
	  $scope.guardar = function(){
		$scope.title_modal = "Guardar configuración";
		$scope.message_modal = "¿Está seguro que desea guardar los cambios?"
	  };
	  
	  $scope.confirmGuardar = function(){
		$scope.editar = false;
      	$scope.profesor.slot = UtilService.schedulerToSlot($("#schedule").jqs('export'));
      	console.log($scope.profesor.slot);
      	ProfesorService.insertSlot($scope.profesor.idSlot, $scope.idProfesor, $scope.profesor.slot, function(response,status){
			if(status==200){
				toaster.pop({	type : response.type,
								title : response.reason,
								body : response.message
				});
    		}else{
				toaster.pop({	type : "error",
		    					title : "Error",
		    					body : mensajes.errorConexion
				});
			}
			solicitarPagina(1);
		});
	  };	
	
}]);

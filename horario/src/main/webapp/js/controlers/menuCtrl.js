var fp_app = angular.module("app");
fp_app.controller('menuCtrl', ['$scope','$http','$location','LoginService','toaster'
                     , function($scope , $http , $location , LoginService , toaster) {
	$location.path("");
	
	// Logica para migas de pan
	$scope.irA = function(r){
		if(!r.active){
			$scope.nuevaRuta(r.label,r.url, r.text, false);
			$location.path(r.url);
		}
	};
	

	$scope.rutas = [];
	
	$scope.ultimaCarreraVisitada="";
	
	$scope.rutas = $scope.rutas;
	
	$scope.ultimoLabel = function(){
		return $scope.rutas[$scope.rutas.length - 1].label;
	}
	
	$scope.nuevaRuta = function(label, url, text, raiz){
		entro = false;
		copia = $scope.rutas;
		$scope.rutas = [];
		if(!raiz){
			r = 0;
			while(r<copia.length){
				if(copia[r].label!=label){
					copia[r].active = false;
					//copia[r].clase = "breadcrumb-item";
					$scope.rutas.push(copia[r]);
				}else{
					entro = true;
					aux = {};
					aux.label = label;
					aux.url = url;
					aux.text = text;
					aux.active = true;
					//aux.clase = "breadcrumb-item active";
					$scope.rutas.push(aux);
					break;
				};
				r++;
			}
		};
		if(!entro){
			aux = {};
			aux.label = label;
			aux.url = url;
			aux.text = text;
			aux.active = true;
			//aux.clase = "breadcrumb-item active";
			$scope.rutas.push(aux);
		};
		
	};
	
	$scope.atras = function(){
		ruta = $scope.rutas.pop();
		return ruta.url;
	};
	
	//
	
	//Prototipo para eliminar elementos duplicados de un array
	Array.prototype.unique=function(a){
		  return function(){return this.filter(a)}}(function(a,b,c){return c.indexOf(a,b+1)<0
		});
	//fin prototipp
	
	$scope.showLogin = false;
	
	$scope.menu = [
	        {url:'#/departamentos/', text:'Departamentos'},
	        {url:'#/carreras/', text:'Carreras'},
	        {url:'#/profesores/nombre/--any--/apellido/--any--/cedula/--any--/pag/--any--/column/--any--/order/--any--', text:'Profesores'},
	        {url:'#/asignatura/--any--/--any--/dpto/--any--/turno/--any--/codAsig/--any--/seccion/--any--/periodo/--any--/pag/--any--/order/true/column/asignatura/vista/--any--', text:'Asignaturas'},
	        /*{url:'#/catedras/', text:'Cátedras'},*/
	        {url:'#/aulas/', text:'Aulas'},
	        ];
		
	$scope.login = function(){
		LoginService.login($scope.username, $scope.password, function(){
			$scope.logueado = false;
			if(LoginService.isLogin()){
				toaster.pop({	type : "success",
					title : "Inicio de sesión",
					body : "Se ha iniciado sesión correctamente"
				});
				LoginService.queryPerfil($scope.setPerfil);
				$scope.idProfesor = LoginService.getIdProfesor();
				$scope.logueado = true;
				$location.path("home");
			}else{
				toaster.pop({	type : "error",
					title : "Inicio de sesión",
					body : "Nombre de Usuario o contraseña incorrectos"
				});
			}
		});
		
		
	};
	
	$scope.setPerfil = function(){
		$scope.permisos = [];
		$scope.permisos["GESTION DE HORARIO"]=false;
        $scope.permisos["GESTION DE USUARIOS"]=false;
        $scope.permisos["GESTION ACADEMICA"]=false;
        $scope.permisos["AUTOGESTION"]=false;
        $scope.permisos["CONFIGURACION"]=false;
		perfil = LoginService.getPerfil();
		for(i=0; i<perfil.length; i++){
			$scope.permisos[perfil[i].nombre] = true;
		}
	}
	
	$scope.logout = function(){
		LoginService.logout();
		$scope.logueado = false;
		$scope.showLogin = false;
		$location.path("");
	};
	
	$scope.loginShow = function(){
		LoginService.logout();
		$scope.logueado = false;
		$location.path("");
		$scope.showLogin = true;
	};
	$scope.filtrar = function(){
		$scope.currentPage = 0;
		$scope.search = angular.fromJson(angular.toJson($scope.search_aux));;
	};
	$scope.mostrarSeleccionados = function(){
		$scope.search = {selected : true}
	};
	
	if(LoginService.isLogin()){
		$scope.logueado = true;
		LoginService.queryPerfil($scope.setPerfil);
		$scope.idProfesor = LoginService.getIdProfesor();
	}else{
		$scope.logueado = false;
	}
		
}]);



fp_app.filter('slice', function() {
	  return function(arr, start, end) {
	    return (arr || []).slice(start, end);
	  };
	});


var fp_app = angular.module("app");

fp_app.controller('usuarioCtrl', ['$scope','UsuarioService','ProfesorService','$location','toaster','mensajes','ngDialog','$routeParams'
                           ,function( $scope , UsuarioService, ProfesorService, $location , toaster , mensajes , ngDialog, $routeParams) {

	
	$scope.$parent.nuevaRuta("usuarios", $location.url(), "Usuarios", true);
		
	$scope.usuarios = [];
	$scope.ultimaUsuario = false;
	$scope.esperandoUsuario = false;
	$scope.errorUsuario = false;
	$scope.vacioUsuario = false;
	var any = "--any--";
	
	
	$scope.busquedaUsu_nombre = $routeParams.fnombre.toString();
	$scope.busquedaUsu_order = $routeParams.forder.toString();
	$scope.busquedaUsu_column = $routeParams.fcolumn.toString();
	$scope.busquedaUsu_pagina = $routeParams.fpag.toString();
	if( $scope.busquedaUsu_nombre == any) $scope.busquedaUsu_nombre="";
	if( $scope.busquedaUsu_order == any) $scope.busquedaUsu_order="true";
	if( $scope.busquedaUsu_column == any) $scope.busquedaUsu_column="nombre";
	if( $scope.busquedaUsu_pagina == any) $scope.busquedaUsu_pagina="0";
	

	
	$scope.page = parseInt($scope.busquedaUsu_pagina)/10 + 1;
	$scope.orderAsc=false;
	if( $scope.busquedaUsu_order == "true") $scope.orderAsc=true;
	$scope.order = $scope.busquedaUsu_column;
	
	$scope.ordenar = function(columna){
		order = !$scope.orderAsc?"true":"false";
		$location.path(
				'/usuarios/nombre/' + $routeParams.fnombre.toString() +
				'/pag/' + $routeParams.fpag.toString() +
				'/column/' + columna +
				'/order/' + order
		);
	}
	
	// END ORDENACION

	
	solicitarPaginaUsu = function(){
		$scope.usuarios = [];
		$scope.esperandoUsuario = true;
		$scope.errorUsuario = false;
		$scope.vacioUsuario = false;
		
		UsuarioService.list($scope.busquedaUsu_pagina, $scope.busquedaUsu_nombre, $scope.busquedaUsu_column, $scope.busquedaUsu_order, function(response,status){

			if(status==200 && response.success){
				$scope.usuarios = response.data;

				$scope.ultimaUsuario = response.message == "ultima";
				$scope.esperandoUsuario = false;
				if(response.data.length==0)
					$scope.vacioUsuario = true;
			}else{
				$scope.esperandoUsuario = false;
				$scope.errorUsuario = true;
			}
			if(status==200){
				if(!response.success)
					toaster.pop({	type : response.type,
									title : response.reason,
									body : response.message
					});
			}else{
				toaster.pop({	type : "error",
								title : "Error",
								body : mensajes.errorConexion
				});
			}
		});
	};
	
	$scope.anteriorUsu = function(){
		pagina = (parseInt($scope.busquedaUsu_pagina) - 10).toString();
		$location.path(
				'/usuarios/nombre/' + $routeParams.fnombre.toString() +
				'/pag/' + pagina +
				'/column/' + $routeParams.fcolumn.toString() +
				'/order/' + $routeParams.forder.toString()
		);
	}
	
	$scope.siguienteUsu = function(){
		pagina = (parseInt($scope.busquedaUsu_pagina) + 10).toString();
		$location.path(
				'/usuarios/nombre/' + $routeParams.fnombre.toString() +
				'/pag/' + pagina +
				'/column/' + $routeParams.fcolumn.toString() +
				'/order/' + $routeParams.forder.toString()
		);
	}
	
	$scope.buscarUsu = function(){
		nombre = $scope.busquedaUsu_nombre == ""?any:$scope.busquedaUsu_nombre;
			
		$location.path(
				'/usuarios/nombre/' + nombre +
				'/pag/' + any +
				'/column/' + $routeParams.fcolumn.toString() +
				'/order/' + $routeParams.forder.toString()
		);
	}

	solicitarPaginaUsu();
	
	  $scope.open = function(tipo) {
		  if(tipo == 1){
			  $scope.title_modal = "Nuevo Usuario";
			  $scope.u = {};
			  $scope.u.nombre = null;
			  $scope.u.nombreProfesor = null;
			  $scope.u.bloqueado=false;  
		  } else {
			  $scope.title_modal = "Edición de Usuario";  
		  }
	  };
		    
		$scope.message_modal = "¿Está seguro que desea guardar los cambios?"
		
		$scope.guardarUsu = function(){
			var form = document.getElementById("usuModal");
			
			if($scope.u.nombre == null || $scope.u.nombreProfesor == null){
				  form.classList.add("was-validated");
			} else {
				UsuarioService.guardar($scope.u, function(response, status){
	        		if(status==200){
	        			toaster.pop({ 	type : response.type,
							title : response.reason,
							body : response.message
						});
	        			solicitarPaginaUsu();

	    	        	$('#usuModal').modal('hide'); // cerramos el modal si esta todo OK
	        		}else{
	        			toaster.pop({ 	type : 'error',
							title : 'Problemas de Conexión',
							body : 'No se puede conectar con el servidor'
						});
	        		}
	        	})
	        	
			}

        	
		};
		 		
		$scope.asignarProfesor = function(){
			$scope.profesores = [];
			$scope.page = 1;
			$scope.ultima = false;
			$scope.esperando = false;
			$scope.error = false;
			$scope.vacio = false;
			$scope.profesorSeleccionado = false;
			// ORDENACION
			$scope.orderAsc = true;
			$scope.order = "nombre";
			// END ORDENACION
			$scope.title_modal_profesor = "Asignar Profesor";
		};
		
		
		solicitarPaginaProf = function(pagina){
			$scope.profesores = [];
			$scope.esperando = true;
			$scope.error = false;
			$scope.vacio = false;
			offset = ((pagina-1)*10);
			ProfesorService.listSinUsuario(offset.toString(),$scope.busqueda_nombre, "",$scope.busqueda_cedula,$scope.order,$scope.orderAsc, function(response,status){
				if(status==200 && response.success){
					$scope.profesores = response.data;
					$scope.ultima = response.message == "ultima";
					$scope.esperando = false;
					if(response.data.length==0)
						$scope.vacio = true;
				}else{
					$scope.page = 1;
					console.log(status);
					console.log(response);
					$scope.esperando = false;
					$scope.error = true;
				}
				if(status==200){
					if(!response.success)
						toaster.pop({	type : response.type,
										title : response.reason,
										body : response.message
						});
				}else{
					toaster.pop({	type : "error",
									title : "Error",
									body : mensajes.errorConexion
					});
				}
			});
		};
		
		$scope.anterior = function(){
			$scope.page = $scope.page>1?$scope.page-1:1;
			solicitarPaginaProf($scope.page);
		};
		
		$scope.siguiente = function(){
			$scope.page = $scope.ultimaUsuario?$scope.page:$scope.page+1;
			solicitarPaginaProf($scope.page);
		};
		
		$scope.buscar = function(nombre, cedula){
			$scope.busqueda_nombre = nombre;
			$scope.busqueda_cedula = cedula;
			$scope.page = 1;
			solicitarPaginaProf($scope.page);
		};
		
		
		$scope.selectedProfesor = function(index){
			$scope.indexProfesor = index;
			$scope.busqueda_nombre = $scope.profesores[$scope.indexProfesor].nombre;
			$scope.busqueda_cedula = $scope.profesores[$scope.indexProfesor].cedula;
			$scope.profesorSeleccionado = true;
			$scope.u.nombreProfesor = $scope.profesores[$scope.indexProfesor].titulo + ' ' + $scope.profesores[$scope.indexProfesor].nombre + ' ' + $scope.profesores[$scope.indexProfesor].apellido;
			$scope.u.idProfesor = $scope.profesores[$scope.indexProfesor].idProfesor;
		};
		
		  $scope.gotoProfesor = function(index){
			  $location.path('profesor/' + $scope.usuarios[index].idProfesor);
		  };
		  
			$scope.fecha = function timeConverter(UNIX_timestamp){
				  var a = new Date(UNIX_timestamp);
				  var months = ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
				  var year = a.getFullYear();
				  var month = months[a.getMonth()];
				  var date = a.getDate();
				  var hour = a.getHours();
				  var min = a.getMinutes();
				  var sec = a.getSeconds();
				  var time = date + ' ' + month + ' ' + year + ' T' + hour + ':' + min + ':' + sec;
				  return time;
				}
	
}]);

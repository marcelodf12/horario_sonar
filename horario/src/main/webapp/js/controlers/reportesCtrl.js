var fp_app = angular.module("app");
fp_app.controller('reportesCtrl', ['$scope','$http','$location','toaster','ReporteService','$routeParams','config','$filter','$interval'
                     , function($scope , $http , $location , toaster,ReporteService,$routeParams,config,$filter,$interval) {

	$scope.$parent.nuevaRuta("Reportes", $location.url(), "Reportes", false);
	
	$scope.idHorario = $routeParams.id.toString();
	
	$scope.search = {label:''};
	
	ReporteService.listQueries(function(response,status){
		if(status==200 && response.success){
			$scope.queries = response.data;
			$scope.identificador='profesores';
		}
	});
	
	$scope.progreso = -1;
	
	
	$scope.consultarProgreso = function(){
		ReporteService.consultarProgreso(function(response,status){
			if(status==200){
				if(typeof(response.data)=='undefined' || response.data == null){
					$scope.progreso = -1;
				}else{
					$scope.progreso = response.data;
				}
			}
		});
	}
	$scope.consultarProgreso();
	
	$scope.refresh_reporte=$interval($scope.consultarProgreso, 5000);
	
	$scope.$on('$destroy', function() {
		console.log("Destruyendo interval reporte");
		$interval.cancel($scope.refresh_reporte);
      });
	
	$scope.actualizarReportes = function(){
		ReporteService.lanzarActualizacion($scope.idHorario, function(response,status){
			console.log(response);
		});
	}
	
	$scope.downloadReport=function(tipo){
		if($scope.identificador=='profesores' && tipo=='horario'){
			$scope.ruta = "HorarioPorProfesor";
			$scope.prefijo = "horario_profesor_";
			$scope.elementos = $scope.queries.profesores;
		}
		if($scope.identificador=='aulas' && tipo=='horario'){
			$scope.ruta = "HorarioPorAula";
			$scope.prefijo = "horario_aula_";
			$scope.elementos = $scope.queries.aulas;
		}
		if($scope.identificador=='carreras' && tipo=='horario'){
			$scope.ruta = "HorarioPorCarrera";
			$scope.prefijo = "horario_carrera_";
			$scope.elementos = $scope.queries.carreras;
		}
		if($scope.identificador=='profesores' && tipo=='examenes'){
			$scope.ruta = "ExamenesPorProfesor";
			$scope.prefijo = "examenes_profesor_";
			$scope.elementos = $scope.queries.profesores;
		}
		if($scope.identificador=='aulas' && tipo=='examenes'){
			$scope.ruta = "ExamenesPorAula";
			$scope.prefijo = "examenes_aula_";
			$scope.elementos = $scope.queries.aulas;
		}
		if($scope.identificador=='carreras' && tipo=='examenes'){
			$scope.ruta = "ExamenesPorCarrera";
			$scope.prefijo = "examenes_carrera_";
			$scope.elementos = $scope.queries.carreras;
		}
		
		query = 
			{
				ruta:$scope.ruta,
				prefijo:$scope.prefijo,
				elements:$scope.elementos
				
			}
		$scope.downURL = config.hostApiRest + 'reportes/horario/'+$scope.idHorario + '?query='+encodeURI(angular.toJson(query));
		window.open($scope.downURL, 'newwindow', 'width=300,height=250');
		console.log($scope.downURL);
	}
	$scope.selectedAll=true;
	
	$scope.selectAll= function(){
		   var filter = $filter('filter');

		   $scope.selectAllFilteredItems = function (){
		      var filtered = filter($scope.queries[$scope.identificador], $scope.search);

		      angular.forEach(filtered, function(item) {
		         item.selected = $scope.selectedAll;
		      });
		   };
		   $scope.selectAllFilteredItems();
		   $scope.selectedAll = !$scope.selectedAll;
	}
		
}]);


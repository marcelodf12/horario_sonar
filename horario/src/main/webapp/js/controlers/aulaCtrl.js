var fp_app = angular.module("app");

fp_app.controller('aulasCtrl', ['$scope','AulaService','ngDialog','toaster','mensajes','$location'
                      , function($scope , AulaService , ngDialog , toaster , mensajes , $location) {
	
	$scope.$parent.nuevaRuta("aulas", $location.url(), "Aulas", true);
	
	$scope.aulas = [];
	$scope.page = 1;
	$scope.ultima = false;
	busqueda_nombre = "";
	busqueda_capacidad = "";
	busqueda_tipo = "";
	$scope.esperando = false;
	$scope.error = false;
	$scope.vacio = false;
	$scope.orderNombre = true;
	$scope.orderCapacidad = false;
	$scope.orderTipo = false;
	$scope.orderAsc = true;
	$scope.tipos = {};
	
	AulaService.listTiposAula(function(response,status){
		if(status==200){
			$scope.tipos = response.data;
		}else{
			toaster.pop({	type : "error",
	    					title : "Error",
	    					body : mensajes.errorConexion
			});
		}
	});
	
	solicitarPagina = function(pagina){
		if($scope.orderNombre) order = "nombre";
		if($scope.orderCapacidad) order = "capacidad";
		if($scope.orderTipo) order = "tipo";
		$scope.aulas = [];
		$scope.esperando = true;
		$scope.error = false;
		$scope.vacio = false;
		$scope.nombre = busqueda_nombre;
		$scope.capacidad = busqueda_capacidad;
		$scope.tipo = busqueda_tipo;
		offset = ((pagina-1)*10);
		AulaService.list(offset.toString(), busqueda_nombre, busqueda_capacidad, busqueda_tipo, order, $scope.orderAsc, function(response,status){
			if(status==200 && response.success){
				$scope.aulas = response.data;
				$scope.ultima = response.message == "ultima";
				$scope.esperando = false;
				if(response.data.length==0)
					$scope.vacio = true;
			}else{
				$scope.page = 1;
				console.log(status);
				console.log(response);
				$scope.esperando = false;
				$scope.error = true;
			}
			if(status==200){
				if(!response.success)
					toaster.pop({	type : response.type,
									title : response.reason,
									body : response.message
					});
			}else{
				toaster.pop({	type : "error",
								title : "Error",
								body : mensajes.errorConexion
				});
			}
		});
	};
	
	$scope.anterior = function(){
		$scope.page = $scope.page>1?$scope.page-1:1;
		solicitarPagina($scope.page);
	}
	
	$scope.siguiente = function(){
		$scope.page = $scope.ultima?$scope.page:$scope.page+1;
		solicitarPagina($scope.page);
	}
	
	$scope.buscar = function(){
		busqueda_nombre = $scope.nombre;
		busqueda_capacidad = $scope.capacidad;
		busqueda_tipo = $scope.tipo;
		$scope.page = 1;
		solicitarPagina($scope.page);
	}
	
	$scope.ordenarNombre = function(){
		if($scope.orderNombre){
			$scope.orderAsc = !$scope.orderAsc;
		}else{
			$scope.orderNombre = true;
			$scope.orderCapacidad = false;
			$scope.orderTipo = false;
			
		}
		$scope.page = 1;
		solicitarPagina(1);
		
	}
	
	$scope.ordenarCapacidad = function(){
		if($scope.orderCapacidad){
			$scope.orderAsc = !$scope.orderAsc;
		}else{
			$scope.orderNombre = false;
			$scope.orderCapacidad = true;
			$scope.orderTipo = false;
		}
		$scope.page = 1;
		solicitarPagina(1);
	}
	
	$scope.ordenarTipo = function(){
		if($scope.orderTipo){
			$scope.orderAsc = !$scope.orderAsc;
		}else{
			$scope.order = false;
			$scope.orderCapacidad = false;
			$scope.orderTipo = true;
		}
		$scope.page = 1;
		solicitarPagina(1);
	}

	solicitarPagina(1);
	
	  $scope.open = function() {
		  $scope.title_modal = "Nueva Aula";
		  $scope.nombre_aula = "";
		  $scope.capacidad_aula = "";
		  $scope.id_aula = -1;
	  };
	  
	  $scope.edit = function(index) {
		  $scope.title_modal = "Editar Aula";
		  $scope.nombre_aula = $scope.aulas[index].nombre;
		  $scope.capacidad_aula = $scope.aulas[index].capacidad;
		  $scope.tipo = $scope.aulas[index].idTipoAula;
		  $scope.id_aula = $scope.aulas[index].idAula;
	  };
		  
	  $scope.eliminar = function(index) {
		  $scope.title_modal = "Eliminar Aula";
		  $scope.message_modal = "Está seguro que desea eliminar el Aula " + $scope.aulas[index].nombre;
		  $scope.id_aula = $scope.aulas[index].idAula;
		  
	  };
	  
	  $scope.confirmDelete = function(){
      	AulaService.eliminar($scope.id_aula, function(response,status){
  			if(status==200){
  				toaster.pop({	type : response.type,
									title : response.reason,
									body : response.message
					});
  			}else{
  				toaster.pop({	type : "error",
			    					title : "Error",
			    					body : mensajes.errorConexion
  				});
  			}
  			solicitarPagina(1);
  		});
      
	  }
	  
	  $scope.guardar = function(id, nombre, capacidad, tipo) {

		var form = document.getElementById("aulaModal");
			
		if(nombre.length == 0 || capacidad.length == 0 || tipo.length == 0){
			  form.classList.add("was-validated");
		} else {
				  
			  if($scope.id_aula == -1){
				  AulaService.nuevo(nombre, capacidad, tipo, function(response,status){
					  if(status==200){
		    				toaster.pop({	type : response.type,
											title : response.reason,
											body : response.message
							});
							$('#aulaModal').modal('hide'); // cerramos el modal si esta todo OK
		    			}else{
		    				toaster.pop({	type : "error",
					    					title : "Error",
					    					body : mensajes.errorConexion
		    				});
		    			}
		    			solicitarPagina(1);
		    		});
			  }
			  else{
				  AulaService.guardar(id, nombre, capacidad,tipo, function(response,status){
					  if(status==200){
		    				toaster.pop({	type : response.type,
											title : response.reason,
											body : response.message
							});
							$('#aulaModal').modal('hide'); // cerramos el modal si esta todo OK
		    			}else{
		    				toaster.pop({	type : "error",
					    					title : "Error",
					    					body : mensajes.errorConexion
		    				});
		    			}
		    			solicitarPagina(1);
		    		});
			  }
		  }
	  };
	  
	  
	
}]);

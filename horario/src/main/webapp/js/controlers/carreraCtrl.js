var fp_app = angular.module("app");

fp_app.controller('carrerasCtrl', ['$scope','CarreraService','ngDialog','$location','toaster','mensajes'
                          ,function($scope , CarreraService , ngDialog , $location , toaster , mensajes ) {
	
	$scope.$parent.nuevaRuta("carrera", $location.url(), "Carreras", true);
	
	$scope.carreras = [];
	$scope.page = 1;
	$scope.ultima = false;
	busqueda_nombre = "";
	busqueda_sigla = "";
	$scope.esperando = false;
	$scope.error = false;
	$scope.vacio = false;
	$scope.orderNombre = true;
	$scope.orderSigla = false;
	$scope.orderAsc = true;
	
	solicitarPagina = function(pagina){
		if($scope.orderNombre) order = "nombre";
		if($scope.orderSigla) order = "sigla";
		$scope.carreras = [];
		$scope.esperando = true;
		$scope.error = false;
		$scope.vacio = false;
		$scope.nombre = busqueda_nombre;
		$scope.sigla = busqueda_sigla;
		offset = ((pagina-1)*10);
		CarreraService.list(offset.toString(), busqueda_nombre, busqueda_sigla, order, $scope.orderAsc, function(response,status){
			if(status==200 && response.success){
				$scope.carreras = response.data;
				$scope.ultima = response.message == "ultima";
				$scope.esperando = false;
				if(response.data.length==0)
					$scope.vacio = true;
			}else{
				$scope.page = 1;
				console.log(status);
				console.log(response);
				$scope.esperando = false;
				$scope.error = true;
			}
			if(status==200){
				if(!response.success)
					toaster.pop({	type : response.type,
									title : response.reason,
									body : response.message
					});
			}else{
				toaster
				.pop({	type : "error",
						title : "Error",
						body : mensajes.errorConexion
				});
			}
		});
	};
	
	$scope.anterior = function(){
		$scope.page = $scope.page>1?$scope.page-1:1;
		solicitarPagina($scope.page);
	}
	
	$scope.siguiente = function(){
		$scope.page = $scope.ultima?$scope.page:$scope.page+1;
		solicitarPagina($scope.page);
	}
	
	$scope.buscar = function(){
		busqueda_nombre = $scope.nombre;
		busqueda_sigla = $scope.sigla;
		$scope.page = 1;
		solicitarPagina($scope.page);
	}
	
	$scope.ordenarNombre = function(){
		if($scope.orderNombre){
			$scope.orderAsc = !$scope.orderAsc;
		}else{
			$scope.orderNombre = true;
			$scope.orderSigla = false;
		}
		$scope.page = 1;
		solicitarPagina(1);
		
	}
	
	$scope.ordenarSigla = function(){
		if($scope.orderSigla){
			$scope.orderAsc = !$scope.orderAsc;
		}else{
			$scope.orderNombre = false;
			$scope.orderSigla = true;
		}
		$scope.page = 1;
		solicitarPagina(1);
	}

	solicitarPagina(1);
	  
	  $scope.gotoCarrera = function(index){
		url = '/asignatura/--any--/--any--/carrera/'+$scope.carreras[index].idCarrera;
		$scope.$parent.ultimaCarreraVisitada = $scope.carreras[index].nombre;
		$scope.$parent.nuevaRuta("asignaturasCarrera", url, "Asignaturas de la Carrera", false);
		$location.path(url);
	  };
	  $scope.gotoConflictoCarrera = function(index){
		url = '/carreras/conflictos/'+$scope.carreras[index].idCarrera;
		$scope.$parent.ultimaCarreraVisitada = $scope.carreras[index].nombre;
		$scope.$parent.nuevaRuta("asignaturasConflictosCarrera", url, "Autoconfiguración de Conflictos", false);
		$location.path(url);
	  };
	  
	  $scope.open = function() {
		  $scope.title_modal = "Nueva Carrera";
		  $scope.nombre_carrera = "";
		  $scope.sigla_carrera = "";
		  $scope.codCarSec= "";
		  $scope.id_carrera = -1;
	  };
	  
	  $scope.edit = function(index) {
		  $scope.title_modal = "Editar Carrera";
		  $scope.nombre_carrera = $scope.carreras[index].nombre;
		  $scope.sigla_carrera= $scope.carreras[index].sigla;
		  $scope.codCarSec= $scope.carreras[index].codCarSec;
		  $scope.id_carrera = $scope.carreras[index].idCarrera;
	};
	
	  
	  $scope.eliminar = function(index) {
		  $scope.title_modal = "Eliminar Carrera";
		  $scope.message_modal = "Está seguro que desea eliminar la Carrera " + $scope.carreras[index].nombre;
		  $scope.id_carrera = $scope.carreras[index].idCarrera;
	  };
	  
	  $scope.confirmDelete = function(){
		  CarreraService.eliminar($scope.id_carrera, function(response,status){
			if(status==200){
				toaster.pop({	type : response.type,
								title : response.reason,
								body : response.message
				});
    		}else{
				toaster.pop({	type : "error",
		    					title : "Error",
		    					body : mensajes.errorConexion
				});
			}
			solicitarPagina(1);
		});
	  };
	  
	  $scope.guardar = function(id, nombre, sigla, codCarSec) {
		  var form = document.getElementById("carreraModal");
			
			if(nombre.length == 0 || sigla.length == 0 || codCarSec.length == 0){
				  form.classList.add("was-validated");
			} else {
		  
				  if($scope.id_carrera == -1){
					  CarreraService.nuevo(nombre, sigla, codCarSec, function(response,status){
						  if(status==200){
			    				toaster.pop({	type : response.type,
												title : response.reason,
												body : response.message
								});
								$('#carreraModal').modal('hide'); // cerramos el modal si esta todo OK
			    			}else{
			    				toaster.pop({	type : "error",
						    					title : "Error",
						    					body : mensajes.errorConexion
			    				});
			    			}
			    			solicitarPagina(1);
			    		});
				  }
				  else{
					  CarreraService.guardar(id, nombre, sigla, codCarSec, function(response,status){
						  if(status==200){
			    				toaster.pop({	type : response.type,
												title : response.reason,
												body : response.message
								});
								$('#carreraModal').modal('hide'); // cerramos el modal si esta todo OK
			    			}else{
			    				toaster.pop({	type : "error",
						    					title : "Error",
						    					body : mensajes.errorConexion
			    				});
			    			}
			    			solicitarPagina(1);
			    		});
				  }
			}
	  };
}]);
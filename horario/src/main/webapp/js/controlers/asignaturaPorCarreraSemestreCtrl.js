var fp_app = angular.module("app");

fp_app.controller('asignaturaPorCarreraSemestreCtrl', ['$scope','AsignaturaService','CarreraService','ngDialog','$routeParams','toaster','mensajes','$location','$sessionStorage'
                           , function($scope , AsignaturaService, CarreraService , ngDialog , $routeParams , toaster , mensajes , $location , $sessionStorage) {


	
	$scope.$parent.nuevaRuta("asignaturasCarrera", $location.url(), "Asignaturas de la Carrera", false);
	$scope.title = $scope.$parent.ultimaCarreraVisitada;

	$scope.idCarrera = $routeParams.idCarrera.toString();
	idCarrera = $routeParams.idCarrera.toString();
	
	$scope.asignaturas = {};	

	$scope.showMaterias = false;
	
	obtenerAsignaturasDeLaCarrera = function(){
		$scope.asignaturas = {};
		$scope.esperando = true;
		$scope.error = false;
		$scope.vacio = false;
		AsignaturaService.listAsignaturasCarrera($scope.idCarrera, function(response, status) {
				$scope.esperando = false;
				if (status == 200 && response.success) {
					$scope.semestres = response.data;
					$scope.asignaturas = $scope.semestres[$scope.semestres.length-1].asignaturas; 	
					for (var i = 0; i < $scope.semestres.length; i++) {
						$scope.semestres[i].clase = ["nav-link"];
					}
					$scope.semestres[$scope.semestres.length-1].clase = ["nav-link","active"];
					$scope.showMaterias = true;
				} else if (status == 200) {
					$scope.vacio = true;
					toaster.pop({
						type : response.type,
						title : response.reason,
						body : response.message
					});
				} else {
					$scope.error = true;
					toaster.pop({
						type : "error",
						title : "Error",
						body : mensajes.errorConexion
					});
				}
				});
			
	};
	
	obtenerAsignaturasDeLaCarrera();
	
	$scope.refreshAsignaturas = function(index){
		$scope.asignaturas = index.asignaturas;
		$scope.showMaterias = true;
	}
	
	$scope.editarAsignaturaPrincipal = function(id){
		$sessionStorage.ultimaURL = $location.url();
		$location.path('/editarAsignatura/' + id);
	};
	
}]);
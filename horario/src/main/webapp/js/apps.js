var fp_app = angular.module("app", ['ngRoute','ngDialog','toaster','ngStorage','ngSanitize']);
fp_app.config(['$routeProvider','$locationProvider', '$httpProvider', function($routeProvider,$locationProvider,$httpProvider) {
		$httpProvider.interceptors.push('RequestHeadersInterceptor');
		$httpProvider.interceptors.push('ResponseInterceptor');
		$locationProvider.html5Mode(true);
        $routeProvider
                .when('/home/', {
                    templateUrl: 'view/home.html',
                    controller: 'homeCtrl'
                })
				.when('/profesores/nombre/:fnombre/apellido/:fapellido/cedula/:fcedula/pag/:fpag/column/:fcolumn/order/:forder', {
                    templateUrl: 'view/profesores.html',
                    controller: 'profesoresCtrl'
                })
                .when('/profesor/:id', {
                    templateUrl: 'view/profesor.html',
                    controller: 'profesorCtrl'
                })
                .when('/profesor/:idProfesor/slot/:id', {
                    templateUrl: 'view/profesorSlot.html',
                    controller: 'profesorSlotsCtrl'
                })
				.when('/carreras/', {
                    templateUrl: 'view/carreras.html',
                    controller: 'carrerasCtrl'
                })
                .when('/carreras/:id', {
                    templateUrl: 'view/carreras_asignaturas.html',
                    controller: 'asignaturasCtrl'
				})
				.when('/carreras/conflictos/:id', {
                    templateUrl: 'view/carreras_conflictos.html',
                    controller: 'carrerasConflictosCtrl'
                })
				.when('/aulas/', {
                    templateUrl: 'view/aulas.html',
                    controller: 'aulasCtrl'
                })
                .when('/departamentos/', {
                    templateUrl: 'view/departamentos.html',
                    controller: 'departamentoCtrl'
                })
                .when('/asignaturaNuevo/', {
                    templateUrl: 'view/asignaturaNuevo.html',
                    controller: 'asignaturaNuevoCtrl'
                })
                .when('/asignatura/:idAsignatura/:fAsignatura/dpto/:fDepartamento/turno/:fTurno/codAsig/:fCodAsig/seccion/:fSeccion/periodo/:fPeriodo/pag/:fPagina/order/:order/column/:column/vista/:vista', {
                    templateUrl: 'view/asignatura.html',
                    controller: 'asignaturaCtrl'
                })
                .when('/editarAsignatura/:id', {
                    templateUrl: 'view/asignaturaNuevo.html',
                    controller: 'asignaturaNuevoCtrl'
                })
                .when('/horarios/', {
                    templateUrl: 'view/horarios/horariosAdmin.html',
                    controller: 'horarioCtrl'
                })
                .when('/horarios/:id/reportes', {
                    templateUrl: 'view/reportes.html',
                    controller: 'reportesCtrl'
                })
                .when('/horarioVer/:id', {
                    templateUrl: 'view/horarios/verHorario.html',
                    controller: 'verHorarioCtrl'
                })
                .when('/verAulas/:id/:dia', {
                    templateUrl: 'view/horarios/verAulas.html',
                    controller: 'verAulasCtrl'
                })
                .when('/recalculo/:id', {
                    templateUrl: 'view/horarios/recalculo.html',
                    controller: 'recalculoCtrl'
                })
                .when('/borrador/:id', {
                    templateUrl: 'view/horarios/editHorario.html',
                    controller: 'editHorarioCtrl'
                })
                .when('/', {
					templateUrl: 'view/home.html',
					controller: 'homeCtrl'
                })
                .when('/micuenta/', {
                    templateUrl: 'view/miCuenta.html',
                    controller: 'miCuentaCtrl'
                })
                .when('/usuarios/nombre/:fnombre/pag/:fpag/column/:fcolumn/order/:forder', {
                    templateUrl: 'view/usuarios.html',
                    controller: 'usuarioCtrl'
                })
                .when('/examenes/horario/:id', {
                    templateUrl: 'view/horarios/examenes.html',
                    controller: 'examenesCtrl'
                })
                .when('/horario/inscriptos/:id', {
                    templateUrl: 'view/horarios/cantidadAlumnos.html',
                    controller: 'cantidadAlumnosCtrl'
                })
                .when('/asignatura/--any--/--any--/carrera/:idCarrera', {
                    templateUrl: 'view/asignaturaPorCarreraSemestre.html',
                    controller: 'asignaturaPorCarreraSemestreCtrl'
                })
                .when('/configuracion/', {
                    templateUrl: 'view/parametros.html',
                    controller: 'configuracionCtrl'
                })
                .when('/404/', {
                    templateUrl: 'view/404.html'
                })
                .otherwise({templateUrl: 'view/404.html'});
    }]);
fp_app.constant('mensajes', {
	errorConexionTitle: "Error al conectarse al servidor.",
	errorConexionMsg: "Favor verifique su conexión a internet.",
	errorInternoTitle: "Se ha producido un error desconocido.",
	errorInternoMsg: "Favor intente nuevamente mas tarde.",
    recordatorioGuardar:"Recuerde guardar los cambios antes de ir a otra pagina.",
    noGuardado:"Se ha restablecido la configuración.",
    catedraNoSeleccionada:"Debe seleccionar una cátedra",
    notFound:"Error 404: El elemento no existe"
})
fp_app.constant('config', {
    hostApiRest: "http://localhost:8080/horario/rest/"
})

fp_app.factory("cookie",["config", function(config){
	return {
		get: function getCookie(cname) {
		    var name = cname + "=";
		    var decodedCookie = decodeURIComponent(document.cookie);
		    var ca = decodedCookie.split(';');
		    for(var i = 0; i <ca.length; i++) {
		        var c = ca[i];
		        while (c.charAt(0) == ' ') {
		            c = c.substring(1);
		        }
		        if (c.indexOf(name) == 0) {
		            return c.substring(name.length, c.length);
		        }
		    }
		    return "";
		},
		set: function setCookie(cname, cvalue) {
			exdays = 1;
		    var d = new Date();
		    d.setTime(d.getTime() + (exdays*24*60*60*1000));
		    var expires = "expires="+ d.toUTCString();
		    document.cookie = cname + "=" + cvalue + ";" + expires + ";path="+config.hostApiRest;
		}
	}
}]);

fp_app.factory("sha256", function() {

	return {

		convertToSHA256: function(data){
			

			var rotateRight = function (n,x) {
				return ((x >>> n) | (x << (32 - n)));
			}
			var choice = function (x,y,z) {
				return ((x & y) ^ (~x & z));
			}
			function majority(x,y,z) {
				return ((x & y) ^ (x & z) ^ (y & z));
			}
			function sha256_Sigma0(x) {
				return (rotateRight(2, x) ^ rotateRight(13, x) ^ rotateRight(22, x));
			}
			function sha256_Sigma1(x) {
				return (rotateRight(6, x) ^ rotateRight(11, x) ^ rotateRight(25, x));
			}
			function sha256_sigma0(x) {
				return (rotateRight(7, x) ^ rotateRight(18, x) ^ (x >>> 3));
			}
			function sha256_sigma1(x) {
				return (rotateRight(17, x) ^ rotateRight(19, x) ^ (x >>> 10));
			}
			function sha256_expand(W, j) {
				return (W[j&0x0f] += sha256_sigma1(W[(j+14)&0x0f]) + W[(j+9)&0x0f] + 
			sha256_sigma0(W[(j+1)&0x0f]));
			}

			/* Hash constant words K: */
			var K256 = new Array(
				0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5,
				0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
				0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3,
				0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
				0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc,
				0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
				0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7,
				0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
				0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13,
				0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
				0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3,
				0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
				0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5,
				0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
				0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208,
				0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
			);

			/* global arrays */
			var ihash, count, buffer;
			var sha256_hex_digits = "0123456789abcdef";

			/* Add 32-bit integers with 16-bit operations (bug in some JS-interpreters: 
			overflow) */
			function safe_add(x, y)
			{
				var lsw = (x & 0xffff) + (y & 0xffff);
				var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
				return (msw << 16) | (lsw & 0xffff);
			}

			/* Initialise the SHA256 computation */
			function sha256_init() {
				ihash = new Array(8);
				count = new Array(2);
				buffer = new Array(64);
				count[0] = count[1] = 0;
				ihash[0] = 0x6a09e667;
				ihash[1] = 0xbb67ae85;
				ihash[2] = 0x3c6ef372;
				ihash[3] = 0xa54ff53a;
				ihash[4] = 0x510e527f;
				ihash[5] = 0x9b05688c;
				ihash[6] = 0x1f83d9ab;
				ihash[7] = 0x5be0cd19;
			}

			/* Transform a 512-bit message block */
			function sha256_transform() {
				var a, b, c, d, e, f, g, h, T1, T2;
				var W = new Array(16);

				/* Initialize registers with the previous intermediate value */
				a = ihash[0];
				b = ihash[1];
				c = ihash[2];
				d = ihash[3];
				e = ihash[4];
				f = ihash[5];
				g = ihash[6];
				h = ihash[7];

			        /* make 32-bit words */
				for(var i=0; i<16; i++)
					W[i] = ((buffer[(i<<2)+3]) | (buffer[(i<<2)+2] << 8) | (buffer[(i<<2)+1] 
			<< 16) | (buffer[i<<2] << 24));

			        for(var j=0; j<64; j++) {
					T1 = h + sha256_Sigma1(e) + choice(e, f, g) + K256[j];
					if(j < 16) T1 += W[j];
					else T1 += sha256_expand(W, j);
					T2 = sha256_Sigma0(a) + majority(a, b, c);
					h = g;
					g = f;
					f = e;
					e = safe_add(d, T1);
					d = c;
					c = b;
					b = a;
					a = safe_add(T1, T2);
			        }

				/* Compute the current intermediate hash value */
				ihash[0] += a;
				ihash[1] += b;
				ihash[2] += c;
				ihash[3] += d;
				ihash[4] += e;
				ihash[5] += f;
				ihash[6] += g;
				ihash[7] += h;
			}

			/* Read the next chunk of data and update the SHA256 computation */
			function sha256_update(data, inputLen) {
				var i, index, curpos = 0;
				/* Compute number of bytes mod 64 */
				index = ((count[0] >> 3) & 0x3f);
			        var remainder = (inputLen & 0x3f);

				/* Update number of bits */
				if ((count[0] += (inputLen << 3)) < (inputLen << 3)) count[1]++;
				count[1] += (inputLen >> 29);

				/* Transform as many times as possible */
				for(i=0; i+63<inputLen; i+=64) {
			                for(var j=index; j<64; j++)
						buffer[j] = data.charCodeAt(curpos++);
					sha256_transform();
					index = 0;
				}

				/* Buffer remaining input */
				for(var j=0; j<remainder; j++)
					buffer[j] = data.charCodeAt(curpos++);
			}

			/* Finish the computation by operations such as padding */
			function sha256_final() {
				var index = ((count[0] >> 3) & 0x3f);
			        buffer[index++] = 0x80;
			        if(index <= 56) {
					for(var i=index; i<56; i++)
						buffer[i] = 0;
			        } else {
					for(var i=index; i<64; i++)
						buffer[i] = 0;
			                sha256_transform();
			                for(var i=0; i<56; i++)
						buffer[i] = 0;
				}
			        buffer[56] = (count[1] >>> 24) & 0xff;
			        buffer[57] = (count[1] >>> 16) & 0xff;
			        buffer[58] = (count[1] >>> 8) & 0xff;
			        buffer[59] = count[1] & 0xff;
			        buffer[60] = (count[0] >>> 24) & 0xff;
			        buffer[61] = (count[0] >>> 16) & 0xff;
			        buffer[62] = (count[0] >>> 8) & 0xff;
			        buffer[63] = count[0] & 0xff;
			        sha256_transform();
			}

			/* Split the internal hash values into an array of bytes */
			function sha256_encode_bytes() {
			        var j=0;
			        var output = new Array(32);
				for(var i=0; i<8; i++) {
					output[j++] = ((ihash[i] >>> 24) & 0xff);
					output[j++] = ((ihash[i] >>> 16) & 0xff);
					output[j++] = ((ihash[i] >>> 8) & 0xff);
					output[j++] = (ihash[i] & 0xff);
				}
				return output;
			}

			/* Get the internal hash as a hex string */
			function sha256_encode_hex() {
				var output = new String();
				for(var i=0; i<8; i++) {
					for(var j=28; j>=0; j-=4)
						output += sha256_hex_digits.charAt((ihash[i] >>> j) & 0x0f);
				}
				return output;
			}

		

			sha256_init();
			sha256_update(data, data.length);
			sha256_final();
			return sha256_encode_hex();

		}

	}

});
fp_app.config(['ngDialogProvider', function (ngDialogProvider) {
    ngDialogProvider.setDefaults({
        className: 'ngdialog-theme-default',
        plain: false,
        showClose: true,
        closeByDocument: true,
        closeByEscape: true,
        appendTo: false//,
        //preCloseCallback: function () {
        //  console.log('default pre-close callback');
        //}
    })
}]);
fp_app.directive('bootstrapTooltip', function() {
	  return function(scope, element, attrs) {
		    attrs.$observe('title',function(title){
		      // Destroy any existing tooltips (otherwise new ones won't get initialized)
		    	$( document ).tooltip();
		      // Only initialize the tooltip if there's text (prevents empty tooltips)
		      if (jQuery.trim(title)) element.tooltip();
		    })
		  }
});
fp_app.directive('fileModel', ['$parse', function ($parse) {
	return {
	    restrict: 'A',
	    link: function(scope, element, attrs) {
	        var model = $parse(attrs.fileModel);
	        var modelSetter = model.assign;

	        element.bind('change', function(){
	            scope.$apply(function(){
	                modelSetter(scope, element[0].files[0]);
	            });
	        });
	    }
	};}]);


//Prototipos
HTMLElement.prototype.click = function() {
	   var evt = this.ownerDocument.createEvent('MouseEvents');
	   evt.initMouseEvent('click', true, true, this.ownerDocument.defaultView, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
	   this.dispatchEvent(evt);
	} 
String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};


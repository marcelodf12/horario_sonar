fp_app.factory("RequestHeadersInterceptor", ["$localStorage",function($localStorage)
{
      var request = function request(config)
      {
          config.headers["authorization"] = $localStorage.token;
          return config;
      };

      return {
          request: request
      };
}]);
fp_app.factory("ResponseInterceptor", ["$location","toaster","$localStorage","mensajes",function($location,toaster,$localStorage,mensajes)
	{
	      var response = function (r)
	      {
	          try{
	        	  if(r.status==403 || !r.data.success){
	        		  if(r.data.reason=="Forbidden"){
        		    	$localStorage.token = "";
        		    	$localStorage.horarioToken = "";
        		    	$localStorage.isLogin = false;
        				$location.path("/home/"); 
	        		  }
	        	  }
	        	  else if(
	        			  typeof(r.data.type)!="undefined" &&
	        			  r.data.type !=null &&
	        			  r.data.reason !=null &&
	        			  r.data.reason!="" &&
	        			  r.data.message !=null &&
	        			  r.data.message!="" &&
	        			  r.data.message!="Consulta Exitosa"
	        		){
					  if(isNaN(r.data.message) && r.data.message!="ultima")
						toaster.pop({	type : r.data.type,
							title : r.data.reason,
							body : r.data.message
						});
				  }
				  else if(r.status==500){
					toaster.pop({	type : "error",
									title : mensajes.errorInternoTitle,
									body : mensajes.errorInternoMsg
					});
				  }
				  else if(r.status!=200){
					toaster.pop({	type : "error",
									title : mensajes.errorConexionTitle,
									body : mensajes.errorConexionMsg
					});
				  }
	          }catch (e) {
				// TODO: handle exception
	          }
	          return r;
	      };

	      return {
	          response: response
	      };
	}]);
fp_app.directive('slotTime', function() {
	  link = function(scope, element, attrs) {
		    scope.$watch("data", function(value) {
		    	toTime = function(x){
		    		y = x%288;
		    		minTotal = y * 5;
		    		hora = Math.floor(minTotal/60);
		    		hora = hora > 9 ? "" + hora: "0" + hora;
		    		min = minTotal - hora*60;
		    		min = min > 9 ? "" + min: "0" + min;
		    		return hora + ":" + min;
		    	}
		    	
		    	
		    	if(typeof(value)!="undefined"){
		    		if(typeof(value.slot)=="undefined" || value.slot==null || value.slot=='')
		    			value.slot = "85-267;373-555;661-843;949-1131;1237-1419;1525-1656";
		    		slots = value.slot.split(";");
		    		binSlot = [];
		    		for (x=0;x<2016;x++){
		    			binSlot.push(false);
		    		}
		    		for (x=0;x<slots.length;x++){
		    			inicio = slots[x].split("-")[0];
		    			fin = slots[x].split("-")[1];
		    			for(j=inicio-1; j<fin; j++){
		    				binSlot[j]=true;
		    			}
		    		}
		    		dataShedule = [];
		    		for(day=0;day<=6;day++){
		    			ant = false;
		    			max = (day+1)*288;
		    			periodosInicio = [];
		    			periodosFin = [];
		    			for(x=day*288;x<=max;x++){
		    				act = binSlot[x];
		    				if(!ant && act){
		    					periodosInicio.push(x);
		    				}
		    				if((!act && ant)||(x==max && act)){
		    					periodosFin.push(x);
		    				}
		    				ant = act;
		    			}
		    			d = {};
		    			d.day = day;
		    			d.periods = [];
		    			for(x=0;x<periodosInicio.length;x++){
		    				d.periods.push([toTime(periodosInicio[x]), toTime(periodosFin[x])]);
		    			}
		    			if(d.periods.length>0){
		    				dataShedule.push(d);
		    			}
		    		}
		    		
		    		$("#schedule").jqs('reset')
		    		$("#schedule").jqs('import', dataShedule);
		    	}else{
		    		$("#schedule").jqs({
		    		    mode: "edit",
		    		    days:["LUN","MAR","MIE","JUE","VIE","SAB", "DOM"]
		    		});
		    	}
		      });
		    
		  };
	  return {
		restrict: 'E',
		link	: link,
		scope: {
		      data: '=data',
		      slot: '=slot'
		    },
		template: '<div id="schedule"></div>'
	  };
});
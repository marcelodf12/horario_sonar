fp_app.factory('ConfiguracionService', ['$http','config'
                       , function ($http , config) {
    var servicesConfiguracion = {};
    
    servicesConfiguracion.listAll = function (callbackFunction) {
        $http({
            url: config.hostApiRest + 'configuracion' ,
            method: 'GET'
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    };    
    
    servicesConfiguracion.guardar = function(clave, valor, callbackFunction){
    	var obj = {};
    	obj.valor = valor;
    	$http({
            url: config.hostApiRest + 'configuracion/'+clave,
            method: 'PUT',
            data: obj
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });	
    };

    return servicesConfiguracion;
}]);
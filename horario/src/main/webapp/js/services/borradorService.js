var fp_app = angular.module("app");

fp_app.factory('BorradorService', ['$http','config'
                        , function ($http , config) {
    var servicesBorrador = {};

    servicesBorrador.list = function (id, carrera, dpto,  asignatura, profesor, order, column, offset, callbackFunction) {
    	var offset = (typeof(offset)=="undefined")?"?offset=0":"?offset="+offset;
    	var asignatura = (typeof(asignatura)=="undefined" || asignatura.trim() == "")?"":"&asignatura="+asignatura;
    	var carrera = (typeof(carrera)=="undefined" || carrera.trim() == "")?"":"&carrera="+carrera;
    	var profesor = (typeof(profesor)=="undefined" || profesor.trim() == "")?"":"&profesor="+profesor;
    	var dpto = (typeof(dpto)=="undefined" || dpto.trim() == "")?"":"&dpto="+dpto;
    	var order = (typeof(order)=="undefined" || order == true)?"&order=true":"&order=false";
    	var column = (typeof(column)=="undefined" || column.trim() == "")?"":"&column="+column;
    	var id = (typeof(id)=="undefined" || id.trim() == "")?"":"&id="+id;
    	var parameter = offset + asignatura + dpto + id + carrera + profesor + order + column;
        $http({
            url: config.hostApiRest + 'consultas/borrador' + parameter ,
            method: 'GET'
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    }
    
    servicesBorrador.guardar = function (b, callbackFunction) {
        $http({
            url: config.hostApiRest + 'consultas/borrador/' + b.id ,
            method: 'PUT',
            data: b
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    }
    
    servicesBorrador.nuevo = function (b, callbackFunction) {
        $http({
            url: config.hostApiRest + 'consultas/horario/' ,
            method: 'POST',
            data: b
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    }

    
    return servicesBorrador;
}]);
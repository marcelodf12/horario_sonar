fp_app.factory('LoginService', ['$http','$localStorage','config','sha256'
                     , function ($http , $localStorage , config , sha256) {
    var servicesLogin = {};

    servicesLogin.login = function (user, password, callbackFunction) {
    	usuario = {};
    	usuario.nombre = user;
    	usuario.password = sha256.convertToSHA256(password);	
    	$http({
            url: config.hostApiRest + "autenticacion/" ,
            method: 'POST',
            data: usuario
        }).success(function (response, status, headers, config) {
        	if(response.success && response.data!="ERROR"){
        		$localStorage.token = response.data.split(";")[0];
        		$localStorage.idProfesor = response.data.split(";")[1];
        		$localStorage.isLogin = true;
        		$localStorage.login = user;
        		$localStorage.idUsuario = response.data.split(";")[2];
        	}else{
        		$localStorage.isLogin = false;
        	};
        	callbackFunction();
        }).error(function (response, status, headers, config) {
        	$localStorage.isLogin = false;
        	callbackFunction();
        });
    };
    
    servicesLogin.queryPerfil = function (callbackFunction) {
    	token = {};
    	token.message = $localStorage.token;
    	$localStorage.perfil = [];
    	$http({
            url: config.hostApiRest + "autenticacion/perfil/" ,
            method: 'POST',
            data: token
        }).success(function (response, status, headers, config) {
        	if(response.success){
        		$localStorage.perfil = response.data
        	}else{
        		console.log(response);
        	};
        	callbackFunction();
        }).error(function (response, status, headers, config) {
        	console.log(response);
        	callbackFunction();
        });
    };
    
    servicesLogin.getPerfil = function(){
    	return $localStorage.perfil;
    };
    
    servicesLogin.isLogin = function () {
    	return $localStorage.isLogin;
    };
    
    servicesLogin.getIdProfesor = function () {
    	return $localStorage.idProfesor=="null"?null:$localStorage.idProfesor;
    };
    
    servicesLogin.logout = function () {
    	$localStorage.token = "";
    	$localStorage.horarioToken = "";
    	$localStorage.isLogin = false;
    };

    return servicesLogin;
}]);
var fp_app = angular.module("app");

fp_app.factory('UsuarioService', ['$http','config'
                        , function ($http , config) {
    var servicesUsuario = {};

    servicesUsuario.list = function (offset, nombre, column, order, callbackFunction) {    	
    	var offset = (typeof(offset)=="undefined" || offset.trim() == "")?"?offset=0":"?offset="+offset;
    	var nombre = (typeof(nombre)=="undefined" || nombre.trim() == "")?"":"&nombre="+nombre;
    	var column = (typeof(column)=="undefined" || column.trim() == "")?"":"&column="+column;
    	var order = (typeof(order)=="undefined")?"":"&order="+order;
    	var parameter = offset + nombre + column + order;
        $http({
            url: config.hostApiRest + 'usuario' + parameter ,
            method: 'GET'
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    }
    
    servicesUsuario.get = function (id, callbackFunction) {
 
    }
    
    servicesUsuario.guardar = function(usuario, callbackFunction){
    	if(typeof(usuario.idUsuario)==undefined || usuario.IdUsuario == null){
    		metodo = 'POST'
    	}else{
    		metodo = 'PUT'
    	}
    	$http({
            url: config.hostApiRest + 'usuario',
            method: metodo,
            data: usuario
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    	
    };
    
    servicesUsuario.put = function (usuario, callbackFunction) {

    }

    servicesUsuario.nuevo = function (usuario, callbackFunction) {

    }
    
    servicesUsuario.eliminar = function(id, callbackFunction){
 
    };
    
    servicesUsuario.changePassword = function (idSlot, idProfesor, slot, callbackFunction) {
 
    }
    
    return servicesUsuario;
}]);
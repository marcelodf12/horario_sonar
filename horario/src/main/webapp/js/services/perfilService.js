var fp_app = angular.module("app");

fp_app.factory('PerfilService', ['$http','config'
                        , function ($http , config) {
    var servicesPerfil = {};

    
    servicesPerfil.getPerfiles = function (idProfesor, callbackFunction) {
        $http({
            url: config.hostApiRest + 'usuario/perfiles/' + idProfesor ,
            method: 'GET'
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    }
    
    servicesPerfil.put = function (profesor, callbackFunction) {
        $http({
            url: config.hostApiRest + 'profesor/' + profesor.idProfesor ,
            method: 'PUT',
            data: profesor
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    }
    
    servicesPerfil.eliminarPerfil = function(perfil, callbackFunction){
    	$http({
            url: config.hostApiRest + 'usuario/perfil/'+perfil.idUsuario+'/'+perfil.codigo,
            method: 'DELETE'
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });	
    };
    
    servicesPerfil.asignarPermiso = function(usuarioPerfil, callbackFunction){
    	$http({
    		 url: config.hostApiRest + 'usuario/usuarioPerfil',
             method: 'POST',
             data: usuarioPerfil
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });	
    };
    
    servicesPerfil.getPermisos = function (callbackFunction) {
        $http({
            url: config.hostApiRest + 'usuario/permisos/',
            method: 'GET'
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    }
    
    return servicesPerfil;
}]);
fp_app.factory('UtilService', ['$http','config'
                          , function ($http , config) {
	
	 var utilService = {};

	 utilService.minutosCombo = function (){
		 return ["00","05","10","15","20","25","30","35","40","45","50","55"];
	 };
	 
	 utilService.horasCombo = function (){
		 var horasCombo = [];
			for(n = 0; n<=23 ; n++){
				horasCombo.push(n+"")
			}
		return horasCombo;
	 };
	 
	 utilService.diasCombo = function (){
		 return ["LUNES","MARTES","MIERCOLES","JUEVES","VIERNES","SABADO","DOMINGO"];
	 };
	 
	 utilService.diasComboAbr = function (){
		 return ["Lun","Mar","Mié","Jue","Vie","Sáb","Dom"];
	 };
	 
	 utilService.slotToDate = function(slot, abr=false){
		 var r={};
		 if(abr)
			 r.dia = utilService.diasComboAbr()[Math.floor(slot/288)];
		 else
			 r.dia = utilService.diasCombo()[Math.floor(slot/288)];
		 slot = (slot-1)%288;
		 minTotal = slot*5;
		 hora = Math.floor(minTotal/60);
 		 r.hora = hora > 9 ? "" + hora: "0" + hora;
 		 min = minTotal - hora*60;
 		 r.minuto = min > 9 ? "" + min: "0" + min;
		 return r;
	 };
	 
	 utilService.dateToSlot = function(objeto){
		 dia = +objeto.dia;
		 hora = +objeto.hora;
		 minuto = +objeto.minuto;
		 r = 1;
		 if(dia == "MARTES")
			 r+=288;
		 if(dia == "MIERCOLES")
			 r+=576;
		 if(dia == "JUEVES")
			 r+=864;
		 if(dia == "VIERNES")
			 r+=1152;
		 if(dia == "SABADO")
			 r+=1440;
		 if(dia == "DOMINGO")
			 r+=1728;
		 minTotal = hora*60 + minuto;
		 slot = Math.floor(minTotal/5);
		 r+=slot;
		 return r;
	 };
	 
	 utilService.modos = ['Fijo','Variable'];
	 
	 utilService.stringToModo = function(string){
		 
		if(string == "Fijo")
			return 'F';
		if(string == "Variable")
			return 'V';
		return '';
	 };
	 
	 utilService.modoToString = function(string){
		if(string == "F")
			return 'Fijo';
		 if(string == "V")
			return 'Variable';;
		 return '';
	 };
	 
	 utilService.tableToExcel = function(){
		 var uri = 'data:application/vnd.ms-excel;base64,'
			    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><meta charset="UTF-8"><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
			    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s)).split('__').join('<br style="mso-data-placement:same-cell;" />')) }
			    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }); }
			  return function(table, name, filename) {
			    if (!table.nodeType) table = document.getElementById(table)
			    var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML
			    		.replaceAll(" class=\"ng-binding\"","")
			    		.replaceAll("S/F ","")
			    		.replaceAll(" ng-repeat=\"h in horario | orderBy:'a'\" class=\"ng-scope\"","")
			    		.replaceAll("<!-- end ngRepeat: h in horario | orderBy:'a' -->","")
			    		.replaceAll("<!-- ngRepeat: h in horario | orderBy:'a' -->","")
			    }
			    down = document.createElement('a');
			    down.href = uri + base64(format(template, ctx));
			    down.download = filename;
			    down.id = "link-horario";
			    down.click();
			  }
	 };
	 
	 utilService.fecha = function timeConverter(UNIX_timestamp){
		 if(UNIX_timestamp!=null){
		  var a = new Date(UNIX_timestamp);
		  var months = ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
		  var year = a.getFullYear();
		  var month = months[a.getMonth()];
		  var date = a.getDate();
		  var hour = a.getHours();
		  var min = a.getMinutes();
		  var sec = a.getSeconds();
		  var time = date + '/' + month + '/' + year;
		  return time;
		 }
		 return "S/F #";
		}
	 
		utilService.fechaHora = function timeConverter(UNIX_timestamp){
			if(UNIX_timestamp!=null){
			  var a = new Date(UNIX_timestamp);
			  var months = ['01','02','03','04','05','06','07','08','09','10','11','12'];
			  var year = a.getFullYear();
			  var month = months[a.getMonth()];
			  var date = a.getDate().toString().length==1?'0'+a.getDate():a.getDate();
			  var hour = a.getHours().toString().length==1?'0'+a.getHours():a.getHours();
			  var min = a.getMinutes().toString().length==1?'0'+a.getMinutes():a.getMinutes();
			  var sec = a.getSeconds().toString().length==1?'0'+a.getSeconds():a.getSeconds();
			  var time = year + '-' + month + '-' + date + 'T' + hour + ':' + min;
			  return time;
			}
			return "S/F #";
		}
		
		
		utilService.schedulerToSlot = function(data){
			data = angular.fromJson(data);
			horaToSlot = function(time){
				h = parseInt(time.split(":")[0]);
				m = parseInt(time.split(":")[1]);
				h = h*12;
				m = m/5;
				return h+m;
			}
			r = "";
			for(x=0;x<data.length;x++){
				s = x*288;
				for(y=0;y<data[x].periods.length;y++){
					inicio = horaToSlot(data[x].periods[y][0]);
					fin = horaToSlot(data[x].periods[y][1]);
					fin = fin==0?287:fin-1;
					r+=""+(inicio+s+1)+"-"+(fin+s+1)+";";
				}
			}
			r = r==""?";":r;
			return r.substring(0, r.length - 1);
		}
    
    return utilService;
}]);
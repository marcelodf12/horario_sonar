fp_app.factory('AsignaturaService', ['$http','config','UtilService'
                          , function ($http , config , UtilService) {
    var servicesAsignaturas = {};

    servicesAsignaturas.list = function (offset, asignatura, codAsig, turno, dpto, seccion, periodo, column, order, callbackFunction) {
    	var offset = (typeof(offset)=="undefined" || offset.trim() == "")?"?offset=0":"?offset="+offset;
    	var asignatura = (typeof(asignatura)=="undefined" || asignatura.trim() == "")?"":"&asignatura="+asignatura;
    	var codAsig = (typeof(codAsig)=="undefined" || codAsig.trim() == "")?"":"&codAsig="+codAsig;
    	var turno = (typeof(turno)=="undefined" || turno.trim() == "")?"":"&turno="+turno;	
    	var dpto = (typeof(dpto)=="undefined" || dpto.trim() == "")?"":"&dpto="+dpto;
    	var seccion = (typeof(seccion)=="undefined" || seccion.trim() == "")?"":"&seccion="+seccion;
    	var periodo = (typeof(periodo)=="undefined" || periodo.trim() == "")?"":"&periodo="+periodo;
    	var column = (typeof(column)=="undefined" || column.trim() == "")?"":"&column="+column;
    	var order = (typeof(order)=="undefined")?"":"&order="+order;
    	var parameter = offset + asignatura + codAsig + turno + dpto + seccion + periodo + column + order;
        $http({
            url: config.hostApiRest + 'asignatura' + parameter ,
            method: 'GET'
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    }
    
    servicesAsignaturas.listRestricciones = function (id,coiciden, callbackFunction) {
    	params = "?coiciden=false";
    	if(coiciden) params = "?coiciden=true";
        $http({
            url: config.hostApiRest + 'asignatura/relaciones/' + id + params ,
            method: 'GET'
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    }
    
    servicesAsignaturas.guardar = function(asignatura, callbackFunction){
    	a = angular.fromJson(angular.toJson(asignatura));
    	delete(a.slot);
    	if(typeof(a.idAsignatura)==undefined || a.idAsignatura == null){
    		metodo = 'POST'
    	}else{
    		metodo = 'PUT'
    	}
    	for(i=0; i<a.divisiones.length ; i++){
    		d = {};
    		d.hora = UtilService.dateToSlot(a.divisiones[i].hora);
    		d.modo = UtilService.stringToModo(a.divisiones[i].modo);
    		d.tipoAula = a.divisiones[i].tipoAula.idTipoAula;
    		d.duracion = a.divisiones[i].duracion * 3;
    		a.divisiones[i] = d;
    	}
    	$http({
            url: config.hostApiRest + 'asignatura',
            method: metodo,
            data: a
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    	
    };
    servicesAsignaturas.get = function (id, callbackFunction) {
    	var id = (typeof(id)=="undefined")?"0":id;
        $http({
            url: config.hostApiRest + 'asignatura/' + id ,
            method: 'GET'
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    }
    
    servicesAsignaturas.eliminar = function(id, callbackFunction){
    	$http({
            url: config.hostApiRest + 'asignatura/'+id,
            method: 'DELETE'
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });	
    };
    
    servicesAsignaturas.addRelacion = function (id1, id2, coicide, callbackFunction) {
    	if(coicide){
    		u = config.hostApiRest + 'asignatura/grupo/' + id1 + '/' + id2;
    	}else{
    		u = config.hostApiRest + 'asignatura/conflicto/' + id1 + '/' + id2;
    	}
    	
        $http({
            url: u ,
            method: 'POST'
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    }
    
    servicesAsignaturas.listAsignaturasCarrera = function (idCarrera, callbackFunction) {
    	
    	//console.log("ESTAMOS EN EL SERVICIO.." + idCarrera);
        $http({
            url: config.hostApiRest + 'asignatura/carrera/' + idCarrera,
            method: 'GET'
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    }
    
    
    
    servicesAsignaturas.listAsignaturasParaRecalcular = function (offset, nombrePrincipal, nombre, carrera, profesor, column, order, callbackFunction) {

    	
    	var offset = (typeof(offset)=="undefined" || offset.trim() == "")?"?offset=0":"?offset="+offset;
    	var nombrePrincipal = (typeof(nombrePrincipal)=="undefined" || nombrePrincipal.trim() == "")?"":"&nombrePrincipal="+nombrePrincipal;
    	var nombre = (typeof(nombre)=="undefined" || nombre.trim() == "")?"":"&nombre="+nombre;
    	var carrera = (typeof(carrera)=="undefined" || carrera.trim() == "")?"":"&carrera="+carrera;
    	var profesor = (typeof(profesor)=="undefined" || profesor.trim() == "")?"":"&profesor="+profesor;
    	var column = (typeof(column)=="undefined" || column.trim() == "")?"":"&column="+column;
    	var order = (typeof(order)=="undefined")?"":"&order="+order;
    	
    	var parameter = offset + nombrePrincipal + nombre + carrera + profesor + column + order;

        $http({
            url: config.hostApiRest + 'asignatura/asignaturasRecalculo/' + parameter ,
            method: 'GET'
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    }
    
    

    servicesAsignaturas.quitar = function (id1, id2, coiciden, callbackFunction) {
    	u = config.hostApiRest + 'asignatura/coiciden/' + id1 + '/' + id2 + '/' + coiciden;
        $http({
            url: u ,
            method: 'DELETE'
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    }
    
    return servicesAsignaturas;
}]);
fp_app.factory('HorarioService', ['$http','config','$localStorage','mensajes','UtilService',
                        function  ($http , config , $localStorage , mensajes , UtilService) {
    var servicesHorario = {};
    
    var version_aux;
    
    servicesHorario.cleanCache = function(){
    	$localStorage.cacheHorarioVersion = -1;
    	$localStorage.cacheHorarioCompleto = [];
    };
    
    servicesHorario.setVersion = function(v, response){
		for(a=0; a<response.data.length; a++){
			response.data[a].slots = jsonToTable(response.data[a].st);
			response.data[a].excel = jsonToExcel(response.data[a].st);
			response.data[a].pExcel = response.data[a].p.split('\n').join('__');
		}
		$localStorage.horarioVersion = v
    	$localStorage.horarioCompleto = response.data
    };
    
    servicesHorario.setIdCache = function(id, response){
    	try{
    		for(a=0; a<response.data.length; a++){
    			response.data[a].slots = jsonToTable(response.data[a].st);
    			response.data[a].excel = jsonToExcel(response.data[a].st);
    			response.data[a].pExcel = response.data[a].p.split('\n').join('__');
    		}
    	}catch(ex){
    		console.log(ex);
    		response.data = [];
    	}
		$localStorage.cacheHorarioVersion = id
    	$localStorage.cacheHorarioCompleto = response.data
    };
    
       
    servicesHorario.horario = function(){
    	return {data:$localStorage.horarioCompleto, success:true};
    };
    
    servicesHorario.cacheHorario = function(){
    	return {data:$localStorage.cacheHorarioCompleto, success:true};
    };
    
    servicesHorario.callServer= function(response, callbackFunction){
		$http({
            url: config.hostApiRest + "consultas/horario/00" ,
            method: 'GET'
        }).success(function (data, status, headers, config) {
        	if(data.success){
        		for (var k = 0; k < data.data.length; k++) {
					var h = data.data[k];
					h.p1 = UtilService.fechaHora(h.p1);
					h.f1 = UtilService.fechaHora(h.f1);
					h.p2 = UtilService.fechaHora(h.p2);
					h.f2 = UtilService.fechaHora(h.f2);
				}
        		servicesHorario.setVersion(version_aux, data) ;
                callbackFunction(data,status);
        	};
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
		version_aux = response.data;
    };
    
    var id_aux;
    
    servicesHorario.getHorarioEspecifico= function(id, callbackFunction){
    	id_aux = id;
    	if(id>0){
    		if($localStorage.cacheHorarioVersion!=id){
    			$http({
    	            url: config.hostApiRest + "consultas/horario/" + id_aux ,
    	            method: 'GET'
    	        }).success(function (data, status, headers, config) {
	            		for (var k = 0; k < data.data.length; k++) {
	    					var h = data.data[k];
	    					h.p1 = UtilService.fechaHora(h.p1);
	    					h.f1 = UtilService.fechaHora(h.f1);
	    					h.p2 = UtilService.fechaHora(h.p2);
	    					h.f2 = UtilService.fechaHora(h.f2);
	    				}
    	        		servicesHorario.setIdCache(id_aux, data) ;
    	                callbackFunction(servicesHorario.cacheHorario(),status);
    	        }).error(function (data, status, headers, config) {
    	            callbackFunction(data,status);
    	        });
    		}else{
    			callbackFunction(servicesHorario.cacheHorario(),200);
    		};
    	}else{
    		callbackFunction({success:false, message:mensajes.notFound },404);
    	}
    };
    
    
    servicesHorario.getHorario = function(callbackFunction){
    	$http({
            url: config.hostApiRest + 'consultas/version',
            method: 'GET'
        }).success(function (data, status, headers, config) {
        	if(data.success){
        		if(data.data > $localStorage.horarioVersion || typeof($localStorage.horarioVersion)=="undefined"){
        			 servicesHorario.callServer(data, callbackFunction)
        		}else{
        			callbackFunction(servicesHorario.horario(),200);
        		};
        	}
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
  	
    	
    };
    
    servicesHorario.getHorarios = function(anho, callbackFunction){
    	$http({
            url: config.hostApiRest + 'consultas/horarios?anho='+anho,
            method: 'GET'
        }).success(function (data, status, headers, config) {
        	callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });  		
    };
    
    servicesHorario.getPapelera = function(callbackFunction){
    	$http({
            url: config.hostApiRest + 'consultas/papelera',
            method: 'GET'
        }).success(function (data, status, headers, config) {
        	callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });  		
    };
    
    servicesHorario.eliminar = function(id, callbackFunction){
    	$http({
            url: config.hostApiRest + 'consultas/horario/'+id,
            method: 'DELETE'
        }).success(function (data, status, headers, config) {
        	callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
  	
    	
    };
    
    servicesHorario.getConflictos = function(id, callbackFunction){
    	idH = +id
    	$http({
            url: config.hostApiRest + 'consultas/horario/conflictos/'+ idH,
            method: 'GET'
        }).success(function (data, status, headers, config) {
        	callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    };
    
    servicesHorario.calcular = function(id, algoritmo ,callbackFunction){
    	idH = +id
    	$http({
            url: config.hostApiRest + 'consultas/calcular/'+ idH + "/" + algoritmo,
            method: 'POST',
            data:[]
        }).success(function (data, status, headers, config) {
        	callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    };
    
    servicesHorario.reCalcular = function(id, algoritmo , aRecalcular, callbackFunction){
    	idH = +id
    	$http({
            url: config.hostApiRest + 'consultas/calcular/'+ idH + "/" + algoritmo,
            method: 'POST',
            data:aRecalcular
        }).success(function (data, status, headers, config) {
        	callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    };
    
    servicesHorario.publicar = function(id ,callbackFunction){
    	idH = +id
    	$http({
            url: config.hostApiRest + 'consultas/publicar/'+ idH,
            method: 'POST'
        }).success(function (data, status, headers, config) {
        	callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    };
    
    servicesHorario.restaurar = function(id ,callbackFunction){
    	idH = +id
    	$http({
            url: config.hostApiRest + 'consultas/restaurar/'+ idH,
            method: 'POST'
        }).success(function (data, status, headers, config) {
        	callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    };
    
    servicesHorario.descartar = function(id ,callbackFunction){
    	idH = +id
    	$http({
            url: config.hostApiRest + 'consultas/descartar/'+ idH,
            method: 'POST'
        }).success(function (data, status, headers, config) {
        	callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    };
    
    servicesHorario.cargarAulas = function(file, idHorario, callbackFunction){
    	uploadUrl = config.hostApiRest + 'file/upload/alumnos/horario/' + idHorario;
        var fd = new FormData();
        fd.append('uploadedFile', file);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined }
        })
        .success(function(data, status, headers, config){
        	callbackFunction(data,status);
        })
        .error(function(data, status, headers, config){
        	callbackFunction(data,status);
        });
    };
    
    
    
	jsonToTable = function(j){
		var rSlot = [];
		slots = angular.fromJson(j);
		for(i=0; i<slots.length; ++i){
			real_start = +slots[i].s.split("-")[0];
			real_end = +slots[i].s.split("-")[1]+1;
			start = UtilService.slotToDate(real_start, true);
			end = UtilService.slotToDate(real_end, true);
			if(typeof(slots[i].a)=="undefined") slots[i].a="-";
			rSlot.push({aula:slots[i].a, hora:start.hora + ":" + start.minuto + " " + end.hora + ":" + end.minuto, dia: start.dia});
		}
		return rSlot;
	};
	
	jsonToExcel = function(j){
		var rSlot = [{aula:" ",hora:" "},
		     {aula:" ",hora:" "},
		     {aula:" ",hora:" "},
		     {aula:" ",hora:" "},
		     {aula:" ",hora:" "},
		     {aula:" ",hora:" "},
		     {aula:" ",hora:" "}
		     ];
		slots = angular.fromJson(j);
		for(k=0; k<6; k++){
			for(i=0; i<slots.length; ++i){
				real_start = +slots[i].s.split("-")[0];
				real_end = +slots[i].s.split("-")[1]+1;
				if(real_start<(k+1)*288 && real_start>k*288){
					start = UtilService.slotToDate(real_start, true);
					end = UtilService.slotToDate(real_end, true);
					
					if(typeof(slots[i].a)=="undefined") slots[i].a="__";
					
					rSlot[k].aula += slots[i].a + "__";
					rSlot[k].hora += start.hora + ":" + start.minuto + " " + end.hora + ":" + end.minuto + "__";
				}
			}
			if(rSlot[k].aula!=" ")
				rSlot[k].aula = rSlot[k].aula.substring(0, rSlot[k].aula.length-2);
			if(rSlot[k].hora!=" ")
				rSlot[k].hora = rSlot[k].hora.substring(0, rSlot[k].hora.length-2);
		} 
		
		return rSlot;
	};
	
    servicesHorario.getExamenes= function(id, callbackFunction){
		$http({
            url: config.hostApiRest + "examenes/horario/" + id ,
            method: 'GET'
        }).success(function (data, status, headers, config) {
        		if(data.success)
        		for (var k = 0; k < data.data.length; k++) {
					var h = data.data[k];
					data.data[k].c = data.data[k].c.split('__').join(' ');
					h.p1 = h.p1!=null?UtilService.fechaHora(h.p1):null;
					h.f1 = h.f1!=null?UtilService.fechaHora(h.f1):null;
					h.p2 = h.p2!=null?UtilService.fechaHora(h.p2):null;
					h.f2 = h.f2!=null?UtilService.fechaHora(h.f2):null;
					h.edit = false;
				};
                callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
        	data = {};
			data.success = false;
			data.message = 'En este momento no puede conectarse con el servidor, favor intente mas tarde';
			data.reason = 'Error al conectarse al servidor.';
			data.type = 'error';
            callbackFunction(data,status);
        });
    };//updateCantidadAlumnos
    
    servicesHorario.updateCantidadAlumnos= function(list, callbackFunction){
		$http({
            url: config.hostApiRest + "consultas/horario/cantidadAlumnos" ,
            method: 'PUT',
            data: list
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
        	data = {};
			data.success = false;
			data.message = 'En este momento no puede conectarse con el servidor, favor intente mas tarde';
			data.reason = 'Error al conectarse al servidor.';
			data.type = 'error';
            callbackFunction(data,status);
        });
    };
    
    servicesHorario.updateExamenes= function(f, id, callbackFunction){
    	data = {};
    	try {
    		data.p1 = f.p1.toISOString();
		} catch (e) {
			data.p1 = f.p1!=null?f.p1+":00.0Z":null;
		}
		try {
			data.p2 = f.p2.toISOString();
		} catch (e) {
			data.p2 = f.p2!=null?f.p2+":00.0Z":null;
		}
		try {
			data.f1 = f.f1.toISOString();
		} catch (e) {
			data.f1 = f.f1!=null?f.f1+":00.0Z":null;
		}
		try {
			data.f2 = f.f2.toISOString();
		} catch (e) {
			data.f2 = f.f2!=null?f.f2+":00.0Z":null;
		}
    	
    	
    	
    	
		$http({
            url: config.hostApiRest + "examenes/horario/" + id ,
            method: 'PUT',
            data: data
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
        	data = {};
			data.success = false;
			data.message = 'En este momento no puede conectarse con el servidor, favor intente mas tarde';
			data.reason = 'Error al conectarse al servidor.';
			data.type = 'error';
            callbackFunction(data,status);
        });
    };

    return servicesHorario;
    
}]);
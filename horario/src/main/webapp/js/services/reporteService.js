fp_app.factory('ReporteService', ['$http','config'
                    , function ($http , config) {
    var servicesReportes = {};
    
    servicesReportes.listQueries = function (callbackFunction) {
        $http({
            url: config.hostApiRest + 'reportes/queries',
            method: 'GET'
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    };
    
    servicesReportes.consultarProgreso = function (callbackFunction) {
        $http({
            url: config.hostApiRest + 'reportes/progreso',
            method: 'GET'
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    };
    
    servicesReportes.lanzarActualizacion = function (idHorario, callbackFunction) {
        $http({
            url: config.hostApiRest + 'reportes/generar/'+idHorario,
            method: 'POST'
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    };
    
    servicesReportes.downloadReport = function (query, idHorario, callbackFunction) {
        $http({
            url: config.hostApiRest + 'reportes/horario/'+idHorario,
            method: 'POST',
            data: query
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status,headers);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status,headers);
        });
    };

    return servicesReportes;
}]);
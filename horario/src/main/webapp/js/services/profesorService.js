var fp_app = angular.module("app");

fp_app.factory('ProfesorService', ['$http','config'
                        , function ($http , config) {
    var servicesProfesor = {};

    servicesProfesor.list = function (offset, nombre, apellido, cedula, column, order, callbackFunction) {
    	var offset = (typeof(offset)=="undefined" || offset.trim() == "")?"?offset=0":"?offset="+offset;
    	var nombre = (typeof(nombre)=="undefined" || nombre.trim() == "")?"":"&nombre="+nombre;
    	var apellido = (typeof(apellido)=="undefined" || apellido.trim() == "")?"":"&apellido="+apellido;
    	var cedula = (typeof(cedula)=="undefined" || cedula.trim() == "")?"":"&cedula="+cedula;
    	var column = (typeof(column)=="undefined" || column.trim() == "")?"":"&column="+column;
    	var order = (typeof(order)=="undefined")?"":"&order="+order;
    	var parameter = offset + nombre + apellido + cedula + column + order;
        $http({
            url: config.hostApiRest + 'profesor' + parameter ,
            method: 'GET'
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    }
    
    servicesProfesor.listSinUsuario = function (offset, nombre, apellido, cedula, column, order, callbackFunction) {
    	var offset = (typeof(offset)=="undefined" || offset.trim() == "")?"?offset=0":"?offset="+offset;
    	var nombre = (typeof(nombre)=="undefined" || nombre.trim() == "")?"":"&nombre="+nombre;
    	var apellido = (typeof(apellido)=="undefined" || apellido.trim() == "")?"":"&apellido="+apellido;
    	var cedula = (typeof(cedula)=="undefined" || cedula.trim() == "")?"":"&cedula="+cedula;
    	var column = (typeof(column)=="undefined" || column.trim() == "")?"":"&column="+column;
    	var order = (typeof(order)=="undefined")?"":"&order="+order;
    	var parameter = offset + nombre + apellido + cedula + column + order;
        $http({
            url: config.hostApiRest + 'profesor/sinUsuario' + parameter ,
            method: 'GET'
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    }
    
    servicesProfesor.get = function (id, callbackFunction) {
        $http({
            url: config.hostApiRest + 'profesor/' + id ,
            method: 'GET'
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    }
    
    servicesProfesor.put = function (profesor, callbackFunction) {
        $http({
            url: config.hostApiRest + 'profesor/' + profesor.idProfesor ,
            method: 'PUT',
            data: profesor
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    }

    servicesProfesor.nuevo = function (profesor, callbackFunction) {
        $http({
            url: config.hostApiRest + 'profesor/',
            method: 'POST',
            data: profesor
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    }
    
    servicesProfesor.eliminar = function(id, callbackFunction){
    	$http({
            url: config.hostApiRest + 'profesor/'+id,
            method: 'DELETE'
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });	
    };
    
    servicesProfesor.insertSlot = function (idSlot, idProfesor, slot, callbackFunction) {
        $http({
            url: config.hostApiRest + 'profesor/' + idProfesor.toString() + '/slot/'  + idSlot.toString(),
            method: 'POST',
            data: slot
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    }
    
    servicesProfesor.getSlots = function (idProfesor, callbackFunction) {
        $http({
            url: config.hostApiRest + 'profesor/' + idProfesor.toString() + '/slots/',
            method: 'GET'
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    }
    
    servicesProfesor.getSlot = function (idSlot, idProfesor, callbackFunction) {
        $http({
        	 url: config.hostApiRest + 'profesor/' + idProfesor.toString() + '/slot/'  + idSlot.toString(),
            method: 'GET'
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    }
    
    return servicesProfesor;
}]);
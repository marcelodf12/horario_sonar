fp_app.factory('DepartamentoService', ['$http','config'
                            , function ($http , config) {
    var servicesDepartamento = {};

    servicesDepartamento.list = function (nombre, sigla, column, order, callbackFunction) {
    	var nombre = (typeof(nombre)=="undefined" || nombre.trim() == "")?"":"&nombre="+nombre;
    	var sigla = (typeof(sigla)=="undefined" || sigla.trim() == "")?"":"&sigla="+sigla;
    	var column = (typeof(column)=="undefined" || column.trim() == "")?"":"&column="+column;
    	var order = (typeof(order)=="undefined")?"":"&order="+order;
    	var parameter = nombre + sigla + column + order;
        $http({
            url: config.hostApiRest + 'departamento?' + parameter ,
            method: 'GET'
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    };
    
    servicesDepartamento.nuevo = function(nombre,sigla, callbackFunction){
    	var departamento = {};
    	departamento.nombre = nombre;
    	departamento.sigla = sigla;
    	$http({
            url: config.hostApiRest + 'departamento',
            method: 'POST',
            data: departamento
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    	
    };
    
    servicesDepartamento.guardar = function(id,nombre,sigla, callbackFunction){
    	var departamento = {};
    	departamento.nombre = nombre;
    	departamento.sigla = sigla;
    	$http({
            url: config.hostApiRest + 'departamento/'+id,
            method: 'PUT',
            data: departamento
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });	
    };
    
    servicesDepartamento.eliminar = function(id, callbackFunction){
    	$http({
            url: config.hostApiRest + 'departamento/'+id,
            method: 'DELETE'
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });	
    };

    return servicesDepartamento;
}]);
fp_app.factory('CarreraService', ['$http','config'
                       , function ($http , config) {
    var servicesCarrera = {};
    
    servicesCarrera.listAll = function (callbackFunction) {
        $http({
            url: config.hostApiRest + 'carrera/all' ,
            method: 'GET'
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    };

    servicesCarrera.list = function (offset, nombre, sigla, column, order, callbackFunction) {
    	var offset = (typeof(offset)=="undefined" || offset.trim() == "")?"?offset=0":"?offset="+offset;
    	var nombre = (typeof(nombre)=="undefined" || nombre.trim() == "")?"":"&nombre="+nombre;
    	var sigla = (typeof(sigla)=="undefined" || sigla.trim() == "")?"":"&sigla="+sigla;
    	var column = (typeof(column)=="undefined" || column.trim() == "")?"":"&column="+column;
    	var order = (typeof(order)=="undefined")?"":"&order="+order;
    	var parameter = offset + nombre + sigla + column + order;
        $http({
            url: config.hostApiRest + 'carrera' + parameter ,
            method: 'GET'
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    };

    servicesCarrera.listCarreraConflictos = function (idCarrera, callbackFunction) {
        $http({
            url: config.hostApiRest + 'carrera/'+idCarrera+'/conflictos/' ,
            method: 'GET'
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    };


    servicesCarrera.updateCarreraConflictos = function (idCarrera, conflictos, callbackFunction) {
        $http({
            url: config.hostApiRest + 'carrera/'+idCarrera+'/conflictos/' ,
            method: 'POST',
            data: conflictos
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    };
    
    servicesCarrera.get = function(id, callbackFunction){
        $http({
            url: config.hostApiRest + 'carrera/' + id ,
            method: 'GET'
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    };
    
    servicesCarrera.nuevo = function(nombre,sigla, codCarSec, callbackFunction){
    	var carrera = {};
    	carrera.nombre = nombre;
    	carrera.sigla = sigla;
    	carrera.codCarSec = codCarSec;
    	$http({
            url: config.hostApiRest + 'carrera',
            method: 'POST',
            data: carrera
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    	
    };
    
    servicesCarrera.guardar = function(id,nombre,sigla,codCarSec, callbackFunction){
    	var carrera = {};
    	carrera.idCarrera = id;
    	carrera.nombre = nombre;
    	carrera.sigla = sigla;
    	carrera.codCarSec = codCarSec;
    	$http({
            url: config.hostApiRest + 'carrera/'+id,
            method: 'PUT',
            data: carrera
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });	
    };
    
    servicesCarrera.eliminar = function(id, callbackFunction){
    	$http({
            url: config.hostApiRest + 'carrera/'+id,
            method: 'DELETE'
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });	
    };

    return servicesCarrera;
}]);
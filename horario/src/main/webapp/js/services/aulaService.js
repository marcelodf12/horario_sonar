fp_app.factory('AulaService', ['$http','config'
                    , function ($http , config) {
    var servicesAulas = {};

    servicesAulas.list = function (offset, nombre, capacidad, tipo, column, order, callbackFunction) {
    	var offset = (typeof(offset)=="undefined" || offset.trim() == "")?"?offset=0":"?offset="+offset;
    	var nombre = (typeof(nombre)=="undefined" || nombre.trim() == "")?"":"&nombre="+nombre;
    	var capacidad = (typeof(capacidad)=="undefined" || capacidad==null || capacidad == "")?"":"&capacidad="+capacidad;
    	var tipo = (typeof(tipo)=="undefined" || tipo.toString().trim() == "")?"":"&tipo="+tipo;
    	var column = (typeof(column)=="undefined" || column.trim() == "")?"":"&column="+column;
    	var order = (typeof(order)=="undefined")?"":"&order="+order;
    	var parameter = offset + nombre + capacidad + tipo + column + order;
        $http({
            url: config.hostApiRest + 'aula' + parameter ,
            method: 'GET'
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    };
    
    servicesAulas.nuevo = function(nombre,capacidad, tipo, callbackFunction){
    	var aula = {};
    	aula.nombre = nombre;
    	aula.capacidad = capacidad;
    	aula.idTipoAula = tipo;
    	console.log(tipo);
    	$http({
            url: config.hostApiRest + 'aula',
            method: 'POST',
            data: aula
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    	
    };
    
    servicesAulas.guardar = function(id,nombre,capacidad,tipo, callbackFunction){
    	var aula = {};
    	aula.nombre = nombre;
    	aula.capacidad = capacidad;
    	aula.idTipoAula = tipo;
    	$http({
            url: config.hostApiRest + 'aula/'+id,
            method: 'PUT',
            data: aula
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });	
    };
    
    servicesAulas.eliminar = function(id, callbackFunction){
    	$http({
            url: config.hostApiRest + 'aula/'+id,
            method: 'DELETE'
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });	
    };
    
    servicesAulas.listTiposAula = function (callbackFunction) {
        $http({
            url: config.hostApiRest + 'aula/tipos',
            method: 'GET'
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    };
    
    servicesAulas.listAgrupacionAula = function (id,callbackFunction) {
        $http({
            url: config.hostApiRest + 'aula/horario/'+ id,
            method: 'GET'
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    };
    
    servicesAulas.setCantidadAlumnos = function (id, t, array, callbackFunction) {
        $http({
            url: config.hostApiRest + 'aula/horario/'+ id + '/tamanho/'+ t,
            method: 'PUT',
            data: array
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    };

    return servicesAulas;
}]);
 fp_app.factory('MiCuentaService', ['$http', '$localStorage','config', 'sha256'
                     , function ($http , $localStorage, config, sha256) {
    var servicesMiCuenta = {};

    servicesMiCuenta.cambiarPass = function (passwordActual, passwordNuevo, callbackFunction) {
    	usuario = {};
    	
    	usuario.idUsuario = $localStorage.idUsuario;
    	usuario.nombre = $localStorage.login;
    	usuario.password = sha256.convertToSHA256(passwordActual);
    	usuario.passwordNuevo = sha256.convertToSHA256(passwordNuevo);		
    
    	$http({
            url: config.hostApiRest + 'autenticacion/'+usuario.idUsuario,
            method: 'PUT',
            data: usuario
        }).success(function (data, status, headers, config) {
            callbackFunction(data,status);
        }).error(function (data, status, headers, config) {
            callbackFunction(data,status);
        });
    	
    };
   

    return servicesMiCuenta;
}]);
 
